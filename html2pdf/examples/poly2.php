<?php
$racine="/web/tests";
/* polycopiés en pdf */

//s contient une liste de ressources t231 t56 etc.

require_once('../../philosophemes/sql_config.php');
require_once('../../philosophemes/ai.php');
require_once('../../philosophemes/philo-labo.php');

$s=$_GET[s];
$s=str_replace('  ',' ',trim($s));
$liste=explode(' ',$s);
//echo "<pre>";print_r($liste);echo "</pre>";

$c='\setlength{\parindent}{1cm}';
//$content='<!DOCTYPE html><html><head><meta charset="utf-8"></head><body>';
foreach ($liste as $numero)
  {
  if (trim($numero)!='')
    {
    $nature=simple_query("select nature from ressources where id=$numero");
    switch ($nature){
      case 'sujet-texte':
      case 'texte':
        $item=select2html("select ressource,texte,id_auteur,reference,questions from ressources".jn(ressource,auteur)." where ressources.id=$numero",'h',array('\par\noindent\textbf{%s}\par\nopagebreak\rm\indent %s\par\nopagebreak\parbox{\textwidth-1cm}{\begin{flushright}\textbf{%s}\\\\%s\end{flushright}}\vspace{2ex}%s'),array('%s','%s','%s'));
        $item=str_replace("\n\n","\n",$item);
        $item=str_replace(array("\n",'«','»','’','—','°','œ','oe','Oe','Œ','...','&'),array('\par\rm\indent ','\og ','\fg{}',"'",'---','$^o$','\oe ','\oe ','\OE ','\OE ','\ldots ',"et"),$item);
        $item=str_replace('----','\rule{0.3\textwidth}{0.1mm}',$item);
        $c.=$item;
        break;
      case 'image':
	$url=simple_query("select url from ressources where id=$numero");
	echo $url;
	$ressource=simple_query("select ressource from ressources where id=$numero");
	if (right($url,4)!='.png')
	  {
	  exec("convert $racine$url $racine$url.png");
	  $url=$url.".png";
	  }
	//$item.=select2html("select url,ressource from ressources where ressources.id=$numero",'h',array('\imago{%s}{%s}'),array('%s','%s','%s'));
	$item='\imago{'.$url.'}{'.$ressource.'}';
        //$item=str_replace('_',"\\textunderscore ",$item);
        $c.=$item;
        break;
      case 'youtube':
        $url=simple_query("select url from ressources where id=$numero");
        $code=explode('/',$url);
        $code=$code[sizeof($code)-1];
        $ressource=simple_query("select ressource from ressources where id=$numero");
        exec("wget http://img.youtube.com/vi/$code/mqdefault.jpg -O $racine/fichiers/$numero.jpg;convert $racine/fichiers/$numero.jpg $racine/fichiers/$numero.png");
	$c.=str_replace('_','\_','\linkimago{/fichiers/'.$numero.'.png}{'.$ressource.'}{'.$url.'}');	
        break;
      case 'sujet-question':
        $ressource=simple_query("select ressource from ressources where id=$numero");
        $c.='\vide{'.$ressource."}";	
        break;
      default:
        $url=simple_query("select url from ressources where id=$numero");
        $nature=simple_query("select nature from ressources where id=$numero");
        $ressource=simple_query("select ressource from ressources where id=$numero");
	$c.='\vide{'."n$^o$$numero\\newline\\newline $ressource\\newline\\newline $nature  "."}";	
        break;
      }
    }
  }
  
//$content=str_replace('<p/>','<br/>',$content);

$path = "$racine/temp/1.txt";
$fp=fopen($path,'w+');
fwrite($fp, $c);
fclose($fp);
unlink("$racine/temp/1.pdf");
chdir("$racine/temp/");
exec('pandoc 1.txt -o 1.pdf --template=simple.latex --latex-engine=xelatex');
header('Content-Type: application/pdf;charset=UTF-8');
readfile("$racine/temp/1.pdf");
exit;

