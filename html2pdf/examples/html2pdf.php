<?php
$doc=$_GET['doc'];

$content="
<page backtop='15mm' backbottom='15mm' backleft='15mm' backright='15mm'>
%s
</page>";

$f=file_get_contents("/web/philo-labo/composition/$doc");

$content=sprintf($content,$f);

require_once(dirname(__FILE__).'/../html2pdf.class.php');

    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8');
        //$html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('times');
                // le chargement d'une css ne semble pas fonctionner
                //$stylesheet = file_get_contents('pub/skins/ians/textes.css'); /// here call you external css file 
                //$html2pdf->writeHTML($stylesheet,1);
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple00.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }


