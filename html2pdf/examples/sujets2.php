<?php
/* sujets de philos en pdf */

require_once('../../philosophemes/sql_config.php');
require_once('../../philosophemes/ai.php');
require_once('../../philosophemes/philo-labo.php');
require_once('../../composition/ressources.php');

$s=$_GET['s'];
$s=trim($s);
$tableau=explode(' ',$s);

$s1=getitem($_POST['s1']);
$s2=getitem($_POST['s2']);
$s3=getitem($_POST['s3']);



//echo indent("<pre>".$s3."<pre>");
$s3=preg_replace('/\*(.*)\*/U','<i>\1</i>',$s3); 

$content = "
<page backtop='15mm' backbottom='15mm' backleft='15mm' backright='15mm'> 

<style type='text/css'>
.sujets {
font-family : Arial, Helvetica, sans-serif;
font-size:12pt;
}
.jolitexte {
 padding: 5px;
}
.sujet {
font-size:12pt;
}
.jolitexte p {
 font-family : times;
 font-size:13pt;
 width:7cm;
 text-indent:10mm;
 text-align:justify;
 margin-bottom:-10px;
}

.auteur {
 font-size: 14pt;
 font-weight:bold;
 text-align:right; 
 margin-bottom: 5px;*/
}

.reference {
 font-style:italic;
 font-size: 14pt;
 font-weight:normal;
 text-align:right;
 font-family:times,serif;
 margin-top: -5px;
}
</style>
<div class='sujets'>
<p align=center><big><big><b>PHILOSOPHIE</b></big></big></p>
<p align=center>au choix</p>

<p/>
<p/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.&nbsp;<div class='sujet'>$s1</div>  
<p/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp;<div class='sujet'>$s2</div>  
<p/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.&nbsp;<i>Expliquez le texte suivant</i>:

<table border=0>
<tr>
<td width=30></td><td width=550><div class='jolitexte'>$s3</div></td>
</tr>
</table>
<p/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>La connaissance de la doctrine de l'auteur n'est pas requise. Il faut et il suffit que l'explication rende compte, par la compréhension précise du texte, du problème dont il est question.</i>
</div>
</page>";

    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8');
//      $html2pdf->setModeDebug();
        $html2pdf->setDefaultFont('times');
		// le chargement d'une css ne semble pas fonctionner
		//$stylesheet = file_get_contents('pub/skins/ians/textes.css'); /// here call you external css file 
		//$html2pdf->writeHTML($stylesheet,1);
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('exemple00.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }
