## PROJET EN COURS DE PUBLICATION

Le projet n'est pas installable en l'état. Merci d'attendre un peu. Merci

![screenshot](images/compositeur.png "Compositeur")

## ASPECTS JURIDIQUES

* philo-labo intègre un PmWiki, le merveilleux wiki de Patrick Michaud http://pmwiki.org
* philo-labo utilise le signe share de la fonte entypo de Daniel Bruce &#59196;
* le compositeur intègre ztree, une bibliothèque magnifique pour gérer des arbres, de Hunter.z. http://www.treejs.cn/v3/main.php#_zTreeInfo
* la mécanique des transformations repose en partie sur pandoc, le couteau suisse des formats de textes écrit par John McFarlane, professeur de philosophie à Berkeley. https://pandoc.org
* Les diaporamas du compositeurs sont produits en Beamer, une classe de LaTeX.
* Les diaporamas en html utilisent un version modifiée de S5 d'Éric Meyer.
* Les graphes sont produit en graphviz
* philo-labo utilise le système de sélection multiple tree-multiselect.js de Patrick Tsai et al. https://github.com/patosai/tree-multiselect.js/

## INSTALLATION

Voir le fichier install/README.md
