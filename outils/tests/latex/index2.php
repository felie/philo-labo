<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<?php
require_once('phplatex.php');
$texte=file_get_contents($_GET[file]);
$texte=str_replace("\n",'²²²',$texte);
$texte=preg_replace_callback(
                '/\$\$(.*?)\$\$/',// le ? pour non-greedy
                function($matches)
                    {
                    $n=$matches[1];
                    return texify("$$ $n $$");
                    }
                ,$texte);
$texte=preg_replace_callback(
                '|<latex>(.*?)</latex>|',// le ? pour non-greedy
                function($matches)
                    {
                    $n=$matches[1];
                    return texify($n);
                    }
                ,$texte);
$texte=str_replace('²²²',"\n",$texte);
echo $texte;
?>
</body>
</html>
