String.prototype.reverse = function () {
    return this.split('').reverse().join('');
};

String.prototype.replaceLast = function (what, replacement) {
    return this.reverse().replace(new RegExp(what.reverse()), replacement.reverse()).reverse();
};

Element.prototype.appendBefore = function (element) {
  element.parentNode.insertBefore(this, element);
},false;

Element.prototype.appendAfter = function (element) {
  element.parentNode.insertBefore(this, element.nextSibling);
},false;

function paginateTexts() {
  var texts = document.getElementsByClassName('texte');
  for (var i in texts)
      {
      var slide=texts[i].parentNode;
      var cible=document.createElement('div');
      cible.appendAfter(slide);
      cible.className="slideSPLIT";
      paginateText(texts[i],cible); // n'est pas dans l'arbre
      slide.className="oldslide";
      while (cible.hasChildNodes()){
        cible.lastChild.appendAfter(slide);
      cible.remove();
      var pages = document.getElementsByClassName('page');
        for (var i in pages){
          pages[i].className="slide";
        }
      }
    };
//     var texts = document.getElementsByClassName('oldslide');
//     while(texts[0]) {
//       var p=texts[0].parentNode;
//       p.removeChild(texts[0]);
//     }​
  }
function paginateText(texte,cible) {
    //var text = document.getElementById("originalText").innerHTML; // gets the text, which should be displayed later on
    var text = texte.innerHTML;
    text=text.replace(' :','&nbsp;:');
    text=text.replace(' ;','&nbsp;;');
    text=text.replace(' ?','&nbsp;?');
    text=text.replace(' !','&nbsp;!');
    text=text.replace('" ','«&nbsp;');
//     text=text.replace(' "','&nbsp;»');
    text=text.replace(/\n/g,' COUCOU'); // for the other paragraph
    text=text+"</div>";
    var textArray = text.split(" "); // makes the text to an array of words
    createPage(cible); // creates the first page
    var npage = 1;
    var page = document.getElementsByClassName("page")[document.getElementsByClassName("page").length-1];
    page.innerHTML = "<p class='pfirst'></p>";
    for (var i = 0; i < textArray.length; i++) { // loops through all the words
        var success = appendToLastPage(textArray[i]); // tries to fill the word in the last page
        if (i==(textArray.length-1)) // last word
          {
          page = document.getElementsByClassName("page")[document.getElementsByClassName("page").length-1];
          var pageText = page.innerHTML;
          pageText=pageText.replaceLast('ptrue','preference');
          pageText=pageText.replaceLast('ptrue','pauthor');
          page.innerHTML=pageText.replaceLast('pfalse','plast');
          
          }
        if (!success) { // checks if word could not be filled in last page
            createPage(cible); // create new empty page
            npage += 1;// une page de plus
            page = document.getElementsByClassName("page")[document.getElementsByClassName("page").length - 1];
            page.innerHTML="<p class='pfalse'></p>";
//             fragment=page.innerHTML;
//             fragment=fragment.replace("<p class='pfalse'><p class='pftrue'>","<p class='ptrue'>");
//             page.innerHTML=fragment.replace("</p></p>");
            appendToLastPage(textArray[i]); // fill the word in the new last element
        }
    }
};

function createPage(cible) {
    var page = document.createElement("div"); // creates new html element
    page.setAttribute("class", "page"); // appends the class "page" to the element
    //document.getElementById("paginatedText").appendChild(page); // appends the element to the container for all the pages
    cible.appendChild(page);
};

function appendToLastPage(word) {
    var page = document.getElementsByClassName("page")[document.getElementsByClassName("page").length - 1]; // gets the last page
    var pageText = page.innerHTML; // gets the text from the last page
    pageText=pageText.replace('COUCOU',"</p><p class='ptrue'>");
    //pageText="<p>"+pageText; //***
    page.innerHTML = pageText.substring(0,pageText.length-4)+word+" </p>"; // insert word at the end
    //page.innerHTML += word + " </p>"; // saves the text of the last page
    if (page.offsetHeight < page.scrollHeight) { // checks if the page overflows (more words than space)
        if (pageText.indexOf('ptrue')!=-1) // if thereis a paragraphe in it
          pageText=pageText.replace('pfalse','plast'); //replace the first occurrence
        pageText=pageText.replaceLast('ptrue','pfirst'); // for the last paragraph
        
        page.innerHTML = pageText; //resets the page-text ***
        return false; // returns false because page is full
    } else {
        //page.innerHTML=pageText;//restore the page
        return true; // returns true because word was successfully filled in the page
    }
};