<?php


$text="L'enseignement doit être résolument retardataire. Non pas rétrograde, tout au contraire. C'est pour marcher dans le sens direct qu'il prend du recul  car, si l'on ne se place point dans le moment dépassé, comment le dépasser ? Ce serait une folle entreprise, même pour un homme dans toute la force, de prendre les connaissances en leur état dernier  il n'aurait point d'élan, ni aucune espérance raisonnable. Ne voyant que l'insuffisance partout, il se trouverait, je le parie, dans l'immobilité pyrrhonienne, c'est-à-dire que, comprenant tout, il n'affirmerait rien. Au contraire celui qui accourt des anciens âges est comme lancé selon le mouvement juste  il sait vaincre  cette expérience fait les esprits vigoureux.
La Bible annonce beaucoup, et encore plus selon l'esprit que selon la lettre  car on n'y peut rester  mais aussi on sait bien qu'on n'y va pas rester. Cette sauvage et abstraite pensée, rocheuse, abrupte, a de l'avenir. Et puisque tant d'hommes ont surmonté l'ancienne loi, chacun peut se donner permission d'y croire  et c'est ainsi qu'on portera à la maturité cette promesse d'un ordre meilleur. 
Il nous manque, pour être chrétiens sérieusement, d'avoir été païens ou juifs. Qui n'est pas pharisien d'abord, comment se guérirait-il de l'être ? Aussi combien d'hommes seront pharisiens étant vieux ? Telle est la marche rétrograde. C'est ce que le droit nous fait sentir  car le droit n'est jamais suffisant, et cela est bien aisé à comprendre  mais aussi cette amère pensée ne mène à rien  c'est le juriste qui change le droit en mieux, justement parce qu'il le sait et parce qu'il y croit et parce qu'il s'y tient. C'est par la suffisance, et non par l'insuffisance, qu'une idée en promet une autre.
Devant l'espèce, le juge de paix pense quelque chose de neuf, par la force doctrinale elle-même  ainsi se fait la jurisprudence, bien plus puissante et de bien plus grande portée que l'ironie du plaideur. L'enfant a besoin d'avenir  ce n'est pas le dernier mot de l'homme qu'il faut lui donner, mais plutôt le premier. C'est ce que font merveilleusement les anciens auteurs, que l'on devrait appeler les Prophètes. 
Ils vous donnent l'amande à casser. La vertu des Belles-Lettres est en ceci qu'il faut entendre l'oracle  et il n'y a point de meilleure manière de s'interroger soi-même, comme le fronton de Delphes l'annonçait. Dans les sciences, au contraire, il arrive souvent que, par la perfection de l'abrégé, on ne voie plus même l'obstacle. En un élégant cours de mécanique, rien n'arrête  et l'on demande : « À quoi cela sert-il ? » 
Au lieu de se demander : « De quoi cela peut-il me délivrer ? » 
Au contraire, dans Descartes, on le voit bien, parce qu'il se trompe et se détrompe  bien plus près de nous  mais Thalès vaut mieux. Socrate avait cet art de ramener toute idée à la première enfance. Et il est bon de raisonner sur les liquides avec Archimède, et sur le baromètre avec Pascal  et même cette confusion qui reste en leurs raisonnements, elle n'est pas encore assez nôtre  toutefois elle approche de nous. Les anciens ont du neuf  c'est ce que les modernes souvent n'ont point, car leur vérité n'est point au niveau de nos erreurs. La terre tourne, cela est vieux et usé  le fanatique n'y voit plus de difficulté. Mais est-il moins fanatique en cela ou plus ? C'est ce que je ne saurais pas dire.
Alain
Propos sur l'Éducation (1932). XVII";

function pretraitement($text)
{
 text=str_replace(array(' :',' ;'' ?',' !','" ',' "'),array('&nbsp;:''&nbsp;;','&nbsp;?','&nbsp;!','«&nbsp;','&nbsp;»',$text);
 return $text;
}
$l = 275;
$t = explode(" ",$text); // on explose en mots
$max = count($text_array);
processText($text);
for ($i=1;$i<$max;$i++)
  {
  }


?>