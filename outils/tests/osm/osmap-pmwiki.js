/**
 Unobtrusive embedded OpenStreetMap for PmWiki
 Written by Petko Yotov www.pmwiki.org/petko
 Version: 20131130
 License: http://creativecommons.org/licenses/by-sa/4.0/
 */
var PmOSMap = {
  OSMheight: '350',
  OSMwidth: "100%",
  sinh:    function(n) { return (Math.exp(n)-Math.exp(-n))/2; },
  sec:     function(a) { return 1/Math.cos(a); },
  rad2deg: function(r) { var d = r*180/Math.PI; return d; },
  deg2rad: function(d) { var r = d/180*Math.PI; return r; },
  tilePos: function(lat,lon,zoom) {
    return {
      x: ( (lon+180)/360 * Math.pow(2,zoom) ),
      y: ( (1 - Math.log(Math.tan(PmOSMap.deg2rad(lat)) + PmOSMap.sec(PmOSMap.deg2rad(lat)))/Math.PI)/2 * Math.pow(2,zoom) )
    };
  },
  coords:  function(xt, yt, z) {
    var n = Math.pow(2,z);
    var lon = xt / n * 360.0;
    var lat = PmOSMap.rad2deg(Math.atan(PmOSMap.sinh(Math.PI * (1 - 2 * yt / n))));
    return lon+','+lat;
  },
  bbox:    function(lat, lon, zoom, width, height) {
    var ts2 = 256*2; //tile size pixels *2
    var tile = PmOSMap.tilePos(lat, lon, zoom);
    var xtile_s = tile.x - width/ts2;
    var xtile_e = tile.x + width/ts2;

    var ytile_s = tile.y - height/ts2;
    var ytile_e = tile.y + height/ts2;

    var coord_s = PmOSMap.coords(xtile_s, ytile_s, zoom);
    var coord_e = PmOSMap.coords(xtile_e, ytile_e, zoom);

    return "bbox="+coord_s+','+coord_e;
  },
  init:    function() {
    var layers = {
      C: 'cyclemap',
      T: 'transportmap',
      H: 'hot',
      Q: 'mapquest'
    };
    var lx = document.links;
    for(var i=0; i<lx.length; i++) {
      var href = lx[i].href;
      if(href.indexOf('http://www.openstreetmap.org')==0) {
        var lnkdata = href.split(/[?#&]/);
        if(! lnkdata || lnkdata == href) continue;
        var lnk = { w:PmOSMap.OSMwidth, h:PmOSMap.OSMheight, z:3 };
        for(var j=0; j<lnkdata.length; j++) {
          a = lnkdata[j].match(/^(mlat|mlon|map|w|h|layers)=(.*)$/);
          if(a) lnk[a[1]] = a[2];
        }
        if(lnk.map) {
          var qq = lnk.map.split("/");
          lnk.z = qq[0]
          lnk.lat = qq[1]
          lnk.lon = qq[2];
        }
        else {
          if(lnk.mlat) lnk.lat = lnk.mlat;
          if(lnk.mlon) lnk.lon = lnk.mlon;
        }
        if(!(lnk.lat && lnk.lat)) continue;

        var d = document.createElement("iframe");
      d.id =  "osm_"+i;
        d.width=lnk.w;
        d.height=lnk.h;
        d.frameborder=0;
        d.scrolling="no";
        d.marginheight=0;
        d.marginwidth=0;
        d.src='about:blank';
        d.style="border: 1px solid black; display: block;"

        var pN = lx[i].parentNode;
        pN.insertBefore(d, lx[i]);

        var w = d.clientWidth;
        var h = d.clientHeight;

        var u = "http://www.openstreetmap.org/export/embed.html?";
        if(lnk.mlat && lnk.mlon) u+= "marker="+lnk.mlat+','+lnk.mlon+"&";
        if(lnk.layers&&layers[lnk.layers]) u+= "layer="+layers[lnk.layers]+"&";
        if(lnk.map) u += PmOSMap.bbox(lnk.lat, lnk.lon, lnk.z, w, h);
        d.src = u;
      }
    }
  }
};

setTimeout("PmOSMap.init()", 50);

