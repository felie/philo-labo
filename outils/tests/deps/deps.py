#!/usr/bin/python
# -*- coding: utf8 -*-
#importing modules
import glob, re, sys, os, fnmatch
br = "\n"
tab = "\t"
 
 
#####
#exiting if all 3 arguments are not passed via command line
def fail():
    print ("ERROR: " + str(len(sys.argv)-1) + " of 3 required arguments provided.")
    sys.exit()
 
 
#####
#getting arguments passed via command line
    
#testing for root DIRECTORY string
try: myDir = sys.argv[1]
except: fail()
 
#testing for RECURSION boolean
try: myRec = sys.argv[2]
except: fail()
 
#testing for OUTPUT filename string
try: myFile = sys.argv[3]
except: fail()
 
#####
#making list of PHP files within DIRECTORY
if myRec == "0": #without recursion
    myDir2 = myDir + "/*.php"
    PHP_list = glob.glob(myDir2)
elif myRec == "1": #with recursion
    PHP_list = []
    for dirname, dirnames, filenames in os.walk(myDir):
        for filename in filenames:
            if fnmatch.fnmatch (filename,("*.php")):
                match = os.path.join(dirname,filename)
                PHP_list.append(match)
                 
#make an empty list;
#tuples will go in the list;
#each tuple will contain a PHP filename and a PHP filename it includes
includeList = []
 
#iterate through each PHP file and place tuples in the list
for a in PHP_list:
    fileOpen = open(a, "r")
    #for each line in a PHP file
    a = a[len(myDir):]
    for line in fileOpen:
            m = re.match(r"(.*)include(.*\()(.*)\)", line) #for include(),include_once()
            if m:
                b = m.group(3)[1:-1]
                if b[-4::] == ".php": #only PHP files
                    b = b.replace("\\","/")
                    b = b.replace("\"","")
                    b = b.replace('\'',"")
                    b = b.replace('$FarmD/','')
                    b = b.replace('myDir/','')
                    b = b.replace('../','')
                    pos=b.find('/')
                    if b==-1:
                      pos=a.rfind('/')
                    includeList.append([a, b])
            else: pass
            m = re.match(r"(.*)require(.*\()(.*)\)", line) #for require(), require_once()
            if m:
                b = m.group(3)[1:-1]
                if b[-4::] == ".php": #only PHP files
                    b = b.replace("\\","/")
                    b = b.replace("\"","")
                    b = b.replace('\'',"")
                    b = b.replace('$FarmD/','')
                    b = b.replace('myDir/','')
                    b = b.replace('../','')
                    pos=b.find('/')
                    if b==-1:
                      pos=a.rfind('/')
                    includeList.append([a, b])
            else: pass
 
 
####
#creating DOT file
dot = open(myFile, "w")
 
#writing to DOT file
dot.write("digraph {\nrankdir=LR;\nnode [shape=box style=filled fillcolor=pink];")
for a,b in includeList:
    dot.write('\t"'+a+'" -> "'+b+'";\n');
dot.write("}")
dot.close()

print myDir+' traité'
  
#####
#exiting
sys.exit()
