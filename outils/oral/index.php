<?php  // système d'oral pour le bac de philo

$default_action='Bienvenue';
//$default_action='Hit parade';

define('ai_standalone',TRUE);
require_once("$_SERVER[DOCUMENT_ROOT]/config.php"); 
$ai_DBName="oral";
require ('ai.php');

$_GET[admin]=false;

$actions=array();

if ($_GET[admin])
$actions=array(
'get csv',
);

$actions=array_merge($actions,array(
'Bienvenue',
'',
'ajouter un auteur',
'ajouter un titre',
'ajouter une ville',
'définir une oeuvre',
'',
'mon oral',
'tri par académie',
'tri global',
'hit auteurs',
'hit titres',
));

function add_consult($objet,$a,$b)
  {
  return "<h1>Ajouter '$objet'</h1>".insert2html("select $a from $objet"."s limit 1")."<h1>Liste des '$objet". "s'</h1>".select2html("select $b from $objet"."s order by $objet ASC",'h','=');  
  }
  
switch ($action) {
  case 'Bienvenue':
    $body.=file_get_contents('info.html');
    break;
  case 'get csv':
    $body.='<a href="getcsv.php">get csv</a>';
    break;
  case 'ajouter une académie':
    $body.=add_consult('academie','*','academie');
    break;
  case 'séries':
    $body.=add_consult('serie','*','serie');
    break;
  case 'ajouter un titre': 
    $body.=add_consult('titre','*','titre,id_auteur');
    break;
  case 'ajouter une ville':
    $body.=add_consult('ville','*','ville,id_academie');
    break;
  case 'ajouter un auteur':
    $body.=add_consult('auteur','*','auteur');
    break;
  case 'définir une oeuvre':
    $body.="<h1>Saisie d'une oeuvre étudiée</h1>".insert2html("select id_titre,id_ville,id_serie from oeuvres limit 1");
    break;
  case 'mon oral':
    $body.='<form method="post" action="?action=oral">'.
    listbox(sql2list("select id,ville from villes order by ville"),"ville").'<br/>'.
    listbox(sql2list("select id,serie from series order by serie"),'serie','',1).' sélection multiple possible<br/>'.
    '<input type="submit" name="ai_form_submit" value="OK"/></form>';
    break;
  case 'oral':
    $ville=$_POST['ville'][0];
    $series=($_POST['serie']);
    $series=join($series,',');
    $body.=select2html("Select villes.ville,series.serie,auteurs.auteur,titres.titre from oeuvres".jn('oeuvre','serie').jn('oeuvre','titre').jn('oeuvre','ville').jn('titre','auteur')." where id_ville=$ville and id_serie in (".$series.") order by ville,serie",'h');
    break;
  case 'tri par académie':
    $body.='<form method="post" action="?action=tri_par_academie">'.
    listbox(sql2list("select id,academie from academies order by academie"),"académie").'<br/>'.
    '<input type="submit" name="ai_form_submit" value="OK"/></form>';
    break;
  case 'tri_par_academie':
    $academie=intval($_POST['académie'][0]);
    $body.='<h1>Tri par ville, puis par série</h1>'.select2html("Select villes.ville,series.serie,auteurs.auteur,titres.titre from oeuvres".jn('oeuvre','serie').jn('oeuvre','titre').jn('oeuvre','ville').jn('titre','auteur').jn('ville','academie').' where villes.id_academie = '.$academie.' order by ville,serie','h');
    break;
  case 'tri global':
    $body.='<h1>Tri par académie, puis par ville, puis par série</h1>'.select2html("Select academie,villes.ville,series.serie,auteurs.auteur,titres.titre from oeuvres".jn('oeuvre','serie').jn('oeuvre','titre').jn('oeuvre','ville').jn('titre','auteur').jn('ville','academie').' order by academie,ville,serie','h');
    break;
  case 'hit auteurs':
    $body.='<h1>Auteurs par fréquence</h1>'.select2html("Select auteurs.auteur,count(*) as nbre from oeuvres".jn('oeuvre','titre').jn('titre','auteur')." group by auteur order by nbre desc",'h');
    break;
  case 'hit titres':
    $query="Select auteurs.auteur, titres.titre, count( * ) AS nbre FROM oeuvres".jn('oeuvre','titre').jn('titre','auteur')." group by titre order by nbre desc";
    $body.='<h1>Titres par fréquence</h1>'.select2html($query,'h');
    break;
  
	case 'update':
	echo $_GET[table].$_GET[id];
	$body.=update2html("select * from ".$_GET[table]." where ". $_GET[table].".id=".$_GET[id],'v');
	break;

	break;
	}

$title=str_replace('_',' ',$action);
$nb_films=simple_query('select count(*) from oeuvres');
$logo="<img src='logo.png' alt='logo' /><br/>[".$nb_films.']';
$header="Oral";
$content.="<div id='centr'><p>$body</p></div>";
$ai_indent=0;

require('ai_avsts2.php');
echo $content;
?>
