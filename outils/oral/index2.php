<?php  // syst�me d'oral pour le bac de philo

$default_action='Liste par ville';
//$default_action='Hit parade';

define('ai_standalone',TRUE);
require('sql_config.php'); // plusieurs bases peuvent utiliser ai sur la m�me machine
require ('ai2.php');

$_GET[admin]=false;

$actions=array();

if ($_GET[admin])
$actions=array(
'Nouveau texte',
'Nouveau titre',
'Nouvel auteur',
'Nouvelle s�rie',
'Nouvelle ville'
);

$actions=array_merge($actions,array(
/*'Nouveau texte',
'Nouveau titre',
'Nouvel auteur',
'Nouvelle s?rie',
'Nouvelle ville',
'Liste brute',*/
'Liste par ville',
'Liste par auteur',
'Liste par serie',
'Hit parade',
'Top auteurs'
/*'Table en csv'*/
));

function nouveau($table)
	{
	return insert2html("select * from $table limit 1").select2html("select * from $table order by $table.id DESC",'h',"z");
	}

function alerte($table,$id,$boolean,$string)
	{
	//echo "test: select ($boolean) as v from $table where id=$id";
	$r=(simple_query("select ($boolean) as v from $table where id=$id"));
	if ($r)
	return "<div style='color:red'>Attention: ".htmlentities($string)."</div>";
	}

function button($s,$action)
	{
	return "<* vers('$s','%s','$action') *>";
	}
	
function vers($s,$id,$action) //  __("edit")
	{
	return "<a href=\"".$_SERVER['PHP_SELF']."?action=$action&id=$id\">$s</a>";
	}

switch ($action) {
	case 'Nouveau texte':
	$body.=nouveau('textes');
	break;
	case 'Nouveau titre':
	$body.=nouveau('titres');
	break;
	case 'Nouvel auteur':
	$body.=nouveau('auteurs');
	break;
	case 'Nouvelle s�rie':
	$body.=nouveau('series');
	break;
	case 'Nouvelle ville':
	$body.=nouveau('villes');
	break;
	case 'Liste brute':
	$body.=select2html("select * from textes order by id","h","z");
	break;
	case 'update':
	echo $_GET[table].$_GET[id];
	$body.=update2html("select * from ".$_GET[table]." where ". $_GET[table].".id=".$_GET[id],'v');
	break;
	case 'Liste par ville':
	$body.=array2html(sql2array("select villes.ville,series.serie,auteurs.auteur,titres.titre from villes,series,titres,textes,auteurs where textes.id_ville=villes.id and textes.id_serie=series.id and textes.id_titre=titres.id and titres.id_auteur=auteurs.id order by villes.ville,series.serie"),$Z);
	break;
	case 'Liste par auteur':
	$body.=select2html("select id_auteur,titre from titres,auteurs order by auteur","h");
	break;
	case 'Hit parade':
	$body.=array2html(sql2list("select auteurs.auteur,titres.titre,count(*) as a from textes,titres,auteurs where (auteurs.id=titres.id_auteur) and (textes.id_titre=titres.id) group by id_titre order by a desc"),$Z);
	break;
	case 'Top auteurs':
	$body.=array2html(sql2list("select auteurs.auteur,count(*) as a from textes,titres,auteurs where (auteurs.id=titres.id_auteur) and (textes.id_titre=titres.id) group by auteur order by a desc"),$Z);
	break;
	case 'Liste par serie':
	$body.=select2html("select id_serie,id_ville,id_titre from textes,series order by serie","h");
	break;
	case 'Table en csv':
	break;
	case 'auteurs':
	$body.=select2html("select id,auteur,id_ville from auteurs");
	break;
	default:
	$body.=$action(); //ex�cution de la commande
	break;
	}

$title=str_replace('_',' ',$action);
$nb_films=simple_query('select count(*) from textes');
$logo="<img src='logo.png' alt='logo' /><br/>[".$nb_films.']';
$header="Oral";
$content.="<div id='centr'><p>$body</p></div>";
$ai_indent=0;

require('ai_avsts2.php');
echo $content;
?>
