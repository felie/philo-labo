<?php

/*
gestion des plugins, recensement et activation/d�sactivation
*/

//------------------------plugins

// d�tection automatique des nouveaux plugins
$rep="$ai_base/plugins/";
$dir=opendir($rep);
$plugin=array();
while ($f = readdir($dir)) // scan and add if not exist
   if (is_dir($rep.$f) and ($f!='.') and ($f!='..'))
		{
		$plugin[].=$f;
		if (simple_query("select count(id) from plugins where plugin='$f'")=='0')
			connect_and_query("insert into plugins (plugin,active,installed) values('$f','no','no')");
		} 

$require_to_build=false;
foreach (sql2seq("select plugin from plugins where active='yes' and installed='no'") as $plugin)
	{
	$require_to_build=true;
	install_plugin($plugin);
	}

//if ($require_to_build)
install_require(sql2seq("select plugin from plugins where active='yes' and installed='yes'"));

// destruction des r�f�rences dans la table des plugins qui ne sont plus l�
foreach (sql2seq("select plugin from plugins") as $plugin)
	if (!is_dir("$ai_base/plugins/$plugin"))
		connect_and_query("delete from plugins where plugin='$plugin'");

function install_plugin($p) // install plugin
	{
	install_base($p);
	//*** installer_documentation($p);
	connect_and_query("update plugins set installed='yes' where plugin='$p'");
	}

function install_base($p) // import tables for plugin $p
	{
	global $ai_Username,$ai_DBName,$ai_Password,$ai_Hostname,$ai_base;
	if (file_exists("$ai_base/plugins/$p/$p.sql"))
		{
		exec("mysql -u $ai_Username -p$ai_Password -h $ai_Hostname -D $ai_DBName < $ai_base/plugins/$p/$p.sql");
		//echo "sql base of plugin $p installed</br>";
		}
	}

function install_require($liste)
	{
	global $ai_base;
	$f = fopen("$ai_base/plugins/ai_plugins.php","w");
	fputs($f,'<?php'."\n");
	foreach ($liste as $plugin)
		fputs($f,'require("plugins/'.$plugin."/".$plugin.'.php"'.");\n");
	//fputs($f,'echo "fin du chargement des plugins";'."\n");
	fputs($f,'?>'."\n");
	fclose($f);
	}

function gestion_plugins()
	{
	return update2html("select * from plugins",'h','==*=');
	}

//echo 'fin lecture plugin management';

?>