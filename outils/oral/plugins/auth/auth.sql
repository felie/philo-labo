-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 08, 2010 at 01:12 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6-1+lenny4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `ai2`
--

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE IF NOT EXISTS `logins` (
  `id` int(20) NOT NULL auto_increment,
  `login` tinytext NOT NULL,
  `truename` tinytext NOT NULL,
  `pwd` tinytext NOT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `login`, `truename`, `pwd`) VALUES
(3, 'toto', 'TOTO', '01b6e20344b68835c5ed1ddedf20d531'),
(4, 'riri', 'RIRI', '08c7b0daa33b1e5e86a230c1801254c9'),
(5, 'fifi', 'FIFI', '75778bf8fde7266d416b0089e7b8b793'),
(6, 'loulou', 'LOULOU', '298de5aba134234d98b8e17f2d16b2df');

