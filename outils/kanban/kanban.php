<?php

function decryptCookie($value){
   if(!$value){return false;}
   $key = 'The Line Secret Key42';
   $crypttext = base64_decode($value); //decode cookie
   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
   $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv);
   return trim($decrypttext);
}

$Author=decryptCookie($_COOKIE[AbsoluteSecureCheckIn421]);
$doc=$_GET[doc];
$work_dir="$_SERVER[DOCUMENT_ROOT]/users/$Author/kanban";
if (!file_exists($work_dir))
    mkdir($work_dir);

// traitement du formulaire d'entrée des tâches
if (isset($_POST['tasks']))
  $todo=format_tasks($_POST['tasks']);
for ($i=1;$i<4;$i++)
	$col[$i]=format_tasks(file_get_contents("$work_dir/kanban$doc-col$i"));
$n=0;
function format_tasks($s)
  {
  global $n;
  $s=explode("\n",$s); // transforme en un tableau
  foreach ($s as $t)
    if ($t!='')
      {
      $t=rawurldecode($t);
      $n++;
      $result.="<span class='arrondi event' id='id$n' draggable='true'><input type='hidden' value=\"$t\"><div contenteditable='true'>$t</div></span>\n";
      }
  return $result;
  }
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dragdroptouch.js"></script>
  <style type="text/css">
table {
margin:auto;
border-collapse:collapse;
}
table th,table td{
        border-left:0px solid black;
        border-collapse:collapse;
//	margin:auto;
//	padding:0;
	text-align:center;
}
table td {
	width:3O0px;
	vertical-align:top;
	padding-bottom:350px;
}
table td:first-child::after {
   content: "";
   display: inline-block;
   vertical-align: top;
   min-height: 60px;
}
.arrondi {
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
}
table span{
	//height:30px;
	width:93%;
	display:block;
	background-color: lightblue;
	margin:2px auto 2px auto;	
	padding:3px;
	border:0px solid black;
	line-height:25px
}
  </style>
    <script type="text/javascript">
    //<![CDATA[
        $(function(){
          $(document).ready(function(){
		$('.event').on("dragstart", function (event) {
			  var dt = event.originalEvent.dataTransfer;
			  dt.setData('Text', $(this).attr('id'));
			});
                $('.arrondi').on("dragenter dragover drop", function (event) {	
		   event.preventDefault();
		   if (event.type === 'drop') {
                        var data = event.originalEvent.dataTransfer.getData('Text',$(this).attr('id'));
			if (data!=$(this).attr('id'))  
			    {
                            de=$('#'+data).detach();
                            de.insertBefore($(this));	
                            $('#kanban').serializeAny();
                            }
			 event.stopPropagation();
		   };
              });
              $('table td').on("dragenter dragover drop", function (event) {	
		   event.preventDefault();
		   if (event.type === 'drop') {
			  var data = event.originalEvent.dataTransfer.getData('Text',$(this).attr('id'));
			  de=$('#'+data).detach();
			  de.appendTo($(this));	
			  $('#kanban').serializeAny();
		   };
	   });
	});
        });
function send(a) {
    $.ajax({
        url: "store.php<?php echo "?doc=$doc"; ?>",
        type: "POST",
        data: {
            ssd: "yes",
            data: a,
        },
        dataType: "text",
        success: function (jsonStr) {
            $("#result").html(jsonStr['back_message']);
        }
    });
}
(function($){
    $.fn.serializeAny = function() {
    var ret = [];
    $.each( $(this).find(':input'), function() {
        ret.push(encodeURIComponent(this.name) + "=" + encodeURIComponent($(this).val()));
    });
   send(ret);
}
})(jQuery);
    	//]]>
</script>
</head>
<body>
<center>Insérez des tâches dans la zone de saisie, et déplacez-les à la souris. <?php echo "<b>$Author/$doc</b>";?></center>
<div id="result"></div>
<div class="wrapper">
<form method="POST" target="">
<textarea style="width:100%;resize:none" name="tasks" rows=5></textarea>
<div id='log'></div>
<button style="width:100%" type="submit">Action (ajoute les tâches et efface ce qu'il faut effacer)</button>
</form>
<form id="myForm">
<table id="kanban">
<th>A faire</th>
<th>En cours</th>
<th>Fait</th>
<th>Effacer</th>
<tr>
	<td><input type='hidden' name='col' value='1'><?php echo $todo; echo $col[1]; ?></td>
	<td><input type='hidden' name='col' value='2'><?php echo $col[2]; ?></div></td>
	<td><input type='hidden' name='col' value='3'><?php echo $col[3]; ?></td>
	<td><input type='hidden' name='col' value='4'><?php echo $col[4]; ?></td>
<tr>
</table>
</form>
</div>

<style>
html body { 
margin:0;
padding:0;
}
table{
border: 1px solid black;
margin:0;
width:100%;
}
 
td{
border: 1px solid black;
text-align: center;
width: 25%;
}
</style>
<script>
$('#kanban').serializeAny();
</script>
</body>
</html>
