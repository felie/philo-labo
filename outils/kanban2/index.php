<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Kanban</title>
</head>
  <body>
<style>
    body {background-color:lightblue;}
    .bl {clear:left;height:80vh;width:25%;background-color:#ffe6cc}
    .br {clear:right;height:80vh;width:25%;background-color:#ffe6cc}
    .z {display: inline-block;border: 0px solid black;margin:0px}
    .l {float:left;}
    .r {float:right;}
    .bt {float:bottom;}
    .t {float:top;}
    .pb {width:100%;height:33%}
    .sc {height:100%;overflow:scroll;overflow-x:hidden;}
    .liste {width:25%;height:80vh;background-color:ivory;}
    .p {width:50%;height:20vh;background-color:lightblue;}ck
    .color0 {background-color:lightblue;}
    .color1 {background-color:yellow;}
    .color2 {background-color:orange;}
    .color3 {background-color:red;}
</style>
  
<?php

// return the variable $Author or exit if not loggedin - for SSO
include_once("../philosophemes/secure.php");
if ($Author=='')
    $userdir="/web/philo-labo/kanban2/guests/$_GET[Author]"; // invités
else
    $userdir="/web/philo-labo/users/$Author/kanban"; // philo-labo
if (!file_exists($userdir))
    mkdir($userdir);
$doc=$_GET[doc];
if ($doc=='')
    $doc='default';
    
 $contexts=glob("$userdir/context*");
 foreach ($contexts as &$context)
    {
    $context=str_replace("$userdir/context-",'',$context);
    }
if (!in_array($doc,$contexts))
    $contexts[].=$doc;
asort($contexts);

// traitement du formulaire d'entrée des tâches
if (isset($_POST['tasks']))
  $todo=format_tasks($_POST['tasks']);
for ($i=1;$i<7;$i++) // on ne charge pas la poubelle
	$col[$i]=format_tasks(file_get_contents("$userdir/$doc-col$i"));
$col[7]=format_tasks(file_get_contents("$userdir/tampon")); // le tampon d'échange
$n=0;
function format_tasks($s)
  {
  global $n;
  $s=explode("\n",$s); // transforme en un tableau
  foreach ($s as $t)
    {
    $t=trim(rawurldecode($t));
    if ($t!='')
      {
      $n++;
      $priority=preg_match_all("/!/",$t);
      $result.="<span class='arrondi color$priority event' id='id$n' draggable='true'><input type='hidden' value=\"$t\"><div class='item' contenteditable='true'>$t</div></span>\n";
      }
    }
  return $result;
  }
function zone($name,$n)
    {
    global $col,$todo;
    if ($n==1)
        $todolist=$todo;
    return "<div class='arrondi' style='background-color:gray;color:white;'><center><b>$name</b></center></div><div class='sc'><table width=100%><tr><td><input type='hidden' name='col' value='$n'>$todolist $col[$n]</td></tr></table></div>";
    }
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dragdroptouch.js"></script>
  <style type="text/css">
table {
margin:auto;
border-collapse:collapse;
}
table th,table td{
        border-left:0px solid black;
        border-collapse:collapse;
	text-align:center;
}
table td {
	width:3O0px;
	vertical-align:top;
	padding-bottom:350px;
}
table td:first-child::after {
   content: "";
   display: inline-block;
   vertical-align: top;
   min-height: 60px;
}
.arrondi {
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
}
table span{
	width:93%;
	display:block;
	background-color: lightblue;
	margin:2px auto 2px auto;	
	padding:3px;
	border:0px solid black;
	line-height:25px
}
  </style>
    <script type="text/javascript">
    //<![CDATA[
        $(function(){
          $(document).ready(function(){
                $('.item').on("blur", function(event) {
                    cetid=$(this).parent().attr('id');
                    document.getElementById(cetid).firstElementChild.value=$(this).html();
                    //alert(document.getElementById(cetid).firstElementChild.value);
                    $('#kanban').serializeAny();
                });
		$('.event').on("dragstart", function (event) {
			  var dt = event.originalEvent.dataTransfer;
			  dt.setData('Text', $(this).attr('id'));
			});
                $('.arrondi').on("dragenter dragover drop", function (event) {	
		   event.preventDefault();
		   if (event.type === 'drop') {
                        var data = event.originalEvent.dataTransfer.getData('Text',$(this).attr('id'));
			if (data!=$(this).attr('id'))  
			    {
                            de=$('#'+data).detach();
                            de.insertBefore($(this));	
                            $('#kanban').serializeAny();
                            }
			 event.stopPropagation();
		   };
              });
              $('table td').on("dragenter dragover drop", function (event) {	
		   event.preventDefault();
		   if (event.type === 'drop') {
			  var data = event.originalEvent.dataTransfer.getData('Text',$(this).attr('id'));
			  de=$('#'+data).detach();
			  de.appendTo($(this));	
			  $('#kanban').serializeAny();
		   };
	   });
	});
        });
function send(a) {
    $.ajax({
        url: "store.php<?php echo "?doc=$doc"; ?>",
        type: "POST",
        data: {
            ssd: "yes",
            data: a,
        },
        dataType: "text",
        success: function (jsonStr) {
            $("#result").html(jsonStr['back_message']);
        }
    });
}
(function($){
    $.fn.serializeAny = function() {
    var ret = [];
     $.each( $(this).find(':input'), function() {
         ret.push(encodeURIComponent(this.name) + "=" + encodeURIComponent($(this).val()));
        
    });
    send(ret);
}
})(jQuery);
    	//]]>
</script>
  <form method="POST" target="">
    <div class="l p z">
    <table width=100%>
    <tr>
    <td width=50%>
    Ajoutez des tâches dans la case ci-contre, et déplacez-les dans les cases à la souris ou avec le doigt (avec firefox)<br/>
    Vous pouvez créer des contextes (la case "tampon" permet des échanges entre les contextes")<br/>
    </td>
    <td>
    <div><textarea style="background-color:white;height:14vh;width:100%;resize:none" name="tasks"></textarea>
    <input class='arrondi' style='width: calc(100%);background-color:lightgray;font-weight: bold;;' type="submit" value="Ajouter">
    </div>
    </td>
    </tr>
    </table>
    </div>
  </form>
    <div style='font-size:14pt;' class="r p z sc"><div style='line-height:26px;padding:5px;'><form style='border:1px solid black;display:inline;background-color:white;' method="GET" target=""><input size='20' type='text' name='doc' value=''></form>&nbsp;<?php foreach ($contexts as $c) { if ($doc!=$c) echo "<a class='arrondi' style='background-color:ivory;text-decoration:none;' href='?doc=$c'>&nbsp;$c&nbsp;</a> ";else echo "<a class='arrondi' style='background-color:orange;text-decoration:none;' href='?doc=$c'>&nbsp;$c&nbsp;</a> ";}?></div></div>
    <form id="kanban">
    <div class="r bl z">
      <div class="pb z"><?php echo zone("Fini",6);?></div>
      <div class="pb z"><?php echo zone("Tampon d'échange",7);?></div>
      <div class="pb z"><?php echo zone("Corbeille",8);?></div>
    </div>
    <div class="liste r z sc" style='background-color:#e0e0d1'><?php echo zone("En cours",5);?></div>
    <div class="l bl z">
      <div class="pb z"><?php echo zone("Planifier",2);?></div>
      <div class="pb z"> <?php echo zone("Faire faire",3);?></div>
      <div class="pb z"><?php echo zone("Un jour peut-être",4);?></div>
    </div>
    <div class="liste t r z sc"><?php echo zone("À faire",1);?></div>
    </form>
<script>
$('#kanban').serializeAny();
</script>
</body>
</html>
