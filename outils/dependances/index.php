<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 <html>
  <head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <link rel="SHORTCUT ICON" href="favicon.ico">
   <title>Inventaire de d�pendances</title>
  </head>
  <body>

<?php
/* syst�me de visualisation de d�pendances
algorithme
liste de points d'entr�e (fichiers)
une liste de rep�rage de d�pendances
  require
  css
  javascript
  php en ajax
chaine � trouver, d�pendance � signaler, fichier � ajouter au traitement
copier liste de points d'entr�e dans une liste de fichiers � traiter
tant qu'il reste des points � traiter
  rep�rer des d�pendances (et ajouter des fichiers � traiter)

*/
// configuration
$path="/web/philo-labo";
//$entrances=array(
//    'philosophemes/index.php',
//    'index.php'
// );
$efface='/composition2/';
$entrance=array(
//    '/dependances/test.php',
//    '/philosophemes/index.php',
    '/composition2/index.php',
//    '/local/config.php'
//      '/index.php'
    );
$reperages=array(
// r�duire
    array('/require\((.*)\)/','\1','php','red'),
    array('/require_once\((.*)\)/','\1','php','red'),
    array('/include\((.*)\)/','\1','php','orange'),
    array('/include_once\((.*)\)/','\1','php','orange'),
    array('/[\'|"](.*)\.php[\?|\'|\"]/','\1','js','blue')
    );
$atraiter=$entrance; // fichiers � traiter
$depends=array();

function traite(&$s,$repere,$hispath,$file)
    {
    global $atraiter,$depends,$efface;
    list($regex,$dep,$type,$coloredge)=$repere;
    //echo "<h2>traitement de la regex: $regex</h2>";
    preg_match_all($regex,$s,$matches,PREG_PATTERN_ORDER);
    foreach ($matches[1] as $m)
        {
        $m=str_replace('&quot','',$m);
        $m=substr($m,1,strlen($m)-2);
        //echo "Le m---------------->$m<br/>";
//         $m=str_replace('"','',$m);
        //  $m=str_replace('$LocalDir/',"",$m);
        $m=str_replace('$FarmC/','',$m);
        $m=str_replace('$FarmD/','/',$m);
         if ($m[0]!='/')             
            $m="$hispath/$m";
        $m=str_replace('/local/','/',$m); 
        $m=str_replace('/web/philo-labo/','/',$m);
        $m=preg_replace('|/.*/\.\.|U','',$m); // traitement des reculs dans les r�pertoires ..
        $m=str_replace('//','/',$m);
//        echo "$file->$m<br/>";
        if (!in_array($m,$atraiter) and !strpos($m,'$')) //non d�j� dans la liste � traiter, et ne contenant pas de variables 
            {
            $atraiter[]=$m;// ajout des fichiers d�couvert dans $atraiter
            //echo "ajout de $m<br/>";
            //$m=str_replace($efface,'',$m);
            $depends[]="\"$file\"->\"$m\" [color=$coloredge] ;"; // \najout de la d�pendance;
            }
        }
    }

for ($i=0;$i<sizeof($atraiter);$i++)
    {
    $f=$atraiter[$i];
    $decoup=explode('/',$f);
    $file=array_pop($decoup);
    $hispath=implode('/',$decoup);
    //echo "d�coupe en: $hispath  / $file</br>";
    //echo "traitement de $f<br/>";
    $s=file_get_contents("$path$f");
    $s=htmlentities($s);
    //echo "<pre>$s</pre>";
    foreach ($reperages as $r)
        {
        traite($s,$r,$hispath,"$f");
        }
    }
    
// echo "<pre>";
// print_r($atraiter);
// echo "</pre>";
    
$graph="digraph truc{rankdir=LR\nnode [shape=Mrecord]\n".implode("\n",$depends)."}";
file_put_contents('graph.dot',$graph);
exec('dot -Tpng graph.dot -o graph.png');
//echo "<pre>$graph</pre>";
?>
<img src="graph.png">
  </body>
 </html>
