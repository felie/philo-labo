version=pmwiki-2.2.0-beta36 ordered=1 urlencoded=1
agent=Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; Media Center PC 3.1; .NET CLR 1.1.4322)
author=Caveman
csum=
ctime=1158768281
host=75.104.48.13
name=Snippets.Login
rev=30
targets=Main.Login
text=Here is a simple login form to log members into your site (using AuthUser) who have a Login page with their member name and password in it.  [[Main.Login|Click here]] if your account stopped working with the 2007-04 release of ZAP.%0a%0aTRY THE CODE:%0a[[#ID]]%0a(:messages:)%0a(:zapform:)%0a(:zap login="":)%0a||Member Name: ||(:input text Member:)%0a||Password: ||(:input password Password:) (:input submit value="Login!":)%0a(:zapend:)%0a[[#ID]]%0a
time=1176920019
author:1176920019=Caveman
diff:1176920019:1175528774:=1,2c1,2%0a%3c Here is a simple login form to log members into your site (using AuthUser) who have a Login page with their member name and password in it.  [[Main.Login|Click here]] if your account stopped working with the 2007-04 release of ZAP.%0a%3c %0a---%0a> Here is a simple login form to log members into your site (using AuthUser) who have a Login page with their member name and password in it.%0a> %0a
host:1176920019=75.104.48.13
