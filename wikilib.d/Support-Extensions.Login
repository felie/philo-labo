version=pmwiki-2.2.0-beta36 ordered=1 urlencoded=1
agent=Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; Media Center PC 3.1; .NET CLR 1.1.4322)
author=Caveman
csum=
ctime=1173760606
host=75.104.48.13
name=Support-Extensions.Login
rev=12
targets=Support-Extensions.Register
text=||border=0 cellpadding=0 cellspacing=0%0a||'''Format:&nbsp;&nbsp;&nbsp;'''  ||[=(:zap=] %25green%25login%25%25:)%0a|| ||[=(:zap=] %25green%25login%25%25="%25red%25auto%25%25":)%0a%0a'''Requirements to Use:''' ZAP | ZAP Toolbox%0a%0a'''Explanation:''' Checks field "Password" in %3cProfiles>.%3cMember> page (based on the value of the "Member" form field) against that entered in the form field "Password".  If it matches, it logs them in as "Member". To change the default group checked for member login information, reset Login: to another group on the Site.ZAPConfig file.%0a%0aIf login is set to auto, ZAP automatically logs them in as "Member" if that name does not already exist in your Profiles group. Normally this is used (optionally) on a new member [[Register|Registration]] form.%0a%0aNormally passwords are encoded using a special ZAP coding mechanism that only allows admins and users to decode them (ie, where authid equals the page name). ZAP, however, can tell if a password is encrypted or plaintext, and authenticate appropriately.  See the [[Register]] command for more information.%0a%0aCurrently, the ZAP login form does not work properly on Site.AuthForm.  You will probably want to change that page to notify them they must login and provide a link to the login page. Do not delete the contents of that page however, until you have verified your login system is working, or you may lock yourself out of your site!%0a%0aAs a security feature, this command can ONLY be set using the (:zap&nbsp;field="value":) format. %0a%0a%0a%0a%0a
time=1176655385
