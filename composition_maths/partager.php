<?php
require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('../philosophemes/ai.php');
require_once('../local/logs.php');

$user=$_GET[user];
$LongName=$_GET[LongName];
$doc=$_GET[doc];
$ok=$_GET[ok];
if ($ok=="ok") // partage
    { // production d'un lien symbolique dans /partage vers le fichier de l'utilisateur
    exec("ln -s \"/web/philo-labo/users/$user/compositeur/$doc.tree\" \"/web/philo-labo/partage/[$LongName] $doc.tree\"");
    $numauteur=simple_query("select id from membres where membre='$user'");
    $x=simple_query("select partage,id_membre from partages where id_membre=$numauteur and partage='$doc'");
    if ($x=='')
        $x=simple_query("INSERT INTO partages (partage,id_membre) VALUES ('$doc', '$numauteur');");
    //echo "cp \"/web/philo-labo/users/$user/compositeur/$doc.tree\" \"/web/philo-labo/partage/$LongName - $doc.tree\"";
    writelog("$user\tshared 1\t$doc");
    }
else // arrêt du partage
    {
    unlink("/web/philo-labo/partage/[$LongName] $doc.tree"); // destruction du lien symbolique
    $numauteur=simple_query("select id from membres where membre='$user'");
    $x=simple_query("delete from partages where id_membre=$numauteur and partage='$doc'"); // suppression dans la base de données 
    writelog("$user\tshared 0\t$doc");
    }
?>