<?php

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ini_set("display_errors", 1);


if (!isset($Author))
  require_once('../philosophemes/secure.php'); 
  
require_once('ressources.php');
require_once('philo-labo2.php');
require_once('../local/logs.php');

$header='<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="/pub/skins/ians/textes.css" type="text/css" />
</head><body>%s';

$LongName=file_get_contents("/web/philo-labo/users/$Author/longname"); //nom long stocké dans le répertoire utilisateur; bof bof

function pandoc($command,$target,$repuser)
    {
    exec("cd $repuser;$command"." -o \"$target\" 2> erreurs");
    return file_get_contents("$repuser/$target");
    }
    
function propulse($type,$filename,$name,$suffix)
    {
    global $Author;
    writelog("$Author\tpropulse\t$suffix\t$filename");
    $name=str_replace(',','',$name); // pas de virgule dans les noms!
    header("Content-Disposition: attachment; filename=\"$name.$suffix\"");
    header("Content-type: $type");
    header('Expires: 0');
    header('Cache-Control: private, max-age=0, must-revalidate');
    header('Pragma: public');
    readfile($filename);
    exit;
    }

function propulsertf($filename,$name) { propulse("application/rtf",$filename,$name,'rtf'); }
function propulseodt($filename,$name) { propulse("application/vnd.oasis.opendocument.text",$filename,$name,'odt'); }
function propulsedocx($filename,$name) { propulse("application/vnd.openxmlformats-officedocument.wordprocessingml.document",$filename,$name,'docx'); }
function propulsepdf($filename,$name) { propulse("application/pdf",$filename,$name,'pdf'); }
function propulsehtml($filename,$name) { propulse("text/html",$filename,$name,'html'); }

function thumbnail($doc) // dans le répertoire pandoc, concerne les diaporama et les textes $doc.pdf et $doc-textes.pdf
    {
    global $Author;
    $repuser="/web/philo-labo/users/$Author/compositeur";
    exec("cd $repuser/pandoc;pdftk $doc.pdf cat 1 output $doc.couv.pdf;convert $doc.couv.pdf $doc.png;convert -border 2x2 $doc.png $doc.border.png;mv $doc.border.png $doc.png;convert $doc.png -page +5+5 -alpha set \( +clone -background gray35 -shadow 60x3+5+5 \) +swap -background none -mosaic $doc.shadow.png;mv $doc.shadow.png $doc.png");
    }
    
function export_solo($name,$format)
    {
    global $Author,$doc,$nosommaire;
    //echo "dans export_solo";
    $repuser="/web/philo-labo/users/$Author/compositeur";
    $source=explode(" ",$name);
    $id=array_shift($source);
    $filtre='';
    if (substr($source[0],0,1)=='=')
        $filtre=array_shift($source);
    $source=implode(" ",$source);
    $doc=$source; // la variable doc est instanciée pour que version puisse l'utilisée pour le nom de fichier à propulser
    switch ($format)
        {
        case 'lpdf':
        $ladate=simple_query("select ladate from ressources where id=$id"); // récupération de la date de modification de la ressource
        //echo temps('avant split');
        if (!file_exists("/web/philo-labo/splits/chunkpdf$id$filtre.pdf") or filemtime("/web/philo-labo/splits/chunkpdf$id$filtre.pdf")<$ladate)
            split_text_pdf($id,$filtre);
        //echo temps("après split id=$id");
        //echo "<a href='/splits/chunkpdf$id.pdf'>cible</a>";
        propulsepdf("/web/philo-labo/splits/chunkpdf$id$filtre.pdf",$name);
        break;
        case 'pdf':
        $nosommaire=true;
        version("\n[texte]@$id $filtre\n",'poly_joli');
        break;
        default:
        $nosommaire=true;
        version("\n@$id",$format); // autres cas
        }
    }
    
function titre_gras($t,$gras=false) // ## truc revient en ## <b>truc</b>
    {
    $t=trim(str_replace("#",'',$t));
    if ($gras)
        return "<b>$t</b>\n";
    else
        return "$t\n";
    }
    
function jolidate()
    {
    setlocale(LC_TIME,'fr','fr_FR','fr_FR.UTF-8');
    return strftime("%d %B %Y");
    }
    
function traite_tableau($s,$mode) // pour l'instant seulement pour le monde LaTeX
    {
    $s=explode("\n",$s); // tableau de lignes
    $capture=0;
    foreach($s as &$l)
        {
        switch ($l) 
            {
            case 'tabcccc': $capture=4; $l="\\".$l;
             break;
            case 'tabvv':
            case 'tabll':$capture=2;$l="\\".$l;
             break;
            case 'tablcc':
            case 'tabccl':
            case 'tabvcc':
            case 'tabccv':$capture=3;$l="\\".$l;
             break;
             default:if ($capture>0)
                {
                $l="{ $l }";$capture=$capture-1;
                }
            }
        }
    return implode("\n",$s);
    }
    
function prefix()
    {
    global $nosommaire;
    if (preference('Sommaire dans les polys') and !$nosommaire)
return '
\renewcommand{\contentsname}{}
\textbf{Sommaire} \leaders\vrule height 2.5pt depth -1.5pt \hfill\null
\vspace{-.7cm}
\begingroup
%\setlength{\beforechapskip}{-10pt}
%\addtocontents{toc}{\protect\thispagestyle{empty}}%pour éviter la numérotation de la première page sous la colonne du sommaire...
\tableofcontents
\renewcommand\listfigurename{}%
\renewcommand\numberline[1]{}%
\vspace{-.5cm}
\listoffigures
\endgroup
\vspace{-.2cm}
\hrulefill
%\vspace{-.5cm}
';
else
    return '';
    }
    
function doc_prepare($source,$mode='')
    {
    global $nosommaire;
    $source=explode("\n",$source);
    if (sizeof($source)==1) // un pdf simple à partir d'une seule ressource
        return
        preg_replace_callback(
        '/\@([0-9]*)/',
        function ($matches) 
            {
            return item_poly($matches[1]);
            }
        ,$source); // on remplace tous les items par leur valeur en markdown
    if (!preference('Titres dans les polys'))
        {
        $source=preg_replace('/^# (.*)$/','\addtocontents{toc}{\vspace{-0.25cm}}\addcontentsline{toc}{section}{\textsc{\1}}\addtocontents{toc}{\vspace{0.2cm}}',$source); 
        $source=preg_replace('/^## (.*)$/','\addcontentsline{toc}{subsection}{\hspace{-0.3cm}\textbf{\1}}',$source);
        $source=preg_replace('/^### (.*)$/','\addcontentsline{toc}{subsubsection}{\hspace{-1cm}\1}',$source);
        }
    else
        {
        $source=preg_replace('/^# (.*)$/','\addtocontents{toc}{\vspace{-0.25cm}}\addcontentsline{toc}{section}{\vspace{-0.1cm}\textsc{\1}}\addtocontents{toc}{\vspace{0.2cm}}\section*{\vspace{-.5cm}\textsc{\1}\vspace{.5cm}}',$source); 
        $source=preg_replace('/^## (.*)$/','\addcontentsline{toc}{subsection}{\hspace{-0.3cm}\textbf{\1}}\vspace{-0.05cm}\subsection*{\hspace{0.35cm}\textbf{\1}}',$source);
        $source=preg_replace('/^### (.*)$/','\addcontentsline{toc}{subsubsection}{\hspace{-1cm}\1}\vspace{-0.25cm}\subsubsection*{\hspace{0.7cm}\1}',$source);
        }
    $source=preg_replace('/^\[(.*)\]@(\d+)( =[a-z]*)*(.*)$/','@$2$3 - $4 $1',$source); // réduction aux ressources brutes
    $source=preg_replace('/^[^\\\@](.*)$/','',$source); // suppression de tout le reste
    $source=implode("\n",$source);
    if ($mode=='joli')
                $source=preg_replace_callback(
            '/\@([0-9]*)( =)?([a-z]*) - (.*) (.*)/',
            function ($matches) 
                {
                return item_poly_joli($matches[1],$matches[3],$matches[4]);
                }
            ,$source); // on remplace tous les items par leur valeur en markdown
    else
        $source=preg_replace_callback(
            '/\@([0-9]*)( =)?([a-z]*) - (.*) (.*)/',
            function ($matches) 
                {
                global $nosommaire;
                //echo "$matches[1] --- $matches[3]<br/>";
                if (preference('Sommaire dans les polys') and !$nosommaire and preference('Ressources dans les sommaires'))
                    $prefix="\addcontentsline{toc}{subsection}{[$matches[5]] $matches[4]}";
                return $prefix.item_poly($matches[1],$matches[3],$matches[4]);
                }
            ,$source); // on remplace tous les items par leur valeur en markdown
    $source=str_replace('[sujet-texte]','[texte]',$source);
    return $source.'\vspace{.5cm}';
    }
    
function version($source,$format) 
    {
    global $Author,$LongName,$doc,$nosommaire;
    $repuser="/web/philo-labo/users/$Author/compositeur";
    $date="Version générée le ".jolidate();
    if (!file_exists($repuser))
      mkdir($repuser,0660);
    // expansion des composites
    $source=preg_replace_callback(
                '/\@([a-z]*)([0-9]*)/',
                function ($matches)
                    {
                    return expandcomposite($matches[2],$matches[1]);
                    }
                ,$source); // on remplace tous les items par leur valeur en markdown
    switch ($format)
        {
        case 'outline':
            $source=preg_replace('/\@([a-z]*)([0-9]*)/',' ',$source);
            $source=str_replace("\n","\n\n".'',$source);
            //$source=str_replace("[","".'',$source);
            //$source=str_replace("]","".'',$source);
            $source=str_replace('#','#',$source);
            $source=str_replace('###### ','                    *',$source);
            $source=str_replace('##### ','                *',$source);
            $source=str_replace('#### ','            * ',$source);
            file_put_contents("$repuser/pdf.md",$source);
            unlink("$repuser/outline.pdf");
            //pandoc("pandoc -N -s pdf.txt -t html5 --atx-headers","outline.html",$repuser);
            //propulsehtml("$repuser/pdf.html","html");
            pandoc("pandoc -N -s pdf.md -t latex --template=/web/philo-labo/composition/outline.tex --atx-headers","outline.tex",$repuser);
            exec("cd $repuser;pdflatex --interaction=nonstopmode outline.tex -o outline.pdf 2> erreurs2");
            propulsepdf("$repuser/outline.pdf","plan du document $doc");
            break;
        case 'outline_html':
            $source=preg_replace('/\@([a-z]*)([0-9]*)/',' ',$source); // on enlève les numéro de ressources
            $source=str_replace("\n","\n\n",$source);
            $source=str_replace('######','                    * ',$source);
            $source=str_replace('#####','                * ',$source);
            $source=str_replace('####','            * ',$source);
            $source=preg_replace('/^#(.*)$/','<h1>$1</h1>',$source);
            $source=preg_replace('/^##(.*)$/','<h2>$1</h2>',$source);
            $source=preg_replace('/^###(.*)$/','<h3>$1</h3>',$source);
            $source=preg_replace('/^###(.*)$/','<h3>$1</h3>',$source);
            $source=preg_replace('/^####(.*)$/','<h4>$1</h4>',$source);
            $source=preg_replace('/^#####(.*)$/','<h5>$1</h5>',$source);
            $source=preg_replace('/^######(.*)$/','<h6->$1</h6>',$source);
            $source=preg_replace('/^\[(.*)$/','<div class="ressource">$1</div>',$source);
            file_put_contents("$repuser/source.txt",$source);
            unlink("$repuser/outline.html");
            pandoc("pandoc -N -s source.txt --css https://philo-labo.fr/css/outline.css -t html5 --atx-headers","outline.html",$repuser);
            propulsehtml("$repuser/outline.html","plan du document $doc");
	    break;
        case 'arbre éditable':
            for ($i=9;$i>0;$i=$i-1)
              $source=str_replace(str_repeat('#',$i),$i-1,$source);
            $lignes=explode("\n\n",$source);
            $prof=0;
            $result='';
            foreach ($lignes as &$l)
              {
              $i=$l[0];
              if ($i!='@')
                $prof=$i;
              $result.=str_repeat('&nbsp;',$prof).substr($l,($i!='@'),strlen($l)-($i!='@'))."<br/>";
              }
            return $result;
        case 'brut':
            return "<pre>$source</pre>";
        case 'pmwiki':
		$lignes=explode("\n",$source);// tableau de lignes
		$lignes=preg_replace('/^\[.*\]/','',$lignes); // on vire la catégorie qui est entre crochet (textes et video en solo dans un titre ?)
		$lignes=preg_replace(array('/#/','/^([!|@])/'),array('!',"<br/>$1"),$lignes);
		//$lignes=preg_replace('/@(\\d+)/',"!!! @tt$1<br/>@$1",$lignes); // le texte devient: le titre en titre de diapo, et dans la diapo le texte
		$lignes=preg_replace_callback('/@(\\d+)/',
                  function ($matches)
                    {
                    //$titre=simple_query('select ressource from ressources where id='.$matches[2],$matches[1]);
                    $res=select2html('select id,nature,ressource from ressources where id='.$matches[1],'h',array('@%s:%s='),array('%s','%s','%s'));
                    $res=str_replace(' ','_',$res);
                    //return "<br/>!!!$titre<br/>$res";
                    return "$res<br/>";
                    }
                  ,$lignes);
		return "$doc<br/>".implode("\n",$lignes);  // première ligne
            break;
        case 'markdown':
            $source=explode("\n","$source");
            //$source=preg_replace('/^\[texte]@(\d+)(.*)$/',"\n@$1",$source);
            $source=preg_replace('/^\[(.*)\]@(\d+)(.*)$/',"### $3\n@$2",$source); // on vire le nom des ressources et leur [type] ainsi que le filtrage
            $source=implode("\n\n",$source);  
//             $source=explode("\n",$source); 
//             $source=preg_replace('/(.*)\@([0-9]*)(.*)/',"@$2",$source);
//             $source=implode("\n",$source);
//             $source=str_replace("\n","\n\n",$source);
            $source=preg_replace_callback(
                '/\@([a-z]*)([0-9]*)/',
                function ($matches) 
                    {
                    return item_markdown($matches[2],$matches[1]);
                    }
                ,$source); // on remplace tous les items par leur valeur en markdown
            return "% $doc \n% $LongName \n% $date \n\n$source";
            break;
        case 'markdown_joli':
            return "\n<style>\npre { white-space: pre-wrap; }\n</style>\n<pre>".version($source,'markdown')."</pre>";
            break;
        case 'html':
            $source=explode("\n",$source);
            //$source=preg_replace('/^\[texte]@(\d+)(.*)$/',"\n@$1",$source);
            $source=preg_replace('/^\[(.*)\]@(\d+)(.*)$/',"### $3\n@$2",$source); // on vire le [type] des ressources et le filtrage
            $source=preg_replace_callback(
                '/\@([a-z]*) ### ([0-9]*)/',
                function ($matches){return item_html($matches[2],'gimpc');},
                $source); // on remplace tous les items par leur valeur en html
	    file_put_contents("$repuser/tmp/$Author.txt",$source);
            pandoc("cd $repuser/tmp/;pandoc -t html5 -s $Author.txt --template=/web/philo-labo/composition/template.html","$Author.html"); // la css est dans la page html
            return file_get_contents("$repuser/tmp/$Author.html");
            break;
        case 'html5':
            $source=explode("\n",$source);
            //$source=preg_replace('/^\[texte]@(\d+)(.*)$/',"\n@$1",$source);
            $source=preg_replace('/^\[(.*)\]@(\d+)( =[a-z]*)*(.*)$/',"### $4\n@$2$3",$source); // on vire le nom des ressources et leur [type]
            $source=implode("\n\n",$source);  
            //echo $source;
            $source=preg_replace_callback(
                '/\@([0-9]*)( =)*([a-z]*)/',
                function ($matches){return item_html5($matches[1],$matches[3]);},
                $source); // on remplace tous les items par leur valeur en html
            //echo $source;
            file_put_contents("$repuser/tmp/$Author.txt",$source);
            pandoc("cd $repuser/tmp/;pandoc -t html5 -s $Author.txt --template=/web/philo-labo/composition/template.html","$Author.html"); // la css est dans la page html
            propulsehtml("$repuser/tmp/$Author.html",'export');
            return file_get_contents("$repuser/tmp/$Author.html");
            break;
        case 'html_inframe': // on pourrait surcharger avec une option le case 'html' aussi
   	   $source=preg_replace_callback(
                '/\@([a-z]*)([0-9]*)/',
                function ($matches)
                    {
                    return '### '.nom_ressource($matches[1])."###\n".'<iframe width="100%" height="600px" src="/composition/ressif.php?id='.$matches[1].'&big=true"></iframe>';
                    }
                ,$source); // on remplace tous les items par leur valeur en html
            return $source;
            break;
        case 'html_pour_S5': // on pourrait surcharger avec une option le case 'html' aussi
        $source=preg_replace("/\[(.*)\]@(\d+)( =[a-z]*)*(.*)\n/","@$2\n",$source); // on vire le nom des ressources et leur type
   	    $source=preg_replace_callback(
                '/\@([0-9]*)/', //'/\@([0-9]*)( =)*([a-z]*)--(.*)/',
                function ($matches)
                    {
                    //echo "------> $matches[1] *** $matches[3] *** $matches[5]";
                    return item_S5($matches[1],$matches[2],$matches[3]); // id, filtre, titre
                    }
                ,$source); // on remplace tous les items par leur valeur en html
            return $source;
            break;
        case 'S5': // Simple Standards-Based Slide Show System
            writelog("$Author\tprepare\tS5\t$doc"); // pour les logs
            //$source=preg_replace("/\n([^#][^@])/","\n\n".'> - $1',$source); // pour les puces incrémentales (si c'est ni un titre, ni une image
            $source=explode("\n",$source);
            //$source=preg_replace('/^\[texte]@(\d+)( =[a-z]*)*(.*)$/',"### $3\n@$1$2---$3",$source); // evec les filtres
            //$source=preg_replace('/^\[texte]@(\d+)( =[a-z]*)*(.*)$/',"@$1$2$3",$source); // pour ne pas être traité comme à la ligne suivante
            //$source=preg_replace('/^\[(.*)\]@(\d+)(.*)$/',"### $3\n@$2",$source); // on vire le nom des ressources et leur [type] et on les transforme en diapo
            $source=implode("\n",$source);
            $source=explode("\n",version($source,'html_pour_S5'));
            $source=implode("\n",$source);
            $source=str_replace("\n#","\n\n#",$source); // sépare les titres sinon pandoc en oublie
            //parsing pour les titres pour le repérage
            $source=explode("\n",$source); // remet en tableau pour traitement
            $titre1=array();
            $titre2=array();
            $niv1=-1;
            foreach ($source as $line) // première passe de repérage
                {
                if (substr($line,0,2)=='# ') // titre 1
                    {
                    $niv1=$niv1+1;
                    $titre1[]=$line;
                    }
                else
                    if (substr($line,0,3)=='## ')
                        $titre2[$niv1][]=$line;
                }
            $niv1=-1;
            foreach ($source as &$line) // deuxième passe, modification du source ajout des diapos de plan et des situations de chaque diapo de niveau 3
                {
                if (substr($line,0,2)=='# ') // titre 1 -> plan de situation avec le titre actuel en gras
                    {
                    $res='';
                    foreach ($titre1 as $t1)
                        {
                        if ($line==$t1)
                            $res.=titre_gras($t1,true)."\n";
                        else
                            $res.=titre_gras($t1)."\n";
                        }
                    $line="\n# \n".$res;
                    $niv1=$niv1+1;
                    $niv2=-1;
                    }
                else
                    {
                    if (substr($line,0,3)=='## ') // titre 2 -> plan de situation avec titre actuel en gras
                        {
                        $res='';
                        foreach ($titre2[$niv1] as $t2)
                            {
                            if ($line==$t2)
                                $res.=titre_gras($t2,true)."\n";
                            else
                                $res.=titre_gras($t2)."\n";
                            }
                        $line="\n# ".titre_gras($titre1[$niv1],false)."\n".$res;
                        $niv2=$niv2+1;
                        }
                    else
                       if (substr($line,0,4)=='### ') // diapositive
                            {
                            $line=str_replace('###','#',$line);
                            $line.="\n\n## ".titre_gras($titre1[$niv1])."\n\n### ".titre_gras($titre2[$niv1][$niv2]);
                            }
                    }   
                }
            $source=implode("\n",$source);
            $view=str_replace("\n","<br/>",$source);
            //return "$view"."<hr/>";
            //exit;
            $source="% $doc \n% $LongName \n% $date \n$source";
            file_put_contents("$repuser/s5.txt",$source);
            //propulsehtml("$repuser/s5.txt","$doc.txt");
            mkdir("$repuser/tmp");
            pandoc("pandoc -t s5 --template=/web/philo-labo/m2b/template --variable=s5-url:/pub/s5simple/ui/default --slide-level=1 -s s5.txt","tmp/$doc.html",$repuser);    // attention à la locale
            $texte=file_get_contents("/web/philo-labo/users/$Author/compositeur/tmp/$doc.html");
            $texte=str_replace('/pub/s5simple','https://philo-labo.fr/pub/s5simple',$texte);
            file_put_contents("/web/philo-labo/users/$Author/compositeur/tmp/$doc.html",$texte);
            propulsehtml("$repuser/tmp/$doc.html","$doc.html");
            $result="<a href='/users/$Author/compositeur/tmp/$doc.html' target='_blank'>Votre présentation est prête - cliquez sur ce lien (et mettez en plein écran par F11)</a>";
            //$result=file_get_contents("$repuser/tmp/$doc.html");
            return $result;
	    break;
        case 'pdf':
	    file_put_contents("$repuser/pdf.txt",version($source,'markdown'));
	    pandoc("pandoc --latex-engine=xelatex -s pdf.txt","pdf.pdf",$repuser);
	    propulsepdf("$repuser/pdf.pdf","$doc");
            //echo "<a href='/users/$Author/compositeur/pdf.pdf'>votre fichier au format pdf est prêt</a> ";
            break;
        case 'splitter': // Simple Standards-Based Slide Show System
            $source=preg_replace_callback(
                '/\@([a-z]*)([0-9]*)/',
                function ($matches)
                    {
                    $n=$matches[1];
                    $res='';
                    $a=split_text(item_html($n));
                    exec("cd $repuser;touch toto;dvipdf ../split.dvi $n.pdf"); // conversion en pdf
                    $res.="\includepdf($n.pdf)\n";
                    return $res;
                    }
                ,$source); // on remplace tous les items par leur valeur en html
//             $file="/web/philo-labo/m2b/$Author";
//             file_put_contents("$file.txt",version($source,'markdown'));
//             exec("/web/philo-labo/m2b/m2b -c -I /web/philo-labo/composition/ $file"); //imagelabo pointe sur ../
//             propulsepdf("$file.pdf","$Author-$doc");
            return $source;
            break;
            file_put_contents("$repuser/doc.txt",$source);
            exec("cd $repuser;pandoc -f markdown -t beamer --smart --wrap=none -o doc.tex --slide-level=3 --template=latex.mask doc.txt;latex -interaction=nonstopmode doc;dvipdf doc");
            propulsepdf("$repuser/doc.pdf","pdf");
            return $source;
            break;
        case 'beamer': // production du beamer sans m2b, avec pandoc seulement
             $source=preg_replace("/\[(.*)\]@(\d+)( =[a-z]*)*(.*)\n/","@$2$3 $4\n",$source); // on vire le nom des ressources et leur [type]
             //echo "1 - $source<br/>";
             $source=traite_tableau($source,'latex');
             //echo "2 - $source<br/>";             
             $source=preg_replace_callback(
                '/\@([0-9]*)( =)*([a-z]*) (.*)/',
                function ($matches)
                    {
                    return item_beamer($matches[1],$matches[3],$matches[4]); 
                    }
                ,$source); // on remplace tous les items par leur valeur en html
            //echo temps('après callback');
            $source=str_replace("\n#","\n\n#",$source);
            $source=Beamer::render($source); // pour les bold et les italic
            if (preference('Mode incremental pour les diaporamas')) // préférence
                $source=preg_replace("/\n( *)-/","\n>$1*",$source);
            else
                $source=preg_replace("/\n( *)-/","\n$1*",$source);
            $source=str_replace("      *",'            *',$source);
            $source=str_replace("    *",'        *',$source);
            $source=str_replace("  *",'    *',$source); 
            mkdir("$repuser/pandoc"); // au cas où il n'existe pas
            setlocale(LC_TIME,'fr','fr_FR','fr_FR.UTF-8');
            if (preference('Nom sur la couverture des diaporamas'))
                $leLongName=$LongName;
            else
                $leLongName='';
            if (preference('Date sur la couverture des diaporamas'))
                $laDate="version du ".jolidate();
            else
                $laDate='\ladate{}';
            file_put_contents("$repuser/pandoc/$doc.md","%$doc \n%$leLongName \n%$laDate\n$source"); // écriture du fichier sur le disque
            //echo temps('avant pandoc');
            exec("cd $repuser/pandoc;cp /web/philo-labo/m2b/*sty .;pandoc -st beamer -f markdown-auto_identifiers --template=/web/philo-labo/m2b/latex.mask --slide-level 3 \"$doc.md\" -o \"$doc.tex\"");
            $tex=file_get_contents("$repuser/pandoc/$doc.tex");
            $tex=preg_replace("/\{frame\}\{fwb(.*)\}/U",'{fwb}{\1}',$tex); // diapo sans barre centrée verticalement
            $tex=preg_replace("/\{frame\}\{fwt(.*)\}/U",'{fwt}{\1}',$tex); // diapo sans barre calée en haut
            $tex=str_replace("fwb\n\n".'\end{frame}',"\n\n".'\end{fwb}',$tex);
            $tex=str_replace("fwt\n\n".'\end{frame}',"\n\n".'\end{fwt}',$tex);
            $tex=str_replace("\begin{frame}{}\n\n".'\end{frame}','',$tex); // suppression des diapos vides
            file_put_contents("$repuser/pandoc/$doc.tex",$tex);
            exec("cd $repuser/pandoc;pdflatex -draftmode --interaction=nonstopmode --shell-escape \"$doc.tex\""); // divise par deux le temps de compilation avec draftmode! 
            //echo temps('après pdflatex 1');
            exec("cd $repuser/pandoc;pdflatex --interaction=nonstopmode --shell-escape \"$doc.tex\""); // deux fois pour l'index
            //echo temps('après pdflatex 2');
            # echo "fabrication de la couverture $1.png"
            thumbnail($doc);
            propulsepdf("$repuser/pandoc/$doc.pdf","$doc - diaporama");
            break;
        case 'poly1':
            $source=doc_prepare($source);
            $source=preg_replace_callback(
                '/\@([0-9]*)/',
                function ($matches) 
                    {
                    return item_poly($matches[1]);
                    }
                ,$source); // on remplace tous les items par leur valeur en markdown
            $prefix2='\vspace{1cm}\fontsize{12}{14}\rmfamily\setlength{\parindent}{2cm}';
            file_put_contents("$repuser/pandoc/FILE.tex",prefix().$prefix2.$source);
            exec("cd $repuser/pandoc;cp /web/philo-labo/m2b/textes-mask-1col.tex container.tex;pdflatex --interaction=nonstopmode container;pdflatex --interaction=nonstopmode container"); // 2 fois pour la table des matières
            propulsepdf("$repuser/pandoc/container.pdf","poly1");
            break;
        case 'polyA5':
           $source=doc_prepare($source);
            $source=preg_replace_callback(
                '/\@([0-9]*)/',
                function ($matches) 
                    {
                    return item_poly($matches[1]);
                    }
                ,$source); // on remplace tous les items par leur valeur en markdown
            $prefix2='\vspace{1cm}\fontsize{12}{14}\rmfamily\setlength{\parindent}{2cm}';
            file_put_contents("$repuser/pandoc/FILE.tex",prefix().$prefix2.$source);
            exec("cd $repuser/pandoc;cp /web/philo-labo/m2b/textes-mask-A5.tex container.tex;pdflatex --interaction=nonstopmode container;pdflatex --interaction=nonstopmode container"); // 2 fois pour la table des matières
            propulsepdf("$repuser/pandoc/container.pdf","A5");
            break;
        case 'poly_dys':
        case 'poly_joli': // concaténation de texte standalon, sans passer par le diaporama
            $source=doc_prepare($source,'joli');
            mkdir("$repuser/pandoc");
            $prefix='\fontsize{9}{10}\rmfamily\setlength{\parindent}{0cm}'.prefix().'\fontsize{11.5}{14}\rmfamily\setlength{\parindent}{1cm}'; // table des matières éventuelle
            file_put_contents("$repuser/pandoc/concattextes.tex",$prefix.$source); // écriture de la concaténation de texte dans un fichier
            $dys='';
            if ($format=='poly_dys')
                exec("cd $repuser/pandoc;rm  rm poly.*;cp /web/philo-labo/m2b/poly_joli_dys.tex poly.tex;sed -i 's|NAME|$doc|g' poly.tex;xelatex -draftmode --interaction=nonstopmode --shell-escape poly.tex;xelatex --interaction=nonstopmode --shell-escape poly.tex");
            else
                exec("cd $repuser/pandoc;rm  rm poly.*;cp /web/philo-labo/m2b/poly_joli.tex poly.tex;sed -i 's|NAME|$doc|g' poly.tex;pdflatex -draftmode --interaction=nonstopmode --shell-escape poly.tex;pdflatex --interaction=nonstopmode --shell-escape poly.tex");
            //thumbnail("$doc-textes");
            propulsepdf("$repuser/pandoc/poly.pdf","$doc - poly");
            break;
        case 'maths_enonce':
            $math_mode="énoncé";
        case 'maths_corr':
            if (!isset($math_mode))
                $math_mode="corrigé";
            $source=preg_replace("/\[(.*)\]@([0-9]*)( =[a-z]*)*(.*)\n/","@$2\n",$source);
            $source=preg_replace_callback(
                '/\@([0-9]*)/',
                function ($matches) 
                    {
                    //return simple_query("select texte from ressources where id=$matches[1]");
                    return item_poly_joli($matches[1]); // cela permet de traiter tous les types et pas seulement les textes
                    }
                ,$source);
            $source=str_replace(array('<lisatex>','</lisatex>'),array('',''),$source);
            file_put_contents("$repuser/pandoc/CONTENT.tex",$source);
            exec("cd $repuser/pandoc;cp /web/philo-labo/m2b/LisaTex* .;sed -i 's|NAME|$doc|g' LisaTex_$math_mode.tex;pdflatex --interaction=nonstopmode --shell-escape LisaTex_$math_mode.tex");
            propulsepdf("$repuser/pandoc/LisaTex_$math_mode.pdf","$doc - $math_mode.");
            break;
	default:
	    echo "format $format inconnu, ce doit être une erreur";
	}
    }

?>
