<?php
// ajouté pour mesure de temps
$debut=0;
$temps=0;
reset_timer();

function reset_timer()
    {
    global $debut,$temps;
    $debut=microtime(true);
    $temps=$debut;
    }
     
function temps($s='')
    {
    global $debut,$temps;
    $oldtime=$temps;
    $temps=microtime(true);
    $diff=round($temps-$oldtime,4);
    $depuisdebut=round($temps-$debut,4);
    if ($s=='')
        return $diff;
    else
        return "$depuisdebut - $diff $s<br/>";
    }
?>