<?php
require_once('../../../../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require('../../../../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('../../../../philosophemes/ai.php');
require_once('../../../../philosophemes/philo-labo.php'); // fonctions spécifiques à philo-labo
$recherches=sprintf($form_search,'?action=searchengine&json=true');
if ($_GET['action']=='searchengine')
 {
 $str='<?php ?>
 [<?php
 echo \''.str_replace("'","\'",resultatrecherche(true,true,true,$_GET['json'])).'\';
 ?>]';
 file_put_contents('../asyncData/getPhilo.php', $str);
 }
//echo $recherches;
?>
<!DOCTYPE html>
<HTML>
<HEAD>
	<TITLE> ZTREE DEMO - multiTree</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../../../css/demo.css" type="text/css">
	<link rel="stylesheet" href="../../../css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="../../../js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.exedit.js"></script>
	<SCRIPT type="text/javascript">
		var setting = {
			async: {
				enable: true,
				type:"get",		
				url:"../asyncData/getPhilo.php",
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			edit: {
				enable: true,

				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				onClick: myOnClick
			},
		};
                var settingA = {
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                        enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick
                        },
                };

		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
			}
			return childNodes;
		}

                function myOnClick(event, treeId, treeNode) {
                        document.getElementById('iframeid').src="/philosophemes/ressif.php?id="+treeNode.id;
                }

		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return targetNode ? targetNode.drop !== false : true;
		}
		var zvide=[];
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), settingA,zvide);
			$.fn.zTree.init($("#treeDemo2"), setting);
			$.fn.zTree.init($("#treeDemoD"), settingA,zvide);
               			
		});
	</SCRIPT>
</HEAD>

<BODY> 
  <table width=100% height=600>
    <td width=24% >
	<div align=center><b>Ressource</b><iframe id='iframeid' style="width:400px;height:640px;margin:5px"></iframe></div>
    </td>
    <td width=24%>
	<div align=center style="background-color:lightblue"><b>Moteur</b><?php echo "<div style='width:350px;'>$recherches</div>"; ?><p/></div>
	<div align=center><div style="width:300px;margin:5px"><b>Résultats</b><ul id="treeDemo2" class="ztree" style="width:300px;height:500px"></ul></div></div>
    </td>
    <td width=24%>
	<div align=center><div style="width:300px;margin:5px"><b>Panier</b> <button id="tbutton">Sérialize</button><ul id="treeDemo" class="ztree" style="width:300px;height:600px"></ul></div></div>
    </td>
    <td width=24%>
	<div align=center><div style="width:300px;margin:5px"><b>document</b> <button id="tbutton2">Sérialise</button><ul id="treeDemoD" class="ztree" style="width:300px;height:600px"></ul></div></div>
    </td>
</table>
</BODY> 
</HTML>
