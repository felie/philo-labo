<?php

require_once('../../../../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require('../../../../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('../../../../philosophemes/ai.php');
// require ('../../../../philosophemes/philo-labo.php');


$doc=$_GET['doc']; // nom du document passé
$id_membre=simple_query("select id from membres where membre='$Author'");

$pagename="$Author-$doc";
$source=file_get_contents("$pagename.tree");

# transformation du source en un tableau de chaine
$sourcetablo=explode("\n",$source);

$titre=$doc;

array_unshift($sourcetablo,"% $titre","% $LongName","% ");

$source=implode("\n",$sourcetablo);
//echo "$source";

// ici il faut remplacer les ressources par leur valeur pour m2b
// test avec un texte

function getitem4m2b($id, $titre=false,$long=false) // adhoc mais généralisable
    {
    global $Author,$youtube;
    if ($titre)
      $titre="### ".simple_query("select ressource from ressources where id=$id")."\n\n";
    else
      $titre='';
    $server='https://'.$_SERVER['HTTP_HOST']; //"https://philo-labo.fr";
    //echo "Author=$Author";
      $nature=simple_query("select nature from ressources where id=$id");
      switch ($nature){
          case 'image': $url=simple_query('select url from ressources where id='.$id);
                        return "$titre"."imgp($url)\n\n";	
                        break;
          case 'texte':
	    if ($long)	
 		$texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("ext_pdf($id pages=-,nup=1,scale=1<SDL>%s<SDL>>%s *%s*<SDL>)ext<SDL>"),array('%s','%s','%s'));
	    else
                {
            	$texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("t(<SDL>%s<SDL>*%s*%s)t <SDL>"),array('%s','%s','%s'));
                $texte=str_replace('<SDL>',"\n",$texte);
                }
            return $titre.$texte."\n\n";
            break;
       }
   }  

function ressource($num)
	{
	return getitem4m2b($num[2],true); // avec titre systématiquement: ce sont des ressources
	}

function ressource_longue($num)
        {
        return getitem4m2b($num[1],false,true);
        }

$source=preg_replace_callback('/\@f([0-9]*)/',"ressource_longue",$source); // habituellement le texte dans une iframe
$source=preg_replace_callback('/\@([t]*)([0-9]*)/',"ressource",$source);
//$source=preg_replace('/>>tip<<²([^²]*)²([^>]*)>><</','a($1²$2)a²',$source);
//$source=preg_replace('/>>warning<<²([^²]*)²([^>]*)>><</','e($1²$2)e²',$source);
//$source=preg_replace('/>>important<<²([^²]*)²([^>]*)>><</','b($1²$2)b²',$source);
//$source=str_replace('Lorem','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim //ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',$source);
$source=str_replace('AROBASE',"@",$source);

//$source=str_replace('²',"\n",$source);

//echo "*$source*";
//exit;

$m2b="/web/philo-labo/m2b/m2b -c ";

file_put_contents("/web/philo-labo/m2b/$pagename.txt",$source);

if ($f = fopen("/web/philo-labo/m2b/$pagename.txt", "w")) 
  { fputs($f, "$source"); pclose($f); 
    $pgmline="$m2b -I /web/philo-labo $pagename"; // -I <répertoire des images>
    // exec($pgmline,$a,$b);
   exec($pgmline);

  }
//print_r($a);
//print($b);
//exit;

$filename = "/web/philo-labo/m2b/$pagename.pdf";
$content = file_get_contents($filename);
header("Content-Disposition: inline; filename=$pagename.pdf");
header("Content-type: application/pdf");
header('Cache-Control: private, max-age=0, must-revalidate');
header('Pragma: public');
echo $content;
exit;
?>
