<?php    
# Written by scarfboy@gmail.com. 
# Use at your own risk.
# revu par francois@elie.org

$path_to_latex   = '/usr/bin/pdflatex';
$path_to_store = '/web/philo-labo/data/imglatex';
$path_to_url = '/data/imglatex';
$imgfmt="svg"; #literally used in extensions, and in parameters to convert. Should be either png or gif.

# 3 modes
# inline (du latex dans une ligne <ilatex> </ilatex>
# document avec des packages spéciaux <dlatex usepackage{truc}></dlatex> dlatex pour "document" latex
# complete avec son propre préambule qui remplace tout <latex><latex> un document latex complet
# pdf un document latex complet qui renvoie l'image de la couverture avec un lien vers le pdf <latex pdf> </latex>

function traiter_latex($source) // traite le latex dans la chaine $s et la renvoie chargée des résultats
  {
  $source=str_replace("\n",'°°',$source);
  // inline
  $source=preg_replace_callback(
            '/<ilatex>(.*?)<\/ilatex>/',
            function ($matches) 
                {
                return texify(str_replace('°°',"\n",$matches[1]),'inline');
                }
            ,$source); 
   // document avec ou sans préambule additionnel
   $source=preg_replace_callback(
            '/<dlatex([^>]*?)>(.*?)<\/dlatex>/',
            function ($matches) 
                {
                return texify(str_replace('°°',"\n",$matches[2]),'document',str_replace('°°',"\n",$matches[1]));
                }
            ,$source); 
    // latex complet 
    $source=preg_replace_callback(
            '/<latex>(.*?)<\/latex>/',
            function ($matches) 
                {
                return texify(str_replace('°°',"\n",$matches[1]),'latex');
                }
            ,$source); 
    // latex complet avec couverture pointant sur un pdf
    $source=preg_replace_callback(
            '/<platex>(.*?)<\/platex>/',
            function ($matches) 
                {
                return texify(str_replace('°°',"\n",$matches[1]),'platex');
                }
            ,$source); 
    $source=preg_replace_callback(
            '/<lisatex>(.*?)<\/lisatex>/',
            function ($matches) 
                {
                $source=file_get_contents("/web/philo-labo/m2b/lisatex.tex");
                $source=str_replace('CONTENT',str_replace('°°',"\n",$matches[1]),$source);
                return texify($source,'latex');
                }
            ,$source); 
  return str_replace('°°',"\n",$source);
  }

function texify($string,$mode='inline',$extrapreamble='') { 
  global $imgfmt,$path_to_store,$path_to_latex,$path_to_url;
  $dpi='300'; 
  if (($mode=='inline') or ($mode=='document'))
    {
    $documentclass='standalone';
    $totex = "\\documentclass[13pt,varwidth]{".$documentclass."}\n".
	    "\\usepackage{color}\n
	    \\usepackage[utf8x]{inputenc}% pour écrire en utf8 dans les fichiers\n
	    \\usepackage[T1]{fontenc}\n 
	    \\usepackage[frenchb]{babel}\n
	    \\usepackage{pgfplots}\n
	    \\usepgfplotslibrary{patchplots}\n
	    \\usepackage{tikz,stackengine}\n
	    \\usepackage{adjustbox}\n
	    \\usepackage{xcolor}\n
	    \\usepackage{amsmath}\n
	    \\usepackage{amsfonts}\n
	    \\usepackage{amssymb}\n".
	    $extrapreamble."\n".
	    "\\pagestyle{empty}\n".  #removes header/footer; necessary for trim
	    "\\begin{document}\n".
	    "\\color[rgb]{0.0,0.0,0.0}\n".
	    "\\nopagecolor\n".
	    $string."\n".
	    "\\end{document}\n";
     }
  else	
    $totex=$string;
  $hashfn = sha1($totex); //.".".$dpi.".".$fore.".".$back.".".intval($trans);  #file cache entry string:  40-char hash string plus size
    // ça fout la merdre en latex, les noms avec les points... le hash suffira!
  $stralt = str_replace("&","&amp;", str_replace("\n","<br/>",$string)); # stuck in the alt and title attributes

if (file_exists("$path_to_store/$hashfn.$imgfmt")) 
    return phplatex_clean($tfn,$path_to_store).toreturn($mode,"$path_to_url/$hashfn",$stralt);
  
  if (chdir("$path_to_store")===FALSE) { return '[directory access error, fix permissions]'; } 
  
  $tfn = tempnam(getcwd(), 'PTX'); #file in tmp dir
  
  #write temporary .tex file
  if ( ($tex = fopen($tfn.'.tex', "w"))==FALSE) { return '[file access error] '.phplatex_cleantmp($tfn,$path_to_store); }
  fwrite($tex, $totex); 
  fclose($tex);
  
  # Run latex to create a .dvi.  Have it try to fix minor errors instead of breaking/pausing on them.
  exec($path_to_latex.' --interaction=nonstopmode '.$tfn.'.tex');
  if ($mode=='platex')
    exec("$path_to_latex --interaction=nonstopmode $tfn.tex;pdftk $tfn.pdf cat 1 output $tfn.couv.pdf;convert $tfn.couv.pdf -resize 158x223! $tfn.png;rm $tfn.couv.pdf;mv $tfn.pdf $path_to_store/$hashfn.pdf;mv $tfn.png $path_to_store/$hashfn.png"); // deux fois pour les références
  else
    {
    exec("$path_to_latex --interaction=nonstopmode $tfn.tex;pdfcrop --margins '2 2 2 2' $tfn.pdf;mv $tfn-crop.pdf $tfn.pdf");
    exec("pdf2svg $tfn.pdf $tfn.svg");
    if (!file_exists($tfn.'.'.$imgfmt))  
      return '[pdf2svg error] '.phplatex_clean($tfn,$path_to_store)."<pre>".file_get_contents("$tfn.log")."</pre>";
    }
  
  # Copy result image to cache.
  copy($tfn.'.'.$imgfmt, "$path_to_store/$hashfn.$imgfmt");
  
  if (file_exists("$path_to_store/$hashfn.$imgfmt") or file_exists("$path_to_store/$hashfn.png")) 
    return phplatex_clean($tfn,$path_to_store).toreturn($mode,"$path_to_url/$hashfn",'new');
} 

function toreturn($mode,$url,$code,$n='')
  {
  switch ($mode)
    {
    case 'inline': return "$n<img border=0 style='vertical-align:middle' height=21  title='' alt='$code' src='$url.svg'/>";     
    case 'document': 
    case 'latex': return "$n<center><img border=0 title='' alt='' src='$url.svg'/></center>";
    case 'platex': return "$n<center><a href='$url.pdf'><img border=1 title='' alt='$code' src='$url.png'/></a></center>";
    }
  }

function phplatex_clean($tempfname,$todir) {
  #specifically removes the various files that probably got created for a specific run, based on the run's filename.
  return '';
  global $imgfmt;
  if (chdir($todir)===FALSE) { return '[directory access error, fix permissions (and empty tmp manually this time)]'; }
  error_reporting(0); #at least one of these probably will not exist, but disable the error reporting related to that.
  unlink($tempfname);     #the longer/cleaner way would be check for existance for each
  unlink($tempfname.".tex");  unlink($tempfname.".log");
  unlink($tempfname.".aux");  unlink($tempfname.".pdf");
  unlink($tempfname.".".$imgfmt);
  //error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
  #try-catch would have been nice. This is rather overkill too, the way I use it.
  return '';
}

?>
