<?php
require_once('../../../../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require('../../../../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('../../../../philosophemes/ai.php');
require_once('../../../../philosophemes/philo-labo.php'); // fonctions spécifiques à philo-labo
$recherches=sprintf($form_search,'?action=searchengine&json=true');
if ($_GET['action']=='searchengine')
 {
 $str='<?php ?>
 [<?php
 echo \''.str_replace("'","\'",resultatrecherche(true,true,true,$_GET['json'])).'\';
 ?>]';
 file_put_contents('../asyncData/getPhilo.php', $str);
 }
//echo $recherches;
?>
<!DOCTYPE html>
<HTML>
<HEAD>
	<TITLE> ZTREE DEMO - multiTree</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../../../css/demo.css" type="text/css">
	<link rel="stylesheet" href="../../../css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="../../../js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.exedit.js"></script>
	<SCRIPT type="text/javascript">
		<!--
		var setting = {
			async: {
				enable: true,
				url:"../asyncData/getPhilo.php",
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				onClick: myOnClick
			},
		};
                var settingA = {
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                        enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick
                        },
                };

		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
			}
			return childNodes;
		}

                function myOnClick(event, treeId, treeNode) {
                        document.getElementById('iframeid').src="/philosophemes/ressif.php?id="+treeNode.id;
                }

		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return targetNode ? targetNode.drop !== false : true;
		}
		var zvide=[];
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), settingA,zvide);
			$.fn.zTree.init($("#treeDemo2"), setting);
			
		});
		//-->
	</SCRIPT>
</HEAD>

<BODY>
<?php
echo "<div style='float:top;'>$recherches</div>";
?>
<iframe id='iframeid' width=480 height=600></iframe>
    <ul style="float:right;background-color:lightblue;margin:2px;width:400px" id="treeDemo" class="ztree" width="400px"></ul>
    <ul style="float:left;background-color:lightblue;margin:2px;width:400px" id="treeDemo2" class="ztree" width="400px"></ul>
 </table>
</BODY>
</HTML>
