<?php

/* les fonction json de php sont une merde, par exemple on ne peut pas y mettre de : */

function protect_comma($s)  {
  return str_replace(",","&comma;",$s); }

function unprotect_comma($s) {
  return str_replace('&comma;',',',$s); }

function delete_arbor($author,$doc)
  {
  $id_membre=simple_query("select id from membres where membre='$author'");
  $doc=str_replace("'","\'",$doc);
  $doc=str_replace('"',"\"",$doc);
  simple_query("delete from arbors where id_membre=$id_membre and document='$doc'");
  echo "document supprimé de la base de données";
  }

function json_decode_nice($json, $assoc = true)
  {
  global $envdoc;
  //echo "<br/>DEBUT json=$json<hr/>";
  $json = str_replace($envdoc,"",$json);
  $json = str_replace(array("\n","\r"),"\\n",$json);
  $json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
  $json = preg_replace('/(,)\s*}$/','}',$json);
  $json = preg_replace("/\\\'/","'",$json);
  //$json=addslashes($json);
  //echo "<br/>AVANT json=$json<hr/>";
  $json=str_replace('\\', '\\\\',$json); // pour le backslash
  //$json=str_replace('&mdash;','—',$json);
  // voir https://stackoverflow.com/questions/32056940/how-to-deal-with-backslashes-in-json-strings-php
  //echo serialize($json);
  $json=json_decode($json,$assoc,20);
  
  return $json;
  }

function json2sql($json,$Author,$doc) // on récupére le json de ztree et on le met en base de donnée
  {
  //echo "dans jsqon2sql=$json"; //ok
    file_put_contents("/web/philo-labo/users/$Author/compositeur/mouchard3",serialize($json));
    $json="{arbor:$json}";
    $b=json_decode_nice($json);
    $id_membre=simple_query("select id from membres where membre='$Author'");
    $doc=str_replace("'","\'",$doc);
    simple_query("delete from arbors where id_membre=$id_membre and document='$doc'");
    $tab=array();
    $b=$b['arbor'];
    if (sizeof($b)>0)
        foreach ($b as $l) 
                { 
                $l['name']=str_replace("  ","→",$l['name']);
                $tab[]=array($l['id'],$l['pId'],$l['name'],$l['icon'],$id_membre,$doc);
                } 
    array2sql('arbors',array('ego','father','name','category','id_membre','document'),$tab,'insert'); // mise dans la base
  return $b;
  }
  
function sql2json($author,$doc)
  {
  $id_membre=simple_query("select id from membres where membre='$author'");
  $doc=str_replace("'","\'",$doc);
  $nbr=simple_query("Select count(*) from arbors where id_membre='$id_membre' and document='$doc'");
  if ($nbr>0)
    {
    //file_put_contents('/web/philo-labo/users/felie/compositeur/pandoc/sql',simple_query("select name from arbors where id_membre='$id_membre' and document='$doc'"));
    $sql="Select ego,father,name,category from arbors where id_membre='$id_membre' and document='$doc'"; // le "S"elec t pour éviter la jointure automatique de id_ressource 
    return str_replace(array("{},","'","→",'$'),array("","\'","→",'°°'),select2html($sql,'h',array('"id":%s','"pId":%s','"name":"%s"','"icon":"%s"'),array("[\n%s\n]",'{%s},','%s,')));
    }
  else
    return '{}';
  }
  
function json2arbor($b)
  {
  $plan='';
  if (sizeof($b)>0)
    foreach ($b as $l) // construction du plan des ressources, pour diaporama ou beamer
        {
        $category=str_replace('/images/iconp/','',$l['icon']);
        $category=str_replace('.png','',$category);
        if (is_numeric($l['name'][0])) // ressource
            $name="[$category]@".$l['name'];//$name="@".explode(' ',trim($l['name']))[0]; // on ne prend que le nombre et c'est une ressource
        else
        if (($l['level']>=0) and ($l['icon']=='/images/iconp/puce.png')) // si c'est un niveau de plan
            $name=str_repeat('#',$l['level']+1).' '.$l['name']; 
        else
            $name=$l['name']; // level = -1 pour les contenu
        $plan.=$name."\n";
        }
  $plan=str_replace("→","  ",$plan); // pour les sous-items d'une liste
  $plan=str_replace("\n\n","\n",$plan);
  //$plan=str_replace('\\','\\\\',$plan);
  $plan=str_replace("&comma;",",",$plan);
  return $plan;
  }

?>
