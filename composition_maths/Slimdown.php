<?php
/**
 * Slimdown - A very basic regex-based Markdown parser. Supports the
 * following elements (and can be extended via Slimdown::add_rule()):
 *
 * - Headers
 * - Links
 * - Bold
 * - Emphasis
 * - Deletions
 * - Quotes
 * - Inline code
 * - Blockquotes
 * - Ordered/unordered lists
 * - Horizontal rules
 *
 * Author: Johnny Broadway <johnny@johnnybroadway.com>
 * Website: https://gist.github.com/jbroadway/2836900
 * License: MIT
 */
 
class Beamer { 
        public static $rules = array (
	/*	'/(#+)(.*)/' => 'self::header',                           // headers
		'/\[([^\[]+)\]\(([^\)]+)\)/' => '<a href=\'\2\'>\1</a>',  // links      */
		'/<ul>/' => '\begin{itemize}',
		'/<\/ul>/' => '\end{itemize}',
		'/<ol>/' => '\begin{enumerate}',
		'/<\/ol>/' => '\end{enumerate}',
		'/<li>/' => '\item[$\bullet$] ',
		'/<\/li>/' => '',
		'/oe/' => '\oe{}',
		'/Oe/' => '\OE{}',
		'/(\*\*|__)(.*?)\1/' => '\textbf{\2}',                    // bold
		'/(\*)(.*?)\1/' => '\textit{\2}',                         // emphasis
		'/<b>(.*)<\/b>/' => '\textbf{\1}',
		'/<strong>(.*)<\/strong>/' => '\textbf{\1}',
		'/<i>(.*)<\/i>/' => '\textit{\1}',
		'/<em>(.*)<\/em>/' => '\textit{\1}',                      // pour Apple
		'/<a href=\'ressif([^\']*)\'>([^<]*)<\/a>/' => '\href{\detokenize{ressif\1}}{\2}',
		'/⏹/' => '\pause ', 
                '/<span class=\'sidenoteleft\'>([^<]*)<\/span>/' => ' \noted{\1} ', // note marginale droite
                '/<span class=\'sidenoteright\'>([^<]*)<\/span>/' => '\noteg{\1}',
                '/<span style=\'background-color: rgb\(([^,]*), ([^,]*), ([^\)]*)\);\'>([^>]*)<\/span>/' => '\hlrgb{\1}{\2}{\3}{\4}', // pour toutes les couleurs en rgb
                '/<latex>/' => '  \nolinenumbers ',
                '/<dlatex>/' => '  \nolinenumbers ',
                '/<ilatex>/' => '',
                '/<platex>/' => '',
                '/<\/latex>/' => '', // on vite les balises latex qui sont pour le html
                '/<\/ilatex>/' => '',
                '/<\/dlatex>/' => '',
                '/<\/platex>/' => '',
                '/&nbsp;/' => '~',
                '/<span class=\'interpolation\'>([^<]*)<\/span>/' => '\interpolation{\1}',
                //'/<div>/' => "\par ",
                //'/<\/div>/' => '',
                //'/<span style="background-color: rgb\((.*)\);">([^>]*)<\/span>/' => '**\2** de couleur \1**', // pour toutes les couleurs en rgb
                                       
	/*	'/(\*|_)(.*?)\1/' => '\textit{\2}',                       // emphasis
                '/\~\~(.*?)\~\~/' => '<del>\1</del>',                      // del
		'/\:\"(.*?)\"\:/' => '<q>\1</q>',                         // quote
		'/`(.*?)`/' => '<code>\1</code>',                         // inline code
		'/\n\*(.*)/' => 'self::ul_list',                          // ul lists
		'/\n[0-9]+\.(.*)/' => 'self::ol_list',                    // ol lists
		'/\n(&gt;|\>)(.*)/' => 'self::blockquote ',               // blockquotes
		'/\n-{5,}/' => "\n<hr />",                                // horizontal rule
		'/\n([^\n]+)\n/' => 'self::para',                         // add paragraphs
		'/<\/ul>\s?<ul>/' => '',                                  // fix extra ul
		'/<\/ol>\s?<ol>/' => '',                                  // fix extra ol
		'/<\/blockquote><blockquote>/' => "\n"           */         // fix extra blockquote 
	);
	/**
	 * Render some Markdown into Beamer.
	 */
	public static function render ($text) {
		$text = "\n" . $text . "\n";
		$text=str_replace("\n","²²²²",$text); // pour de faire qu'une ligne FE
		foreach (self::$rules as $regex => $replacement) {
			if (is_callable ( $replacement)) {
				$text = preg_replace_callback ($regex, $replacement, $text);
			} else {
				$text = preg_replace ($regex, $replacement, $text);
			}
		}
		$text=str_replace("²²²²","\n",$text); // pour de faire qu'une ligne FE
		return trim ($text);
	}
}

class Slimdown {
	public static $rules = array (
		//'/(#+)(.*)/' => 'self::header',                           // headers (bidouille! FE)
		'/\[([^\[]+)\]\(([^\)]+)\)/' => '<a href=\'\2\'>\1</a>',  // links
		'/(\*\*|__)(.*?)\1/' => '<b>\2</b>',                      // bold
		'/(\*|_)(.*?)\1/' => '<em>\2</em>',                       // emphasis
		'/\~\~(.*?)\~\~/' => '<del>\1</del>',                     // del
		'/\:\"(.*?)\"\:/' => '<q>\1</q>',                         // quote
		'/`(.*?)`/' => '<code>\1</code>',                         // inline code
		'/\n\*(.*)/' => 'self::ul_list',                          // ul lists
		'/\n[0-9]+\.(.*)/' => 'self::ol_list',                    // ol lists
		'/\n(&gt;|\>)(.*)/' => 'self::blockquote ',               // blockquotes
		'/\n-{5,}/' => "\n<hr />",                                // horizontal rule
		'/\n([^\n]+)\n/' => 'self::para',                         // add paragraphs
		'/<\/ul>\s?<ul>/' => '',                                  // fix extra ul
		'/<\/ol>\s?<ol>/' => '',                                  // fix extra ol
		'/<\/blockquote><blockquote>/' => "\n",                    // fix extra blockquote
		//'/⏹/' => '',                                              // pause (pas de pause en html) 
	);
	private static function para ($regs) {
		$line = $regs[1];
		$trimmed = trim ($line);
		if (preg_match ('/^<\/?(ul|ol|li|h|p|bl)/', $trimmed)) {
			return "\n" . $line . "\n";
		}
		return sprintf ("\n<p>%s</p>\n", $trimmed);
	}
	private static function ul_list ($regs) {
		$item = $regs[1];
		return sprintf ("\n<ul>\n\t<li>%s</li>\n</ul>", trim ($item));
	}
	private static function ol_list ($regs) {
		$item = $regs[1];
		return sprintf ("\n<ol>\n\t<li>%s</li>\n</ol>", trim ($item));
	}
	private static function blockquote ($regs) {
		$item = $regs[2];
		return sprintf ("\n<blockquote>%s</blockquote>", trim ($item));
	}
	private static function header ($regs) {
		list ($tmp, $chars, $header) = $regs;
		$level = strlen ($chars);
		return sprintf ('<h%d>%s</h%d>', $level, trim ($header), $level);
	}
	/**
	 * Add a rule.
	 */
	public static function add_rule ($regex, $replacement) {
		self::$rules[$regex] = $replacement;
	}
	/**
	 * Render some Markdown into HTML.
	 */
	public static function render ($text) {
		$text = "\n" . $text . "\n";
		$text=str_replace("\n","²²²²",$text); // pour de faire qu'une ligne FE
		foreach (self::$rules as $regex => $replacement) {
			if (is_callable ($replacement)) {
				$text = preg_replace_callback ($regex, $replacement, $text);
			} else {
				$text = preg_replace ($regex, $replacement, $text);
			}
		}
		$text=str_replace("²²²²","\n",$text); // pour de faire qu'une ligne FE
		return trim ($text);
	}
}
