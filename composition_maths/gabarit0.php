<?php
/* sujets de philos en pdf */

require_once('../philosophemes/secure.php');
require_once('ressources.php');
require_once('version.php');

$s1=item_wiki($_GET['s1']); // en mode relax
$s2=item_wiki($_GET['s2']);
$s3=item_poly($_GET['s3']);

// via pandoc avec sprintf qui passe les paramètres

/*$gabarit='
\documentclass[a4paper,12pt]{article}
\usepackage[left=1.5cm,right=1.5cm,top=1.5cm,bottom=1.5cm]{geometry}
\usepackage[utf8x]{inputenc} 
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{lmodern}
\usepackage{times}
\usepackage{lmodern}
\usepackage{times}
\usepackage{graphicx}
\pagestyle{empty} */
$gabarit='
\documentclass[a4paper,12pt,oneside]{memoir}
\usepackage[left=2.5cm,right=2.5cm,top=1.5cm,bottom=1.5cm]{geometry}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{lmodern}
\usepackage{times}
\usepackage{graphicx}
\usepackage{moresize}
\usepackage[svgnames]{xcolor}
\usepackage{lipsum}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{soulutf8} 
\renewcommand{\underline}[1]{\ul{#1}}
\usepackage{etoolbox}
\usepackage{adjustbox}
\usepackage{microtype}
\usepackage{tikz,stackengine}	

\usepackage{pgfplots}
\usepgfplotslibrary{patchplots}
\usepackage{amsfonts}

\def\asterism{\par\vspace{1em}{\centering\scalebox{1.5}{
  \stackon[-0.5pt]{\bfseries*~*}{\bfseries*}}\par}\vspace{.5em}\par}
\hyphenpenalty=1000
\usepackage{nopageno}
\setlength{\textwidth}{16cm}


\begin{document}
\begin{center}
{\LARGE PHILOSOPHIE}

\vspace{0.5cm}
{\large au choix}
\end{center}

\vspace{1.5cm}
\vfill
{\large 1.~~~ %s}
\vspace{1cm}
\vfill

{\large 2.~~~ %s}
\vspace{1cm}
\vfill

{\large 3.~~~ \emph{Expliquez le texte suivant}~:
\vspace{0.5cm}

%s}
\vfill
\emph{\large La connaissance de la doctrine de l\'auteur n\'est pas requise. Il faut et il suffit que l\'explication rende compte, par la compréhension précise du texte, du problème dont il est question.}
\end{document}
';

$gabarit=sprintf($gabarit,$s1,$s2,$s3);
$gabarit=str_replace('-2cm','-0.7cm',$gabarit); // bidouille

$repuser="/web/philo-labo/users/$Author/compositeur/tmp";
file_put_contents("$repuser/gabarit.tex",$gabarit);
exec("cd $repuser;pdflatex -interaction=nonstopmode gabarit");
propulsepdf("$repuser/gabarit.pdf","sujets");

?>
