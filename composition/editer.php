<?php
define('ai_standalone',1);
require('/web/philo-labo/philosophemes/secure.php');
require_once('/web/philo-labo/philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('/web/philo-labo/philosophemes/ai.php');
//require_once('/web/philo-labo/local/logs.php');

simple_query("update ressources set ladate=".time()." where id=$_GET[id]"); // indique la date à laquelle l'enregistrement a été mis à jour
//$content=update2html("select * from ressources where id=$_GET[id]",'v',array(),array(),array(),"ressif.php?id=$_GET[id]");//,$_SERVER[HTTP_REFERER]);
//require('/web/philo-labo/philosophemes/ai_avsts2.php');
//writelog("edit\t$Author\t$_GET[id]\t".simple_query("select nature from ressources where id=$_GET[id]")."\t".simple_query("select ressource from ressources where id=$_GET[id]"));

$nature=simple_query("select nature from ressources where id=$_GET[id]");
switch ($nature)
    {
    case 'sujet-texte':
    case 'texte':
        
        if (!isset($_GET[brut]))
            {
            $goodies="En travaux... Une limitation: les marges droites en surcharge de coloriage n'apparaissent pas en pdf.";
        $result=ai_bloc("update
select ressource,texte,reference,tags,id_auteur,disciplines from ressources where id=$_GET[id]
v
s,A,s","ressif.php?id=$_GET[id]");
            }
    else 
            {
         $goodies="Attention à ce que vous faites à la main...<br/>*italique* et **gras** \\\\ pour un saut de ligne forcé";
         $result=ai_bloc("update
select ressource,texte,reference,tags,id_auteur,disciplines from ressources where id=$_GET[id]
v
s,B,s","ressif.php?id=$_GET[id]");
            }
    break;
    case 'graphique':
    case 'graphe':
    $result=ai_bloc("update
select ressource,texte,id_auteur from ressources where id=$_GET[id]
v
s,B","ressif.php?id=$_GET[id]");
    break;
    default:
        $result=update2html("select * from ressources where id=$_GET[id]",'v',array(),array(),array(),"ressif.php?id=$_GET[id]");//,$_SERVER[HTTP_REFERER]);
    
    }
    
echo "<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='stylesheet' href='https://philo-labo.fr/pub/skins/ians/ai.css' type='text/css' />
<script src='/js/jquery.min.js'></script>
<link rel='stylesheet' href='/css/gre.css'>
<link rel='stylesheet' href='/css/textes.css'>
<script src='/js/jquery.gre.fe.js'></script>
</head>
<body>
<script>$(document).ready(function(){ $('.gre').gre(); });</script>
<h1>Correction de la ressource <i>$_GET[id]</i> de type <i>$nature</i></h1>$goodies
$result
</body>
</html>";
?>
