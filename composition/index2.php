<?php
// id_content pour charger une fenetre modale (pour l'ardoise de plan)

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ini_set("display_errors", 1);

require_once('timer.php');
require_once('../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once ('../philosophemes/ai.php');
require_once('philo-labo2.php'); // fonctions spécifiques à philo-labo
require_once('../cookbook/jquerychat.php');
//require_once('phplatex.php');

$repuser="/web/philo-labo/users/$Author/compositeur";
if (!file_exists($repuser))
  mkdir($repuser); // crée le répertoire compositeur s'il n'existe pas
if (!file_exists("$repuser/docs"))
  mkdir("$repuser/docs"); // crée le répertoire pour ranger les docs et leur couverture s'il n'existe pas
  
//echo $Author;
$LongName=file_get_contents("/web/philo-labo/users/$Author/longname");

// formulaire de recherche mais en ajax pour chargement dans l'arbre sans rechargement

if (isset($_POST['chaine']))
    file_put_contents("$repuser/search",$_POST['chaine']); // si la page est rechargée, la valeur sera rechargée
$recherche_precedente=file_get_contents("$repuser/search");

    // enregistre le résultats de la recherche en json
//if ($_GET['action']=='searchengine')
// file_put_contents('resultat.json',"[\n".str_replace("'","\'",resultatrecherche(true,true,true,$_GET['json']))."\n]");
if ($_POST['action']=='searchengine')
 file_put_contents("$repuser/resultat.json","[\n".str_replace(array("'",'&mdash;'),array("\'",'—'),resultatrecherche(true,true,true,$_POST['json']))."\n]"); // get json
 
if (!isset($_GET[doc]))
  {
  $doc=file_get_contents("$repuser/lastdoc"); // pour rechargement du document après l'exécutio du moteur de recherche.
  if ($doc=='')
    $doc='panier';
  }
else
  $doc=$_GET[doc];
$_GET[doc]=$doc; // indispensable pour récupérer le nom du document actuel pour l'afficher dans la boite doc

require('icon_partage.php'); // recupére la valeur de l'icone de partage (partager ou ne pas partager selon l'état du document actuel

$partage_actuel=file_get_contents("$repuser/partage_actuel");
$_GET['partage_actuel']=$partage_actuel;

/*$maj_button='<div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Enregistrer" 
    data-text="Mettre à jour le document. Lorsque vous l\'avez modifié à la souris (en glisser-déposer) il faut indiquer que vous confirmez les modifications en mettant à jour le document.">
    <img width=24 id="maj" alt="icone mise à jour" src="/images/menucompo/update2.png"/>
</div>';*/
$edit_button='<div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="editer(\'doc\');"
    data-title="Éditer" 
    data-text="Ouvre le document pour le modifier à la main. On peut facilement mettre des titres et des sous-titres.">
    <img width=24 alt="icone nouveau" src="/images/menucompo/edit.png"/>
</div>';
$delete_button='<div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Supprime le document" 
    data-text="Une confirmation va vous être demandée, mais attention...à manipuler avec précaution pour éviter les moments de solitude.">
    <img width=24 id="delete" alt="icone nouveau" src="/images/menucompo/delete.png"/>
</div>';
$sharing_button='<div class="skinnytip" id="icon_partage" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Partage/Cesse le partage" 
    data-text="Bascule. Permet de partager un document, ou cesser de le faire.">'.$icon_partage.'
</div>';
$creation_button='
<div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Créer un nouveau document" 
    data-text="Vous pouvez créer ici plusieurs documents vides">
    <img width=20 id="create" alt="icone nouveau" src="/images/menucompo/new.png"/>
</div>';
$save_as_button='
<div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Enregistrer sous"
    data-text="Enregistrer sous un autre nom, qui sera rechargé ici">
    <img width=20 id="enregistrer_sous" alt="icone copier" src="/images/copy.png"/>
</div>';
$outline_button='
<div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_sans_popup(\'doc\',\'outline\');"
    data-title="Export du plan de l\'arbre"
    data-text="Exporte le plan de l\'arbre en pdf, avec juste le titre des ressources">
    <img height=24 alt="icone poly" src="/images/menucompo/outline.png"/>
</div>';
$beamer_button='
<div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF"  onclick="export_sans_popup(\'doc\',\'beamer\');"
    data-title="Diaporama en pdf"
    data-text="Le diaporama peut prendre un peu de temps si les textes sont nouveaux.<br/>Il sera produit très vite si les textes ont déjà été traités.">
    <img height=24 alt="icone beamer" src="/images/menucompo/beamer.png"/>
</div>';
$poly_button='
<div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF"  onclick="export_sans_popup(\'doc\',\'synchro3\');"
    data-title="Extraire un polycopié"
    data-text="Extraire les textes et les images dans un polycopié en pdf. Les lignes sont les mêmes que dans le diaporama: très pratiques pour les étudiants qui ont une mémoire visuelle."    >
    <img height=24 alt="icone poly" src="/images/menucompo/poly.png"/>
</div>';
$S5_button='
<div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_sans_popup(\'doc\',\'S5\');"
    data-title="Diaporama s5"
    data-text="Produit un diaporama jouable dans le navigateur, avec les textes découpés si nécessaires en plusieurs diapos, gère les textes, les images et les videos youtube pour l\'instant">
    <img height=24 alt="icone poly" src="/images/presentationp.png"/>
</div>';
$html5_button='
<div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_sans_popup(\'doc\',\'html5\');"
    data-title="Export en html pour les traitements de textes"
    data-text="Produit un export en html5, que vous pouvez copier dans votre traitement de texte préféré, qui est, j\'en suis sûr, Libreoffice">
    <img height=24 alt="icone html5" src="/images/menucompo/html5.png"/>
</div>';
$markdown_button='
<div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_avec_popup(\'doc\',\'markdown_joli\');"
    data-title="Export en markdown pour les amateurs"
    data-text="Si vous ne connaissez pas le format markdown, vous devriez jeter un coup d\oeil au concept, et au passage, à pandoc, un couteau suisse surpuissant de conversion entre des formats de documents, écrit par John McFarlane, qui est professeur de philosophie à Berkeley">
    <img height=24 alt="icone markdown" src="/images/menucompo/markdown.png"/>
</div>';
$brut_button='
<div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_avec_popup(\'doc\',\'brut\');"
    data-title="Export brut"
    data-text="Exporte de façon à ce que vous puissiez le copier et le coller dans un autre document.">
    <img height=16 alt="icone export brut" src="/images/menucompo/brut.png"/>
    </div>';
$evolution_button='
    <div class="skinnytip" style="height:21px;vertical-align:middle;float:right;display:inline;margin-left:0px;margin-right:2px" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="popupf(\'https://philo-labo.fr/?n=Ian.Evolution?skin=minimal\')"
    data-title="Gestion des évolutions du compositeur"
    data-text="Demandes d\'évolutions.">
    <img height=21 alt="icone gabarits" src="/images/menucompo/gabaritwhite.png"/>
    </div>';
$sujet_form="<div class=\"skinnytip\" style=\"float:top;display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"
                        data-title=\"Production d'un sujet\"
                        data-text=\"Deux numéros de questions et le numéro d'un texte, et cela vous produit le pdf\">
                        <form method=\"get\" target=\"_blank\" action=\"https://philo-labo.fr/composition/gabarit0.php\"><input name=\"s1\" type=\"text\" size=1><input name=\"s2\" type=\"text\" size=1><input name=\"s3\" type=\"text\" size=1><input type=\"submit\" value=\"sujet\"></form>
                </div>";
    
$menus_complets=preference('Interface avec toutes les icones');

if ($menus_complets)
    $exports="$sujet_form$beamer_button$poly_button$outline_button$S5_button$html5_button$markdown_button$brut_button";
else
    $exports="$beamer_button$poly_button$S5_button";

// pour le formulaire de choix du document - le submit du formulaire est bloqué par event.prevent.Default();
$selection_document="<div style='float:right;margin-right:5px'>$exports</div>
<div style='float:left;margin-left:5px'>$creation_button$save_as_button$edit_button$delete_button$sharing_button</div>
    <form style='display:inline;' id='selection_document'>
&nbsp;<div style='display:inline' id='list_box'></div>
<input type='hidden' id='user_name' value='".$Author."'/>
<input type='hidden' id='LongName' value='".$LongName."'/>
<input type='hidden' id='partage_actuel' value='".$partage_actuel."'/></form>
"    ; // il y avait ici le bouton de mise à jour

//$selpartage="<form id='selection_partage'><div id='list_partage'>$selecteurpartages</div></form>";
$selpartage="<form id='selection_partage'><div style='display:inline' id='list_partage'></div></form>";

// le submit du formulaire est bloqué par event.prevent.Default() voir plus bas



?>
<!DOCTYPE html>
<HTML lang="fr">
<HEAD>
	<TITLE>Le compositeur de philo-labo</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="/ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<link rel="stylesheet" href="/js/jqmodal/jqModal.css" type="text/css">
	<link rel="stylesheet" href="/css/textes.css" type="text/css" />
	<link rel="stylesheet" href="/css/compositeur.css" type="text/css">
	<script src="/js/xnotify.js" ></script>
	
<!-- pour les popup en alink -->
<?php
echo "
<script src='/js/jquery-1.6.4.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
<script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    width:930px;border-width:3px;border-radius: 10px;
    border:3px solid black;
    }
 .ui-dialog-shadow { 
/*-webkit-box-shadow: 6px 6px 6px 0 #000000;
box-shadow: 6px 6px 6px 0 #000000;*/
 }
  .ui-dialog-titlebar {
  height:128;
  background:white;
  border:0;
  }
.ui-dialog-titlebar-close { 
  background: url('/js/images/ui-icons_888888_256x240.png') repeat scroll -93px -128px rgba(1, 2, 0, 0);
  //background: url('/images/close.png') repeat scroll -100px -200px rgba(1, 2, 0, 0);
}
  </style>
	<script>
	jq132(document).ready(function() {
               jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div>Patientez un instant...</div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('alt'),
					width: 930,
					height:600
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href')+'');
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";
?>

<?php echo $HTMLHeaderFmt['chat'];?>
	<!--<script src="/ztree/js/jquery-1.4.4.min.js"></script>-->
	<script src="/js/jquery-1.8.0.min.js"></script>
        <script src="/js/jqmodal/jqModal.js"></script>
		<!--<script src="/js/jqmodal/jqDnRfe.js"></script>-->
	<script src="/ztree/js/jquery.ztree.core.js"></script>
	<script src="/ztree/js/jquery.ztree.excheck.js"></script>
	<script src="/ztree/js/jquery.ztree.exedit.js"></script>
	<script src="dragdroptouch.min.js"></script>
	<script src="/ai/multiselect2/src/jquery.tree-multiselect.js"></script>
	<script src="/js/skinnytip.js"></script>
	        
    <link rel="stylesheet" href="/ai/multiselect2/dist/jquery.tree-multiselect.min.css"> 
	<script>
	 function gabarits(){
            popupf("gabarits.php");
            }
	 function functionOK(){
                alert('Utilisez le bouton OK pour soumettre votre requête au moteur de recherche du compositeur');
                }
        function formrecherchesubmit(){
                 $.post('resultats_recherche.php', // Un script PHP que l'on va créer juste après
                    {
                    chaine : $('#chaine').val(),  // Nous récupérons la valeur de nos inputs que l'on fait passer à connexion.php
                    filtre : $('#test-select').val()
                    },
                    function(data){ // Cette fonction ne fait rien encore, nous la mettrons à jour plus tard
                        if(data == 'Success'){
                            $.fn.zTree.init($("#recherche"), settingRecherche);
                            }
                        },
                    'text' // Nous souhaitons recevoir "Success" ou "Failed", donc on indique text !
                    );
                }
            $('#submit_recherche').click(function(){ // fonction de recherche
                formrecherchesubmit();
                });
	$(document).ready(function(){ 
            SkinnyTip.init();
            function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                break;
                    }
                }
            }
            // chargement initial de la list_box
            var username = document.getElementById("user_name").value;
            var doc = document.getElementById("selection_document").elements[0].value;
            $('#list_box').load('lb.php?user='+username); 
            $("#delete").click(function(){
              var doc = document.getElementById("selection_document").elements[0].value;
              var username = document.getElementById("user_name").value;
              if (confirm('Confirmez-vous la destruction de '+doc+' ?')) {
                 xnotify({body:'Effacement du fichier '+doc});
                 $.get("delete.php?doc="+doc,
                  {doc:doc,}, 
                  function(data,status)
                    {
                    xnotify({body:data,duration:1});
                    $('#list_box').load('lb.php?user='+username); // mise à jour de la list_box // inutile puisqu'on recharge
                    window.location.reload();
                    });
                 }
              });
            // chargement de la liste déroulante des documents partagés
            $("#list_partage").load('lb_partage.php');
            $("#create").click(function(){
                var doc=prompt('Nom du fichier à créer:','');
                if ((doc == '') || (doc.includes('"')) || (doc.includes("'")))
                    {
                    alert('Nom incorrect, vide ou contenant "');
                    //xnotify({body:'Nom incorrect, vide ou contenant "'});
                    exit();
                    }
                doc=doc.replace("'","’"); // guillements exotiques bof bof
                var str='{id:"doc_1",pId:"null",name:"pensez à sauver votre document régulièrement",icon:"/images/iconp/vide.png",level:"0"}';
                var tempResult = [];
                tempResult.push(str);
                $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                from:'create',
                }, 
                function(data,status)
                    {
                    xnotify({body:'creation du document '+doc,duration:1});
                    $('#list_box').load('lb.php?from=create&doc='+"doc");  // mise à jour de la liste de documents - c'est mieux
                    //xnotify({body:'après la creation du document '+doc,duration:1});
                    });
            });
            $("#enregistrer_sous").click(function(){
	      var doc = prompt('Enregistrer le document courant sous:','');
	      doc=doc.replace("'","’");
               if ((doc == '') || (doc.includes('"')))
                {
                alert('Nom incorrect, vide ou contenant "');
                exit();
                }
              xnotify({body:'Enregistrer sous le nom '+doc});
              if ($.fn.zTree.getZTreeObj("doc").getNodeByTId('doc_1') == null)
                {
                xnotify({body:'Création impossible: arbre vide'});
                exit();
                }
              var username = document.getElementById("user_name").value;
              var zTree = $.fn.zTree.getZTreeObj("doc");
              nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray (nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
              //alert(tempResult);
              $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                }, 
                function(data,status)
                {
                xnotify({body:'Le fichier '+doc+' a été créé',duration:1});
                //alert('le fichier '+doc+' a été créé');
                $('#list_box').load('lb.php?doc='+encodeURIComponent(doc)); // mise à jour de la listbox des documents
                });
              //alert('le fichier '+doc+' a été créé');
              //$('#list_box').load('lb.php?doc='+doc); // mise à jour de la listbox des documents
            });
           function maj_doc(){
              //alert('maj_doc');
              var doc = document.getElementById("selection_document").elements[0].value;
              var zTree = $.fn.zTree.getZTreeObj("doc");
              var nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray(nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
              $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                },           
                function(data,status){
                xnotify({body:'sauvegarde du document '+doc,duration:1});
              });
              // $.fn.zTree.init($("#doc"), settingDocument,zvide); 
            };
// désormais inutile
//             $(document).ready(function(){
//                 setInterval(maj_doc, 60000); // sauvegarde toutes les minutes
//             });
           $("#maj").click(maj_doc);
           function maj_panier(){
              //alert('maj_panier');
              var zTree = $.fn.zTree.getZTreeObj("panier");
	      var nodes = zTree.getNodes();
	      var nodes_array = zTree.transformToArray(nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
	      $.post("save_doc.php",
        	{
		arbre:tempResult,
		doc:'panier',
        	},              
        	function(data,status){
            	xnotify({body:'sauvegarde du panier',duration:1});
              });
           };
           $("#maj_panier").click(maj_panier);
           $('#list_box').change(function(){ // changement dans la seléction du document
	                var doc = document.getElementById("selection_document").elements[0].value; // récupération du nom du document à charger
                        var settingChargement = {
                        async: {
                                enable: true,
                                type:"get",             
                                url:"read_doc.php?doc="+doc,  // le script php produit un json à manger par le javascript
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest"},
                                dataFilter: filter
                        },
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick,
                                onRightClick: OnRightClick,
                        },
                };
                $.fn.zTree.destroy("doc"); // on le détruit
                $.fn.zTree.init($("#doc"), settingChargement); // on le recrée avec la nouvelles données
                sleep(100);
                $("#icon_partage").load('icon_partage.php?doc='+doc+'&appelajax=ok');
            });
            $('#list_partage').change(function(){ // changement dans la seléction du document
	                var doc = document.getElementById("selection_partage").elements[0].value; // récupération du nom du document à charger
                        var settingChargement = {
                        async: {
                                enable: true,
                                type:"get",             
                                url:"read_doc.php?doc="+encodeURIComponent(doc)+"&share=1",  // le script php produit un json à manger par le javascript
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest"},
                                dataFilter: filter
                        },
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick,
                                onRightClick: OnRightClick,
                        },
                };
            $.fn.zTree.destroy("partage"); // on le détruit
            $.fn.zTree.init($("#partage"), settingChargement); // on le recrée avec la nouvelles données
           });
          });
        var settingRecherche = {
			async: {     
				enable: true,
				type:"get",		
				url:"read_json.php?json=resultat.json",
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				simpleData: {
				enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				onDrop: onDrop,
				onClick: myOnClick
			},
		};
		var settingShare = {
			async: {
				enable: true,
				type:"get",		
				url:"read_doc.php?share=1&doc="+'<?php echo $_GET['partage_actuel'];?>',
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				simpleData: {
				enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				onDrop: onDrop,
				onClick: myOnClick
			},
		};
                var settingPanier = {
                        async: {
                                enable: true, 
                                type:"get",             
                                url:"read_doc.php?doc=panier",
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest"},
                                dataFilter: filter
                        },
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick,
                                onDrop: onDrop,
                                onRightClick: OnRightClick,
                        },
                        view: {
				dblClickExpand: false,
				fontCss:{color:"red"},
			},
                        
                };
                var settingDocument = {
                        async: {
                                enable: true, 
                                type:"get",             
                                url:"read_doc.php?doc="+'<?php echo $_GET[doc];?>',
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest"},
                                dataFilter: filter
                        },
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick,
                                onDrop: onDrop,
                                onRightClick: OnRightClick,
                        },
                        view: {
                                dblClickExpand: false,
                        },
                };
		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		                childNodes[i].open = true; // pour expand par defaut
			}
			return childNodes;
		}
        function onDrop(event,treeId,treeNodes,x,y,z) // reprise du code de la function maj_doc
            { 
             var doc = document.getElementById("selection_document").elements[0].value;
              var zTree = $.fn.zTree.getZTreeObj("doc");
              var nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray(nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
              $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                },           
                function(data,status){
                xnotify({body:'mise à jour du document '+doc,duration:1});
              });
            }
		function isNumeric(n) {
 			 return (!isNaN(parseFloat(n)) && isFinite(n));
		}
                function myOnClick(event, treeId, treeNode) { // affichage d'une resources dans une fenêtre modale centrale, que l'on peut refermer avec ESC
			var n = treeNode.name.split(' ');
			if (isNumeric(n[0])){ // si le nom commence par un nombre (numéro de ressource)
				var url="ressif.php?id="+treeNode.name; // treeNode.name (le id qui commence le name sera extrait au début de ressif.php
				popupf(url);
				//document.getElementById('contentid').src=url;
                        	//$('#dialog').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
			}
                }

		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return targetNode ? targetNode.drop !== false : true;
		}
		function OnRightClick(event, treeId, treeNode) {
                        var zTree = $.fn.zTree.getZTreeObj(treeId);
                        if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
                                //zTree.cancelSelectedNode();
                                showRMenu(treeId,"root", event.clientX, event.clientY);
                        } else if (treeNode && !treeNode.noR) {
                                zTree.selectNode(treeNode);
                                showRMenu(treeId,"node", event.clientX, event.clientY);
                        }
		}
		function showRMenu(treeId,type, x, y) {
                        if (treeId=='panier') 
                          {
                          $("#rMenu ul").show();
                          if (type=="root") {
                                  $("#m_del").hide();
                                  $("#m_edit").hide();
                          } else {
                                  $("#m_del").show();
                                  $("#m_edit").show();
                          }
                          rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
                          }
                        if (treeId=='doc') {
                          $("#dMenu ul").show();
                          if (type=="root") {
                                  $("#m_del").hide();
                                  $("#m_edit").hide();
                          } else {
                                  $("#m_del").show();
                                  $("#m_edit").show();
                          }
                          dMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
                        }
			$("body").bind("mousedown", onBodyMouseDown);
		}
		function hideRMenu() {
			if (rMenu) rMenu.css({"visibility": "hidden"});
			if (dMenu) dMenu.css({"visibility": "hidden"});
			$("body").unbind("mousedown", onBodyMouseDown);
		}
		function onBodyMouseDown(event){
			if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
				rMenu.css({"visibility" : "hidden"});
			}
			if (!(event.target.id == "dMenu" || $(event.target).parents("#dMenu").length>0)) {
				dMenu.css({"visibility" : "hidden"});
			}
		}
		var addCount = 1;
		function addTreeNode(treeId) {
			hideRMenu();
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			var newNode = { name:"Élément " + (addCount++)};
			if (zTree.getSelectedNodes()[0]) {
				//newNode.checked = zTree.getSelectedNodes()[0].checked;
				zTree.addNodes(zTree.getSelectedNodes()[0], newNode);
			} else {
				zTree.addNodes(null, newNode);
			}
		}
		function resetTree() {
			hideRMenu();
			$.fn.zTree.init($("#panier"), setting, zNodes);
		}
		function editTreeNode(treeId) {
			var zTree = $.fn.zTree.getZTreeObj(treeId),
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			hideRMenu();
			if (nodes.length == 0) {
				alert("Sélectionnez d'abord un élément...");
				return;
			}
			zTree.editName(treeNode);
		};
		function removeTreeNode(treeId) {
			hideRMenu();
			var zTree = $.fn.zTree.getZTreeObj(treeId)
			var nodes = zTree.getSelectedNodes();
			if (nodes && nodes.length>0) {
				if (nodes[0].children && nodes[0].children.length > 0) {
					var msg = "Si vous détruisez ce noeud, vous détruirez aussi tout ce qui lui est subordonné. \n\nMerci de confirmer";
					if (confirm(msg)==true){
						zTree.removeNode(nodes[0]);
					}
				} else {
					zTree.removeNode(nodes[0]);
				}
			}
		}
		function traiter_le_document(treeId,format) {
                    hideRMenu();
                    var zTree = $.fn.zTree.getZTreeObj(treeId);
                    var nodes = zTree.getSelectedNodes();
                    traite_document(nodes[0].name,format);
		}
                async function partager() {
		  var LongName = document.getElementById("LongName").value;
		  //var LongName=LongName.replace(' ','\ ');
		  var user = document.getElementById("user_name").value;
		  //alert(LongName);
		  var doc = document.getElementById("selection_document").elements[0].value; // récupère le nom du doc
		  if (confirm('Confirmez-vous le partage du plan '+doc+' ?')) {
                    xnotify({body:'Partage du plan '+doc});
                    $('#content1').load("partager.php?LongName="+encodeURIComponent(LongName)+"&ok=ok&doc="+encodeURIComponent(doc)+"&user="+username,function() // callback 
                        {
                        $("#list_box").load('lb.php?user='+user); // mise à jour de list box des documents (pour affichage du nouveau partage ou de s
                        $("#list_partage").load('lb_partage.php'); // on met à jour la listbox de partage qui a peut-être changé
                        document.getElementById('icon_partage').innerHTML="<div style='display:inline;margin-right:5px' onclick=\"cesserdepartager();\" title='Cesser de partager le document'><div id='sharing' style='display:inline'><img height=21 alt='icone ne pas partager' src='/images/menucompo/noshare.png'/></div></div>";
                        });
                    }
                 }
                 function cesserdepartager() {
		  var LongName = document.getElementById("LongName").value;
		  var user = document.getElementById("user_name").value;
		  var doc = document.getElementById("selection_document").elements[0].value; // récupère le nom du doc
		  if (confirm('Confirmez-vous que vous cessez de partager le plan '+doc+' ?')) {
                    xnotify({body:'Arrêt du partage du plan '+doc});
                    $('#content1').load("partager.php?LongName="+encodeURIComponent(LongName)+"&ok=nok&doc="+encodeURIComponent(doc)+"&user="+username,function() // callback
                        {
                        $("#list_box").load('lb.php?user='+user); // mise à jour de list box des documents (pour affichage du nouveau partage ou de son arrêt
                        $("#list_partage").load('lb_partage.php'); // on met à jour la listbox de partage qui a peut-être changé
                        document.getElementById('icon_partage').innerHTML="<div style='display:inline;margin-right:5px' onclick=\"partager();\" title='Partager le document'><div id='sharing' style='display:inline'><img height=21 alt='icone partager' src='/images/menucompo/share.png'/></div></div>";
                        });
                    }
                 }
		function add_ressource(type) {// Ajouter une ressource (fonction générique, on passe le type à la fonction) 
		  xnotify({body:"Ajouter une ressource de type "+type,duration:1});
                  popupf("/Ai/Ajouter"+type+"?skin=minimal"); // pour affichage dans une boite modale centrale
		}
		function export_avec_popup(treeId,format) { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
		  if (treeId=='partage') 
                    { 
                    partage = 'oui';
                    doc = document.getElementById("selection_partage").elements[0].value; // récupère le nom du doc
                    }
                  else
                    {
                    partage='non'
                    doc = document.getElementById("selection_document").elements[0].value; // récupère le nom du doc
                    }
		  xnotify({body:format+" "+doc,duration:1});
		  popup("version_echo.php?format="+format+"&partage="+partage+"&doc="+encodeURI(doc));
		}
		function export_panier(format) {
                    doc='panier';
                    xnotify({body:format+" "+doc,duration:1});
                    popup("version_echo.php?format="+format+"&doc="+doc);
                    }
		function export_sans_popup(treeId,format) { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
                   if (treeId=='partage') 
                    { 
                    partage = 'oui';
                    doc = document.getElementById("selection_partage").elements[0].value; // récupère le nom du doc
                    }
                  else
                    {
                    partage='non'
                    doc = document.getElementById("selection_document").elements[0].value; // récupère le nom du doc
                    }
		  xnotify({body:format+" "+doc+'en préparation',duration:1});
                  document.getElementById('contentf').src="version_echo.php?format="+format+"&partage="+partage+"&doc="+encodeURI(doc); // pour exécution
		}
		function popup(url){
                  $('#content1').html(''); // effacement
                  //$('#content1').load(url);
                  //$('#dialog1').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
                  $('#dialog1').jqm({ajax:encodeURI(url),target:'#content1',ajaxText:'Patientez...',closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
		}
		function popupf(url){
                    var iframe=document.getElementById("contentf");
                    iframe.contentWindow.document.body.innerHTML = ""; // pour vider une iframe!!!
                    iframe.src=url;
                    $('#dialogf').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
		}
		function traite_document(name,format) { // action sur un élément dont e nom (contenant le numéro est donné
		  xnotify({body:format+" "+name,duration:1});
                  document.getElementById('contentf').src="version_echo.php?solo=oui&format="+format+"&doc="+encodeURI(name); // pour exécution
                  //popupf("version_echo.php?solo=oui&format="+format+"&doc="+encodeURI(name)); // pour exécution
		}
		function editer(treeId) { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
                  doc = document.getElementById("selection_document").elements[0].value; // récupère le nom du doc
		  document.getElementById('contentf').src="edit_doc4.php?doc="+doc; // pour affichage dans une boite modale centrale 
		  // une ruse pour éviter d'utiliser les promesses - c'est la fenêtre modale qui met à jour l'arbre
		  settingDocument.async.url="read_doc.php?doc="+doc;  
		  $('#dialogf').jqm({onHide: function(hash) { hash.w.hide() && hash.o && hash.o.remove(); $.fn.zTree.init($("#doc"),settingDocument,zvide);}, closeOnEsc: true,overlay:50 ,modal:true,}).jqmShow({overlay: 70});
		}
		function editer_panier() { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
                  doc = 'panier'; // récupère le nom du doc
		  document.getElementById('contentf').src="edit_doc4.php?doc="+doc; // pour affichage dans une boite modale centrale 
		  // une ruse pour éviter d'utiliser les promesses - c'est la fenêtre modale qui met à jour l'arbre
		  settingDocument.async.url="read_doc.php?doc="+doc;  
		  $('#dialogf').jqm({onHide: function(hash) { hash.w.hide() && hash.o && hash.o.remove(); $.fn.zTree.init($("#panier"),settingDocument,zvide);}, closeOnEsc: true,overlay:50 ,modal:true,}).jqmShow({overlay: 70});
		}
		var zvide=[];
		$(document).ready(function(){
                        $.fn.zTree.init($("#panier"), settingPanier);
			$.fn.zTree.init($("#recherche"), settingRecherche);
			$.fn.zTree.init($("#doc"), settingDocument,zvide);
			$.fn.zTree.init($("#partage"), settingShare);
			zTree = $.fn.zTree.getZTreeObj("panier");
			rMenu = $("#rMenu");
			zTreeD = $.fn.zTree.getZTreeObj("doc");
			zTreeS = $.fn.zTree.getZTreeObj("partage");
			dMenu = $("#dMenu");
			$('#formulaire_de_creation').submit(function(event) {
    				event.preventDefault(); // évite le rechargement de la page
			});
			$('#selection_document').submit(function(event) {
                                event.preventDefault(); // évite le rechargement de la page
                        });

		});
</SCRIPT>
<style>
div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
div#rMenu ul li{
	margin: 1px 0;
	padding: 0 5px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #DFDFDF;
}
div#dMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
div#dMenu ul li{
	margin: 1px 0;
	padding: 0 5px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #DFDFDF;
}
</style>
</HEAD>
<BODY> 
<div id="test"></div>
<div id='export'></div>
<!--<button id="testai">Testai</button><a href="#" class="jqModal">view</a>-->

<!--<div align=center style="left:0px;top:0px"><iframe id='iframeid' style="width:400px;height:640px;margin:5px" ></iframe></div>-->

<!-- pour popup() écriture directe sans iframe -->
<div class="jqmWindow" id="dialog1" style="width:75vw;height:80vh;border-radius:10px;">
<a style="text-decoration:none" href="#" class="jqmClose"><img style="float: right;position: relative; bottom: -5px;" alt='fermer' src="/images/close.png"/></a>
<div id='content1' style="overflow-y:scroll;width:75vw;height:75vh;margin:5px;"></div>
</div>
<!-- pour popupf() avec iframe -->
<div class="jqmWindow" id="dialogf" style="width:70vw;border-width:1px;border-radius:10px;"> <!--background-color: rgba(169, 169, 169, 0.0);">-->
<a style="text-decoration:none" href="#" class="jqmClose"><img style="float:right;position: relative; bottom: -5px;" alt='fermer' src="/images/close.png"/></a>
<div style="text-align:center;"><iframe src="" scrolling="yes" class='contentf' id='contentf' style="width:70vw;height:80vh;margin:5px;"></iframe></div>
</div><!-- pour le menu déroulant d'ajout des ressources-->
<style>
.drop-down { 
 position: relative;  
 display: inline-block;  
 width: auto;       
 margin-top: 0;   
 font-family: Arial;    
 }      
 .drop-down select {   
 display: none;    
 }      
 .drop-down .select-list {   
 position: absolute;
 width:100%;
 top: 0;      
 left: 0;     
 z-index: 1;    
 margin:0px;
 margin-top: 25px;
 padding: 0;         
 background-color: white;      
 }      
 .drop-down .select-list li {   
 display: none;
list-style:none; 
 }    
 .drop-down .select-list li span {  
 display: inline-block;
 min-height: 20px;        
 min-width: 280px;      
 width: 100%;        
 padding: 4px 5px 4px 40px; 
 background-color: white;     
 background-position: left 10px center;   
 background-repeat: no-repeat;       
 font-size: 16px;       
 text-align: left;       
 color: black;        
 opacity: 1;      
 box-sizing: border-box;  
 }     
 .drop-down .select-list li span img:hover,  
 .drop-down .select-list li span img:focus {     
 opacity: 1; 
 background-color:orange;
 }
 .drop-down .select-list li span:hover,  
 .drop-down .select-list li span:focus {     
 opacity: 1; 
 background-color:orange;
 }
a:hover {background-color:transparent;}
</style>

<script>
jQuery().ready(function() {  
/* Custom select design */    
jQuery('.drop-down').append('<div style="display:inline-block;text-decoration:none" class="button"></div>');    
jQuery('.drop-down').append('<ul class="select-list"></ul>');    
jQuery('.drop-down select option').each(function() {  
var bg = jQuery(this).css('background-image');    
jQuery('.select-list').append('<li class="clsAnchor"><span value="' + jQuery(this).val() + '" onclick="add_ressource(\''+jQuery(this).text()+'\');" class="' + jQuery(this).attr('class') + '" style=background-image:' + bg + '><big><big>&nbsp;Ajouter ' + jQuery(this).text() + ' à la base</big></big></span></li>');   
});    
jQuery('.drop-down .button').html('<span style=background-image:' + jQuery('.drop-down select').find(':selected').css('background-image') + '>' + '</span>' + '<a href="javascript:void(0);" class="select-list-link"><img margin=0 width=22 src="/images/menucompo/add.png"></a>');   
jQuery('.drop-down ul li').each(function() {   
if (jQuery(this).find('span').text() == jQuery('.drop-down select').find(':selected').text()) {  
jQuery(this).addClass('active');       
}      
});     
jQuery('.drop-down .select-list span').on('click', function()
{          
var dd_text = jQuery(this).text();  
var dd_img = jQuery(this).css('background-image'); 
var dd_val = jQuery(this).attr('value');   
jQuery('.drop-down .button').html(' ' + '<a href="javascript:void(0);" class="select-list-link"><img width=24 style="text-decoration:none" src="/images/menucompo/add.png"></a>');      
jQuery('.drop-down .select-list span').parent().removeClass('active');    
jQuery(this).parent().addClass('active');     
$('.drop-down select[name=options]').val( dd_val ); 
$('.drop-down .select-list li').slideUp();     
});       
jQuery('.drop-down .button').on('click','a.select-list-link', function()
{      
jQuery('.drop-down ul li').slideToggle(10);  
});     
/* End */       
});
</script>
<!-- fin du menu déroulant d'ajout des ressources-->

<?php

function iconadd($nature) // cette fonction n'est n'est plus utilisée
    {
    $icon=strtolower($nature);
    return "<div class='skinnytip' style='float:left;margin_bottom:0px;margin-top:1px;margin-right:3px;' data-options='borderColor:#99CCFF,backColor:#D5EAFF' onclick=\"add_ressource('$nature');\"
    data-title='Ajouter une ressource de type $nature   '
    data-text=''>
    <img height=24 alt='icone Texte' src='/images/icons/24x24/$icon.png'/>
    </div>";
    }

if ($menus_complets)
    $iconadd=iconadd('Texte').iconadd('Document').iconadd('Pdf').iconadd('Epub').iconadd('Image').iconadd('Mp3').iconadd('SujetQuestion').iconadd('SujetTexte').iconadd('Mp4').iconadd('Dailymotion').iconadd('Vimeo').iconadd('Youtube').iconadd('composite').iconadd('H5P').iconadd('Graphe').iconadd('Graphique');
else
    $iconadd=iconadd('Texte').iconadd('Image').iconadd('Pdf').iconadd('Epub').iconadd('Youtube');
    
$information_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;margin-right:1px;float:right;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/Site/InfoCompositeur?skin=iansclean');\"
                    data-title=\"Informations\"
                    data-text=\"Quelques informations sur le compositeur. Crédits, etc.\">
                    <img height=21 alt=\"icone export brut\" src=\"/images/menucompo/info.png\"/>
                </div>";
$quit_button="<div class=\"skinnytip\" style=\"float:right;margin-top:0px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" 
                    data-title=\"Déconnexion\"
                    data-text=\"Il est prudent de se déconnecter pour éviter que quelqu'un passant derrière vous usurpe votre identité ici.\">
                    <a href=\"https://philo-labo.fr?action=logout\"><img style=\"height:21px;float:right;margin-right:5px;display:inline;\" src=\"/images/logout.png\"></a>
                </div>";
$preferences_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;float:right;display:inline;margin-left:0px;margin-right:2px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popup('preferences.php');\"
                            data-title=\"Préférences\"
                            data-text=\"Vos préférences\">
                            <img height=21 src='/images/menucompo/prefwhite.png'>
                    </div>";
$help_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;float:right;display:inline;margin-left:0px;margin-right:2px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/Public/AideDuCompositeur?skin=minimal');\"
                            data-title=\"Aide\"
                            data-text=\"Affiche l'aide du compositeur.\">
                            <img height=21 src='/images/menucompo/bouee32.png'>
                    </div>";
$timer_button="<div class=\"skinnytip\" style=\"margin-right:3px;float:right;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"
           data-title=\"Un timer pour la correction des copies\"
           data-text=\"Avec moyenne, pause etc...\">
           <img onclick=\"popupf('/timer/timer.html');\" width=20 src='/images/menucompo/timerwhite.png'>
        </div>";
$goodies="<div class=\"skinnytip\" style=\"float:right;margin-top:1px;margin-bottom:5px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/kanban/eisen.php?doc=compositeur');\"
            data-title=\"Eisenhower\"
            data-text=\"Une matrice d'Eisenhower pour organiser son travail sur le compositeur.\">
            <b>&nbsp;&nbsp;E&nbsp;&nbsp;</b>
        </div>
         <div class=\"skinnytip\" style=\"float:right;margin-top:1px;margin-bottom:5px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/kanban/kanban.php?doc=compositeur');\"
            data-title=\"Kanban\"
            data-text=\"Un kanban pour organiser son travail sur le compositeur.\">
            <b>&nbsp;&nbsp;K</b>
        </div>
        <div class=\"skinnytip\" style=\"float:right;margin-top:1px;margin-bottom:5px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/kanban2');\"
            data-title=\"Kanban2\"
            data-text=\"Le kanban v2 pour organiser son travail sur le compositeur.\">
            <b>&nbsp;&nbsp;K2</b>
        </div>";
$wiki_button="<div class=\"skinnytip\" style=\"margin:-1px;padding:0px;float:left;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"
           data-title=\"Retourner à la page d'accueil de philo-labo\"
           data-text=\"En particulier vers le wiki, les e-books, etc.\">
           <a href=\"/\"><img width=18 src=\"/images/menucompo/home.png\"/></a>&nbsp;
           </div>";
$cc_button="<img onclick=\"popup('/composition/licence.html');\" style=\"height:21px;vertical-align:middle;float:right;display:inline\" src=\"/images/cc-by-nc-sa-p.png\"/>";
           

//$natures=array('JCross','JQuizz','JMix','JCloze','JMatch','document','epub','pdf','texte','sujet-question','sujet-texte','LearningApps','youtube','dailymotion','vimeo','mp4','mp3','Quizz','image','composite','h5p','graphe','graphique');
$natures=array('Texte','Pdf','Epub','Document','SujetQuestion','SujetTexte','Youtube','Dailymotion','Vimeo','Mp4','Mp3','Image','H5P','Graphe','Graphique');
function add_ressources()
    {
    global $natures;
    $result='';
    foreach ($natures as $nature)
          {
          $img=strtolower($nature);
          $titre=str_replace('_','',$nature);
          $result.="<option value=\"$nature\" style=\"background-image:url('/images/icons/24x24/$img.png');\">$nature</option>";
          }
    return $result;
    }
$addressources="<div style='clear:both;float:left;display:inline' class=\"drop-down\"'>    
	   <select name=\"options\">".add_ressources()."</select>     
</div>";

?>  
<div class="compositeur" style="border:0px dotted black;">
<table style="width:100%">
 <tr>
  <td style="width:33%;border:0px dotted black">
   <div class="zone">
    <div class="titre_zone"><?php echo "$wiki_button$timer_button";?><?php if ($menus_complets) echo $goodies;?><center>Recherche</center>
        
    </div>
    <div class="entetecompo">
      <div style="margin:0px;text-align:left;background-color:lightblue"><!--Pensez à faire une MAJ de votre document avant...<br/>S'il n'y a pas de résultats, c'est qu'il y en a trop. Affinez la recherche.-->
        <form style='float:left;background-color:lightblue;display:inline-block;width:100%' style='margin:0px' id="formrecherche" onKeyPress="if (event.keyCode == 13){event.preventDefault();formrecherchesubmit()}">
            <input type="hidden" name="action" value="searchengine">
            <input type="hidden" name="json" value="true">
            <?php echo $addressources;?>
            <center><input name="chaine" id="chaine" value="<?php echo $recherche_precedente; ?>" style="width:85%"></center>
            <!--<input type="button" id="submit_recherche" style="display:inline" name="submit" value="OK"/>-->
            <select name="nature[]" id="test-select" multiple="multiple">
                <option selected value="pdf" data-section="Tout/ebooks">pdf</option>
                <option selected value="texte" data-section="Tout/textes">textes</option>
                <option selected value="image" data-section="Tout/Images">images</option>
                <option selected value="graphe" data-section="Tout/Images">graphe</option>
                <option selected value="graphique" data-section="Tout/Images">graphique</option>
                <option selected value="JCross" data-section="Tout/Exercices/hotpatatoes">JCross</option>
                <option selected value="JQuizz" data-section="Tout/Exercices/hotpatatoes">JQuizz</option>
                <option selected value="JCloze" data-section="Tout/Exercices/hotpatatoes">JCloze</option>
                <option selected value="JMix" data-section="Tout/Exercices/hotpatatoes">JMix</option>
                <option selected value="JMatch" data-section="Tout/Exercices/hotpatatoes">JMatch</option>
                <option selected value="document" data-section="Tout/ebooks">documents</option>
                <option selected value="epub" data-section="Tout/ebooks">epub</option>
                <option selected value="sujet-question" data-section="Tout">sujets-question</option>
                <option selected value="sujet-texte" data-section="Tout/textes">sujets-texte</option>
                <option selected value="LearningsApps" data-section="Tout/Exercices">LearningsApps</option>
                <option selected value="youtube" data-section="Tout/Multimedia/Video">youtube</option>
                <option selected value="dailymotion" data-section="Tout/Multimedia/Video">dailymotion</option>
                <option selected value="vimeo" data-section="Tout/Multimedia/Video">vimeo</option>
                <option selected value="mp4" data-section="Tout/Multimedia/Video">mp4</option>
                <option selected value="mp3" data-section="Tout/Multimedia">mp3</option>
                <option selected value="Quizz" data-section="Tout/Exercices">Quizz</option>
                <option selected value="composite" data-section="Tout" data-description="Ressources composées de ressources">composite</option>
                <option selected value="h5p" data-section="Tout/Exercices" data-description="Ressources h5p">H5P</option>
            </select>
        </form>
        <script>
        $("#test-select").treeMultiselect({ enableSelectAll: false, sortable: true, hideSidePanel:true, startCollapsed:true  });
        </script>
      </div>
    <ul id="recherche" class="ztree arbre"></ul>
   </div>
  </td>
<!--
  <td style="width:25%;border:1px dotted black">
   <div class="zone">
    <div class="titre_zone">Panier
        </div>
    </div>
    <div class="entetecompo">
    </span>
        <div class="skinnytip"  style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
                    data-title="Mettre à jour le panier"
                    data-text="Mettez à jour le panier avant de quitter le compositeur. Il est sauvegardé par l'invocation du moteur de recherche, mais sauvegardez-le avant de quitter. C'est, outre les documents, ce qui restera de votre session de travail.">
                    <img id="maj_panier" width=24 src='/images/menucompo/update2.png'>
            </div>
        <div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="popupf('/Public/AideDuCompositeur?skin=minimal');"
                    data-title="Aide"
                    data-text="Affiche l'aide du compositeur.">
                    <img width=24 src='/images/menucompo/bouee32.png'>
        </div>
        <div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="popup('preferences.php');"
                    data-title="Préférences"
                    data-text="Bientôt un popup avec quelques préférences pour l'utilisateur">
                    <img  width=24 src='/images/menucompo/preferences.png'>
        </div>
        <div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_panier('html5');"
            data-title="Export en html pour les traitements de textes"
            data-text="Produit un export en html5, que vous pouvez copier dans votre traitement de texte préféré, qui est, j'en suis sûr, Libreoffice">
            <img height=24 alt="icone html5" src="/images/menucompo/html5.png"/>
        </div>
        <div class="skinnytip" style="display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_panier('markdown_joli');"
            data-title="Export en markdown pour les amateurs"
            data-text="Si vous ne connaissez pas le format markdown, vous devriez jeter un coup d\oeil au concept, et au passage, à pandoc, un couteau suisse surpuissant de conversion entre des formats de documents, écrit par John McFarlane, qui est professeur de philosophie à Berkeley">
            <img height=24 alt="icone markdown" src="/images/menucompo/markdown.png"/>
        </div>
         <div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="export_panier('brut');"
            data-title="Export brut"
            data-text="Exporte de façon à ce que vous puissiez le copier et le coller dans un document à vous.">
            <img height=16 alt="icone export brut" src="/images/menucompo/brut.png"/>
        </div>
        <div class="skinnytip" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="editer_panier();"
            data-title="Éditer" 
            data-text="Ouvre le document pour le modifier à la main. On peut facilement mettre des titres et des sous-titres.">
            <img width=24 alt="icone nouveau" src="/images/menucompo/edit.png"/>
        </div>
        <div style="float:right;display:inline;">
            <div class="skinnytip" style="float:right;display:inline;margin-left:0px;" data-options="borderColor:#99CCFF,backColor:#D5EAFF"
                    data-title="Production d'un sujet"
                    data-text="Deux numéros de questions et le numéro d'un texte, et cela vous produit le pdf">
                    <form method="get" target="_blank" action="https://philo-labo.fr/composition/gabarit0.php"><input name="s1" type="text" size=1><input name="s2" type="text" size=1><input name="s3" type="text" size=1><input type="submit" value="sujet"></form>
            </div>
        </div>
    </div>
    <ul id="panier" class="ztree arbre"></ul>
   </div>
   </td>
-->
   <td style="width:33%;border:0px dotted black">
        <div class="zone">
                <div class="titre_zone">Mes documents<!-- - <?php if ($Author=="admin") echo "Admin"; else echo $LongName;?>-->
                    <div style="float:right;margin-right:5px;color:white">chat: <?php echo chats();?>
                    </div>
                </div>
                <div class="entetecompo">
                    <?php echo "$selection_document"; ?>
                    
                </div>
                <ul id="doc" class="ztree arbre"></ul>
        </div>
  </td> 
  <td style="width:33%;border:0px dotted black">
        <div class="zone">
            <div class="titre_zone">En partage
                <?php echo "$quit_button$cc_button$information_button$evolution_button$help_button$preferences_button";?>
            </div>
            <div class="entetecompo">
<?php
$export_outline="<div class=\"skinnytip\" style=\"display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"export_sans_popup('partage','outline');\"
                    data-title=\"Export du plan de l'arbre\"
                    data-text=\"Exporte le plan de l'arbre en pdf, avec juste le titre des ressources\">
                    <img height=24 alt=\"icone poly\" src=\"/images/menucompo/outline.png\"/>
                </div>";
$export_beamer="<div class=\"skinnytip\" style=\"display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"  onclick=\"export_sans_popup('partage','beamer');\"
                    data-title=\"Diaporama en pdf\"
                    data-text=\"Le diaporama peut prendre un peu de temps si les textes sont nouveaux.<br/>Il sera produit très vite si les textes ont déjà été traités.\">
                    <img height=24 alt=\"icone beamer\" src=\"/images/menucompo/beamer.png\"/>
                </div>";
$export_poly="<div class=\"skinnytip\" style=\"display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"  onclick=\"export_sans_popup('partage','synchro3');\"
                    data-title=\"Extraire un polycopié\"
                    data-text=\"Extraire les textes et les images dans un polycopié en pdf. Les lignes sont les mêmes que dans le diaporama: très pratiques pour les étudiants qui ont une mémoire visuelle.\"    >
                    <img height=24 alt=\"icone poly\" src=\"/images/menucompo/poly.png\"/>
                </div>";
$export_S5="<div class=\"skinnytip\" style=\"display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"export_avec_popup('partage','S5');\"
                    data-title=\"Diaporama s5\"
                    data-text=\"Produit un diaporama jouable dans le navigateur, avec les textes découpés si nécessaires en plusieurs diapos, gère les textes, les images et les videos youtube pour l'instant\">
                    <img height=24 alt=\"icone poly\" src=\"/images/presentationp.png\"/>
                </div>";
$export_html5="<div class=\"skinnytip\" style=\"display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"export_avec_popup('partage','html5');\"
                    data-title=\"Export en html pour les traitements de textes\"
                    data-text=\"Produit un export en html5, que vous pouvez copier dans votre traitement de texte préféré, qui est, j'en suis sûr, Libreoffice\">
                    <img height=24 alt=\"icone html5\" src=\"/images/menucompo/html5.png\"/>
                </div>";
$export_markdown="<div class=\"skinnytip\" style=\"display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"export_avec_popup('partage','markdown_joli');\"
                    data-title=\"Export en markdown pour les amateurs\"
                    data-text=\"Si vous ne connaissez pas le format markdown, vous devriez jeter un coup d\oeil au concept, et au passage, à pandoc, un couteau suisse surpuissant de conversion entre des formats de documents, écrit par John McFarlane, qui est professeur de philosophie à Berkeley\">
                    <img height=24 alt=\"icone markdown\" src=\"/images/menucompo/markdown.png\"/>
                </div>";
$export_brut="<div class=\"skinnytip\" style=\"display:inline;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"export_avec_popup('partage','brut');\"
                    data-title=\"Export brut\"
                    data-text=\"Exporte de façon à ce que vous puissiez le copier et le coller dans un document à vous.\">
                    <img height=16 alt=\"icone export brut\" src=\"/images/menucompo/brut.png\"/>
                </div>";

if ($menus_complets)
    $exports="$export_outline$export_beamer$export_poly$export_S5$export_html5$export_markdown$export_brut";
else
    $exports="$export_beamer$export_poly$export_S5";
echo "<div style='padding:0px;margin:0px;display:block;text-align:right;margin-right:5px;padding:0px'>$exports</div>";
?>

                <?php echo $selpartage;?>   
            </div>
            <ul id="partage" class="ztree arbre"></ul>
        </div>
  </td>
 </tr>
</table>
</div>

<div id="rMenu">
	<ul>
                <li style="background-color:lightblue;"><b>Panier</b></li>
		<li id="p_lpdf" onclick="traiter_le_document('panier','lpdf');">Ce texte en pdf divisé en diapos</li>
		<li id="p_lpng" onclick="traiter_le_document('panier','pdf');">Ce texte en pdf</li>
		<li style="background-color:lightblue;"><b>Opérations globales</b></li>
		<li id="pexpandAll" onclick="zTree.expandAll(true);">Développer l'arbre</li>
		<li id="pcollapseAll" onclick="zTree.expandAll(false);">Envelopper l'arbre</li>
	</ul>
</div>
<div id="dMenu">
	<ul>
		<li style="background-color:lightblue;"><b>Opérations locales</b></li>
		<!--<li id="m_add2" onclick="addTreeNode('doc');">Ajouter un noeud</li>-->
		<li id="m_lpdf" onclick="traiter_le_document('doc','lpdf');">Ce texte en pdf divisé en diapos</li>
		<li id="m_lpng" onclick="traiter_le_document('doc','pdf');">Ce texte en pdf</li>
		<li id="m_del2" onclick="removeTreeNode('doc');">Supprimer ce noeud</li>
		<!--<li id="m_rename2" onclick="editTreeNode('doc');">Renommer ce noeud</li>-->
		<li style="background-color:lightblue;"><b>Opérations globales</b></li>
		<li id="expandAll" onclick="zTreeD.expandAll(true);">Développer l'arbre</li>
		<li id="collapseAll" onclick="zTreeD.expandAll(false);">Envelopper l'arbre</li>
		<!--<li id="m_edit" onclick="editer('doc');">Editer ce document (expérimental)</li>
		<li style="background-color:lightblue;"><b>Exportation de l'arbre</b></li>
		<li id="m_arbreedit" onclick="export_avec_popup('doc','arbre éditable');">arbre éditable</li>-->
		<li id="m_outline" onclick="export_sans_popup('doc','outline_html');">plan en html</li>
		<li id="m_brut" onclick="export_avec_popup('doc','brut');">brut</li>
		<li id="m_markdown" onclick="export_avec_popup('doc','markdown_joli');">Markdown joli</li>
		<li id="m_html" onclick="export_avec_popup('doc','html');">html</li>
		<li id="m_docx" onclick="export_sans_popup('doc','docx');">export en docx</li>
		<li id="m_pmwiki" onclick="export_avec_popup('doc','pmwiki');">PmWiki à copier dans une nouvelle page</li>
		<li id="m_s5" onclick="export_avec_popup('doc','S5');">S5 rustique</li>
		<li id="m_dys" onclick="export_sans_popup('doc','synchro3dys');">Poly dys</li>
		<!--<li id="m_beamer" onclick="export_sans_popup('doc','beamer');">Beamer [obsolète] (veiller patienter)</li>
                <li id="m_poly1" onclick="export_sans_popup('doc','poly1');">Poly (après Beamer) normal</li>
		<li id="m_poly2" onclick="export_sans_popup('doc','poly2');">Poly (après Beamer) 2 col</li>
		<li id="m_polyA5" onclick="export_sans_popup('doc','polyA5');">Poly paysage A4 - 2xA5</li>
		<li id="m_synchro" onclick="export_sans_popup('doc','synchro2');">Poly synchro 2col</li>
                <li id="m_pdf" onclick="export_sans_popup('doc','pdf');">pdf direct</li>-->
	</ul>
</div>
</BODY> 
</HTML>
<?php
//echo temps('lecteur du compositeur');
?>
