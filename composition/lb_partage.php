<?php

require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once ('../philosophemes/ai.php');
require_once ('../philosophemes/secure.php');

$rep="/web/philo-labo/partage";
$trees = glob("$rep/*.tree");
$npart=0;
$partages=array();
foreach ($trees as &$d)
    {
    $name=substr($d,strlen($rep)+1,-5);
    $partages[$npart][0]=$name;
    $partages[$npart][1]=$name; 
    $npart++;
    }

if (!file_exists("/web/philo-labo/users/$Author/compositeur/partage_actuel"))
    file_put_contents("/web/philo-labo/users/$Author/compositeur/partage_actuel",$partages[0][0]); // cas pour un premier utilisateur
    
$partage_actuel=file_get_contents("/web/philo-labo/users/$Author/compositeur/partage_actuel");
$l=listbox($partages,'partages',$partage_actuel);
echo $l;

?>
