<html>
<head>
<meta charset="utf-8">
</head>
<body>
<?php
require_once('../../../../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require('../../../../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('../../../../philosophemes/ai.php');

$s="{toto:".file_get_contents('panier.json')."}";

echo $s;
echo "<hr/>";   
//$s=' {toto:[{id:28,pId:null,name:"[texte] Critique de la critique du machinisme - Bergson"},{id:52,pId:null,name:"[texte] Le temps n\'est pas - Augustin"},{id:149,pId:52,name:"[texte] Les souvenirs, fantômes invisibles - Bergson"},{id:28,pId:149,name:"[texte] Critique de la critique du machinisme - Bergson"},{id:84,pId:null,name:"[texte] Travail et consommation - Arendt"},{id:130,pId:84,name:"[texte] Liberté égalité fraternité - Bergson"},{id:28,pId:130,name:"[texte] Critique de la critique du machinisme - Bergson"}]}';
echo $s;
echo "<hr/>";	

function json_decode_nice($json, $assoc = true){
    $json = str_replace(array("\n","\r"),"\\n",$json);
    $json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
    $json = preg_replace('/(,)\s*}$/','}',$json);
    //$json = preg_replace('/^\[/','',$json);
    //$json = preg_replace('/\]$/','',$json);
    echo $json;
    return json_decode($json,$assoc,10);
}

$t=json_decode_nice($s,true);	
print_r($t);
echo json_last_error()

//echo array2sql('arbors',array('id_ressource','father','name'),array(array(1,2,3)),'insert');

?>
</body>
</html>
