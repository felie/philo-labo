#!/bin/bash

echo "début de after - rétablissement du latex après le traitement par pandoc"

sed -i "s/³/TrOiS/g" $1 # on protège le ³ dont on va user et abuser

sed -i "s/\\\{/{/g" $1 # on supprime les échappements de { et de}
sed -i "s/\\\}/}/g" $1

# rétablissement des commandes latex qu'on a autorisé à la fin de before à contenir du markdown
#sed -i "s/texte{/\\\texte{/g" $1
sed -i "s/textbackslash{}/\\\/g" $1

cp $1 $1.compact0

sed -i "s/+++/\\\/g" $1
sed -i "s/DeB/{/g" $1
sed -i "s/EnD/}/g" $1

# gestion des 7 espèces de tableaux
#
echo "tableaux carrés"
sed -i "s/(tabcccc/\\\tabcccc/g" $1
sed -i "s/(tablcc/\\\tablcc/g" $1
sed -i "s/(tabccl/\\\tabccl/g" $1
sed -i "s/(tabll/\\\tabll/g" $1
sed -i "s/(tabvv/\\\tabvv/g" $1
sed -i "s/(tabvcc/\\\tabvcc/g" $1
sed -i "s/(tabccv/\\\tabccv/g" $1

echo " accolades"
sed -i "s/((/{/g" $1
sed -i "s/))/}/g" $1 
sed -i "s/)(/}{/g" $1 
#--------------------------------traitement pour l'extraction des textes

# protection des ²
sed -i "s/²/DeUx/g" $1
echo "protection des sauts de lignes"
sed -i ':a;N;$!ba;s/\n/²/g' $1 # remplacement de tous les sauts de ligne, sed ne traitera qu'une ligne ensuite

# On va rechercher les textes pour faire la liste-des-textes
sed -i "s/t(/\nt(/g" $1
sed -i "s/)t/)t\n/g" $1
> liste-des-textes

grep 't(' $1 >> liste-des-textes # tous les textes, mêmes les ext_pdf

# enlever le déguisement des chunk.pdf
sed -i "s/t(\\\includepdf\(.*\))t/\n\\\includepdf\1\n/g" $1
# et le déguisement de ext_pdfin
sed -i "s/t(\\\input{\(.*\)})t/\n\\\end{frame}\n\\\input{\1.tex}\n\\\begin{frame}{bidon}\n/g" $1 # la diapo bidon sera supprimée plus loin, mais avec son end!

sed -i ':a;N;$!ba;s/\n/²/g' $1 ###
#sed -i "s/\nt(/t(/g" $1
#sed -i "s/)t\n/)t/g" $1
sed -i "s/²t(/t(/g" $1
sed -i "s/)t²/)t/g" $1
  
sed -i "s/²/\n/g" $1
sed -i "s/DeUx/²/g" $1

sed -i "s/jt(/\\\jt{/g" $1
sed -i "s/)tj/}/g" $1

# ----------------------------------
# gestion des textes
# texte sur une ligne
sed -i "s/^t(\(.*\))t$/txt{\1}/" $1
# autres textes (on traite la ligne de début; avec ou sans titre; et la ligne de fin, avec ou sans signature
sed -i "s/^t(\(..*\)$/texteavectitre{\1³{/" $1 # le < indiquera que c'est un titre
sed -i "s/^\(.[^)]*\))t$/³{\1³signature/" $1 # le > indiquera que c'est une signature 
sed -i "s/^t($/textesanstitre{/" $1
sed -i "s/^)t$/³sanssignature/" $1

cp $1 $1.protex

# protection des ²
sed -i "s/²/DeUx/g" $1
echo "protection des sauts de lignes"
sed -i ':a;N;$!ba;s/\n/²/g' $1 # remplacement de tous les sauts de ligne, sed ne traitera qu'une ligne ensuite
sed -i "s/³signature/³signature\n/g" $1
sed -i "s/³sanssignature/³sanssignature\n/g" $1
sed -i "s/texteavectitre/\ntexteavectitre/g" $1
sed -i "s/textesanstitre/\ntextesanstitre/g" $1


## éviter les } en début de ligne
#sed -i "s/}\(²*\){/}{/g" $1
##sed -i "s/}²{/}{/g" $1
##sed -i "s/}²{/}{/g" $1
#sed -i "s/}\(²\)$/}/g" $1
#sed -i "s/\(²*\){/{/g" $1	

cp $1 $1.protex2

# il faut maintenant discriminer parmi les textes
# texte < }{}{ > titre et signature
sed -i "s/^texteavectitre{\(.*\)³signature$/\\\textets{\1}/" $1
# texte < }{} titre sans signature
sed -i "s/^texteavectitre{\(.*\)³sanssignature$/\\\textet{\1}/" $1
# titre {}{ > texte sans titre avec signature
sed -i "s/^textesanstitre{\(.*\)³signature$/\\\textes{\1}/" $1
# titre {} texte simple
sed -i "s/^textesanstitre{\(.*\)³sanssignature$/\\\texte{\1}/" $1
sed -i "s/txt{/\\\texte{/g" $1

# on remet les }
sed -i "s/³/}/g" $1

cp $1 $1.compact

# Environnements spéciaux (le traitement aurait pu se faire avec rep22, qui a été écrit pour l'occasion :)
# fwb
a="begin{frame}{fwb"
b="end{frame}"
c="begin{fwb}{"
d="end{fwb}"
sed -i "s/$b/³/g" $1
sed -i "s/$a\([^³]*\)³/$c\1$d/g" $1
sed -i "s/³/$b/g" $1
a="begin{frame}{fwtc"
b="end{frame}"
c="begin{fwtc}{"d="end{fwtc}"
sed -i "s/$b/³/g" $1
sed -i "s/$a\([^³]*\)³/$c\1$d/g" $1
sed -i "s/³/$b/g" $1
# fwmb
a="begin{frame}{fwmb"
b="end{frame}"
c="begin{fwmb}{"
d="end{fwmb}"
sed -i "s/$b/³/g" $1
sed -i "s/$a\([^³]*\)³/$c\1$d/g" $1
sed -i "s/³/$b/g" $1

cp $1 $1.compact-fin

# bclogo
# bclogo d'une ligne
#sed -i "s/bc\([a-z]*\)(\([^)]*\))/\\\begin{bclogo}[logo=\\\bc\1,couleur = blue !30 , arrondi = 0.1]{}\2\\\end{bclogo}/g" $1
sed -i "s/bc\([a-z]*\)(\([^²]*\)²\([^)]*\))/\\\begin{bclogo}[logo=\\\bc\1,couleur = blue !30 , arrondi = 0.1]{\2}\3\\\end{bclogo}/g" $1

# éviter les } en début de ligne
sed -i "s/}\(²*\){/}{/g" $1
#sed -i "s/}²{/}{/g" $1
#sed -i "s/}²{/}{/g" $1
sed -i "s/}\(²\)$/}/g" $1 
sed -i "s/\(²*\){/{/g" $1

# bidouille infâme: lorsqu'une injection est suivie d'un \end{truc}, mettre le \end{truc} avant... :) c'est sale mais ça marche
# de façon à les mettre en dehors des frame
# c'est vrai des \includepdf
#
# sed -i "s/²\([^²]*\)\\\includepdf\([^²]*\)²²\\\end{\([^}]*\)}/\\\end{\3}²²\1\\\includepdf\2²/g" $1
# # lorsque on a un \includepdf en début de ligne, on fait pareil, on fait passer le end avant lui
# sed -i "s/²\\\includepdf\([^²]*\)²²\\\end{\([^}]*\)}/\\\end{\2}²²\1\\\includepdf\1²/g" $1

sed -i "s/²\([^²]*\)\\\includepdf\([^\\\]*\)\\\end{\([^}]*\)}/\\\end{\3}²\1\\\setbeamercolor{background canvas}{bg=}\\\includepdf\2²/g" $1
# lorsque on a un \includepdf en début de ligne, on fait pareil, on fait passer le end avant lui
#sed -i "s/²\\\includepdf\([^\\\]*\)\\\end{\([^}]*\)}/\\\end{\2}²\1\\\setbeamercolor{background canvas}{bg=}\\\includepdf\1²/g" $1

sed -i "s/²\([^²]*\)\\\setbeamercolor{background\([^²]*\)²²\\\end{\([^}]*\)}/\\\end{\3}²²\1\\\setbeamercolor{background\2²/g" $1
# lorsque on a un \includepdf en début de ligne, on fait pareil, on fait passer le end avant lui
sed -i "s/²\\\setbeamercolor{background\([^²]*\)²²\\\end{\([^}]*\)}/\\\end{\2}²²\1\\\setbeamercolor{background\1²/g" $1

# gére les organigramme - sauve un fichier et pointe sur orga (qui fera la transformation et affichera l'organigramme)
# sed -i "s/\\\orga{\([^}]*\)}{\([^}]*\)}/\\\begin{writefile}{\1.arbor}\2\\\end{writefile}\\\organigram{\1}/g" $1

# supprimer les frames vides (fabriquées par pandoc à cause des sauts de lignes vspace(0.5cm) ?) SALE

sed -i "s/\\\begin{frame}\([^²]*\)\([²|§]*\)\\\end{frame}//g" $1 # diapo vide

sed -i "s/\\\begin{frame}²²§²²\\\end{frame}//g" $1

# ***
sed -i "s/§/²/g" $1

echo " rétablissement des sauts de ligne forcés"
sed -i "s/²/\n/g" $1
sed -i "s/DeUx/²/g" $1

# déprotéger +++
sed -i "s/plusplusplus/+++/g" $1
# déprotéger EnD
sed -i "s/EnDEnDEnD/EnD/g" $1
# déprotéger DeB
sed -i "s/DeBDeBDeB/DeB/g" $1

# supprimer toutes les lignes vides - dangereux, ça fait apparaître block[rounde] sur la page de couverture
# apparemment moins dangereux que prévu
#sed -i '/^$/d' $1

sed -i "s/TrOiS/³/g" $1
sed -i "s/^§/\\\vspace{0.5cm}/g" $1 # pour avoir les sauts de lignes propres avant pdflatex
#sed -i "s/§$/\\\vspace{0.5cm}/g" $1 
sed -i "s/§}$/}/g" $1
sed -i "s/§//g" $1 # ???

sed -i "s/SautDeLigne/\\\\\\\/g" $1

sed -i "s/:)/\\\bcsmbh /g" $1  # emiticon de bclogo :)
sed -i "s/:(/\\\bcsmmh /g" $1  # emiticon de bclogo :(

echo "fin de after"
