% aspectratio défini pour le mackage multimedia apparemment
%\documentclass[a4paper,12pt,aspectratio=1610]{beamer}
\documentclass[a4paper,12pt]{beamer}
%\setbeameroption{show notes}% voir les  notes

%\usepackage{fullpage}
%\usepackage{morewrites}% \newrite stream limité à 16, alors on étend avec ce paquet
\usepackage[marginparwidth=4cm,marginparsep=2mm]{geometry}% pour les marges

\geometry{
  top=0in,            % <-- you want to adjust this
  inner=0in,
  outer=0in,
  bottom=0in,
  headheight=0ex,       % <-- and this
  headsep=0ex,  	% <-- and this
  vmargin=0ex,
  foot=0.5cm,
  showframe=false
}

%\usepackage{fancyhdr}
%\pagestyle{fancyplain}

\usepackage[utf8]{inputenc}% pour écrire en utf8 dans les fichiers
\usepackage[T1]{fontenc}% pour les accents dans les pdf
\usepackage{aeguill} %
\usepackage{parskip}
\usepackage{version}% permet l'utilisation de \begin{comment} \end{comment}
\usepackage{multimedia}% pour les videos
\usepackage{graphicx}
\usepackage{ccaption} % pour pouvoir enlever les figures n° des légendes
\usepackage{booktabs}
\renewcommand{\tabcolsep}{0.1cm}%%
\usepackage{tikz}
\usepackage{tikz-qtree}
\usepackage{qtree}
\usepackage{forest}
\usepackage{adjustbox}
\usepackage{soulutf8} % pour les soulignement qui peuvent faire plus d'une ligne - ne pas utliser \underline mais \ul
\renewcommand{\underline}[1]{\ul{#1}}
%\newcolumntype{C}[1]{>{\centering}p{#1}}
\usetikzlibrary{shadows}
\usepackage{dirtree}

\usepackage{listings}% pour les paragraphes qui supoporte d'être divisés
%\usepackage{changepage}% pour adjustwidth
%\usepackage{longtable}
%\usepackage{supertabular}
\usepackage{ifthen}
%\usepackage{multido}
\usepackage{layout}% pour afficher la configuration des slides par \layout dans l'environnement frame (activer l'affichage des frame dans geometry)

%pour mettre le numéro de la diapo dans la barre de navigation!
% merci à http://tex.stackexchange.com/questions/137022/how-to-insert-page-number-in-beamer-navigation-bars

%pour mettre la barre de navigation verticale
%\setbeamertemplate{navigation symbols}[vertical]

%\addtobeamertemplate{navigation symbols}{}{%
%    \usebeamerfont{footline}%
%    \usebeamercolor[fg]{footline}%
%    \hspace{1em}%
%    \insertframenumber/\inserttotalframenumber
%}

% On pourrait ne pas tout afficher de la barre de navigation!
%\setbeamertemplate{navigation symbols}{%
%\insertslidenavigationsymbol
%\insertframenavigationsymbol
%\insertsectionnavigationsymbol
%\insertdocnavigationsymbol
%\insertbackfindforwardnavigationsymbol
%}

\setbeamertemplate{blocks}[rounded=true, shadow=true]

\mode<presentation>

\def\mylogo{mylogo.png}
\mode<beamer>{
  \addtobeamertemplate{navigation symbols}
   {
   \usebeamerfont{footline}%
   \insertframenumber{}/\inserttotalframenumber{}\hfill}
   }%

%\usetheme[sidebartab,width=70pt,hideothersubsections,right]{Hannover}% largeur de la barre latérale

%\useoutertheme{default}
%\useoutertheme{progressbar}% barre de progression de Sylvain Bouveret
\usetheme[sidebartab,height=20pt,baselineskip=1pt,hideothersubsections]{Goettingen}
%\setbeamercovered{transparent}% les trucs pas affichés en grisé
%\beamertemplatetransparentcovered
\usecolortheme{crane}
\useinnertheme[shadow=true]{rounded}
%\usecolortheme{sidebartab}

%\useoutertheme[width=80pt]{sidebar}
%\beamer@headheight=5\baselineskip% taille de l'entête de la barre latérale'
\setbeamercolor{sidebar right}{bg=white,fg=yellow}
%    \setbeamercolor{normal text}{fg=black,bg=white}
%    \setbeamercolor{alerted text}{fg=red}
\setbeamercolor{example text}{fg=green!50!black}
% \setbeamercolor{structure}{fg=beamer@blendedred} % d'où ce bleu par défaut
\setbeamercolor{structure}{fg=black}% fg couleur des titres


\iffalse
\definecolor{craneblue}{RGB}{4,6,76}

\usecolortheme{crane}
\setbeamertemplate{blocks}[rounded]
\useinnertheme[shadow=true]{rounded}
\setbeamercolor{sidebar right}{text=black,bg=craneblue!50,fg=red}% couleur de la sidebar
\setbeamercolor{structure}{fg=black}% fg couleur des titres
\setbeamercolor*{section in sidebar shaded}{parent=palette sidebar secondary}
\setbeamercolor*{section in sidebar}
  {parent=section in sidebar shaded,use={sidebar,section in sidebar shaded},%
   bg=sidebar.bg!90!section in sidebar shaded.fg}
\setbeamercolor*{subsection in sidebar shaded}{parent=palette sidebar primary}
\setbeamercolor*{subsection in sidebar}
  {parent=subsection in sidebar shaded,use=section in sidebar,%
   bg=section in sidebar.bg}

%ici Goettingen

\useoutertheme[height=0pt,right]{sidebar}

%{\usebeamercolor{structure}}

%\setbeamertemplate{sidebar canvas \beamer@sidebarside}[vertical shading][top=structure.fg!10,bottom=structure.fg!10]
\fi
% Cela permettrait de découper les textes en plusieurs diapo, mais ça casse le mode incrémental, j'ai trouvé une autre solution :) 
% avec des pdf créés avec des pauses, qui utilisent le package pdfscreen, et en includepdf! les pauses continuent à marcher!
% \let\oldframe\frame%
% \renewcommand\frame[1][]{%
% %\setbeamertemplate{frametitle continuation}{}% pour supprimer les numéros dans les titres des diapos
% \oldframe[allowframebreaks,#1]}% découpage automatique des contenus longs en diapos

%%% http://tex.stackexchange.com/questions/131290/equivalent-of-beamer-pause-for-other-document-classes
% pour les pauses dans les pdf inclus
%\usepackage[screen,nopanel]{pdfscreen}
%\usepackage[display]{texpower}


%----------------
% \usepackage{lplfitch}% pour les démonstrations logiques à la Fitch mais dépend d'un paquet qui est cass"é par array et tabular... donc exit
%   \newcommand{\syllogisme}[3]{\fitcharg{%
%     \text{#1}\\
%     \text{#2}
%     }{
%     \text{#3}
%     }}%
%    \newcommand{\deduction}[2]{\fitcharg{%
%     \text{#1}
%     }{
%     \text{#2}
%     }}%

%\usepackage{wiki}% pour gérer les '' ''' ''''''
\usepackage{xspace}% pour gérer les espaces avec les guillemets
\usepackage{xstring}% fournit par exemple la fonction de remplacement d'une chaine par une autre
\usepackage{ragged2e}% pour justifier
\usepackage[babel=true]{csquotes}%pour que les guillements prennent la valeur donnée à babel
\usepackage{times}
% pour afficher la police actuelle
\DeclareTextFontCommand{\textttup}{\normalfont\ttfamily}
\newcommand{\printinternalcurrentfont}{%
  \expandafter\textttup\expandafter{\expandafter\string\the\font}%
}
\newcommand{\printexternalcurrentfont}{%
  \expandafter\textttup\expandafter{\fontname\font}%
}
\usepackage[polutonikogreek,french]{babel}
    \newcommand{\greek}[1]{{\selectlanguage{polutonikogreek}#1}} % pour écrire en grec
\usepackage{listings}% pour les listings, contient lstlisting qui est un environnement préservant les espaces
\usepackage{fancyvrb}% encore mieux, pour pouvoir les écrire dans un fichier!
\usepackage{datetime}%
\usepackage{textcomp}
\usepackage[official]{eurosym}
\usepackage{lmodern}
\usepackage{marginnote}% pour les notes dans la marge
\usepackage{esvect}% pour les vecteurs 
\usepackage{boiboites}% pour de jolies boites pour les théorèmes... ou pour autre chose
\newboxedtheorem[boxcolor=orange, background=blue!5, titlebackground=blue!20,titleboxcolor = black]{theo}{Théorème}{test}

%\usepackage[french]{minitoc}% semble incompatible avec beamer
%\usepackage{titlesec,titletoc}

\usepackage{pgfplots}
\usepgfplotslibrary{patchplots}
\usepackage{tikz,stackengine}
\usepackage{eqnarray}
\usepackage{adjustbox}
\usepackage{xcolor}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\usepackage{pslatex}
\usepackage{array}
\usepackage{setspace}% gérer l'espacement entre les paragraphes
\usepackage{beamerbaseboxes}
%\usepackage{luacode,luatexbase} % pour avoir les *italic* **gras** et ***gras italic*** dans le latex ala markdown 
\usepackage{float}
\usepackage{ulem}
\usepackage{fancybox}
\usepackage{listings}

\usepackage{graphvizzz}
\usepackage{multirow}
\usepackage{color}
\usepackage{tabularx}
\usepackage{pdfpages}
\usepackage{etoolbox}

\newcommand*{\Scale}[2][4]{\scalebox{#1}{\ensuremath{#2}}}% pour écrire les maths en grande taille

% \newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}} % zentrierte  
% \newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}} %  
% \newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}} %  
% \newcolumntype{J}[1]{>{p{3cm}\arraybackslash}p{#1}} % Blocksatz 

% pour la coloration des liens: bleu standard et gris pour les liens internes
\definecolor{links}{HTML}{2A1B81}
\hypersetup{colorlinks,linkcolor=,urlcolor=links}

\usepackage{lipsum} % pour le bolo bolo
\usepackage{epic}
\usepackage{eepic}
%\usepackage{eclbip}
\usepackage{caption}
\usepackage{calc}% pour pouvoir utiliser des variables calculées
\usepackage{shadow}
\usepackage{ctable} % charge array et booktabs
\usepackage{tikz}
\usepackage[tikz]{bclogo}
\usepackage{latexsym,amsmath,amsfonts,ifthen}
\usepackage{fitch} % package
\newcommand {\syllogism}[3]{
\begin{nd} 
  \have[~]{}{\text{#1}}
  \hypo[~]{}{\text{#2}}
  \have[~]{}{\text{#3}}
\end{nd}}
\usepackage{hyperref}% mettre en dernier, car il redéfinit beaucoup de choses

\usepackage[absolute,overlay]{textpos}
\setlength{\TPHorizModule}{1mm}
\setlength{\TPVertModule}{1mm}

%\logo{\includegraphics[height=0.55cm]{logo.png}}

% ----- beaux textes avec les commentaires

\usepackage{microtype}%ligature 
\usepackage{stackengine}
\def\asterism{
  \par\vspace{1em}{\centering\scalebox{1.5}
    {%
    \stackon[-0.5pt]{\bfseries *~*}{\bfseries *}
    }
    \par}\vspace{.5em}\par
    }
\usepackage[svgnames]{xcolor}% pour avoir le nom des couleurs défini voir https://fr.sharelatex.com/learn/Using_colours_in_LaTeX#Setting_the_page_background_colour

% soul permet de définir des zones avec \hl \sethlcolor{green}
% \usepackage{soul}
% \newcommand{\hlcolor}[2]{%
%     \colorlet{foo}{#1!30}%
%     \sethlcolor{foo}\hl{#2}%
% }

%\pagecolor{Ivory}

%\usepackage[landscape,centering,top=1cm,bottom=1cm,marginparwidth=3.5cm,heightrounded,marginparsep=.5cm]{geometry}
% voir https://en.wikibooks.org/wiki/LaTeX/Footnotes_and_Margin_Notes#Margin_Notes
\newcommand{\jolititre}[1]{\begin{center}\large{\textbf{#1}}\end{center}\par}

\newcommand{\jt}[1]{
  {\rmfamily\setlength{\parindent}{1cm}\justify #1} %\vspace*{\stretch{1}}}   % \newgeometry{vmargin=4cm} pour les marges
  }

%---------

\addto\captionsfrench{\def\figurename{}}% pas de nom de figure genre FIGURE -

%\titlegraphic{\includegraphics[width=.8\textwidth,height=.3\textheight]{books.png}}

%---------------- pour les notes


%----------------

%\usepackage[pdftex]{eforms} % pour mettre un chronomtre
%\begin{insDLJS}{showtime}{Show time}
%function tClock()
%{this.getField("datetime").value = util.printd("hh:MM:ss tt dd/mm/yyyy", new Date());
%} var timeout =app.setInterval("tClock()",1000);
%\end{insDLJS}

%\newcommand{\w}[2]        % pour les lien internet
%   {\href{#1}{\color{blue}#2}}

% -------------- différents liens décorés avec des images
% le lien standard est \href{url}{texte}

\newcommand{\linkurl}[2]{\href{#1}{#2\includegraphics[width=20,height=20]{/web/philo-labo/m2b/link.png}}}
\newcommand{\linkvideo}[2]{\href{#1}{#2\includegraphics[width=20,height=20]{/web/philo-labo/m2b/tele.png}}}
\newcommand{\linkfile}[2]{\href{#1}{#2\includegraphics[width=20,height=20]{/web/philo-labo/m2b/file.png}}}
\newcommand{\linkbook}[2]{\href{#1}{#2\includegraphics[width=20,height=20]{/web/philo-labo/m2b/books.png}}}
\newcommand{\linksound}[2]{\href{#1}{#2\includegraphics[width=20,height=20]{/web/philo-labo/m2b/son.png}}}

\newcommand{\diapo}[1]{   % pour une diapo image en ajustant la taille
\begin{figure}
\centering
    \includegraphics[width=\textwidth,height=.9\textheight,keepaspectratio]{#1}
\end{figure}
}%

%\renewcommand\textsuperscript[1]{\ensuremath{^{\textrm{#1}}}}
%\newcommand\textsubscr[1]{\ensuremath{_{\textrm{#1}}}}

\makeatletter
\newcommand\textsubscr[1]{\@textsubscr{\selectfont#1}}
\def\@textsubscr#1{{\m@th\ensuremath{_{\mbox{\fontsize\sf@size\z@#1}}}}}
\newcommand\textbothscript[2]{%
  \@textbothscript{\selectfont#1}{\selectfont#2}}
\def\@textbothscript#1#2{%
  {\m@th\ensuremath{%
    ^{\mbox{\fontsize\sf@size\z@#1}}%
    _{\mbox{\fontsize\sf@size\z@#2}}}}}
\def\@super{^}\def\@sub{_}
%\catcode`^\active\catcode`_\active              % je l'ai désactivé, parce que ça empêchait de lire les fichiers avec un undersocre dedans, mais le subscripts marchent encore dans la maths....
\def\@super@sub#1_#2{\textbothscript{#1}{#2}}
\def\@sub@super#1^#2{\textbothscript{#2}{#1}}
\def\@@super#1{\@ifnextchar_{\@super@sub{#1}}{\textsuperscript{#1}}}
\def\@@sub#1{\@ifnextchar^{\@sub@super{#1}}{\textsubscript{#1}}}
\def^{\let\@next\relax\ifmmode\@super\else\let\@next\@@super\fi\@next}
\def_{\let\@next\relax\ifmmode\@sub\else\let\@next\@@sub\fi\@next}
\makeatother%

% italic gras dans le latex ala markdown

%\begin{luacode}
%   -- Use Lua captures to extract material affected by markdown
%   function allstars (line) 
%      line = string.gsub( line, "(%*%*%*)(.-)(%*%*%*)", "{\\bfseries\\itshape %2}")
%      line = string.gsub( line, "(%*%*)(.-)(%*%*)", "{\\bfseries %2}" )
%      line = string.gsub( line, "(%*)(.-)(%*)", "{\\itshape %2}" )
%      return line
%   end
%\end{luacode}
%
%\newcommand\markdownon{%
%   \directlua{luatexbase.add_to_callback( "process_input_buffer", allstars, "allstars" )}}
%\newcommand\markdownoff{%
%   \directlua{luatexbase.remove_from_callback( "process_input_buffer", "allstars" )}}
%
% fin

%\let\oldfootnotesize\footnotesize%
%\renewcommand*{\footnotesize}{\oldfootnotesize\tiny}%

\let\oldmarginnote\marginnote%
\renewcommand{\marginnote}[1]{\oldmarginnote{\color{red}\setlength{\parindent}{0cm}\footnotesize #1}}%

\let\oldfootnote\footnote%
\renewcommand*{\footnote}{\oldfootnote[frame]}%

% special frame - pour des diapos sans barre latérale mais avec titre

\newenvironment{fwb}[1] % sans rien
{
    \begingroup
    \advance\textwidth2cm % see beamerthemeGoettingen.sty for the number
    \hsize\textwidth
    \columnwidth\textwidth
    \usebackgroundtemplate{%
      \includegraphics[width=\paperwidth,height=\paperheight]{paper}} 
    \begin{frame}[plain]{#1}
}
{
    \end{frame}
    \endgroup
}

\newenvironment{fwt}[1]  % verticalement
{
    \begingroup
    \advance\textwidth2cm % see beamerthemeGoettingen.sty for the number
    \hsize\textwidth
    \columnwidth\textwidth
    \usebackgroundtemplate{%
      \includegraphics[width=\paperwidth,height=\paperheight]{paper}} 
    \begin{frame}[plain]{#1}
}
{
    \vspace{\fill}
    \end{frame}
    \endgroup
}


%\newcommand{\textwithcomment}[1]{\parbox{.7\textwidth}{\textrm{\small{#1}}}}%


% special frame - pour des diapos sans barre latérale mais avec titre

%special framme - pour des diapos sans barre latérale et sans titre

\newenvironment{fwmb}
{
    \begingroup
    \advance\textwidth2cm % see beamerthemeGoettingen.sty for the number
    \hsize\textwidth
    \columnwidth\textwidth
    \begin{frame}[plain]{}
}
{
    \end{frame}
    \endgroup
}

\newenvironment{fwtc}
{
    \begingroup
    %\advance\textwidth2cm % see beamerthemeGoettingen.sty for the number
    \hsize\textwidth
    \columnwidth\textwidth
    \begin{adjustwidth}{0cm}{0cm}
    \begin{frame}[plain]{title}
    \begin{flushleft}
}
{   
    \end{flushleft}
    \end{frame}
    \end{adjustwidth}
    \endgroup
}

%% une table simple, on passe 1) les colonnes, 2) les titres et 3) les data
% look dans l'esprit de booktab
\newcommand{\tabl}[3]
{
\begin{tabular}{#1}\toprule
#2\\ \midrule
#3\\
\bottomrule
\end{tabular}}

% même chose mais avec des traits verticaux
\newcommand{\tablt}[3]
{
\begin{tabular}{#1}\hline
#2\\ \hline
#3\\
\hline
\end{tabular}}


% macro pour les jolis blocs de textes
% texte sans titre avec signature
\newcommand{\textes}[2]
{
    \begin{block}{}
        \normalfont \scriptsize
	\rightskip=0pt \leftskip=0pt
        \setlength{\parindent}{1cm}
    		%\og
    		#1
    		%\fg{} 
    		%\vspace{-0.3cm}
        \begin{flushright}#2\end{flushright}
    \end{block}
}
% texte sans titre ni signature
\newcommand{\texte}[1]
{
    \begin{block}{}
        \normalfont \scriptsize
	\rightskip=0pt\leftskip=0pt%
        \setlength{\parindent}{1cm}
        %\og
        #1
        %\fg{}%
    \end{block}
}
% texte avec titre sans signature
\newcommand{\textet}[2]
 {    
      \begin{block}{\textbf{#1}}
        \normalfont \scriptsize
	\rightskip=0pt \leftskip=0pt
        \setlength{\parindent}{1cm}
    		%\og
    		#2
    		%\fg{}
        \end{block}
}
% texte avec titre et signature
\newcommand{\textets}[3]
{   
    \begin{block}{\textbf{#1}}
        \normalfont \scriptsize
	\rightskip=0pt \leftskip=0pt
        \setlength{\parindent}{1cm}
    		%\og
    		#2
    		%\fg{} 
    		%\vspace{-0.3cm}
    	\begin{flushright}#3\end{flushright}
        \end{block}
}

% macro pour les video

% video 'la taille s'adapte à ce qui est disponible
\newcommand{\video}[1]
{
\movie[label=cells,showcontrols]{\includegraphics[width=0.8\textwidth,keepaspectratio]{film.png}}{#1}
\href{#1}{\textcolor{lightgray}{ext}}
}

% macro pour les sons (des dummy videos)
\newcommand{\dummyvideo}[1]
{
\movie[width=0.7\textwidth,height=28pt,showcontrols,keepaspectratio]{}{#1}
\href{#1}{\textcolor{lightgray}{ext}}
}
% ------------------ macros pour les images

\newcommand{\diapobrute}[1]{   % pour une diapo image sans ajuster la taille
\begin{figure}
\centering
    \includegraphics{\string#1}
\end{figure}
}%

\newbool{inmini}%
\newbool{incolonne}%
\newbool{intab}%

\newlength{\hauteur}
\newcommand{\calculehauteur}% calcule la hauteur de l'endroit où l'on se trouve
{
\ifthenelse{\boolean{inmini}}
  {% cas des cases carrés
   \setlength{\hauteur}{0.9\textwidth}
  }
  {
  \ifthenelse{\boolean{incolonne}}
     {% case des hautes cases
     \setlength{\hauteur}{0.9*\textheight}
     }
     {% cas des longues cases
     \ifthenelse{\boolean{intab}}
	{% cas des longues cases
	\setlength{\hauteur}{0.4\textwidth}
	}
	{% cas des diapos ordinaires sans tableau
 	\setlength{\hauteur}{0.85\textheight}
 	\vspace{-0.16cm} %attention aux effets de bords!
	}
     }	
  }
}

% à remplacer ?
\newcommand{\nickelimage}[1]
{
\ifthenelse{\boolean{inmini}}
 {% cas des cases carrés
  \begin{figure}\vspace*{\stretch{1}}\includegraphics[width=\textwidth,height=0.8\textwidth,keepaspectratio]{#1}\vspace*{\stretch{1}}\end{figure}
  }
  {
  \ifthenelse{\boolean{incolonne}}
     {% case des hautes cases
     \begin{figure}\vspace*{\stretch{1}}\includegraphics[width=\textwidth,height=0.9\textheight,keepaspectratio]{#1}\vspace*{\stretch{1}}\end{figure}
     }
     {% cas des longues cases
     \ifthenelse{\boolean{intab}}
	{% cas des longues cases
	\begin{figure}\vspace*{\stretch{1}}\includegraphics[width=\textwidth,height=0.4\textwidth,keepaspectratio]{#1}\vspace*{\stretch{1}}\end{figure}
	}
	{% cas des diapos ordinaires sans tableau
	\vspace{-0.10cm}\begin{figure}\vspace*{\stretch{1}}\includegraphics[width=\textwidth,height=0.85\textheight,keepaspectratio]{#1}\vspace*{\stretch{1}}\end{figure}
	}
     }
  }
}

% \newcommand{\nickelimage}[1]
% {
% %   \calculehauteur%
% %   \ifthenelse{\boolean{inmini}}
% %     {}
% %     {\ifthenelse{\boolean{incolonne}}
% %       {}
% %       {\ifthenelse{\boolean{intab}}
% % 	{}
% % 	{\vspace{-0.2cm}}
% %       }	
% %     }
%   \begin{figure}
%     \vspace*{\stretch{1}}\includegraphics[width=\textwidth,height=\hauteur,keepaspectratio]{#1}\vspace*{\stretch{1}}
%   \end{figure}
% }

\newcommand{\img}[1]{   % pour une image, taille automatique en fonction du contexte
\begin{figure}
\centering
    \includegraphics{#1}
\end{figure}
}%

\newcommand{\imgp}[2]{
\begin{figure}\includegraphics[width=3.5cm,height=3.3cm,keepaspectratio]{#1}\caption{#2}\end{figure}
}%

\newcommand{\thumb}[1]{\includegraphics[width=30,height=30]{#1}}

\newcommand{\imgpsl}[1]{
\begin{figure}\includegraphics[width=3.5cm,height=3.5cm,keepaspectratio]{#1}\end{figure}
}%

\newcommand{\imgm}[2]{
\begin{figure}\includegraphics[width=5cm,height=4.2cm,keepaspectratio]{#1}\caption{#2}\end{figure}
}%

\newcommand{\imgmsl}[1]{
\begin{figure}\includegraphics[width=5cm,height=4.7cm,keepaspectratio]{#1}\end{figure}
}%

\newcommand{\imgg}[2]{
\vspace*{\stretch{1}}\begin{figure}\centering\includegraphics[width=0.9\textwidth,height=0.9\textheight,keepaspectratio]{#1}\caption{#2}\end{figure}\vspace*{\stretch{1}} % ,height=.7\textheight
}%

\newcommand{\imggsl}[1]{
\nickelimage{#1}
}%

\renewcommand{\citation}[2]{
\begin{center}
\fcolorbox{yellow}{yellow}{\begin{minipage}[b]{.80\linewidth}\og#1\fg\end{minipage}}
\fcolorbox{yellow}{yellow}{\begin{minipage}[b]{.80\linewidth}\begin{flushright}
      #2\end{flushright}\end{minipage}}
\end{center}}

\newcommand{\cit}[2]{\begin{singlespace}\justifying\textrm{\og#1\fg{}}\vspace{-0.5em}\begin{flushright}
      \textsc{#2}\end{flushright}\end{singlespace}}

\setbeamertemplate{enumerate items}[bigsphere] % les puces des listes sont des boules\newcommand{\mini}[4]{\begin{minipage}[c][#1cm][t]{#2cm}

% pour les graphes

\newcommand{\graphe}[2]{
  \begin{figure}
  \digraph{height=0.9\textheight,width=0.9\textwidth,keepaspectratio}{#1}{#2}
  \end{figure}
}

\newcommand{\graphep}[2]{%
  \begin{figure}%
  \digraph{height=0.4\textheight,width=0.4\textwidth,keepaspectratio}{#1}{#2}%
  \end{figure}%
}

\newcommand{\digraphe}[2]{%
\begin{figure}%
\digraph{height=0.9\textheight,width=0.9\textwidth,keepaspectratio}{#1}{#2}%
\end{figure}%
}

\newcommand{\digraphep}[2]{
\begin{figure}
\digraph{height=0.4\textheight,width=0.4\textwidth,keepaspectratio}{#1}{#2}
\end{figure}
}

% la minipage pour les tableaux dans chaque case d'un tableau

\newcommand{\mini}[4]
{ \fbox{
  \begin{minipage}[c][#1cm][t]{#2cm}
\vspace*{\stretch{1}}
  \parbox{#2cm}{\begin{flushleft} \vspace{-0.1cm}#4\end{flushleft}}
\vspace*{\stretch{1}}
  \end{minipage}
  }
}

\newcommand{\casecarre}[1]
{   
\setboolean{inmini}{true}
\mini{4}{4.2}{4.4}{#1}
\setboolean{inmini}{false}
}

\newcommand{\casehaute}[1]
{
\setboolean{incolonne}{true}
\mini{8}{4.2}{\unecase}{#1}
\setboolean{incolonne}{false}
}

\newcommand{\caselongue}[1]
{
\multicolumn{2}{c}{\mini{4}{9.2}{9.2}{#1}}
}

\newcommand{\tabcccc}[4]
{
\hspace*{-0.2}\makebox[\linewidth][c]{
\begin{tabular}{@{\extracolsep{\fill}}|c|c|}%\begin{tabular}{@{\extracolsep{\fill}}|c|c|}
%hline
\casecarre{#1} & \casecarre{#2}\\
%\hline
\casecarre{#3} & \casecarre{#4}\\
%\hline
\end{tabular}
}}

\newcommand{\tablcc}[3]
{
\hspace*{-0.2}\makebox[\linewidth][c]{
\setboolean{intab}{true}
\begin{tabular}{cc}%\begin{tabular}{|c|c|}
%\hline
\caselongue{#1} \\
%\hline
\casecarre{#2} & \casecarre{#3} \\
%\hline
\end{tabular}
\setboolean{intab}{false}
}}

\newcommand{\tabccl}[3]
{ 
\hspace*{-0.2}\makebox[\linewidth][c]{
\setboolean{intab}{true}
\begin{tabular}{cc}%\begin{tabular}{|c|c|}
%\hline
\casecarre{#1} & \casecarre{#2}\\
%\hline
\caselongue{#3} \\
%\hline
\end{tabular}
\setboolean{intab}{false}
}}

\newcommand{\tabll}[2]
{   
\hspace*{-0.2}\makebox[\linewidth][c]{
\setboolean{intab}{true}
\begin{tabular}{c}%\begin{tabular}{|c|}
%\hline
\mini{4}{9.2}{9.2}{#1}\\
%\hline
\mini{4}{9.2}{9.2}{#2}\\
%\hline
\end{tabular}
\setboolean{intab}{false}
}}

\newcommand{\tabvv}[2]
{ 
\hspace*{-0.2}\makebox[\linewidth][c]{
\begin{tabular}{cc}%\begin{tabular}{|c|c|}
%\hline
\casehaute{#1} & \casehaute{#2}\\
%\hline
\end{tabular}
}}

\setlength{\arrayrulewidth}{0.1pt}% règle pour tous les filets des tableaux
%\setcellgapes{0pt}% les espacement
%\setlength{\tabcolsep}{0em}

\newcommand{\tabvcc}[3]
{
\hspace*{-0.2}\makebox[\linewidth][c]{
\begin{tabular}{cc}%\begin{tabular}{|c|c|}
%\hline
\casehaute{#1} & \begin{tabular}{c}
%\hline
\casecarre{#2}\\
%\hline
\casecarre{#3}\\
%\hline
\end{tabular}\\
%\hline
\end{tabular}
}}

\newcommand{\tabccv}[3]
{
\hspace*{-0.2}\makebox[\linewidth][c]{
\begin{tabular}{cc}%\begin{tabular}{|c|c|}
%\hline
\begin{tabular}{c}
\casecarre{#1}\\
%\hline
\casecarre{#2}\\
\end{tabular} & \casehaute{#3}\\
%\hline
\end{tabular}
}}

\let\olddirtree\dirtree
\renewcommand{\dirtree}[1]
{ 
  \olddirtree{#1}
}

% pour les textes à découper en tranches - utilise un mécanisme analogue à graphviz.sty

% pour une ligne en plus gros \g
\newcommand{\g}[1]{\vfill \Large #1 \normalsize \vfill}

\newcommand{\samplepar}{Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ligula lorem, hendrerit eget venenatis non, volutpat sed est.}
\newcommand{\sampleimg}{\imggsl{sample.jpg}}

\newcommand{\adapte}[1] % usage général: adapate l'image en hauteur et en largeur à la zone où l'on se trouve
  {
  \calculehauteur
  \fbox{
    \centering
    \resizebox{\textwidth}{\the\hauteur}{#1}
    }
  }
  
\newcommand{\test}[1]
  {
  \begin{restofframe}
  \includegraphics[keepaspectratio]{#1.pdf}
  \end{restofframe}
  }
 
\newcommand{\chunk}[1]
  {
  \ifthenelse{\boolean{inmini}}
    {
    \nickelimage{#1.pdf}
    }
    {
    \ifthenelse{\boolean{incolonne}}
      {
      \nickelimage{#1.pdf}
      }
      {
      \ifthenelse{\boolean{intab}}
	{
	\nickelimage{#1.pdf}
	}
	{
	\begin{restofframe}
	  \includegraphics[page=1,width=4cm,keepaspectratio]{#1.pdf}
	\end{restofframe}
	}
      }
    }
  }

\newcommand{\clear}
  {
  \setbeamercolor{background canvas}{bg=}
  }

% une petite merveille pour redimmensionner automatiquement une image sur l'espace restant! Pas vraiment trivial parce que figure n'est pas flottant en beamer
% voir http://tex.stackexchange.com/questions/27315/scale-tikzpicture-to-the-remaining-height-of-a-beamer-frame
\usepackage{zref-savepos}
\newcounter{restofframe}
\newsavebox{\restofframebox}
\newlength{\mylowermargin}
\setlength{\mylowermargin}{2pt} % marge de l'objet et non de la mage

\newenvironment{restofframe}{%
    \par%\centering
    \stepcounter{restofframe}%
    \zsavepos{restofframe-\arabic{restofframe}-begin}%
    \begin{lrbox}{\restofframebox}%
}{%
    \end{lrbox}%
    \setkeys{Gin}{keepaspectratio}%
    \raisebox{\dimexpr-\height+\ht\strutbox\relax}[0pt][0pt]{%
    \resizebox*{\textwidth}{\dimexpr\zposy{restofframe-\arabic{restofframe}-begin}sp-\zposy{restofframe-\arabic{restofframe}-end}sp-\mylowermargin\relax}%
        {\usebox{\restofframebox}}%
    }%
    \vskip0pt plus 1filll\relax
    \mbox{\zsavepos{restofframe-\arabic{restofframe}-end}}%
    \par
}  

\newenvironment{colore}[1][green]%
  {\small\itshape\color{#1}}%
  {}

% % convertit un arbre "naturel" en un arbre perclus de [ pour forest - maintenant traité dans before 
% \newcommand{\cvarbor}[1]
% { 
% \immediate\write18{./brackets #1 #1.for}%
% } 

%pour les minitoc
\newcommand{\tablesection}{%
\setcounter{tocdepth}{2} 
{%\setlength{\baselineskip}{0.1\baselineskip} % OK réglace de l'interligne dans la table des matières de niveau 1
\tableofcontents[currentsection,sectionstyle=show/shaded,subsectionstyle=hide/hide/hide]%
\par}
}

\newcommand{\tablesubsection}{% jamais utilisé ???
\setcounter{tocdepth}{2} 
{%\setlength{\baselineskip}{0.1\baselineskip} % réglace de l'interligne dans la table des matière
\tableofcontents[currentsection,currentsubsection,sectionstyle=show/hide,subsectionstyle=show/shaded/hide]%
\par}
}

\AtBeginSection[]{
 \begin{frame}{}
 \setcounter{tocdepth}{1}
 \normalsize
 %\tableofcontents[currentsection,hideothersubsections,sectionstyle=show/shadow]
 \tableofcontents[currentsection,sectionstyle=show/shaded]
 \end{frame}
}

\AtBeginSubsection[]{
  \begin{frame}{}
  \setcounter{tocdepth}{2}
  \large
  {%\setlength{\baselineskip}{1\baselineskip} % réglace de l'interligne dans la table des matière des sous-sections
  \tableofcontents[currentsection,currentsubsection,sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \par} 
  \end{frame}
}

%\let\@afterindentfalse\@afterindenttrue %indentation à la française des paragraphes
%\@afterindenttrue

$if(title)$
\title{$title$}
$endif$
$if(author)$
\author{$for(author)$$author$$sep$ \and $endfor$}
$endif$
$if(date)$
\date{$date$}
$endif$

%\hypersetup{pdfpagemode=FullScreen}% d'emblée en plein écran - à placer dans le préambule
%\setbeamertemplate{navigation symbols}{}% si l'on veut ne pas montrrer la barre de navigation

\usepackage{totcount}% pour compter les sections et afficher ou pas le sommaire
\newcounter{mytotalcounter}

\begin{document}

\sloopy % as de lignes qui dépassent avec des souci d'hyphénation!
\hyphenpenalty=1000

\def\marginnotetextwidth{0.7\textwidth}% pour indiquer à marginnote la taille du texte % edef ???
\setlength{\marginparwidth}{3.5cm}
$if(title)$
%\MyLogo
\maketitle
$endif$

$for(include-before)$
$include-before$
$endfor$

% Affichage de la table des matières après la page de couverture seulement s'il y a des sections à montrer
\regtotcounter{section}
\setcounter{mytotalcounter}{\totvalue{section}}

\ifthenelse{\equal{\themytotalcounter}{0}}
  {}
  {
  \begin{frame}{Sommaire}
    \setcounter{tocdepth}{1}
    \normalsize
    \tableofcontents
  \end{frame}
  }

$body$

$if(natbib)$
 $if(biblio-files)$
  $if(biblio-title)$
   $if(book-class)$
   \renewcommand\bibname{$biblio-title$}
   $else$
   \renewcommand\refname{$biblio-title$}
   $endif$
  $endif$
  \bibliography{$biblio-files$}
 $endif$
$endif$

$if(biblatex)$
\printbibliography$if(biblio-title)$[title=$biblio-title$]$endif$
$endif$

$for(include-after)$
$include-after$
$endfor$

\end{document}
