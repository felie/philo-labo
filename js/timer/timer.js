var t=[0, 0, 0, 0, 0, 0, 0, 1];
var lap=0;
var laps=[];
var trunc = function(n) { return Number(String(n).replace(/\..*/, "")) }
var ok=1;
var begin=0;
var origin=0;
var duration=0;
var actual=0;
var during=0;
var total=0;
var suspend=0;
var time_suspend=0;
var display_timer=null;
var display_laps=null;

function two(n){
    return n > 9 ? "" + n: "0" + n;
  }
  
function ms(n){
  var milli=trunc(n%1000)
  n=trunc(n/1000);
  var min=two(trunc(n/60));
  return min+':'+two(n-min*60);//+':'+milli;
  }
  
function button(){
  if (suspend!=1){
    if (during==1){
      lap+=1;
      during=0;
      console.log(lap);
      laps.push(duration);
      total+=duration;
      brut_total=actual-origin;
      var avg=laps.map(function(x,i,arr){return x/arr.length}).reduce(function(a,b){return a + b});
      var a = document.createElement("div");
      a.id="div1";
      a.innerHTML+="<table width=500 align=center><tr><td align=center width=100>"+two(lap)+"</td><td align=center>"+ms(brut_total)+"</td><td align=center>"+ms(total)+"</td><td align=center><b>"+ms(duration)+"</b></td><td align=center>"+ms(avg)+"</td></tr></table>";
      display_laps.appendChild(a);
      }
    else{
      begin=(new Date()).valueOf();
      during=1;
      }
    }
  }
  
function reset(){
  origin=(new Date()).valueOf();
  total=0;
  during=0;
  lap=0;
  display_timer.value=ms(0)+':000';
  display_laps.innerHTML='';
  display();
  }
  
function pause(){
 if (suspend==0) {
  suspend=1;
  display_pause.style.color='red';
  }
 else{
  suspend=0;
  time_suspend=actual;
  display_pause.style.color='black';
  }
 }

 function display(){
  actual=(new Date()).valueOf();
  milli=actual%1000;
  duration=actual-begin;
  if (during==1 & suspend==0){
    display_timer.value=ms(duration)+':'+milli;
    }
  }
  
function the_timer() 
{
        origin=(new Date()).valueOf();
	display_timer=document.getElementById('timer');
        display_laps=document.getElementById('laps');
        display_pause=document.getElementById('pause');
        setInterval(display,43);
        reset();
}