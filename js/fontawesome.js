/*!
 * Font Awesome Free 5.7.0 by @fontawesome - https://fontawesome.com
 * License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)
 */
!function() {
    "use strict";
    function r(t) {
        return (r = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
            return typeof t;
        } : function(t) {
            return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
        })(t);
    }
    function i(t, n) {
        for (var e = 0; e < n.length; e++) {
            var a = n[e];
            a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), 
            Object.defineProperty(t, a.key, a);
        }
    }
    function W(r) {
        for (var t = 1; t < arguments.length; t++) {
            var i = null != arguments[t] ? arguments[t] : {}, n = Object.keys(i);
            "function" == typeof Object.getOwnPropertySymbols && (n = n.concat(Object.getOwnPropertySymbols(i).filter(function(t) {
                return Object.getOwnPropertyDescriptor(i, t).enumerable;
            }))), n.forEach(function(t) {
                var n, e, a;
                n = r, a = i[e = t], e in n ? Object.defineProperty(n, e, {
                    value: a,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0
                }) : n[e] = a;
            });
        }
        return r;
    }
    function m(t, n) {
        return function(t) {
            if (Array.isArray(t)) return t;
        }(t) || function(t, n) {
            var e = [], a = !0, r = !1, i = void 0;
            try {
                for (var o, s = t[Symbol.iterator](); !(a = (o = s.next()).done) && (e.push(o.value), 
                !n || e.length !== n); a = !0) ;
            } catch (t) {
                r = !0, i = t;
            } finally {
                try {
                    a || null == s.return || s.return();
                } finally {
                    if (r) throw i;
                }
            }
            return e;
        }(t, n) || function() {
            throw new TypeError("Invalid attempt to destructure non-iterable instance");
        }();
    }
    function d(t) {
        return function(t) {
            if (Array.isArray(t)) {
                for (var n = 0, e = new Array(t.length); n < t.length; n++) e[n] = t[n];
                return e;
            }
        }(t) || function(t) {
            if (Symbol.iterator in Object(t) || "[object Arguments]" === Object.prototype.toString.call(t)) return Array.from(t);
        }(t) || function() {
            throw new TypeError("Invalid attempt to spread non-iterable instance");
        }();
    }
    var t = function() {}, n = {}, e = {}, a = null, o = {
        mark: t,
        measure: t
    };
    try {
        "undefined" != typeof window && (n = window), "undefined" != typeof document && (e = document), 
        "undefined" != typeof MutationObserver && (a = MutationObserver), "undefined" != typeof performance && (o = performance);
    } catch (t) {}
    var s = (n.navigator || {}).userAgent, c = void 0 === s ? "" : s, h = n, g = e, l = a, f = o, u = !!h.document, p = !!g.documentElement && !!g.head && "function" == typeof g.addEventListener && "function" == typeof g.createElement, k = ~c.indexOf("MSIE") || ~c.indexOf("Trident/"), v = "___FONT_AWESOME___", A = 16, b = "fa", y = "svg-inline--fa", U = "data-fa-i2svg", w = "data-fa-pseudo-element", x = "data-fa-pseudo-element-pending", C = "data-prefix", M = "data-icon", N = "fontawesome-i2svg", S = [ "HTML", "HEAD", "STYLE", "SCRIPT" ], z = function() {
        try {
            return !1;
        } catch (t) {
            return !1;
        }
    }(), O = {
        fas: "solid",
        far: "regular",
        fal: "light",
        fab: "brands",
        fa: "solid"
    }, E = {
        solid: "fas",
        regular: "far",
        light: "fal",
        brands: "fab"
    }, j = "fa-layers-text", P = /Font Awesome 5 (Solid|Regular|Light|Brands|Free|Pro)/, L = {
        900: "fas",
        400: "far",
        normal: "far",
        300: "fal"
    }, T = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ], _ = T.concat([ 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 ]), I = [ "class", "data-prefix", "data-icon", "data-fa-transform", "data-fa-mask" ], R = [ "xs", "sm", "lg", "fw", "ul", "li", "border", "pull-left", "pull-right", "spin", "pulse", "rotate-90", "rotate-180", "rotate-270", "flip-horizontal", "flip-vertical", "stack", "stack-1x", "stack-2x", "inverse", "layers", "layers-text", "layers-counter" ].concat(T.map(function(t) {
        return "".concat(t, "x");
    })).concat(_.map(function(t) {
        return "w-".concat(t);
    })), H = h.FontAwesomeConfig || {};
    if (g && "function" == typeof g.querySelector) {
        [ [ "data-family-prefix", "familyPrefix" ], [ "data-replacement-class", "replacementClass" ], [ "data-auto-replace-svg", "autoReplaceSvg" ], [ "data-auto-add-css", "autoAddCss" ], [ "data-auto-a11y", "autoA11y" ], [ "data-search-pseudo-elements", "searchPseudoElements" ], [ "data-observe-mutations", "observeMutations" ], [ "data-keep-original-source", "keepOriginalSource" ], [ "data-measure-performance", "measurePerformance" ], [ "data-show-missing-icons", "showMissingIcons" ] ].forEach(function(t) {
            var n, e = m(t, 2), a = e[0], r = e[1], i = "" === (n = function(t) {
                var n = g.querySelector("script[" + t + "]");
                if (n) return n.getAttribute(t);
            }(a)) || "false" !== n && ("true" === n || n);
            null != i && (H[r] = i);
        });
    }
    var D = W({}, {
        familyPrefix: b,
        replacementClass: y,
        autoReplaceSvg: !0,
        autoAddCss: !0,
        autoA11y: !0,
        searchPseudoElements: !1,
        observeMutations: !0,
        keepOriginalSource: !0,
        measurePerformance: !1,
        showMissingIcons: !0
    }, H);
    D.autoReplaceSvg || (D.observeMutations = !1);
    var V = W({}, D);
    h.FontAwesomeConfig = V;
    var F = h || {};
    F[v] || (F[v] = {}), F[v].styles || (F[v].styles = {}), F[v].hooks || (F[v].hooks = {}), 
    F[v].shims || (F[v].shims = []);
    var X = F[v], B = [], Y = !1;
    function q(t) {
        p && (Y ? setTimeout(t, 0) : B.push(t));
    }
    p && ((Y = (g.documentElement.doScroll ? /^loaded|^c/ : /^loaded|^i|^c/).test(g.readyState)) || g.addEventListener("DOMContentLoaded", function t() {
        g.removeEventListener("DOMContentLoaded", t), Y = 1, B.map(function(t) {
            return t();
        });
    }));
    var K, G = "pending", J = "settled", Q = "fulfilled", Z = "rejected", $ = function() {}, tt = "undefined" != typeof global && void 0 !== global.process && "function" == typeof global.process.emit, nt = "undefined" == typeof setImmediate ? setTimeout : setImmediate, et = [];
    function at() {
        for (var t = 0; t < et.length; t++) et[t][0](et[t][1]);
        K = !(et = []);
    }
    function rt(t, n) {
        et.push([ t, n ]), K || (K = !0, nt(at, 0));
    }
    function it(t) {
        var n = t.owner, e = n._state, a = n._data, r = t[e], i = t.then;
        if ("function" == typeof r) {
            e = Q;
            try {
                a = r(a);
            } catch (t) {
                lt(i, t);
            }
        }
        ot(i, a) || (e === Q && st(i, a), e === Z && lt(i, a));
    }
    function ot(n, e) {
        var a;
        try {
            if (n === e) throw new TypeError("A promises callback cannot return that same promise.");
            if (e && ("function" == typeof e || "object" === r(e))) {
                var t = e.then;
                if ("function" == typeof t) return t.call(e, function(t) {
                    a || (a = !0, e === t ? ct(n, t) : st(n, t));
                }, function(t) {
                    a || (a = !0, lt(n, t));
                }), !0;
            }
        } catch (t) {
            return a || lt(n, t), !0;
        }
        return !1;
    }
    function st(t, n) {
        t !== n && ot(t, n) || ct(t, n);
    }
    function ct(t, n) {
        t._state === G && (t._state = J, t._data = n, rt(ut, t));
    }
    function lt(t, n) {
        t._state === G && (t._state = J, t._data = n, rt(mt, t));
    }
    function ft(t) {
        t._then = t._then.forEach(it);
    }
    function ut(t) {
        t._state = Q, ft(t);
    }
    function mt(t) {
        t._state = Z, ft(t), !t._handled && tt && global.process.emit("unhandledRejection", t._data, t);
    }
    function dt(t) {
        global.process.emit("rejectionHandled", t);
    }
    function ht(t) {
        if ("function" != typeof t) throw new TypeError("Promise resolver " + t + " is not a function");
        if (this instanceof Promise == !1) throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
        this._then = [], function(t, n) {
            function e(t) {
                lt(n, t);
            }
            try {
                t(function(t) {
                    st(n, t);
                }, e);
            } catch (t) {
                e(t);
            }
        }(t, this);
    }
    ht.prototype = {
        constructor: ht,
        _state: G,
        _then: null,
        _data: void 0,
        _handled: !1,
        then: function(t, n) {
            var e = {
                owner: this,
                then: new this.constructor($),
                fulfilled: t,
                rejected: n
            };
            return !n && !t || this._handled || (this._handled = !0, this._state === Z && tt && rt(dt, this)), 
            this._state === Q || this._state === Z ? rt(it, e) : this._then.push(e), e.then;
        },
        catch: function(t) {
            return this.then(null, t);
        }
    }, ht.all = function(s) {
        if (!Array.isArray(s)) throw new TypeError("You must pass an array to Promise.all().");
        return new ht(function(e, t) {
            var a = [], r = 0;
            function n(n) {
                return r++, function(t) {
                    a[n] = t, --r || e(a);
                };
            }
            for (var i, o = 0; o < s.length; o++) (i = s[o]) && "function" == typeof i.then ? i.then(n(o), t) : a[o] = i;
            r || e(a);
        });
    }, ht.race = function(r) {
        if (!Array.isArray(r)) throw new TypeError("You must pass an array to Promise.race().");
        return new ht(function(t, n) {
            for (var e, a = 0; a < r.length; a++) (e = r[a]) && "function" == typeof e.then ? e.then(t, n) : t(e);
        });
    }, ht.resolve = function(n) {
        return n && "object" === r(n) && n.constructor === ht ? n : new ht(function(t) {
            t(n);
        });
    }, ht.reject = function(e) {
        return new ht(function(t, n) {
            n(e);
        });
    };
    var gt = "function" == typeof Promise ? Promise : ht, pt = A, vt = {
        size: 16,
        x: 0,
        y: 0,
        rotate: 0,
        flipX: !1,
        flipY: !1
    };
    function bt(t) {
        if (t && p) {
            var n = g.createElement("style");
            n.setAttribute("type", "text/css"), n.innerHTML = t;
            for (var e = g.head.childNodes, a = null, r = e.length - 1; -1 < r; r--) {
                var i = e[r], o = (i.tagName || "").toUpperCase();
                -1 < [ "STYLE", "LINK" ].indexOf(o) && (a = i);
            }
            return g.head.insertBefore(n, a), t;
        }
    }
    var yt = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    function wt() {
        for (var t = 12, n = ""; 0 < t--; ) n += yt[62 * Math.random() | 0];
        return n;
    }
    function xt(t) {
        for (var n = [], e = (t || []).length >>> 0; e--; ) n[e] = t[e];
        return n;
    }
    function kt(t) {
        return t.classList ? xt(t.classList) : (t.getAttribute("class") || "").split(" ").filter(function(t) {
            return t;
        });
    }
    function At(t, n) {
        var e, a = n.split("-"), r = a[0], i = a.slice(1).join("-");
        return r !== t || "" === i || (e = i, ~R.indexOf(e)) ? null : i;
    }
    function Ct(t) {
        return "".concat(t).replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/'/g, "&#39;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    }
    function Mt(e) {
        return Object.keys(e || {}).reduce(function(t, n) {
            return t + "".concat(n, ": ").concat(e[n], ";");
        }, "");
    }
    function Nt(t) {
        return t.size !== vt.size || t.x !== vt.x || t.y !== vt.y || t.rotate !== vt.rotate || t.flipX || t.flipY;
    }
    function St(t) {
        var n = t.transform, e = t.containerWidth, a = t.iconWidth, r = {
            transform: "translate(".concat(e / 2, " 256)")
        }, i = "translate(".concat(32 * n.x, ", ").concat(32 * n.y, ") "), o = "scale(".concat(n.size / 16 * (n.flipX ? -1 : 1), ", ").concat(n.size / 16 * (n.flipY ? -1 : 1), ") "), s = "rotate(".concat(n.rotate, " 0 0)");
        return {
            outer: r,
            inner: {
                transform: "".concat(i, " ").concat(o, " ").concat(s)
            },
            path: {
                transform: "translate(".concat(a / 2 * -1, " -256)")
            }
        };
    }
    var zt = {
        x: 0,
        y: 0,
        width: "100%",
        height: "100%"
    };
    function Ot(t) {
        var n = t.icons, e = n.main, a = n.mask, r = t.prefix, i = t.iconName, o = t.transform, s = t.symbol, c = t.title, l = t.extra, f = t.watchable, u = void 0 !== f && f, m = a.found ? a : e, d = m.width, h = m.height, g = "fa-w-".concat(Math.ceil(d / h * 16)), p = [ V.replacementClass, i ? "".concat(V.familyPrefix, "-").concat(i) : "", g ].filter(function(t) {
            return -1 === l.classes.indexOf(t);
        }).concat(l.classes).join(" "), v = {
            children: [],
            attributes: W({}, l.attributes, {
                "data-prefix": r,
                "data-icon": i,
                class: p,
                role: "img",
                xmlns: "http://www.w3.org/2000/svg",
                viewBox: "0 0 ".concat(d, " ").concat(h)
            })
        };
        u && (v.attributes[U] = ""), c && v.children.push({
            tag: "title",
            attributes: {
                id: v.attributes["aria-labelledby"] || "title-".concat(wt())
            },
            children: [ c ]
        });
        var b, y, w, x, k, A, C, M, N, S, z, O, E, j, P, L, T, _, I, R, H, D, F, X = W({}, v, {
            prefix: r,
            iconName: i,
            main: e,
            mask: a,
            transform: o,
            symbol: s,
            styles: l.styles
        }), B = a.found && e.found ? (y = (b = X).children, w = b.attributes, x = b.main, 
        k = b.mask, A = b.transform, C = x.width, M = x.icon, N = k.width, S = k.icon, z = St({
            transform: A,
            containerWidth: N,
            iconWidth: C
        }), O = {
            tag: "rect",
            attributes: W({}, zt, {
                fill: "white"
            })
        }, E = {
            tag: "g",
            attributes: W({}, z.inner),
            children: [ {
                tag: "path",
                attributes: W({}, M.attributes, z.path, {
                    fill: "black"
                })
            } ]
        }, j = {
            tag: "g",
            attributes: W({}, z.outer),
            children: [ E ]
        }, P = "mask-".concat(wt()), L = "clip-".concat(wt()), T = {
            tag: "defs",
            children: [ {
                tag: "clipPath",
                attributes: {
                    id: L
                },
                children: [ S ]
            }, {
                tag: "mask",
                attributes: W({}, zt, {
                    id: P,
                    maskUnits: "userSpaceOnUse",
                    maskContentUnits: "userSpaceOnUse"
                }),
                children: [ O, j ]
            } ]
        }, y.push(T, {
            tag: "rect",
            attributes: W({
                fill: "currentColor",
                "clip-path": "url(#".concat(L, ")"),
                mask: "url(#".concat(P, ")")
            }, zt)
        }), {
            children: y,
            attributes: w
        }) : function(t) {
            var n = t.children, e = t.attributes, a = t.main, r = t.transform, i = Mt(t.styles);
            if (0 < i.length && (e.style = i), Nt(r)) {
                var o = St({
                    transform: r,
                    containerWidth: a.width,
                    iconWidth: a.width
                });
                n.push({
                    tag: "g",
                    attributes: W({}, o.outer),
                    children: [ {
                        tag: "g",
                        attributes: W({}, o.inner),
                        children: [ {
                            tag: a.icon.tag,
                            children: a.icon.children,
                            attributes: W({}, a.icon.attributes, o.path)
                        } ]
                    } ]
                });
            } else n.push(a.icon);
            return {
                children: n,
                attributes: e
            };
        }(X), Y = B.children, q = B.attributes;
        return X.children = Y, X.attributes = q, s ? (I = (_ = X).prefix, R = _.iconName, 
        H = _.children, D = _.attributes, F = _.symbol, [ {
            tag: "svg",
            attributes: {
                style: "display: none;"
            },
            children: [ {
                tag: "symbol",
                attributes: W({}, D, {
                    id: !0 === F ? "".concat(I, "-").concat(V.familyPrefix, "-").concat(R) : F
                }),
                children: H
            } ]
        } ]) : function(t) {
            var n = t.children, e = t.main, a = t.mask, r = t.attributes, i = t.styles, o = t.transform;
            if (Nt(o) && e.found && !a.found) {
                var s = e.width / e.height / 2, c = .5;
                r.style = Mt(W({}, i, {
                    "transform-origin": "".concat(s + o.x / 16, "em ").concat(c + o.y / 16, "em")
                }));
            }
            return [ {
                tag: "svg",
                attributes: r,
                children: n
            } ];
        }(X);
    }
    function Et(t) {
        var n = t.content, e = t.width, a = t.height, r = t.transform, i = t.title, o = t.extra, s = t.watchable, c = void 0 !== s && s, l = W({}, o.attributes, i ? {
            title: i
        } : {}, {
            class: o.classes.join(" ")
        });
        c && (l[U] = "");
        var f, u, m, d, h, g, p, v, b, y = W({}, o.styles);
        Nt(r) && (y.transform = (u = (f = {
            transform: r,
            startCentered: !0,
            width: e,
            height: a
        }).transform, m = f.width, d = void 0 === m ? A : m, h = f.height, g = void 0 === h ? A : h, 
        p = f.startCentered, b = "", b += (v = void 0 !== p && p) && k ? "translate(".concat(u.x / pt - d / 2, "em, ").concat(u.y / pt - g / 2, "em) ") : v ? "translate(calc(-50% + ".concat(u.x / pt, "em), calc(-50% + ").concat(u.y / pt, "em)) ") : "translate(".concat(u.x / pt, "em, ").concat(u.y / pt, "em) "), 
        b += "scale(".concat(u.size / pt * (u.flipX ? -1 : 1), ", ").concat(u.size / pt * (u.flipY ? -1 : 1), ") "), 
        b += "rotate(".concat(u.rotate, "deg) ")), y["-webkit-transform"] = y.transform);
        var w = Mt(y);
        0 < w.length && (l.style = w);
        var x = [];
        return x.push({
            tag: "span",
            attributes: l,
            children: [ n ]
        }), i && x.push({
            tag: "span",
            attributes: {
                class: "sr-only"
            },
            children: [ i ]
        }), x;
    }
    var jt = function() {}, Pt = V.measurePerformance && f && f.mark && f.measure ? f : {
        mark: jt,
        measure: jt
    }, Lt = 'FA "5.7.0"', Tt = function(t) {
        Pt.mark("".concat(Lt, " ").concat(t, " ends")), Pt.measure("".concat(Lt, " ").concat(t), "".concat(Lt, " ").concat(t, " begins"), "".concat(Lt, " ").concat(t, " ends"));
    }, _t = {
        begin: function(t) {
            return Pt.mark("".concat(Lt, " ").concat(t, " begins")), function() {
                return Tt(t);
            };
        },
        end: Tt
    }, It = function(t, n, e, a) {
        var r, i, o, s, c, l = Object.keys(t), f = l.length, u = void 0 !== a ? (s = n, 
        c = a, function(t, n, e, a) {
            return s.call(c, t, n, e, a);
        }) : n;
        for (o = void 0 === e ? (r = 1, t[l[0]]) : (r = 0, e); r < f; r++) o = u(o, t[i = l[r]], i, t);
        return o;
    };
    var Rt = X.styles, Ht = X.shims, Dt = {}, Ft = {}, Xt = {}, Bt = function() {
        var t = function(a) {
            return It(Rt, function(t, n, e) {
                return t[e] = It(n, a, {}), t;
            }, {});
        };
        Dt = t(function(t, n, e) {
            return n[3] && (t[n[3]] = e), t;
        }), Ft = t(function(n, t, e) {
            var a = t[2];
            return n[e] = e, a.forEach(function(t) {
                n[t] = e;
            }), n;
        });
        var i = "far" in Rt;
        Xt = It(Ht, function(t, n) {
            var e = n[0], a = n[1], r = n[2];
            return "far" !== a || i || (a = "fas"), t[e] = {
                prefix: a,
                iconName: r
            }, t;
        }, {});
    };
    function Yt(t, n) {
        return Dt[t][n];
    }
    Bt();
    var qt = X.styles, Wt = function() {
        return {
            prefix: null,
            iconName: null,
            rest: []
        };
    };
    function Ut(t) {
        return t.reduce(function(t, n) {
            var e = At(V.familyPrefix, n);
            if (qt[n]) t.prefix = n; else if (V.autoFetchSvg && -1 < [ "fas", "far", "fal", "fab", "fa" ].indexOf(n)) t.prefix = n; else if (e) {
                var a = "fa" === t.prefix ? Xt[e] || {
                    prefix: null,
                    iconName: null
                } : {};
                t.iconName = a.iconName || e, t.prefix = a.prefix || t.prefix;
            } else n !== V.replacementClass && 0 !== n.indexOf("fa-w-") && t.rest.push(n);
            return t;
        }, Wt());
    }
    function Vt(t, n, e) {
        if (t && t[n] && t[n][e]) return {
            prefix: n,
            iconName: e,
            icon: t[n][e]
        };
    }
    function Kt(t) {
        var e, n = t.tag, a = t.attributes, r = void 0 === a ? {} : a, i = t.children, o = void 0 === i ? [] : i;
        return "string" == typeof t ? Ct(t) : "<".concat(n, " ").concat((e = r, Object.keys(e || {}).reduce(function(t, n) {
            return t + "".concat(n, '="').concat(Ct(e[n]), '" ');
        }, "").trim()), ">").concat(o.map(Kt).join(""), "</").concat(n, ">");
    }
    var Gt = function() {};
    function Jt(t) {
        return "string" == typeof (t.getAttribute ? t.getAttribute(U) : null);
    }
    var Qt = {
        replace: function(t) {
            var n = t[0], e = t[1].map(function(t) {
                return Kt(t);
            }).join("\n");
            if (n.parentNode && n.outerHTML) n.outerHTML = e + (V.keepOriginalSource && "svg" !== n.tagName.toLowerCase() ? "\x3c!-- ".concat(n.outerHTML, " --\x3e") : ""); else if (n.parentNode) {
                var a = document.createElement("span");
                n.parentNode.replaceChild(a, n), a.outerHTML = e;
            }
        },
        nest: function(t) {
            var n = t[0], e = t[1];
            if (~kt(n).indexOf(V.replacementClass)) return Qt.replace(t);
            var a = new RegExp("".concat(V.familyPrefix, "-.*"));
            delete e[0].attributes.style;
            var r = e[0].attributes.class.split(" ").reduce(function(t, n) {
                return n === V.replacementClass || n.match(a) ? t.toSvg.push(n) : t.toNode.push(n), 
                t;
            }, {
                toNode: [],
                toSvg: []
            });
            e[0].attributes.class = r.toSvg.join(" ");
            var i = e.map(function(t) {
                return Kt(t);
            }).join("\n");
            n.setAttribute("class", r.toNode.join(" ")), n.setAttribute(U, ""), n.innerHTML = i;
        }
    };
    function Zt(e, t) {
        var a = "function" == typeof t ? t : Gt;
        0 === e.length ? a() : (h.requestAnimationFrame || function(t) {
            return t();
        })(function() {
            var t = !0 === V.autoReplaceSvg ? Qt.replace : Qt[V.autoReplaceSvg] || Qt.replace, n = _t.begin("mutate");
            e.map(t), n(), a();
        });
    }
    var $t = !1;
    function tn() {
        $t = !1;
    }
    var nn = null;
    function en(t) {
        if (l && V.observeMutations) {
            var r = t.treeCallback, i = t.nodeCallback, o = t.pseudoElementsCallback, n = t.observeMutationsRoot, e = void 0 === n ? g : n;
            nn = new l(function(t) {
                $t || xt(t).forEach(function(t) {
                    if ("childList" === t.type && 0 < t.addedNodes.length && !Jt(t.addedNodes[0]) && (V.searchPseudoElements && o(t.target), 
                    r(t.target)), "attributes" === t.type && t.target.parentNode && V.searchPseudoElements && o(t.target.parentNode), 
                    "attributes" === t.type && Jt(t.target) && ~I.indexOf(t.attributeName)) if ("class" === t.attributeName) {
                        var n = Ut(kt(t.target)), e = n.prefix, a = n.iconName;
                        e && t.target.setAttribute("data-prefix", e), a && t.target.setAttribute("data-icon", a);
                    } else i(t.target);
                });
            }), p && nn.observe(e, {
                childList: !0,
                attributes: !0,
                characterData: !0,
                subtree: !0
            });
        }
    }
    function an(t) {
        for (var n = "", e = 0; e < t.length; e++) {
            n += ("000" + t.charCodeAt(e).toString(16)).slice(-4);
        }
        return n;
    }
    function rn(t) {
        var n, e, a = t.getAttribute("data-prefix"), r = t.getAttribute("data-icon"), i = void 0 !== t.innerText ? t.innerText.trim() : "", o = Ut(kt(t));
        return a && r && (o.prefix = a, o.iconName = r), o.prefix && 1 < i.length ? o.iconName = (n = o.prefix, 
        e = t.innerText, Ft[n][e]) : o.prefix && 1 === i.length && (o.iconName = Yt(o.prefix, an(t.innerText))), 
        o;
    }
    var on = function(t) {
        var n = {
            size: 16,
            x: 0,
            y: 0,
            flipX: !1,
            flipY: !1,
            rotate: 0
        };
        return t ? t.toLowerCase().split(" ").reduce(function(t, n) {
            var e = n.toLowerCase().split("-"), a = e[0], r = e.slice(1).join("-");
            if (a && "h" === r) return t.flipX = !0, t;
            if (a && "v" === r) return t.flipY = !0, t;
            if (r = parseFloat(r), isNaN(r)) return t;
            switch (a) {
              case "grow":
                t.size = t.size + r;
                break;

              case "shrink":
                t.size = t.size - r;
                break;

              case "left":
                t.x = t.x - r;
                break;

              case "right":
                t.x = t.x + r;
                break;

              case "up":
                t.y = t.y - r;
                break;

              case "down":
                t.y = t.y + r;
                break;

              case "rotate":
                t.rotate = t.rotate + r;
            }
            return t;
        }, n) : n;
    };
    function sn(t) {
        var n, e, a, r, i, o, s, c = rn(t), l = c.iconName, f = c.prefix, u = c.rest, m = (n = t.getAttribute("style"), 
        e = [], n && (e = n.split(";").reduce(function(t, n) {
            var e = n.split(":"), a = e[0], r = e.slice(1);
            return a && 0 < r.length && (t[a] = r.join(":").trim()), t;
        }, {})), e), d = on(t.getAttribute("data-fa-transform")), h = null !== (a = t.getAttribute("data-fa-symbol")) && ("" === a || a), g = (i = xt((r = t).attributes).reduce(function(t, n) {
            return "class" !== t.name && "style" !== t.name && (t[n.name] = n.value), t;
        }, {}), o = r.getAttribute("title"), V.autoA11y && (o ? i["aria-labelledby"] = "".concat(V.replacementClass, "-title-").concat(wt()) : (i["aria-hidden"] = "true", 
        i.focusable = "false")), i), p = (s = t.getAttribute("data-fa-mask")) ? Ut(s.split(" ").map(function(t) {
            return t.trim();
        })) : Wt();
        return {
            iconName: l,
            title: t.getAttribute("title"),
            prefix: f,
            transform: d,
            symbol: h,
            mask: p,
            extra: {
                classes: u,
                styles: m,
                attributes: g
            }
        };
    }
    function cn(t) {
        this.name = "MissingIcon", this.message = t || "Icon unavailable", this.stack = new Error().stack;
    }
    (cn.prototype = Object.create(Error.prototype)).constructor = cn;
    var ln = {
        fill: "currentColor"
    }, fn = {
        attributeType: "XML",
        repeatCount: "indefinite",
        dur: "2s"
    }, un = {
        tag: "path",
        attributes: W({}, ln, {
            d: "M156.5,447.7l-12.6,29.5c-18.7-9.5-35.9-21.2-51.5-34.9l22.7-22.7C127.6,430.5,141.5,440,156.5,447.7z M40.6,272H8.5 c1.4,21.2,5.4,41.7,11.7,61.1L50,321.2C45.1,305.5,41.8,289,40.6,272z M40.6,240c1.4-18.8,5.2-37,11.1-54.1l-29.5-12.6 C14.7,194.3,10,216.7,8.5,240H40.6z M64.3,156.5c7.8-14.9,17.2-28.8,28.1-41.5L69.7,92.3c-13.7,15.6-25.5,32.8-34.9,51.5 L64.3,156.5z M397,419.6c-13.9,12-29.4,22.3-46.1,30.4l11.9,29.8c20.7-9.9,39.8-22.6,56.9-37.6L397,419.6z M115,92.4 c13.9-12,29.4-22.3,46.1-30.4l-11.9-29.8c-20.7,9.9-39.8,22.6-56.8,37.6L115,92.4z M447.7,355.5c-7.8,14.9-17.2,28.8-28.1,41.5 l22.7,22.7c13.7-15.6,25.5-32.9,34.9-51.5L447.7,355.5z M471.4,272c-1.4,18.8-5.2,37-11.1,54.1l29.5,12.6 c7.5-21.1,12.2-43.5,13.6-66.8H471.4z M321.2,462c-15.7,5-32.2,8.2-49.2,9.4v32.1c21.2-1.4,41.7-5.4,61.1-11.7L321.2,462z M240,471.4c-18.8-1.4-37-5.2-54.1-11.1l-12.6,29.5c21.1,7.5,43.5,12.2,66.8,13.6V471.4z M462,190.8c5,15.7,8.2,32.2,9.4,49.2h32.1 c-1.4-21.2-5.4-41.7-11.7-61.1L462,190.8z M92.4,397c-12-13.9-22.3-29.4-30.4-46.1l-29.8,11.9c9.9,20.7,22.6,39.8,37.6,56.9 L92.4,397z M272,40.6c18.8,1.4,36.9,5.2,54.1,11.1l12.6-29.5C317.7,14.7,295.3,10,272,8.5V40.6z M190.8,50 c15.7-5,32.2-8.2,49.2-9.4V8.5c-21.2,1.4-41.7,5.4-61.1,11.7L190.8,50z M442.3,92.3L419.6,115c12,13.9,22.3,29.4,30.5,46.1 l29.8-11.9C470,128.5,457.3,109.4,442.3,92.3z M397,92.4l22.7-22.7c-15.6-13.7-32.8-25.5-51.5-34.9l-12.6,29.5 C370.4,72.1,384.4,81.5,397,92.4z"
        })
    }, mn = W({}, fn, {
        attributeName: "opacity"
    }), dn = {
        tag: "g",
        children: [ un, {
            tag: "circle",
            attributes: W({}, ln, {
                cx: "256",
                cy: "364",
                r: "28"
            }),
            children: [ {
                tag: "animate",
                attributes: W({}, fn, {
                    attributeName: "r",
                    values: "28;14;28;28;14;28;"
                })
            }, {
                tag: "animate",
                attributes: W({}, mn, {
                    values: "1;0;1;1;0;1;"
                })
            } ]
        }, {
            tag: "path",
            attributes: W({}, ln, {
                opacity: "1",
                d: "M263.7,312h-16c-6.6,0-12-5.4-12-12c0-71,77.4-63.9,77.4-107.8c0-20-17.8-40.2-57.4-40.2c-29.1,0-44.3,9.6-59.2,28.7 c-3.9,5-11.1,6-16.2,2.4l-13.1-9.2c-5.6-3.9-6.9-11.8-2.6-17.2c21.2-27.2,46.4-44.7,91.2-44.7c52.3,0,97.4,29.8,97.4,80.2 c0,67.6-77.4,63.5-77.4,107.8C275.7,306.6,270.3,312,263.7,312z"
            }),
            children: [ {
                tag: "animate",
                attributes: W({}, mn, {
                    values: "1;0;0;0;0;1;"
                })
            } ]
        }, {
            tag: "path",
            attributes: W({}, ln, {
                opacity: "0",
                d: "M232.5,134.5l7,168c0.3,6.4,5.6,11.5,12,11.5h9c6.4,0,11.7-5.1,12-11.5l7-168c0.3-6.8-5.2-12.5-12-12.5h-23 C237.7,122,232.2,127.7,232.5,134.5z"
            }),
            children: [ {
                tag: "animate",
                attributes: W({}, mn, {
                    values: "0;0;1;1;0;0;"
                })
            } ]
        } ]
    }, hn = X.styles;
    function gn(r, i) {
        return new gt(function(t, n) {
            var e = {
                found: !1,
                width: 512,
                height: 512,
                icon: dn
            };
            if (r && i && hn[i] && hn[i][r]) {
                var a = hn[i][r];
                return t(e = {
                    found: !0,
                    width: a[0],
                    height: a[1],
                    icon: {
                        tag: "path",
                        attributes: {
                            fill: "currentColor",
                            d: a.slice(4)[0]
                        }
                    }
                });
            }
            r && i && !V.showMissingIcons ? n(new cn("Icon is missing for prefix ".concat(i, " with icon name ").concat(r))) : t(e);
        });
    }
    var pn = X.styles;
    function vn(t) {
        var i, n, o, s, c, l, f, e, u, a = sn(t);
        return ~a.extra.classes.indexOf(j) ? function(t, n) {
            var e = n.title, a = n.transform, r = n.extra, i = null, o = null;
            if (k) {
                var s = parseInt(getComputedStyle(t).fontSize, 10), c = t.getBoundingClientRect();
                i = c.width / s, o = c.height / s;
            }
            return V.autoA11y && !e && (r.attributes["aria-hidden"] = "true"), gt.resolve([ t, Et({
                content: t.innerHTML,
                width: i,
                height: o,
                transform: a,
                title: e,
                extra: r,
                watchable: !0
            }) ]);
        }(t, a) : (i = t, o = (n = a).iconName, s = n.title, c = n.prefix, l = n.transform, 
        f = n.symbol, e = n.mask, u = n.extra, new gt(function(r, t) {
            gt.all([ gn(o, c), gn(e.iconName, e.prefix) ]).then(function(t) {
                var n = m(t, 2), e = n[0], a = n[1];
                r([ i, Ot({
                    icons: {
                        main: e,
                        mask: a
                    },
                    prefix: c,
                    iconName: o,
                    transform: l,
                    symbol: f,
                    mask: a,
                    title: s,
                    extra: u,
                    watchable: !0
                }) ]);
            });
        }));
    }
    function bn(t) {
        var e = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : null;
        if (p) {
            var n = g.documentElement.classList, a = function(t) {
                return n.add("".concat(N, "-").concat(t));
            }, r = function(t) {
                return n.remove("".concat(N, "-").concat(t));
            }, i = V.autoFetchSvg ? Object.keys(O) : Object.keys(pn), o = [ ".".concat(j, ":not([").concat(U, "])") ].concat(i.map(function(t) {
                return ".".concat(t, ":not([").concat(U, "])");
            })).join(", ");
            if (0 !== o.length) {
                var s = xt(t.querySelectorAll(o));
                if (0 < s.length) {
                    a("pending"), r("complete");
                    var c = _t.begin("onTree"), l = s.reduce(function(t, n) {
                        try {
                            var e = vn(n);
                            e && t.push(e);
                        } catch (t) {
                            z || t instanceof cn && console.error(t);
                        }
                        return t;
                    }, []);
                    return new gt(function(n, t) {
                        gt.all(l).then(function(t) {
                            Zt(t, function() {
                                a("active"), a("complete"), r("pending"), "function" == typeof e && e(), n();
                            });
                        }).catch(t).finally(c);
                    });
                }
            }
        }
    }
    function yn(t) {
        var n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : null;
        vn(t).then(function(t) {
            t && Zt([ t ], n);
        });
    }
    function wn(u, m) {
        var d = "".concat(x).concat(m.replace(":", "-"));
        return new gt(function(a, t) {
            if (null !== u.getAttribute(d)) return a();
            var n = xt(u.children).filter(function(t) {
                return t.getAttribute(w) === m;
            })[0], e = h.getComputedStyle(u, m), r = e.getPropertyValue("font-family").match(P), i = e.getPropertyValue("font-weight");
            if (n && !r) return u.removeChild(n), a();
            if (r) {
                var o = e.getPropertyValue("content"), s = ~[ "Light", "Regular", "Solid", "Brands" ].indexOf(r[1]) ? E[r[1].toLowerCase()] : L[i], c = Yt(s, an(3 === o.length ? o.substr(1, 1) : o));
                if (n && n.getAttribute(C) === s && n.getAttribute(M) === c) a(); else {
                    u.setAttribute(d, c), n && u.removeChild(n);
                    var l = {
                        iconName: null,
                        title: null,
                        prefix: null,
                        transform: vt,
                        symbol: !1,
                        mask: null,
                        extra: {
                            classes: [],
                            styles: {},
                            attributes: {}
                        }
                    }, f = l.extra;
                    f.attributes[w] = m, gn(c, s).then(function(t) {
                        var n = Ot(W({}, l, {
                            icons: {
                                main: t,
                                mask: Wt()
                            },
                            prefix: s,
                            iconName: c,
                            extra: f,
                            watchable: !0
                        })), e = g.createElement("svg");
                        ":before" === m ? u.insertBefore(e, u.firstChild) : u.appendChild(e), e.outerHTML = n.map(function(t) {
                            return Kt(t);
                        }).join("\n"), u.removeAttribute(d), a();
                    }).catch(t);
                }
            } else a();
        });
    }
    function xn(t) {
        return gt.all([ wn(t, ":before"), wn(t, ":after") ]);
    }
    function kn(t) {
        return !(t.parentNode === document.head || ~S.indexOf(t.tagName.toUpperCase()) || t.getAttribute(w) || t.parentNode && "svg" === t.parentNode.tagName);
    }
    function An(r) {
        if (p) return new gt(function(t, n) {
            var e = xt(r.querySelectorAll("*")).filter(kn).map(xn), a = _t.begin("searchPseudoElements");
            $t = !0, gt.all(e).then(function() {
                a(), tn(), t();
            }).catch(function() {
                a(), tn(), n();
            });
        });
    }
    var Cn = 'svg:not(:root).svg-inline--fa {\n  overflow: visible;\n}\n\n.svg-inline--fa {\n  display: inline-block;\n  font-size: inherit;\n  height: 1em;\n  overflow: visible;\n  vertical-align: -0.125em;\n}\n.svg-inline--fa.fa-lg {\n  vertical-align: -0.225em;\n}\n.svg-inline--fa.fa-w-1 {\n  width: 0.0625em;\n}\n.svg-inline--fa.fa-w-2 {\n  width: 0.125em;\n}\n.svg-inline--fa.fa-w-3 {\n  width: 0.1875em;\n}\n.svg-inline--fa.fa-w-4 {\n  width: 0.25em;\n}\n.svg-inline--fa.fa-w-5 {\n  width: 0.3125em;\n}\n.svg-inline--fa.fa-w-6 {\n  width: 0.375em;\n}\n.svg-inline--fa.fa-w-7 {\n  width: 0.4375em;\n}\n.svg-inline--fa.fa-w-8 {\n  width: 0.5em;\n}\n.svg-inline--fa.fa-w-9 {\n  width: 0.5625em;\n}\n.svg-inline--fa.fa-w-10 {\n  width: 0.625em;\n}\n.svg-inline--fa.fa-w-11 {\n  width: 0.6875em;\n}\n.svg-inline--fa.fa-w-12 {\n  width: 0.75em;\n}\n.svg-inline--fa.fa-w-13 {\n  width: 0.8125em;\n}\n.svg-inline--fa.fa-w-14 {\n  width: 0.875em;\n}\n.svg-inline--fa.fa-w-15 {\n  width: 0.9375em;\n}\n.svg-inline--fa.fa-w-16 {\n  width: 1em;\n}\n.svg-inline--fa.fa-w-17 {\n  width: 1.0625em;\n}\n.svg-inline--fa.fa-w-18 {\n  width: 1.125em;\n}\n.svg-inline--fa.fa-w-19 {\n  width: 1.1875em;\n}\n.svg-inline--fa.fa-w-20 {\n  width: 1.25em;\n}\n.svg-inline--fa.fa-pull-left {\n  margin-right: 0.3em;\n  width: auto;\n}\n.svg-inline--fa.fa-pull-right {\n  margin-left: 0.3em;\n  width: auto;\n}\n.svg-inline--fa.fa-border {\n  height: 1.5em;\n}\n.svg-inline--fa.fa-li {\n  width: 2em;\n}\n.svg-inline--fa.fa-fw {\n  width: 1.25em;\n}\n\n.fa-layers svg.svg-inline--fa {\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  position: absolute;\n  right: 0;\n  top: 0;\n}\n\n.fa-layers {\n  display: inline-block;\n  height: 1em;\n  position: relative;\n  text-align: center;\n  vertical-align: -0.125em;\n  width: 1em;\n}\n.fa-layers svg.svg-inline--fa {\n  -webkit-transform-origin: center center;\n          transform-origin: center center;\n}\n\n.fa-layers-counter, .fa-layers-text {\n  display: inline-block;\n  position: absolute;\n  text-align: center;\n}\n\n.fa-layers-text {\n  left: 50%;\n  top: 50%;\n  -webkit-transform: translate(-50%, -50%);\n          transform: translate(-50%, -50%);\n  -webkit-transform-origin: center center;\n          transform-origin: center center;\n}\n\n.fa-layers-counter {\n  background-color: #ff253a;\n  border-radius: 1em;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  color: #fff;\n  height: 1.5em;\n  line-height: 1;\n  max-width: 5em;\n  min-width: 1.5em;\n  overflow: hidden;\n  padding: 0.25em;\n  right: 0;\n  text-overflow: ellipsis;\n  top: 0;\n  -webkit-transform: scale(0.25);\n          transform: scale(0.25);\n  -webkit-transform-origin: top right;\n          transform-origin: top right;\n}\n\n.fa-layers-bottom-right {\n  bottom: 0;\n  right: 0;\n  top: auto;\n  -webkit-transform: scale(0.25);\n          transform: scale(0.25);\n  -webkit-transform-origin: bottom right;\n          transform-origin: bottom right;\n}\n\n.fa-layers-bottom-left {\n  bottom: 0;\n  left: 0;\n  right: auto;\n  top: auto;\n  -webkit-transform: scale(0.25);\n          transform: scale(0.25);\n  -webkit-transform-origin: bottom left;\n          transform-origin: bottom left;\n}\n\n.fa-layers-top-right {\n  right: 0;\n  top: 0;\n  -webkit-transform: scale(0.25);\n          transform: scale(0.25);\n  -webkit-transform-origin: top right;\n          transform-origin: top right;\n}\n\n.fa-layers-top-left {\n  left: 0;\n  right: auto;\n  top: 0;\n  -webkit-transform: scale(0.25);\n          transform: scale(0.25);\n  -webkit-transform-origin: top left;\n          transform-origin: top left;\n}\n\n.fa-lg {\n  font-size: 1.3333333333em;\n  line-height: 0.75em;\n  vertical-align: -0.0667em;\n}\n\n.fa-xs {\n  font-size: 0.75em;\n}\n\n.fa-sm {\n  font-size: 0.875em;\n}\n\n.fa-1x {\n  font-size: 1em;\n}\n\n.fa-2x {\n  font-size: 2em;\n}\n\n.fa-3x {\n  font-size: 3em;\n}\n\n.fa-4x {\n  font-size: 4em;\n}\n\n.fa-5x {\n  font-size: 5em;\n}\n\n.fa-6x {\n  font-size: 6em;\n}\n\n.fa-7x {\n  font-size: 7em;\n}\n\n.fa-8x {\n  font-size: 8em;\n}\n\n.fa-9x {\n  font-size: 9em;\n}\n\n.fa-10x {\n  font-size: 10em;\n}\n\n.fa-fw {\n  text-align: center;\n  width: 1.25em;\n}\n\n.fa-ul {\n  list-style-type: none;\n  margin-left: 2.5em;\n  padding-left: 0;\n}\n.fa-ul > li {\n  position: relative;\n}\n\n.fa-li {\n  left: -2em;\n  position: absolute;\n  text-align: center;\n  width: 2em;\n  line-height: inherit;\n}\n\n.fa-border {\n  border: solid 0.08em #eee;\n  border-radius: 0.1em;\n  padding: 0.2em 0.25em 0.15em;\n}\n\n.fa-pull-left {\n  float: left;\n}\n\n.fa-pull-right {\n  float: right;\n}\n\n.fa.fa-pull-left,\n.fas.fa-pull-left,\n.far.fa-pull-left,\n.fal.fa-pull-left,\n.fab.fa-pull-left {\n  margin-right: 0.3em;\n}\n.fa.fa-pull-right,\n.fas.fa-pull-right,\n.far.fa-pull-right,\n.fal.fa-pull-right,\n.fab.fa-pull-right {\n  margin-left: 0.3em;\n}\n\n.fa-spin {\n  -webkit-animation: fa-spin 2s infinite linear;\n          animation: fa-spin 2s infinite linear;\n}\n\n.fa-pulse {\n  -webkit-animation: fa-spin 1s infinite steps(8);\n          animation: fa-spin 1s infinite steps(8);\n}\n\n@-webkit-keyframes fa-spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n\n@keyframes fa-spin {\n  0% {\n    -webkit-transform: rotate(0deg);\n            transform: rotate(0deg);\n  }\n  100% {\n    -webkit-transform: rotate(360deg);\n            transform: rotate(360deg);\n  }\n}\n.fa-rotate-90 {\n  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=1)";\n  -webkit-transform: rotate(90deg);\n          transform: rotate(90deg);\n}\n\n.fa-rotate-180 {\n  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=2)";\n  -webkit-transform: rotate(180deg);\n          transform: rotate(180deg);\n}\n\n.fa-rotate-270 {\n  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=3)";\n  -webkit-transform: rotate(270deg);\n          transform: rotate(270deg);\n}\n\n.fa-flip-horizontal {\n  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1)";\n  -webkit-transform: scale(-1, 1);\n          transform: scale(-1, 1);\n}\n\n.fa-flip-vertical {\n  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)";\n  -webkit-transform: scale(1, -1);\n          transform: scale(1, -1);\n}\n\n.fa-flip-both, .fa-flip-horizontal.fa-flip-vertical {\n  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1)";\n  -webkit-transform: scale(-1, -1);\n          transform: scale(-1, -1);\n}\n\n:root .fa-rotate-90,\n:root .fa-rotate-180,\n:root .fa-rotate-270,\n:root .fa-flip-horizontal,\n:root .fa-flip-vertical,\n:root .fa-flip-both {\n  -webkit-filter: none;\n          filter: none;\n}\n\n.fa-stack {\n  display: inline-block;\n  height: 2em;\n  position: relative;\n  width: 2.5em;\n}\n\n.fa-stack-1x,\n.fa-stack-2x {\n  bottom: 0;\n  left: 0;\n  margin: auto;\n  position: absolute;\n  right: 0;\n  top: 0;\n}\n\n.svg-inline--fa.fa-stack-1x {\n  height: 1em;\n  width: 1.25em;\n}\n.svg-inline--fa.fa-stack-2x {\n  height: 2em;\n  width: 2.5em;\n}\n\n.fa-inverse {\n  color: #fff;\n}\n\n.sr-only {\n  border: 0;\n  clip: rect(0, 0, 0, 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n}\n\n.sr-only-focusable:active, .sr-only-focusable:focus {\n  clip: auto;\n  height: auto;\n  margin: 0;\n  overflow: visible;\n  position: static;\n  width: auto;\n}';
    function Mn() {
        var t = b, n = y, e = V.familyPrefix, a = V.replacementClass, r = Cn;
        if (e !== t || a !== n) {
            var i = new RegExp("\\.".concat(t, "\\-"), "g"), o = new RegExp("\\.".concat(n), "g");
            r = r.replace(i, ".".concat(e, "-")).replace(o, ".".concat(a));
        }
        return r;
    }
    function Nn(t) {
        return {
            found: !0,
            width: t[0],
            height: t[1],
            icon: {
                tag: "path",
                attributes: {
                    fill: "currentColor",
                    d: t.slice(4)[0]
                }
            }
        };
    }
    function Sn() {
        V.autoAddCss && !Pn && (bt(Mn()), Pn = !0);
    }
    function zn(n, t) {
        return Object.defineProperty(n, "abstract", {
            get: t
        }), Object.defineProperty(n, "html", {
            get: function() {
                return n.abstract.map(function(t) {
                    return Kt(t);
                });
            }
        }), Object.defineProperty(n, "node", {
            get: function() {
                if (p) {
                    var t = g.createElement("div");
                    return t.innerHTML = n.html, t.children;
                }
            }
        }), n;
    }
    function On(t) {
        var n = t.prefix, e = void 0 === n ? "fa" : n, a = t.iconName;
        if (a) return Vt(jn.definitions, e, a) || Vt(X.styles, e, a);
    }
    var En, jn = new (function() {
        function t() {
            !function(t, n) {
                if (!(t instanceof n)) throw new TypeError("Cannot call a class as a function");
            }(this, t), this.definitions = {};
        }
        var n, e, a;
        return n = t, (e = [ {
            key: "add",
            value: function() {
                for (var n = this, t = arguments.length, e = new Array(t), a = 0; a < t; a++) e[a] = arguments[a];
                var r = e.reduce(this._pullDefinitions, {});
                Object.keys(r).forEach(function(t) {
                    n.definitions[t] = W({}, n.definitions[t] || {}, r[t]), function t(n, a) {
                        var e = (2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {}).skipHooks, r = void 0 !== e && e, i = Object.keys(a).reduce(function(t, n) {
                            var e = a[n];
                            return e.icon ? t[e.iconName] = e.icon : t[n] = e, t;
                        }, {});
                        "function" != typeof X.hooks.addPack || r ? X.styles[n] = W({}, X.styles[n] || {}, i) : X.hooks.addPack(n, i), 
                        "fas" === n && t("fa", a);
                    }(t, r[t]), Bt();
                });
            }
        }, {
            key: "reset",
            value: function() {
                this.definitions = {};
            }
        }, {
            key: "_pullDefinitions",
            value: function(i, t) {
                var o = t.prefix && t.iconName && t.icon ? {
                    0: t
                } : t;
                return Object.keys(o).map(function(t) {
                    var n = o[t], e = n.prefix, a = n.iconName, r = n.icon;
                    i[e] || (i[e] = {}), i[e][a] = r;
                }), i;
            }
        } ]) && i(n.prototype, e), a && i(n, a), t;
    }())(), Pn = !1, Ln = {
        i2svg: function() {
            var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {};
            if (p) {
                Sn();
                var n = t.node, e = void 0 === n ? g : n, a = t.callback, r = void 0 === a ? function() {} : a;
                return V.searchPseudoElements && An(e), bn(e, r);
            }
            return gt.reject("Operation requires a DOM of some kind.");
        },
        css: Mn,
        insertCss: function() {
            Pn || (bt(Mn()), Pn = !0);
        },
        watch: function() {
            var t = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}, n = t.autoReplaceSvgRoot, e = t.observeMutationsRoot;
            !1 === V.autoReplaceSvg && (V.autoReplaceSvg = !0), V.observeMutations = !0, q(function() {
                In({
                    autoReplaceSvgRoot: n
                }), en({
                    treeCallback: bn,
                    nodeCallback: yn,
                    pseudoElementsCallback: An,
                    observeMutationsRoot: e
                });
            });
        }
    }, Tn = (En = function(t) {
        var n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}, e = n.transform, a = void 0 === e ? vt : e, r = n.symbol, i = void 0 !== r && r, o = n.mask, s = void 0 === o ? null : o, c = n.title, l = void 0 === c ? null : c, f = n.classes, u = void 0 === f ? [] : f, m = n.attributes, d = void 0 === m ? {} : m, h = n.styles, g = void 0 === h ? {} : h;
        if (t) {
            var p = t.prefix, v = t.iconName, b = t.icon;
            return zn(W({
                type: "icon"
            }, t), function() {
                return Sn(), V.autoA11y && (l ? d["aria-labelledby"] = "".concat(V.replacementClass, "-title-").concat(wt()) : (d["aria-hidden"] = "true", 
                d.focusable = "false")), Ot({
                    icons: {
                        main: Nn(b),
                        mask: s ? Nn(s.icon) : {
                            found: !1,
                            width: null,
                            height: null,
                            icon: {}
                        }
                    },
                    prefix: p,
                    iconName: v,
                    transform: W({}, vt, a),
                    symbol: i,
                    title: l,
                    extra: {
                        attributes: d,
                        styles: g,
                        classes: u
                    }
                });
            });
        }
    }, function(t) {
        var n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}, e = (t || {}).icon ? t : On(t || {}), a = n.mask;
        return a && (a = (a || {}).icon ? a : On(a || {})), En(e, W({}, n, {
            mask: a
        }));
    }), _n = {
        noAuto: function() {
            V.autoReplaceSvg = !1, V.observeMutations = !1, nn && nn.disconnect();
        },
        config: V,
        dom: Ln,
        library: jn,
        parse: {
            transform: function(t) {
                return on(t);
            }
        },
        findIconDefinition: On,
        icon: Tn,
        text: function(t) {
            var n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}, e = n.transform, a = void 0 === e ? vt : e, r = n.title, i = void 0 === r ? null : r, o = n.classes, s = void 0 === o ? [] : o, c = n.attributes, l = void 0 === c ? {} : c, f = n.styles, u = void 0 === f ? {} : f;
            return zn({
                type: "text",
                content: t
            }, function() {
                return Sn(), Et({
                    content: t,
                    transform: W({}, vt, a),
                    title: i,
                    extra: {
                        attributes: l,
                        styles: u,
                        classes: [ "".concat(V.familyPrefix, "-layers-text") ].concat(d(s))
                    }
                });
            });
        },
        counter: function(t) {
            var n = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : {}, e = n.title, a = void 0 === e ? null : e, r = n.classes, i = void 0 === r ? [] : r, o = n.attributes, s = void 0 === o ? {} : o, c = n.styles, l = void 0 === c ? {} : c;
            return zn({
                type: "counter",
                content: t
            }, function() {
                return Sn(), function(t) {
                    var n = t.content, e = t.title, a = t.extra, r = W({}, a.attributes, e ? {
                        title: e
                    } : {}, {
                        class: a.classes.join(" ")
                    }), i = Mt(a.styles);
                    0 < i.length && (r.style = i);
                    var o = [];
                    return o.push({
                        tag: "span",
                        attributes: r,
                        children: [ n ]
                    }), e && o.push({
                        tag: "span",
                        attributes: {
                            class: "sr-only"
                        },
                        children: [ e ]
                    }), o;
                }({
                    content: t.toString(),
                    title: a,
                    extra: {
                        attributes: s,
                        styles: l,
                        classes: [ "".concat(V.familyPrefix, "-layers-counter") ].concat(d(i))
                    }
                });
            });
        },
        layer: function(t) {
            return zn({
                type: "layer"
            }, function() {
                Sn();
                var n = [];
                return t(function(t) {
                    Array.isArray(t) ? t.map(function(t) {
                        n = n.concat(t.abstract);
                    }) : n = n.concat(t.abstract);
                }), [ {
                    tag: "span",
                    attributes: {
                        class: "".concat(V.familyPrefix, "-layers")
                    },
                    children: n
                } ];
            });
        },
        toHtml: Kt
    }, In = function() {
        var t = (0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : {}).autoReplaceSvgRoot, n = void 0 === t ? g : t;
        (0 < Object.keys(X.styles).length || V.autoFetchSvg) && p && V.autoReplaceSvg && _n.dom.i2svg({
            node: n
        });
    };
    !function(t) {
        try {
            t();
        } catch (t) {
            if (!z) throw t;
        }
    }(function() {
        u && (h.FontAwesome || (h.FontAwesome = _n), q(function() {
            In(), en({
                treeCallback: bn,
                nodeCallback: yn,
                pseudoElementsCallback: An
            });
        })), X.hooks = W({}, X.hooks, {
            addPack: function(t, n) {
                X.styles[t] = W({}, X.styles[t] || {}, n), Bt(), In();
            },
            addShims: function(t) {
                var n;
                (n = X.shims).push.apply(n, d(t)), Bt(), In();
            }
        });
    });
}();