/**
 * gbRichEdit5 - getButterfly Rich Text Editor plugin - https://getbutterfly.com/
 * 
 * @author	Ciprian Popescu + François Elie
 * @version 5.9.1
 * 
 * Licensed under The MIT License
 * https://opensource.org/licenses/mit-license.php
 * 
 */

/* eslint-env browser */
/* jslint-env browser */
/* global window */
/* global document */
/* global console */
/* global jQuery */

// define the gre plugin
(function ($) {
	if (typeof $.fn.gre === 'undefined') {
		// define default options
		var defaults = {
			content_css_url: '/css/gre.css',
			content_css_url2: '/css/textes.css', // ajouté par moi FE pour les interprétation des marges et des interpolations
			height: 400
		};
		$.fn.gre = function (options) {
			$.fn.gre.html = function (iframe) {
				return iframe.contentWindow.document.getElementsByTagName('body')[0].innerHTML;
			};

			// build main options before element iteration
			var opts = $.extend(defaults, options);

			// iterate and construct the rich text editors
			return this.each(function () {
				var textarea = $(this),
                    iframe;
				var element_id = textarea.prop('id');

				// enable design mode
				function enableDesignMode() {
					var content = textarea.val();

					// Mozilla needs this to display caret
					if ($.trim(content) === '') {
						content = '';
                    }

					// already created? show/hide
					if (iframe) {
						textarea.hide();
						$(iframe).contents().find('body').html(content);
						$(iframe).show();
						$('#toolbar-' + element_id).remove();
						textarea.before(toolbar());
						return true;
					}

					// for compatibility reasons, need to be created this way
                    iframe = document.createElement('iframe');
                    iframe.frameBorder = 0;
					iframe.frameMargin = 0;
					iframe.framePadding = 0;
					iframe.height = opts.height;

					if (textarea.prop('class')) {
						iframe.className = textarea.prop('class');
                    }
					if (textarea.prop('id')) {
						iframe.id = element_id;
                    }
					if (textarea.prop('name')) {
						iframe.title = textarea.prop('name');
                    }

					textarea.after(iframe);

					var css = '';
					if (opts.content_css_url) {
						css = "<link rel='stylesheet' href='" + opts.content_css_url + "'><link rel='stylesheet' href='" + opts.content_css_url2 + "'>";
                    }

					var doc = '<!doctype html><html><head>' + css + '</head><body class="frameBody"><div class="jolitexte">'+ content + '</div></body></html>';
					tryEnableDesignMode(doc, function () {
						$('#toolbar-' + element_id).remove();
						textarea.before(toolbar());
						// hide textarea
						textarea.hide();
					});
				}

				function tryEnableDesignMode(doc, callback) {
					if (!iframe) {
						return false;
                    }
			    
					iframe.contentWindow.document.open();
					iframe.contentWindow.document.write(doc);
					iframe.contentWindow.document.close();

					if (document.contentEditable) {
						iframe.contentWindow.document.designMode = 'On';
						callback();
						return true;
					} else if (document.designMode !== null) {
						iframe.contentWindow.document.designMode = 'on';
						callback();
						return true;
					}
					setTimeout(function () {
                        tryEnableDesignMode(doc, callback);
                    }, 500);

                    return false;
				}

				function disableDesignMode(submit) {
					var content = $(iframe).contents().find('body').html();

					if ($(iframe).is(':visible')) {
						textarea.val(content);
                    }

					if (submit !== true) {
						textarea.show();
						$(iframe).hide(); 
					}
				}
				// create toolbar and bind events to its elements
				
				function toolbar() {
					var tb = $("\
                                                <style>\
                                                .gre-toolbar a {text-decoration:none;border:1px solid black;border-radius: 3px;}\
                                                </style>\
						<div class='gre-toolbar' id='toolbar-"+ element_id +"'>\
							<a href='#' class='bold'><b>B</b></a>\
							<a href='#' class='italic'><i>I</i></a>\
							<a href='#' class='bullet'>&nbsp;&nbsp;&bull;&nbsp;&nbsp;</a>\
							<a href='#' class='notegauche'><span style='color:red'>&nbsp;note à gauche&nbsp;</span></a>\
							<a href='#' class='notedroite'><span style='color:blue'>&nbsp;note à droite&nbsp;</span></a>\
							<a href='#' class='interpol'><span style='color:black'>&nbsp;interpolation&nbsp;</span></a>\
							<a href='#' class='col1'><span style='background-color:lightsalmon'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col2'><span style='background-color:yellow'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col3'><span style='background-color:lightgreen'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col4'><span style='background-color:#cce6ff'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col5'><span style='background-color:C5C5FF'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col6'><span style='background-color:FFC5FF'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col7'><span style='background-color:C5C5C5'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='col8'><span style='background-color:E2E2E2'>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>\
							<a href='#' class='normal'>&nbsp;sans couleur&nbsp;</a>\
							<a href='#' class='pause'>Pause</a>\
							<!--<a href='#' class='undo'>&nbsp;undo&nbsp;</a>-->\
							<!--<a href='#' class='redo'>&nbsp;redo&nbsp;</a>-->\
							<!--<a href='#' class='ref'>&nbsp;Référence&nbsp;</a>-->\
						</div>\
					");

					$('select', tb).change(function () {
						var index = this.selectedIndex;
						if (index !== 0) {
							var selected = this.options[index].value;
							formatText('formatblock', '<' + selected + '>');
						}
					});
					$('.bold', tb).click(function () { formatText("insertHTML","**"+iframe.contentWindow.document.getSelection()+"**");});
					$('.italic', tb).click(function () { formatText("insertHTML","*"+iframe.contentWindow.document.getSelection()+"*");});
                                        $('.bullet  ', tb).click(function () { formatText('insertunorderedlist');return false; });
                                        $('.notegauche', tb).click(function () { formatText("insertHTML","<span class='sidenoteleft'>"+iframe.contentWindow.document.getSelection()+"</span>");});
                                        $('.notedroite', tb).click(function () { formatText("insertHTML","<span class='sidenoteright'>"+iframe.contentWindow.document.getSelection()+"</span>");});
					$('.interpol', tb).click(function () { formatText("insertHTML","<span class='interpolation'>"+ iframe.contentWindow.document.getSelection()+"</span>");});
					$('.col1', tb).click(function () { formatText('backcolor','#FFA07B');return false; }); // presque lightsalmon...
                                        $('.col2', tb).click(function () { formatText('backcolor','#FFFF03');return false; }); // presque yellow
                                        $('.col3', tb).click(function () { formatText('backcolor','#90EE93');return false; }); // presque lightgreen
                                        $('.col4', tb).click(function () { formatText('backcolor','#cce6ff');return false; });
                                        $('.col5', tb).click(function () { formatText('backcolor','#C5C5FF');return false; });
                                        $('.col6', tb).click(function () { formatText('backcolor','#FFC5FF');return false; });
                                        $('.col7', tb).click(function () { formatText('backcolor','#C5C5C5');return false; });
                                        $('.col8', tb).click(function () { formatText('backcolor','#E2E2E2');return false; });
                                        $('.normal', tb).click(function () { formatText('removeFormat');return false; });
                                        $('.pause', tb).click(function () { formatText("insertHTML"," ⏹"+ iframe.contentWindow.document.getSelection()+" ");});
					$('.undo', tb).click(function () { formatText('undo',null);return false; });
					$('.redo', tb).click(function () { formatText('redo',null);return false; });
					$('.ref', tb).click(function(){
						var p = prompt('Numéro de la ressource à mentionner ?');
						if(p)
							formatText('InsertHTML',"<a href='ressif.php?id="+p+"'>"+iframe.contentWindow.document.getSelection()+"</a>");
						return false;
					});
					// * more possible options
					// decreaseFontSize // Adds a SMALL tag around the selection or at the insertion point. (Not supported by Internet Explorer.)
					// foreColor // Changes a font color for the selection or at the insertion point. This requires a color value string to be passed in as a value argument.
					// indent/outdent // Indents the line containing the selection or insertion point. In Firefox, if the selectisectetur adipiscingon spans multiple lines at different levels of indentation, only the least indented lines in the selection will be indented.
					// removeFormat
					// unlink

					/*
					insertHTML
					Inserts an HTML string at the insertion point (deletes selection). Requires a valid HTML string to be passed in as a value argument. (Not supported by Internet Explorer.)
					*/

					$('.image', tb).click(function(){
						var p = prompt('Insert image URL address (starting with https://):');
						if(p)
							formatText('InsertImage', p);
						return false;
					});

					// Insert HTML code
					$('.insertHTML', tb).click(function(){
						var p = prompt('Insert HTML code:');
						if(p)
							formatText('insertHTML', p);
						return false;
					});

					$('.disable', tb).click(function(){
						disableDesignMode();
						var edm = $('<small><a class="gre-minilink" href="#"><i class="fa fa-bars"></i></a></small>');
						tb.empty().append(edm);
						edm.click(function(e){
							e.preventDefault();
							enableDesignMode();
							$(this).remove();
						});
						return false;
					});

					$(iframe).parents('form').submit(function(){
						disableDesignMode(true);
					});

					var iframeDoc = $(iframe.contentWindow.document);

					var select = $('select', tb)[0];
					iframeDoc.mouseup(function(){
						setSelectedType(getSelectionElement(), select);
						return true;
					});

					return tb;
				}

				function formatText(command, option=null) {
					iframe.contentWindow.focus();
					//alert('coucou3');
                                        iframe.contentWindow.document.execCommand("styleWithCSS",null,true);
					iframe.contentWindow.document.execCommand(command, false, option); 
					
					// convert nasty markup to light xhtml
					var markup = iframe.contentWindow.document.body.innerHTML;

					markup = markup.replace(/<span\s*(class="Apple-style-span")?\s*style="font-weight:\s*bold;">([^<]*)<\/span>/ig, '<strong>$2</strong>');
					markup = markup.replace(/<span\s*(class="Apple-style-span")?\s*style="font-style:\s*italic;">([^<]*)<\/span>/ig, '<em>$2</em>');

					iframe.contentWindow.document.body.innerHTML = markup;
					iframe.contentWindow.focus();
				}

				function setSelectedType(node, select) {
					while(node.parentNode) {
						var nName = node.nodeName.toLowerCase();
						for(var i=0;i<select.options.length;i++) {
							if(nName == select.options[i].value) {
								select.selectedIndex = i;
								return true;
							}
						}
						node = node.parentNode;
					}
					select.selectedIndex = 0;
					return true;
				}

				function getSelectionElement() {
                    var selection,
                        range,
                        node;

                    if (iframe.contentWindow.document.selection) {
						// IE selections
						selection = iframe.contentWindow.document.selection;
						range = selection.createRange();
						node = range.parentElement();
					} else {
						// Mozilla selections
						selection = iframe.contentWindow.getSelection();
						range = selection.getRangeAt(0);
						node = range.commonAncestorContainer;
					}

                    return node;
				}

				enableDesignMode();
			}); //return this.each
		}; // gre
	} // if
})(jQuery);
