<?php if (!defined('PmWiki')) exit();
//echo temps('lecture de la configuration');
#FE attaque qui cible un wordpress
if (
$_GET['option']=='com_content')
	exit;


$maintenance=0;

if ($maintenance==1)
  if ($_SERVER[REMOTE_ADDR]!='82.64.95.11')
    {
    echo "<center><h1>Site en maintenance. Excusez-moi du d&eacute;rangement.</h1><img src='images/maintenance.jpg'></center>"; 
    exit();
    }


// chargement pour philo-labo

$SitePath=$_SERVER[DOCUMENT_ROOT]; // chemin vers les fichiers
$SiteUrl = "$_SERVER[REQUEST_SCHEME]://$_SERVER[HTTP_HOST]"; // url du site

// configuration philo-labo
require_once("$SitePath/config.php");

// fonction pour les logs
require_once("$SitePath/local/logs.php"); 

//error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
//ini_set("display_errors", 1);

##------------------------------------------------notifications par courrier
$MetaRobots="User-Agent: *";
include_once("scripts/robots.php"); // pour les moteurs de recherche

include_once("scripts/notify.php");
$EnableNotify=1;
$NotifiyFrom=$SiteDomainName;
$NotifyFrom = "$SiteDomainName <notifications@$SiteDomainName>"; 
$NotifyList[]="notify=$SiteMyMail group=*";
$NotifySquelch = 86400; // pas plus d'une notification par jour
$NotifyDelay = 3600; // après l'édition initiale


$FarmPubDirUrl="$PubDirUrl";

##  $WikiTitle is the name that appears in the browser's title bar.
$WikiTitle = 'Philo-labo';//'Laboratoire des IANs de philosophie';
//$DefaultGroup = 'Public'; # Groupe par défaut
//$DefaultName = 'Accueil'; # Page de démarrage groupe - défaut 'HomePage' - 
##  $SiteUrl is your preferred URL for accessing wiki pages
##  $PubDirUrl is the URL for the pub directory.

$PubDirUrl = "$SiteUrl/pub";

//Pour la gestion des images sur les réseaux sociaux
$HTMLHeaderFmt['reseausociaux']='
	<meta property="og:image" content="$SiteUrl/images/compositeur2.png" /><!-- pour les réseaux sociaux -->
	<meta property="og:description" content="Un laboratoire de philosophie" />
	<meta property="og:url" content="$SiteUrl:443" />
	<meta property="og:title" content="$SiteName" />';


##  If you want to use URLs of the form .../pmwiki.php/Group/PageName
##  instead of .../pmwiki.php?p=Group.PageName, try setting
##  $EnablePathInfo below.  Note that this doesn't work in all environments,
##  it depends on your webserver and PHP configuration.  You might also 
##  want to check http://www.pmwiki.org/wiki/Cookbook/CleanUrls more
##  details about this setting and other ways to create nicer-looking urls.
$EnablePathInfo = 0;

## $PageLogoUrl is the URL for a logo image -- you can change this
## to your own logo if you wish.
# $PageLogoUrl = "$PubDirUrl/skins/pmwiki/pmwiki-32.gif";
## ça ralentit
$EnableAutoSkinList = 1;
include_once('cookbook/skinchange.php');
include_once('cookbook/skinlist.php'); 
  
## You'll probably want to set an administrative password that you
## can use to get into password-protected pages.  Also, by default 
## the "attr" passwords for the PmWiki and Main groups are locked, so
## an admin password is a good way to unlock those.  See PmWiki.Passwords
## and PmWiki.PasswordsAdmin.
#$DefaultPasswords['admin'] = pmcrypt('secret');

# Unicode (UTF-8) allows the display of all languages and all alphabets.
## Highly recommended for new wikis.
include_once("scripts/xlpage-utf-8.php");
XLPage('fr','PmWikiFr.XLPage');

## If you're running a publicly available site and allow anyone to
## edit without requiring a password, you probably want to put some
## blocklists in place to avoid wikispam.  See PmWiki.Blocklist.
# $EnableBlocklist = 1;                    # enable manual blocklists
# $EnableBlocklist = 10;                   # enable automatic blocklists

##  PmWiki comes with graphical user interface buttons for editing;
##  to enable these buttons, set $EnableGUIButtons to 1.  
$EnableGUIButtons = 1;

##  To enable markup syntax from the Creole common wiki markup language
##  (http://www.wikicreole.org/), include it here:
# include_once("scripts/creole.php");

##  Some sites may want leading spaces on markup lines to indicate
##  "preformatted text blocks", set $EnableWSPre=1 if you want to do
##  this.  Setting it to a higher number increases the number of
##  space characters required on a line to count as "preformatted text".
# $EnableWSPre = 1;   # lines beginning with space are preformatted (default)
# $EnableWSPre = 4;   # lines with 4 or more spaces are preformatted
# $EnableWSPre = 0;   # disabled

##  If you want uploads enabled on your system, set $EnableUpload=1.
##  You'll also need to set a default upload password, or else set
##  passwords on individual groups and pages.  For more information
##  see PmWiki.UploadsAdmin.
$EnableUpload = 1;

$UploadDir="$SitePath/fichiers";
$UploadUrlFmt = "$SiteUrl/fichiers";

$UploadMaxSize= 3000000;
$UploadPermAdd = 0;
$DefaultPasswords['upload'] = pmcrypt('secret');
##  Setting $EnableDiag turns on the ?action=diag and ?action=phpinfo
##  actions, which often helps others to remotely troubleshoot 
##  various configuration and execution problems.
$EnableDiag = 0;                         # enable remote diagnostics

##  By default, PmWiki doesn't allow browsers to cache pages.  Setting
##  $EnableIMSCaching=1; will re-enable browser caches in a somewhat
##  smart manner.  Note that you may want to have caching disabled while
##  adjusting configuration files or layout templates.
# $EnableIMSCaching = 1;                   # allow browser caching

##  Set $SpaceWikiWords if you want WikiWords to automatically 
##  have spaces before each sequence of capital letters.
# $SpaceWikiWords = 1;                     # turn on WikiWord spacing

##  Set $EnableWikiWords if you want to allow WikiWord links.
##  For more options with WikiWords, see scripts/wikiwords.php .
$EnableWikiWords = 0;                    # enable WikiWord links

##  $DiffKeepDays specifies the minimum number of days to keep a page's
##  revision history.  The default is 3650 (approximately 10 years).
# $DiffKeepDays=30;                        # keep page history at least 30 days

## By default, viewers are prevented from seeing the existence
## of read-protected pages in search results and page listings,
## but this can be slow as PmWiki has to check the permissions
## of each page.  Setting $EnablePageListProtect to zero will
## speed things up considerably, but it will also mean that
## viewers may learn of the existence of read-protected pages.
## (It does not enable them to access the contents of the pages.)
# $EnablePageListProtect = 0;

##  The refcount.php script enables ?action=refcount, which helps to
##  find missing and orphaned pages.  See PmWiki.RefCount.
# if ($action == 'refcount') include_once("scripts/refcount.php");

##  The feeds.php script enables ?action=rss, ?action=atom, ?action=rdf,
##  and ?action=dc, for generation of syndication feeds in various formats.
# if ($action == 'rss')  include_once("scripts/feeds.php");  # RSS 2.0
# if ($action == 'atom') include_once("scripts/feeds.php");  # Atom 1.0
# if ($action == 'dc')   include_once("scripts/feeds.php");  # Dublin Core
# if ($action == 'rdf')  include_once("scripts/feeds.php");  # RSS 1.0

##  In the 2.2.0-beta series, {$var} page variables were absolute, but now
##  relative page variables provide greater flexibility and are recommended.
##  (If you're starting a new site, it's best to leave this setting alone.)
# $EnableRelativePageVars = 1; # 1=relative; 0=absolute

##  By default, pages in the Category group are manually created.
##  Uncomment the following line to have blank category pages
##  automatically created whenever a link to a non-existent
##  category page is saved.  (The page is created only if
##  the author has edit permissions to the Category group.)
# $AutoCreate['/^Category\\./'] = array('ctime' => $Now);

##  PmWiki allows a great deal of flexibility for creating custom markup.
##  To add support for '*bold*' and '~italic~' markup (the single quotes
##  are part of the markup), uncomment the following lines. 
##  (See PmWiki.CustomMarkup and the Cookbook for details and examples.)
# Markup("'~", "inline", "/'~(.*?)~'/", "<i>$1</i>");        # '~italic~'
# Markup("'*", "inline", "/'\\*(.*?)\\*'/", "<b>$1</b>");    # '*bold*'

##  If you want to have to approve links to external sites before they
##  are turned into links, uncomment Authroine below.  See PmWiki.UrlApprovals.
##  Also, setting $UnapprovedLinkCountMax limits the number of unapproved
##  links that are allowed in a page (useful to control wikispam).
# $UnapprovedLinkCountMax = 10;
# include_once("scripts/urlapprove.php");

##  The following lines make additional editing buttons appear in the
##  edit page for subheadings, lists, tables, etc.
# $GUIButtons['h2'] = array(400, '\\n!! ', '\\n', '$[Heading]',
#                     '$GUIButtonDirUrlFmt/h2.gif"$[Heading]"');
# $GUIButtons['h3'] = array(402, '\\n!!! ', '\\n', '$[Subheading]',
#                     '$GUIButtonDirUrlFmt/h3.gif"$[Subheading]"');
# $GUIButtons['indent'] = array(500, '\\n->', '\\n', '$[Indented text]',
#                     '$GUIButtonDirUrlFmt/indent.gif"$[Indented text]"');
# $GUIButtons['outdent'] = array(510, '\\n-<', '\\n', '$[Hanging indent]',
#                     '$GUIButtonDirUrlFmt/outdent.gif"$[Hanging indent]"');
# $GUIButtons['ol'] = array(520, '\\n# ', '\\n', '$[Ordered list]',
#                     '$GUIButtonDirUrlFmt/ol.gif"$[Ordered (numbered) list]"');
# $GUIButtons['ul'] = array(530, '\\n* ', '\\n', '$[Unordered list]',
#                     '$GUIButtonDirUrlFmt/ul.gif"$[Unordered (bullet) list]"');
# $GUIButtons['hr'] = array(540, '\\n----\\n', '', '',
#                     '$GUIButtonDirUrlFmt/hr.gif"$[Horizontal rule]"');
# $GUIButtons['table'] = array(600,
#                       '||border=1 width=80%\\n||!Hdr ||!Hdr ||!Hdr ||\\n||     ||     ||     ||\\n||     ||     ||     ||\\n', '', '', 
#                     '$GUIButtonDirUrlFmt/table.gif"$[Table]"');
include_once("$FarmD/cookbook/flipbox.php");
include_once("$FarmD/cookbook/sectionedit.php");
#include_once("$FarmD/cookbook/s5r.php")
if ($action == 'slideshow2')
	{
	$SlidesSkin="actual";
	include_once("$FarmD/cookbook/slideshow-2.0.php");
	}
if ($action == 'slideshow3')
	{
	include_once("$FarmD/cookbook/slideshow-2.1.php");
	}
include_once("cookbook/slideshow-2.0-plus.php"); // markup spéciaux pour les tableaux par exemple

// --------- pour l'exploitation des ressources et d'ai

include_once('cookbook/wsplus.php');
include_once('cookbook/blocs.php');
include_once('cookbook/wsplus2.php');

#include_once('cookbook/slimtoc.php');
#$DefaultTocTitle = '<b>Table des matières</b>';
$TocLevelMax = 3; # niveau 3 ce sont les diapos, 4: les texts dans les diapos ?

include_once('cookbook/numtoc.php'); // on utilise le (:toc:) de numtoc
#include_once('cookbook/quicktoc.php');

#if ($action=='browse')
#	include_once("$FarmD/cookbook/autotoc.php"); // ne fonctionne pas - incompatibilité probable avec plein d'autres choses

#include_once('cookbook/pagetoc.php'); # permet une table des matières en flottant, mais incompatible avec numtoc qui est si bien, donc on garde slimtoc

// thmbnail for documents doc odt pdf
/*
Markup('[[ext|','<[[|',"/(?>\\[\\[([^|\\]]*.([Pp][Dd][Ff]|[Dd][Oo][Cc]|[Oo][Dd][Tt]))\\s*\\|\\s*)(.*?)\\s*\\]\\]($SuffixPattern)/e", 
"MakeLinkWithIcon(\$pagename, PSS('$1'), PSS('$2'), PSS('$3'),'$4')");

Markup('[[ext','<[[', "/(?>\\[\\[\\s*(.*?.([Pp][Dd][Ff]|[Dd][Oo][Cc]|[Oo][Dd][Tt]))\\s*\\]\\])($SuffixPattern)/e", 
"MakeLinkWithIcon(\$pagename, PSS('$1'), PSS('$2'), NULL, '$3')");

function MakeLinkWithIcon($pagename, $target, $ext, $text, $suffix)
{
    global $UrlLinkFmt;

    $fmt = $UrlLinkFmt."&nbsp;<img src=/images/icons/".$ext.".png>";
    if (strtolower($ext) != "zip")
        $fmt = "%newwin%".$fmt;

    if (is_null($text))
        return Keep(MakeLink($pagename, PSS($target), NULL, $suffix, $fmt), 'L');
    else 
        return Keep(MakeLink($pagename, PSS($target), PSS($text), $suffix, $fmt), 'L');
}
*/

//$DefNum = 'A.1.a.i.';
//include_once('cookbook/graphviz.php'); // au lieu de pmgraphviz, graphviz est plus simple - retouché par moi
###include_once('cookbook/bloc.php');
include_once('cookbook/quizz.php');
include_once('cookbook/TrueLatex/truelatex.php');
//include_once('cookbook/LaTeXMathML.php');
include_once('cookbook/chess.php');

include_once("$FarmD/cookbook/HTML5Video.php");
$HTML5VideoDir = "$SiteUrl/fichiers/"; // a cause de EnablePathInfo=1

include_once("$FarmD/cookbook/video5.php"); // en test

$Dailymotion["html"] = 1;
include_once("cookbook/dailymotion.php");
# pour le support du mp3
include_once("$FarmD/cookbook/HTML5Audio_fe.php"); 

//include_once('cookbook/audio5.php');

# pour la musique écrire en notation abc
$ABCProgs['abcm2ps'] = 'abcm2ps';
$ABCProgs['abc2midi'] = 'abc2midi';
$ABCProgs['pnmcrop'] = 'pnmcrop';
$ABCProgs['gs'] = 'gs';
include("$FarmD/cookbook/abcmusic.php");

//include_once('cookbook/swf.php');
//include_once("$FarmD/cookbook/flash2.php");
include_once("$FarmD/cookbook/swf-sites.php"); // on évite le flash sur les mobile ?

# pour ape
# if($action=="browse" || $_REQUEST['preview']) {
#   $HTMLFooterFmt['ape'] = '<script type="text/javascript" 
#     src="$FarmPubDirUrl/ape/ape.js"></script>';
# }

//include_once("$FarmD/cookbook/latexmarkup.php");

// ne fonctionne plus
#$HTMLHeaderFmt['osmap'] = "<script src='$FarmPubDirUrl/osmap-pmwiki.js'></script>";
//remplacé par:
include_once("$FarmD/cookbook/OpenLayersAPI/ola.php");

// semble inutile pmwiki récupère la favicon qui se trouve dans la racine du site automatiquement
#$HTMLHeaderFmt['favicon']='<link rel="icon" href="/favicon.ico" type="image/x-icon" />';

// chargement de ai pour les bases de données
include_once("philosophemes/sql_config.php");
include_once("philosophemes/ai.php");

// gestion du titre, des petites icones et de la navigation, voir dans Site.PageActions

// Supprime l'affiche du header dans le skin, quel que soit le skin
// où ça se passe ? semble produire l'affichage d'un signe = au début de la page

$formsearch='<form style="display: inline;" action="">
            <input type="hidden" name="n" value="{$FullName}" />
            <input type="hidden" name="action" value="search" />
            <input type="text" name="q" value="" class="inputbox searchbox" />
            <input type="submit" class="inputbutton searchbutton" value="Rechercher" />
          </form>';

$iconsREP="<a href='$SiteUrl?action=slideshow2'><img src='/images/presentationp.png' title='Mode présentation de la page actuelle'/></a>
<a href='$SiteUrl?action=slideshow3'><img src='/images/presentationp.png' title='Expérimental'/></a><a href='?action=pdf'><img src='/images/icons/pdf.png' title='Voir cette page en format pdf - Attention si la page contient des images gif, ce n\'est pas encore possible'></a><a href='/?n=Public.Accueil'><img src='/images/vitrinep.png' title='vitrine' title='Se rendre sur le site vitrine'/></a><a href='?n=Main/Accueil'><img src='/images/labopp.png' title='Se rendre dans le labo interne'/></a><a href='?n=Site.Login?action=logout'><img src='/images/logout.png' title='Se déconnecter'/></a>";

$iconsREPdec='<a href="?n=Main/Accueil"><img src="/images/labop.png" title="Se rendre dans le labo interne"/></a<a href="?n=Site.Login?action=login"><img src="/images/key.png" title="Se connecter"/></a>';
/*
$iconsAJ='<a class="alink" href="/Ai/AjouterTexte?skin=minimal" title="Ajouter une ressource de type Texte à la base"><img src="/images/icons/16x16/texte.png"></a>
<a class="alink" href="/Ai/AjouterDocument?skin=minimal" title="Ajouter une ressource de type Document à la base"><img src="/images/icons/16x16/document.png"></a>
<a class="alink" href="/Ai/AjouterPdf?skin=minimal" title="Ajouter une ressource de type Pdf à la base"><img src="/images/icons/16x16/pdf.png"></a>
<a class="alink" href="/Ai/AjouterEpub?skin=minimal" title="Ajouter une ressource de type Epub à la base"><img src="/images/icons/16x16/epub.png"></a>
<a class="alink" href="/Ai/AjouterImage?skin=minimal" title="Ajouter une ressource de type Image à la base"><img src="/images/icons/16x16/image.png"></a>
<a class="alink" href="/Ai/AjouterMp3?skin=minimal" title="Ajouter une ressource de type Mp3 à la base"><img src="/images/icons/16x16/mp3.png"></a>
<a class="alink" href="/Ai/AjouterSujetQuestion?skin=minimal" title="Ajouter une ressource de type SujetQuestion à la base"><img src="/images/icons/16x16/sujet-question.png"></a>
<a class="alink" href="/Ai/AjouterSujetTexte?skin=minimal" title="Ajouter une ressource de type SujetTexte à la base"><img src="/images/icons/16x16/sujet-texte.png"></a>
<a class="alink" href="/Ai/AjouterMp4?skin=minimal" title="Ajouter une ressource de type Mp4 à la base"><img src="/images/icons/16x16/mp4.png"></a>
<a class="alink" href="/Ai/AjouterDailymotion?skin=minimal" title="Ajouter une ressource de type Dailymotion à la base"><img src="/images/icons/16x16/dailymotion.png"></a>
<a class="alink" href="/Ai/AjouterVimeo?skin=minimal" title="Ajouter une ressource de type Vimeo à la base"><img src="/images/icons/16x16/vimeo.png"></a>
<a class="alink" href="/Ai/AjouterYoutube?skin=minimal" title="Ajouter une ressource de type Youtube à la base"><img src="/images/icons/16x16/youtube.png"></a>
<a class="alink" href="/Ai/AjouterComposite?skin=minimal" title="Ajouter une ressource de type Composite à la base"><img src="/images/icons/16x16/composite.png"></a>
<a class="alink" href="/Ai/AjouterH5P?skin=minimal" title="Ajouter une ressource de type H5P à la base"><img src="/images/icons/16x16/h5p.png"></a>
<a class="alink" href="/Ai/AjouterGraphe?skin=minimal" title="Ajouter une ressource de type Graphe à la base"><img src="/images/icons/16x16/graphe.png"></a>
<a class="alink" href="/Ai/AjouterGraphique?skin=minimal" title="Ajouter une ressource de type Graphique à la base"><img src="/images/icons/16x16/graphique.png"></a>';
*/
Markup("iconsAJ", ">urllink", "/\\(:iconsAJ:\\)/","$iconsREP"); // enlevé le formulaire de recherche $formsearch
Markup("iconsNotLoggedin", ">urllink", "/\\(:iconsNotLoggedin:\\)/","%rfloat%$iconsREPdec");


// ajout des css dans la page

$HTMLHeaderFmt['css1']='<link rel="stylesheet" href="/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/textes.css" type="text/css" />';

//if($action=="browse" || $_REQUEST['preview']) {
//   $HTMLHeaderFmt['ape'] = "<script src='$FarmPubDirUrl/ape/ape.js'></script>";
// }

include_once('cookbook/treemenu.php');
// (:noleft:)
Markup('noleft', 'directives','/\\(:noleft:\\)/e', "SetTmplDisplay('PageLeftFmt', 0)");
Markup('noright', 'directives','/\\(:noright:\\)/e', "SetTmplDisplay('PageRightFmt', 0)");
Markup('noheader', 'directives','/\\(:noheader:\\)/e', "SetTmplDisplay('PageHeaderFmt', 0)");
 

// modifier si logué ou pas
// $HomePage  = "Public.Accueil";
//$LoginPage = "Site.Login";// "Public.Inscription"; 
// redirection si login ou logout

$UA2AfterSILoginRedirectTo="Site.Redirect";
$UA2AfterLogoutRedirectTo="Public.Accueil";
include_once('cookbook/userauth2.php'); // le login est fait ?

if ($_GET[action]=='logout')
  {
  //echo 'destruction de la session'; 
  //print_r($_COOKIE);
  foreach($_COOKIE as $cookie_nom=>$cookie_valeur)
    {
    //echo "$cookie_nom<br/>";
    // Suppression du cookie en fixant une date d'expiration dans le passé
    setcookie($cookie_nom, '', time() - 3600, '/');
    // Suppression de la valeur du cookie dans la variable $_COOKIE
    unset($_COOKIE[$cookie_nom]);
    }
  //print_r($_COOKIE);
  session_destroy();
  }

// système d'inscription automatique - écrit par moi
XLPage('fr','PmWikiFr.XLPageElie'); // pour la traduction des chaines de caractères
include_once("$FarmD/pub/userauth2inscription/userauth2inscription.php");

## If you want to have a custom skin, then set $Skin to the name
## of the directory (in pub/skins/) that contains your skin files.
## See PmWiki.Skins and Cookbook.Skins.

if (strpos('/',$_GET['n'])==0)
  $Group=explode('.',$_GET['n'])[0];
else
  $Group=explode('/',$_GET['n'])[0];
//echo $Group;

// suppression des en-têtes des skins (pour gagner de la place
//SetTmplDisplay('PageHeaderFmt', 0);

// if (($_SERVER['REMOTE_ADDR']=="82.240.218.183"))
// {
//   //$x=parse_ini_file("/web/philo-labo/users/$Author/config");
//   //print_r($x);
// //error_reporting(E_ERROR);
// //ini_set('display_errors', 1);
// }

if ($Author!='')
    {
    $DefaultGroup=$IntDefaultGroup;
    $DefaultName=$IntDefaultName;
    $Skin = $IntSkin;
    }
else
    {
    $DefaultGroup=$ExtDefaultGroup;
    $DefaultName=$ExtDefaultName;
    $Skin = $ExtSkin;
    }

if (isset($_GET[skin]))
    $Skin=$_GET[skin];
    
Markup_e('developpement', 'inline','/\\(:developpement:\\)/',"developpement()");
function developpement()
  {
  return select2html("select * from developpements order by ladate desc",'h');
  }
Markup_e('remarques', 'inline','/\\(:remarques:\\)/',"remarques()");
function remarques()
  {
  return select2html("select * from remarques order by ladate desc",'h');
  }
Markup_e('remarquesR', 'inline','/\\(:remarquesR:\\)/',"remarquesR()");
function remarquesR()
  {
  return select2html("select * from remarques order by ladate desc",'h',array('<a class="alink" href="/philosophemes/editerremarque.php?id=%s">Editer</a>'));
  }
 
// pour detecter les mobiles
#include_once("$FarmD/cookbook/detect_mobile.php");
#if(detect_mobile_device()) {
#   $Skin = 'amber';
#} 

if (isset($_GET['setskin']))
  $Skin=$_GET['setskin'];
  
include_once("composition/ressources.php");

// pour les affichage des videos sur mobiles en html5 et en flash sur les autres... bof , mais on garde à cause du plein écran qui ne marche pas
//if (detect_mobile_device())
//$inline='<urllink';
//else
  $inline='>links';
Markup_e('ressource',$inline,'/\\(:ress (.*?):\\)/',"getitem(\$m[1],\$m[2])"); // le texte
//Markup_e('aliasressource','fulltext','/@(\\d+)/',"getitem(\$m[1],\$m[2])"); // le texte
Markup_e('aliasressource',$inline,'/@(\\d+)(([^ ]*)?)/',"getitemx(\$m[1],\$m[2])"); // le texte
Markup_e('ressourcef',$inline,'/\\(:ressf (.*?):\\)/',"getitemf(\$m[1],\$m[2])"); // le texte dans une iframe
Markup_e('aliasressourcef',$inline,'/@f(\\d+)/',"getitemf(\$m[1],\$m[2])"); // le texte dans une iframe
Markup_e('ressourcet',$inline,'/\\(:resst (.*?):\\)/',"getitemt(\$m[1],\$m[2])"); // le titre puis le texte
Markup_e('aliasressourcet',$inline,'/@t(\\d+)/',"getitemt(\$m[1],\$m[2])"); // le titre puis le texte
Markup_e('aliasressourcett',$inline,'/@tt(\\d+)/',"getitemtt(\$m[1],\$m[2])"); // le titre du texte
Markup_e('ressourcetf',$inline,'/\\(:resstf (.*?):\\)/',"getitemtf(\$m[1],\$m[2])"); // le titre puis le texte dans une iframe
Markup_e('aliasressourcetf',$inline,'/@tf(\\d+)(\S*?)/',"getitemtf(\$m[1],\$m[2])"); // le titre puis le texte dans une iframe
Markup_e('ressourceT',$inline,'/@T(\\d+)/',"getitemText(\$m[1])"); // le Texte
Markup_e('ressourceA',$inline,'/@A(\\d+)/',"getitemAuthor(\$m[1])"); // l'Auteur

function arobaseto() 
	{
	 return "@"; 
	}
	
Markup('arobase','>links','/AROBASE/',"@");

Markup_e('sujetsdephilo','<aliasressource','/\\(:sujets([^:]*):\\)/',"sujetsdephilo(\$m[1])");
function sujetsdephilo($s)
  {
  $s=str_replace('@',' ',$s); // pour éviter l'interprétation
  return "<a href='/html2pdf/examples/sujets.php?s=$s'>sujets $s</a>"; 
  }
  
Markup_e('poly','<aliasressource','/\\(:poly ([^:]*):\\)/',"poly(\$m[1])");
function poly($s)
  {
  $s=str_replace('@',' ',$s); // pour éviter l'interprétation
  return "<a href='/html2pdf/examples/poly.php?s=$s'><img src='/images/pdf.png' /></a>"; 
  }
Markup_e('poly2','<aliasressource','/\\(:poly2 ([^:]*):\\)/',"poly2(\$m[1])");
function poly2($s)
  {
  $s=str_replace('@',' ',$s); // pour éviter l'interprétation
  return "<a href='/html2pdf/examples/poly2.php?s=$s'><img src='/images/pdf.png' /></a>"; 
  }

/*Markup_e('aliasressourceB', $inline,'/@(\\d+)(\S*?)[\s]/',"montre(\$m[1],\$m[2])"); // le texte
Markup_e('ressource', $inline,'/\\(:ress (.*?):\\)/',"getitem(\$m[1])"); // le texte
Markup_e('aliasressource', $inline,'/@(\\d+)/',"getitem(\$m[1])"); // le texte
Markup_e('ressourcef',$inline,'/\\(:ressf (.*?):\\)/',"getitemf(\$m[1])"); // le texte dans une iframe
Markup_e('aliasressourcef',$inline,'/@f(\\d+)/',"getitemf(\$m[1])"); // le texte dans une iframe
Markup_e('ressourcet',$inline,'/\\(:resst (.*?):\\)/',"getitemt(\$m[1])"); // le titre puis le texte
Markup_e('aliasressourcet',$inline,'/@t(\\d+)/',"getitemt(\$m[1])"); // le titre puis le texte
Markup_e('ressourcetf',$inline,'/\\(:resstf (.*?):\\)/',"getitemtf(\$m[1])"); // le titre puis le texte
Markup_e('aliasressourcetf',$inline,'/@tf(\\d+)/',"getitemtf(\$m[1])"); // le titre puis le texte*/
  
Markup_e('rpp', '<ressource','/\\(:rpp (.*?):\\)/',"getressbyp(\$m[1])");
Markup_e('aliasrpp', '<ressource','/rpp(\\d+)/',"getressbyp(\$m[1])");
function getressbyp($id)
    {
    global $lienpop,$numdrag;
    return "En cliquant sur le numéro de la ressource, elle s'affichera<br/>".select2html("Select ressources.id,ressources.id,ressource,auteur,nature from ressources,ressourcesphilosophemes,auteurs where ressourcesphilosophemes.id_ressource=ressources.id and ressourcesphilosophemes.id_philosopheme=$id and auteurs.id=ressources.id_auteur",'h',array($numdrag,$lienpop),$Z,array('id','ressource','auteur','nature')); 
    }
Markup_e('rppt', '<ressource','/\\(:rppt (.*?):\\)/',"getressbypt(\$m[1])"); // ressource par philosopheme tags
Markup_e('aliasrppt', '<ressource','/rppt(\\d+)/',"getressbypt(\$m[1])"); // ressource par philosopheme tags
function getressbypt($id)
    {
    $result=select2html("Select ressources.id,ressource,auteur,nature from ressources,ressourcesphilosophemes,auteurs where ressourcesphilosophemes.id_ressource=ressources.id and ressourcesphilosophemes.id_philosopheme=$id and auteurs.id=ressources.id_auteur",'h',array('[=%s--%s--%s--%s=]<p/>'),array('%s','%s','%s'));
    return str_replace(' ','-',$result);
    }
Markup_e('nomphilosopheme', 'inline','/\\(:philosopheme (\\d+):\\)/',"nomphilosopheme(\$m[1])"); 
function nomphilosopheme($id)
  {
  return simple_query("select philosopheme from philosophemes where id=$id");
  }
Markup_e('rppj', '<ressource','/\\(:rppj (.*?):\\)/',"getressbypj(\$m[1])");
Markup_e('aliasrppj', '<ressource','/rppj(\\d+)/',"getressbypj(\$m[1])");
function getressbypj($id)
  {
  $liste=sql2array("select ressources.id from ressources,ressourcesphilosophemes where ressourcesphilosophemes.id_ressource=ressources.id and ressourcesphilosophemes.id_philosopheme=$id");
  foreach ($liste as $item)
    {
    $result.="(:ress ".$item[0].":)\n\n<br/>";
    }
  return $result;
  }
Markup_e('rpp_pdf', '<poly','/\\(:rpp_pdf (.*?):\\)/',"getressbyp_pdf(\$m[1])");
Markup_e('aliasrpp_pdf', '<poly','/rpp_pdf(\\d+)/',"getressbyp_pdf(\$m[1])");
function getressbyp_pdf($id)
  {
  global $titre;
  $titre=nomphilosopheme($id);
  $liste=sql2array("select ressources.id from ressources,ressourcesphilosophemes where ressourcesphilosophemes.id_ressource=ressources.id and ressourcesphilosophemes.id_philosopheme=$id");
  $result='(:poly2 ';
  foreach ($liste as $item)
    {
    $result.="$item[0] ";
    }
  $result.=':)';
  return $result;
  }
function nomwiki($i)
  {
  $i=str_replace(array('(',')','"',"'",'/','-','_'),array('','','',"",' ',' ',' '),$i);
  return preg_replace('/[ ](\S)/','\1',ucwords($i));
  }
Markup_e('ppn', '<ressource','/\\(:ppn:\\)/',"getphilobynotions()");
function getphilobynotions()
  {
  return select2html("select id_notion2,id_philosopheme from philosophemesnotion2s".jn(philosophemesnotion2,philosopheme).jn(philosophemesnotion2,notion2)." order by notion2",'h',array('%s','<a href="/Philosophemes/(:nomwiki %1$s:)">%1$s</a>') ,$Z,array('notion','philosophème'));
  }
Markup_e('nomwiki', '>ppn','/\\(:nomwiki([^:]*):\\)/',"nomwiki(\$m[1])");
Markup_e('ppnid', '<ressource','/ppn(\\d+)/',"getphilobyidnotions(\$m[1])");
function getphilobyidnotions($id)
  {
  global $Z,$content;
  return select2html("select id_notion2,id_philosopheme from philosophemesnotion2s".jn(philosophemesnotion2,philosopheme).jn(philosophemesnotion2,notion2)." where (id_notion2=$id) order by notion2",'h',array('%s','<a href="/Philosophemes/(:nomwiki %1$s:)">%1$s</a>'),$Z,array('notion','philosophème'));
  }
Markup_e('listephilosophemes', '<nomwiki','/\\(:lp:\\)/',"listephilosophemes()");
function listephilosophemes()
  {
  return select2html("select id,philosopheme from philosophemes",'h',array('%s','<a href="/Philosophemes/(:nomwiki %1$s:)">%1$s</a>'));
  }
  
Markup_e('listenotions', '<ressource','/\\(:ln:\\)/',"listenotions()");
function listenotions()
  {
  global $Z;
  return select2html("select id,notion2 from notion2s",'h',array('%s','%s'),$Z,array('id','notion'));
  }


Markup_e('philotexte', 'inline','/\\(:ph(\\d+):\\)/',"getitem(\$m[1])");
    
Markup("philo", "inline", "/ ph(\\d+)/"," [[$PubdirUrl/../philosophemes/?action=voir&id=$1|$PubDirUrl/../book.png]]");
//$IMapLinkFmt['http:'] = "\$LinkUrl'";
//$IMapLinkFmt['https:'] = "\$LinkUrl";
//$IMapLinkFmt['exthttp:'] = "\$LinkUrl"; // bidouille infame pour éviter qu'il me mettre des class=urllink dans tous les coins 
Markup("iframe", "inline", "/\\(:iframe (.*?) (.*?):\\)/","<iframe name='$1' src='$2' frameborder=0 width=100% height=600></iframe>");
//Markup("exthttp", ">urllink", "/exthttp:/","http:"); // suite de la biudouille infâme  

Markup_e('arbors','<compositeur','/\\(:arbors:\\)/',"getarbors()");
function getarbors()
  {
  global $Author;
  $id=simple_query("select id from membres where membre='$Author'");
  return "(:compositeur:) <- Nouveau".select2html("Select distinct document,document from arbors where id_membre=$id",h,array("<a class='thickbox' title='Compositeur' href='/composition?doc=%s'>%s</a>"));
  }

// ---------------------

$permrecord = loadPermHolderRecord($AuthId);

//global $LongName 
// utile ?
$FmtPV['$LongName'] ='$GLOBALS["LongName"]';

$LongName=$permrecord[description]; // pour le nom long des gens dans userauth2

if ($Author=='admin')
  $LongName="";
else
  $LongName=$permrecord[description]; // pour le nom long des gens dans userauth2

function cree_au_cas_ou($s)
    {
    if (!file_exists($s))
        mkdir($s);
    }

if ($Author!='') // si auteur identifié
  {
  cree_au_cas_ou("$SitePath/users/$Author"); 
  cree_au_cas_ou("$SitePath/users/$Author/compositeur");
  cree_au_cas_ou("$SitePath/users/$Author/compositeur/pandoc");
  cree_au_cas_ou("$SitePath/users/$Author/compositeur/tmp");
  // si le membre n'existe pas dans la base, lui donner un numero
  $num_membre=simple_query("select id from membres where membre='$Author'");
  //echo "numero=$num_membre";
  if ($num_membre=='')
    simple_query("insert into membres (membre) values('$Author')"); // creation de l'auteur dans la base
  file_put_contents("$SitePath/users/$Author/longname",$LongName);
  }

// un nouveau format pour enlever le mot Accueil des pages perso
function FPLzoli($pagelist, &$matches, $opt) {
      $matches = array_values(MakePageList($pagename, $opt, 0));
      //echo "<pre>";print_r($matches);echo "</pre>";
      foreach ($matches as &$page)
        {
        //echo "*$page*";
        $link=str_replace('.','/',$page);
        $page=str_replace('Accueil','',$page);
        $page=explode('.',$page);
        $page=$page[1];
        $page="<li><a href='/$link'>".trim(preg_replace('/([A-Z])/',' \1',$page))."</a></li>";
        //echo "<pre>$page</pre>";
        }
      return "<ul>".implode('',$matches)."</ul>";
    }
$FPLFormatOpt['zoli'] = array('fn' => 'FPLzoli');

function FPLzoliR($pagelist, &$matches, $opt) {
      $matches = array_values(MakePageList($pagename, $opt, 0));
      //echo "<pre>";print_r($matches);echo "</pre>";
      foreach ($matches as &$page)
        {
        //echo "*$page*";
        $link=str_replace('.','/',$page);
        /*$page=str_replace('Accueil','',$page);
        $page=explode('.',$page);
        $page=$page[1];*/
        $page="<li><a href='/$link'>".trim(preg_replace('/([A-Z])/',' \1',$page))."</a></li>";
        //echo "<pre>$page</pre>";
        }
      return "<ul>".implode('',$matches)."</ul>";
    }
$FPLFormatOpt['zoliR'] = array('fn' => 'FPLzoliR');

include_once("cookbook/inputdraw.php");

$HTMLHeaderFmt['aicss'] = "<link rel='stylesheet' href='$PubDirUrl/skins/ians/ai.css' type='text/css' />";
//$HTMLHeaderFmt['multiselect'] = "<script src='/ai/js/jquery.multi-select.js''></script>";

$HTMLHeaderFmt['confirmation']="
<script language='Javascript' type=''>
      function Confirmation(prompt,cancel,cible)
        { 
        resultat = confirm(prompt);
        if(resultat==1)
          window.location=cible;
        else
          alert(cancel);
        }
        </script>";

// Pour les réseaux sociaux

Markup('fbgpmeta',
      'directives',
      '/\\(:fbgpmeta\\s(.*?):\\)/ei',
      "FbGpMeta(PSS('$1'))");

function FbGpMeta($a){
global $HTMLHeaderFmt;

 $o = ParseArgs($a);

 if($o['image']){
  $o['image'] = PUE($o['image']);
  $im = "\n<meta property=\"og:image\" content=\"".$o['image']."\"/>\n"
        ."<meta itemprop=\"image\" content=\"".$o['image']."\" />\n";
 }

 if($o['title']){
  $title = htmlspecialchars(  $o['title'], ENT_QUOTES);
  $tm = "\n<meta itemprop=\"name\" content=\"".$title."\" />\n"
        ."<meta property=\"og:title\" content=\"".$title."\"/>\n";
 }

 if($o['description']){
  $description =  htmlspecialchars(  $o['description'], ENT_QUOTES);
  $dm = "\n<meta itemprop=\"description\" content=\"".$description."\" />\n"
        ."<meta property=\"og:description\" content=\"".$description."\"/>\n";
 }

 $HTMLHeaderFmt['fbgpmeta'] = $im.$tm.$dm;
}

// cookies (only duration max and crypted username)

require_once('crypt.php');

$duration=time()+60*60*6; // 6 heures
setcookie('AbsoluteSecureCheckIn42',md5('labo'.date('Y-m-d')), $duration, '/');
setcookie('AbsoluteSecureCheckIn421',encrypt_decrypt('encrypt',$Author), $duration, '/');
setcookie('username',$Author,$duration,'/'); // non chiffré (pour le chat) à revoir

// utilisé par la commande (:redirection:) qui se trouve dans la page Site.Redirect (et qui est fixée au login en fonction de la case cochée)
if (isset($_POST[redirect]))
    setcookie('redirection',$_POST[redirect],$duration, '/');
    
// print_r($_COOKIE);


include_once('cookbook/jquerychat.php');
// le chat pour afficher les textes en popup a aussi besoin d'un contentid dans une jqmwindow, comme dans le compositeur

//include_once("philosophemes/sql_config.php");
//include_once("philosophemes/ai.php");
# production de beamer avec m2b
# a besoin de ai pour récupérer les ressources, donc soit être placé après les lignes précédentes

#include_once("$FarmD/cookbook/markdown.php"); // provoque plein d'erreurs
#$EnableMarkdown = 1; 

//include_once("philosophemes/philo-labo.php");
include_once("composition/philo-labo2.php");
Markup_e("Ajout_ressources",">urllink","/\\(:Ajout de ressources:\\)/","ajoutderessources()");
function ajoutderessources()
  {
  global $addressources;
  return code_pour_popup()."$addressources  ";
  }

if ($action == 'beamer')
        {
        include_once("$FarmD/cookbook/m2b.php");
        }

if (isset($ai_lastID))
   echo "la nouvelle ressource porte le numero <a href='/composition/ressif.php?id=$ai_lastID'>$ai_lastID</a>";


   
// pour la création des pages perso
// le groupe Perso/Login et ensuite Login/Pages
//include_once("$FarmD/cookbook/newpageboxplus.php");

// creéation de nouvelles pages

# load script
include_once('cookbook/import.php'); // est-ce qu'il y a un souci avec admin ?
# j'ai été obligé de squizzer le contrôle de version
# check for new files at least once per hour
$ImportFreq = 3600;
$ImportDir='import';

// gestion des cartes clickables
$GroupeActuel=strstr($_GET['n'],'/',true);
if ($GroupeActuel=='')
  $GroupeActuel="Public";
$UploadExtSize['map'] = 1; // autorise l'uload de fichiers map
$UploadExtSize['map'] = 20000;  
// attention danger
// Markup_e('includefile', '>urllink', '/\\(:includefile\\s+([-\\/.\\w]+)\\s*:\\)/',"Keep(implode('', file('$UploadDir/$GroupeActuel/'.\$m[1])))");
// FE ne fonctionne plus apparemment
Markup("clickableimage", "<includefile", "/\\(:imgmap (.*) (.*):\\)/","<img src='$SiteUrl/fichiers/$GroupeActuel/$1' usemap='#$2'>");// (:includefile $2:)");

// Lorem ipsum simple

Markup('ptxt','inline','/t\((.*)\)t/','%justify round frame bgcolor=ivory border=\'0px solid green\'%$1');

Markup("lorem","inline",'/Lorem/','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

// gestion des popup en ajax
Markup("alinkphilosopheme", ">urllink", "/\\(:pop(\\d+) ([^:]*):\\)/","<a class='alink' href='/philosophemes/ress.php?id=$1'>$2</a>");
Markup("alink2", ">urllink", "/\\(:pop ([^:]*):\\)/","<a class='alink' href='/philosophemes/ress.php?id=$1&skin=minimal'>$1</a>");
Markup("alink3", ">urllink", "/\\(:popj (\\w*).(\\w*) (.*):\\)/","<a class='alink' href='/philosophemes/ress.php?$1/$2?skin=minimal'>$3</a>");

Markup("popjqm", ">urllink", "/\\(:popjqm([^:]*):\\)/","<a class='thickbox' title='$1' href='$1?largeur=600&hauteur=400' >ouvre $1 dans un pop up jqModal</a>");
Markup("popalink", ">urllink", "/\\(:popalink (.*):\\)/","<a class='alink' title='$1' href='/$1'>ouvre $1 dans un pop up alink</a>");

Markup("poptimer", ">urllink", "/\\(:poptimer([^:]*):\\)/","<a class='thickbox' title='Timer' target='a' href='/outils/timer/timer.html'>T</a>");
Markup("popkanban", ">urllink", "/\\(:popkanban([^:]*):\\)/","<a class='thickbox' target='a' href='/outils/kanban/kanban.php?doc=$1' title='kanban' >K $1</a>");
Markup("popeisen", ">urllink", "/\\(:popeisen([^:]*):\\)/","<a class='thickbox' title='Matrice d&apos;Eisenhower' target='a' href='/outils/kanban/eisen.php?doc=$1'>E $1</a>");
Markup("compositeur", ">urllink", "/\\(:compositeur:\\)/","<a class='thickbox' title='Compositeur ($Author)' width='1400' target='a' href='/composition'>C</a>");
Markup("compositeur2", ">urllink", "/\\(:compositeur2:\\)/","<a class='thickbox' title='Compositeur' width='1200' target='a' href='/composition/index2.php'>C2</a>");
Markup("compo", ">urllink", "/\\(:compo (.*):\\)/","<a class='thickbox' title='Compositeur' target='a' href='/composition?doc=$1'>composer $1</a>");

Markup("kanban", "inline", "/\\(:kanban([^:]*):\\)/","<iframe width=800 height=600 scrollborder='true' src='/outils/kanban/kanban.php?doc=$1'></iframe>");
Markup("eisen", "inline", "/\\(:matrice_eisen([^:]*):\\)/","<iframe width=800 height=600 scrollborder='true' src='/outils/kanban/eisen.php?doc=$1'></iframe>");
//déclaration de la fonction arbor

//require('composition/version.php');

// imagettes des tutos videos
Markup_e("tutos","<fulltext","/\\(:tutos:\\)/","tutosfunc()");
function tutosfunc()
    {
    $f=glob("/web/philo-labo/tutoriels/*mp4");
    foreach ($f as $file)
        {
        $nom=substr($file,0,-4);
        $tablo=explode('/',$nom);
        $simple=$tablo[sizeof($tablo)-1];
        if (!file_exists("$nom.png"))
            exec("cd /web/philo-labo/tutoriels;ffmpegthumbnailer -s 300 -t -1 -i \"$nom.mp4\" -o \"$nom.png\";exiftool \"$nom.mp4\" | grep 'Media Duration' | sed -E 's/(.*): //' > \"$nom.duration\"");
        $duration=file_get_contents("$nom.duration");
        $result.="<a href='/tutoriels/$simple.mp4'><figure><img style='margin:10px;border:1px dotted black;' src='/tutoriels/$simple.png'/><figcaption style='position: relative;right:10px;top:-16px;text-align:right'>$duration</figcaption></figure></a>&nbsp;&nbsp;";
        }
    return $result;
    }

// affiche une de mes prez par sa couverture
Markup_e("prez","<fulltext","/\\(:prez ([^:]*):\\)/","prezfunc(\$m[1])"); 
// affiche toutes mes prez par leurs couverture
Markup_e("prezs","<fulltext","/\\(:prezs:\\)/","prezsfunc()");
// affiche toutes mes prez partagées par leurs couvertures
Markup_e("prezsp","<fulltext","/\\(:prezsp:\\)/","prezspfunc()");

// test systématiques

Markup_e("systematictests","<urllink","/\\(:systematictests:\\)/","systematictests()");
function systematictests()
    {
    $sql = "SHOW COLUMNS FROM `ressources` LIKE 'nature'";
    $x=explode(',',preg_replace('/(.*)enum\((.*)\)(.*)/','$2',select2html($sql))); // tableau entre quotes
    foreach ($x as &$item)
        $item=str_replace("'","",$item);
    foreach ($x as $item)
        {
        $r.=select2html("select nature,ressource,id from ressources where nature='$item' limit 1",'h',array('<b>%s</b>','%s','@%1$s (:pop %1$s:)' ),array('%s','<tr>%s</tr>','<td>%s<td>'));
        }
    return "<b>Test systématiques d'affichages</b><br/><table class='t'>$r</table>";
    }

// ebooks

Markup_e("sources",">urllink","/\\(:sources:\\)/","affiche_sources()");
function affiche_sources()
    {
    return select2html('select * from sources order by volume DESC,source','h',array('h','<p/><b><big>%s</big></b><br/>','%s<br/>','u','<br/>Volume: <* stars("%s") *>','*','<br/>Qualité: <* stars("%s") *>','*','<br/>Liberté: <* stars("%s") *>','*'),array('%s','%s<hr/>    ','%s'));
    }
    
Markup_e('actus',">urllink","/\\(:actus:\\)/","actus()");
function actus()
  {
  return "<iframe height=225 width=600 frameborder='0' src='/actus.html'></iframe>";
  }

Markup_e("pastille_ebooks",">urllink","/\\(:pastille_ebooks:\\)/","pastille_ebooks()");
function pastille_ebooks()
     {
     return "<a href='?n=Public.Ebooks&skin=minimal'><img style='position:fixed;top300px; left:5%;' src='$SiteUrl/images/ebooks.png'/></a>";
     }

setlocale(LC_ALL, 'fr_FR.UTF8');

function utfsort(&$arr) {
function utfcmp($a, $b) {
return strcmp(iconv('UTF-8', 'ASCII//TRANSLIT', $a), iconv('UTF-8', 'ASCII//TRANSLIT', $b));
}
usort($arr, 'utfcmp');
}

function cover($doc,$file,$page_couv,$nature) // production des couvertures des pdf, epub et mobi
    {
    echo "Génération de $file ($nature)<br/>";
    switch ($nature)
        {
        case 'pdf':
            if ($page_couv=='' or $page_couv==0)
                $page_couv=1;
            exec("pdftk \"$doc.pdf\" cat $page_couv output \"$file.couv.pdf\";convert \"$file.couv.pdf\" -resize 158x223! \"$file.png\";rm \"$file.couv.pdf\"");
            break;
        case 'epub':
            exec("/web/philo-labo/data/scripts_cover/epub-thumbnailer.py \"$doc.epub\" \"$file.png\" 223;");
            break;
        case 'mobi':
            //echo "ebook-convert \"$doc.mobi\" \"$file.epub\";/web/philo-labo/data/scripts_cover/epub-thumbnailer.py \"$file.epub\" \"$file.png\" 223;";
            exec("ebook-convert \"$doc.mobi\" \"$file.epub\";/web/philo-labo/data/scripts_cover/epub-thumbnailer.py \"$file.epub\" \"$file.png\" 223;rm \"$file.epub\"");
            break;
        }   
    exec("pngquant \"$file.png\" -o \"$file.red.png\";mv \"$file.red.png\" \"$file.png\""); // compression de l'imagette
    }

$auteurs=sql2array('select auteur from auteurs');
foreach ($auteurs as $auteur)
    {
    if (trim($auteur[0])!='')
        $auteurs_philo[]=$auteur[0];
    }
utfsort($auteurs_philo);

$liste_ebooks=array();
// affiche les couvertures des pdf et epub
Markup_e("tous_ebooks","<ebooks","/\\(:tous_ebooks:\\)/","all_ebooks()");
function all_ebooks()
    {
    global $auteurs_philo,$total_ebooks,$liste_ebooks;
    $res='';
    $total_ebooks=0;
    $liste_ebooks=array();
    foreach ($auteurs_philo as $auteur)
        {
        $res.=ebooks($auteur);
        }
    $res="<br/><center><b>Il y a $total_ebooks ebooks.</b></center>\n\n$res";
    //echo select2html('select id,auteur from auteurs','h');
    file_put_contents('/web/philo-labo/fichiers/liste_ebooks',implode("\n",$liste_ebooks));
    return "<style>body {background-color:#BBDDBB;}</style>$res";
    }
Markup_e("ebooks",">urllink","/\\(:ebooks ([^:]*):\\)/","ebooks(\$m[1])");
function ebooks($auteur)
    {
    global $total_ebooks,$liste_ebooks;
    $liste1=glob("/web/philo-labo/fichiers/$auteur*pdf");
    $liste2=array_merge($liste1,glob("/web/philo-labo/fichiers/$auteur*epub")); // fusionne les deux tableaux
    $liste=array_merge($liste2,glob("/web/philo-labo/fichiers/$auteur*mobi"));
    $liste_ebooks=array_merge($liste_ebooks,$liste); // on ajoute la liste de cet auteur à la liste générale (celle qui va permettre de fabriquer le zip de tous les ebooks
    asort($liste); //trie le tableau
    $total_ebooks=$total_ebooks+sizeof($liste);
    if ($liste!=array())
        {
        $res="<li class='case'><h3 style='padding-top:80px;font-variant:small-caps;text-align:center;clear:both'>$auteur</h3></li>";
        foreach ($liste as $item)
            {
            $url=str_replace(array('/web/philo-labo',"'"),array('',"\\\\''"),$item);// le \\\'' pour mysql
            $ladate=simple_query("select ladate from ressources where url='$url'");
            $nature=explode(".",$item);
            $nature=array_pop($nature); // on récupère "pdf", "epub" ou "mobi"
            $doc=substr($item,0,-strlen($nature)-1);
            $file=str_replace('/web/philo-labo/fichiers/',"/web/philo-labo/data/cover_$nature/",$doc);
            if (!file_exists("$file.png") or filemtime("$file.png")<$ladate)
                {
                $page_couv=simple_query("select page_couv from ressources where url='$url'");
                cover($doc,$file,$page_couv,$nature);
                }
            $titre=str_replace(array("/web/philo-labo/data/cover_$nature/",'_'),array('',' '),$doc);
            $titre=preg_replace("/$auteur([^-]*)- /",'',$titre);
            $titre=str_replace("/web/philo-labo/fichiers/","",$titre);
            $file=str_replace(array("/web/philo-labo","?"),array("$SiteUrl","%3F"),$file);
            $document=str_replace("$SiteUrl/data/cover_$nature/","/fichiers/",$file).".$nature";
            $res.="<li class='case'><img width=32 style='position:relative;top:230px;left:130px;' src='$SiteUrl/images/icons/32x32/$nature.png'><div><a style='text-decoration:none;' href=\"$document\"><img style='border:1px gray solid;display:inline-block;background-color:white;' width=\"158\" height=\"223\" src=\"$file.png\" alt=\"$file\"><div style='vertical-align:top;font-size:80%;word-wrap:break-word;overflow-wrap: break-word;text-align:left;white-space: normal;'>$titre</div></a></div></li>";
            }
        return "$res";
        }
    }
//$SiteUrl/images/icons/24x24/epub.png
// ***
function prezfunc($doc) // regarde toutes les couverture dans le
  {
  global $Author;
  $doc=trim($doc);
  $rep="/users/$Author/compositeur/pandoc";
  return "<a href='$rep/$doc.pdf'><img width='300' height='300' style='display:inline;float:left;' src='$rep/$doc.png'/></a>";
  }
  
function prezsfunc() 
  {
  global $Author;
  $rep="/web/philo-labo/users/$Author/compositeur/pandoc";
  $pdfs = glob("$rep/*.pdf");
  $result='';
  foreach ($pdfs as &$d)
    $result.=prezfunc(substr($d,strlen($rep)+1,-4)); 
  return $result;   
  }
  
function prezspfunc() 
  {
  global $Author;
  $rep="/web/philo-labo/partage";
  $trees = glob("$rep/*.tree");
  $result='';
  foreach ($trees as &$d)
    {
    $name=substr($d,strlen($rep)+1,-5);
    //$result.="(:popjqm /partage/$name.tree:)"; //<a class='alink' alt='ceci est le titre' href=\"partage/$name.tree\">$name</a>";
    $result.="<a class='thickbox' title='$name' href='/partage/$name.tree'>$name</a><br>";
    }
  return $result;   
  }

// pour les popup: tous les liens de la classe 'alink'
/*$HTMLHeaderFmt['popups'] ="
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js'></script>
  link rel='stylesheet' type='text/css' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css'>*/

 
//peut-être inutile! à remplacer par jqmodal - à voir s'il n'y a pas conflit
$HTMLHeaderFmt['popups'] ="
<script src='/js/jquery-1.3.2.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
<script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    }
 .ui-dialog-shadow { 
-webkit-box-shadow: 12px 12px 16px 0 #000000;
box-shadow: 12px 12px 16px 0 #000000;
 }
  .ui-dialog-titlebar {
  height:0;
  background:ivory;
  border:0;
  }
.ui-dialog-titlebar-close {
  background: url('/js/images/ui-icons_888888_256x240.png') repeat scroll -93px -128px rgba(1, 2, 0, 0);
}
  </style>
	<script>
	jq132(document).ready(function() {
		jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div></div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('alt'),
					width: 800
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href')+'');
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";
	
// pour les  jqmodal
if (!isset($_GET[hauteur]))
  $hauteur=720;
if (!isset($_GET[largeur]))
  $largeur=1280;
$HTMLHeaderFmt['jqmodal']="
<style type='text/css'>
        .jqmOverlay { background-color: #FFF; }
            .jqmWindow {
                background: ivory;
                /*color: #000;*/
                border: 1px solid #888888;
                padding: 0 0 0 0;
                width:".$largeur."px;
                height:".$hauteur."px;
                position: absolute;
                display: none;
                top:15px;
                left:50px;
            }

            button.jqmClose {
                background: none;
                border: 0px solid #EAEAEB;
                color: #000;
                clear: right;
                float: right;
                padding: 0;
                margin-top:5px;
                margin-left:5px;
                cursor: pointer;
                font-size: 8px;
                letter-spacing: 1px;
            }

            button.jqmClose:hover, button.jqmClose:active {
                color: #FFF;
				border: 0px solid #FFF;
            }

            #jqmTitle {
                background: lightblue;
                color: black;
                text-transform: capitalize;
                height: 20px;
                padding: 0px 5px 0 10px;

            }
            #jqmContent {
                width: ".$largeur."px;
                height: ".$hauteur."px;
                display: block;
                clear: both;
                margin: 0;
                margin-top: 0px;
                /*background: #e8e8e8;*/
                border: 1px solid #888888;
                }
        </style>
		<!--[if gte IE 5.5]>
<![if lt IE 7]>
<style type='text/css'>
* html .jqmWindow {
    position: absolute;
    top: expression((document.documentElement.scrollTop || document.body.scrollTop) + Math.round(17 * (document.documentElement.offsetHeight || document.body.clientHeight) / 100) + 'px');
}
</style>
<![endif]>
<![endif]-->
<script>var jq132 = jQuery.noConflict();</script>
<script src='/js/jqmodal/jqModal2fe.js'></script>
<script src='/js/jqmodal/jqDnRfe.js'></script>
<script>
jq132('.jqmWindow').hide();
jq132(document).ready(function(){
    var closeModal = function(hash)
    {
        var \$modalWindow = jq132(hash.w);
        \$modalWindow.fadeOut('0', function()
        {
            hash.o.remove();
        });
    };
    var openInFrame = function(hash)
    {
        var \$trigger = jq132(hash.t);
        var \$modalWindow = jq132(hash.w);
        var \$modalContainer = jq132('iframe', \$modalWindow);
        var myUrl = \$trigger.attr('href');
        var myTitle = \$trigger.attr('title');
        \$modalContainer.html('').attr('src', myUrl);
        jq132('#jqmTitleText').text(myTitle);
        \$modalWindow.show();
        \$modalWindow.jqmShow();
        \$modalWindow.jqResize('.jqResize');
        \$modalWindow.jqDrag();
        \$modalWindow.opacity=1;
      }

    jq132('#modalWindow').jqm({
        /*overlay: 70,*/
        modal: false,// true
        trigger: 'a.thickbox',
        target: '#jqmContent',
        onHide: closeModal,
        onShow: openInFrame,
    });

            });
jq132(document).keydown( function( e ) {
if( e.which == 27) { // escape, close box
jq132('.jqmWindow').jqmHide();
}
});
</script>";

$HTMLFooterFmt['jqmodal']="
<div id='modalWindow' class='jqmWindow'>
        <div id='jqmTitle'>
            <button class='jqmClose'>
            X
            </button>
            <b><span id='jqmTitleText'></span></b>
        </div>
        <iframe id='jqmContent' src='' width=".$largeur."px height=640px>
        </iframe>
    </div>";

//moteur de recherchew
$HTMLHeaderFmt['moteur1']='<script src="/js/jquery-3.2.1.min.js"></script>
           <script src="/js/jquery-ui.min.js"></script>';
           
$HTMLHeaderFmt['gre']="
<script src='/js/jquery.min.js'></script>
<link rel='stylesheet' href='/css/gre.css'>
<script src='/js/jquery.gre.fe.js'></script>";

// pour la saisie multiple
$HTMLHeaderFmt['moteur2']='<script src="/ai/multiselect2/src/jquery.tree-multiselect.js"></script>
    <style>
      * {
        font-family: sans-serif;
      }
      .machin 
        {
        border:none;
        display:inline;
        }
    </style>
    <link rel="stylesheet" href="/ai/multiselect2/dist/jquery.tree-multiselect.min.css">'; 
Markup_e('moteurderessources', 'fulltext','/\\(:moteur de recherche interne:\\)/',"rechercheressource()");
function rechercheressource()
  {
  global $form_search;
  return sprintf($form_search,"")."(:resultatrecherche:)"; // la page de résultat
  }
Markup_e('resultatrecherche', 'fulltext','/\\(:resultatrecherche:\\)/',"resultatrecherche()");

# pour faire un pdf avec la page actuelle
# echo 'c1';
if ($action=='pdf')
	include_once("$FarmD/cookbook/pmwiki2pdf/pmwiki2pdf.php");
#DisableMarkup('dl');
# echo 'c2';

# pour utiliser les image map
include_once("$FarmD/cookbook/imagemap.php");

# pour accepter les svg en upload
include_once('cookbook/svg.php');
$UploadExts['svg']  = 'image/svg+xml';
$UploadExts['svgz'] = 'image/svg+xml';
  
// pour faire du drag'n drop vers d'autres applis
$HTMLHeaderFmt['dragndrop']='<script>
function allowDrop(ev) {
    ev.preventDefault();
}
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}
</script>';

// pour simplifier l'édition des menus

// élément de menu sans image
function monoline($s)
	{ return str_replace(' ','&nbsp;',$s);}
Markup_e('monoline','<elmenui','/\\(:monoline (.*?):\\)/',"monoline(\$m[1])"); // ligne avec des espaces -> ligne sans espace
function correcturl($s)
        { $s[0]=strtoupper($s[0]);$s=str_replace(' ','',$s);return trim($s);}
Markup_e('correcturl','<elmenui','/\\(:correcturl (.*?):\\)/',"correcturl(\$m[1])"); // corrige une url du genre {Authid}Perso/{$LongName}Accueil
Markup('elmenu','fulltext','/\\(:elmenu ([^ ]*?) (.*?):\\)/',
"<span style='margin-left:36px'></span>[[$1|<b>$2</b>]]"); // url texte
//élément de menu avec image
Markup('elmenui','fulltext','/\\(:elmenui ([^ ]*?) ([^ ]*?) (.*?):\\)/',
"||%height=30px%<span style='margin-left:36px'></span>[[$1|$SiteUrl/fichiers/Site/$2]]||%color=white%[[$1|<b>$3</b>]]"); 
// menu vers un lien  avec une image
Markup('tmenuli','fulltext','/\\(:tmenuli ([^ ]*?) ([^ ]*?) (.*?):\\)/',"||%color=orange height=50px%[[$1|$SiteUrl/fichiers/Site/$2]] ||%color=orange%[[$1|<big>$3</big>]]||"); //$
// menu vers un lien 
Markup('tmenul','fulltext','/\\(:tmenul ([^ ]*?) (.*?):\\)/',
"%color=orange%[[$1|<big>$2</big>]]<br/>"); // url texte
// menu sans lien
Markup('tmenu','fulltext','/\\(:tmenu (.*?):\\)/',
"%color=orange%<b><big>$1</big></b><br/>"); // texte
// menu sans lien avec une image
Markup('tmenui','fulltext','/\\(:tmenui ([^ ]*?) (.*?):\\)/',
"||%color=orange height=50px%$SciptUrl/fichiers/Site/$1||<b><big>%color=orange%$2</big></b>||"); // texte

// notes en bas de pages [^footnote text^] pour les appels de notes et [^#^] ou sans ligne bleu: [^@^]
include_once("$FarmD/cookbook/footnote2.php");

$FilesNotify['mailto'] = array("$SiteMyMail");
include_once("$FarmD/cookbook/filesNotify.php");
//echo temps('fin de la configuration');

//print_r($_COOKIE);
//echo "Author=".encrypt_decrypt('decrypt',$_COOKIE['AbsoluteSecureCheckIn421']).'<br/>';
//echo $_COOKIE['username'].'<br/>';
