<?php if (!defined('PmWiki')) exit();
/*  Copyright 2004 Patrick R. Michaud (pmichaud@pobox.com)
    This file is distributed under the terms of the GNU General Public 
    License as published by the Free Software Foundation; either 
    version 2 of the License, or (at your option) any later version.  

    This module enables embedding of Flash animations (.swf) files into
    wiki pages.  It also allows the the size of the animation to be
    controlled by width= and height= in WikiStyles.

    To use this module, simply place this file in the cookbook/ directory
    and add the following line into config.php:

        include_once('cookbook/swf.php');

*/

SDV($SwfTagFmt,
  "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' 
    codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0' >
    <param name='movie' value='\$LinkUrl' />
    <param name='quality' value='high' />
    <embed src='\$LinkUrl' quality='high' 
      pluginspage='http://www.macromedia.com/go/getflashplayer' 
      type='application/x-shockwave-flash'></embed></object>");

Markup('swf', '<urllink', 
  "/\\b(?>(\\L))([^\\s$UrlExcludeChars]+\\.swf)/e",
  "Keep(\$GLOBALS['LinkFunctions']['$1'](\$pagename,'$1','$2',NULL,'$1$2',
    \$GLOBALS['SwfTagFmt']), 'L')");

SDVA($WikiStyleAttr,array(
  'height' => 'img|object|embed',
  'width' => 'img|object|embed'));

?>