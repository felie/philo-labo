<?php if (!defined('PmWiki')) exit();
/*
******************************************************************************************
*                                                                                        *
*    TrueLatex - Enables interpretation and rendering of real LaTeX markups in PmWiki.   *
*    Version: 2.0 (Jul 5, 2012)                                                          *
*    Copyright (C) 2009  Subhrajit Bhattacharya                                          *
*                                                                                        *
*    This program is free software: you can redistribute it and/or modify                *
*    it under the terms of the GNU General Public License as published by                *
*    the Free Software Foundation, either version 3 of the License, or                   *
*    (at your option) any later version.                                                 *
*                                                                                        *
*    This program is distributed in the hope that it will be useful,                     *
*    but WITHOUT ANY WARRANTY; without even the implied warranty of                      *
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                       *
*    GNU General Public License for more details.                                        *
*                                                                                        *
*    You should have received a copy of the GNU General Public License                   *
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.               *
*                                                                                        *
*    Contact: subhrajit@gmail.com, http://subhrajit.net                                  *
*                                                                                        *
*    For installation and usage instructions see                                         *
*        http://www.pmwiki.org/wiki/Cookbook/TrueLatex                                   *
*                                                                                        *
******************************************************************************************
*/

SDV( $TrueLatex->TemplateStyleFolder,  $FarmD.'/cookbook/TrueLatex/' );
SDV( $TrueLatex->TemplateFile, "default.tmpl" );  // TODO: Change TEX template file
SDV( $TrueLatex->StyleFile,  array('geometry.sty')); // TODO: Add style/package files used in template
SDVA( $TrueLatex->TemplateParams, array( 'paperwidth'=>"21", 'paperheight'=>"29", 'fontsize'=>"Huge", 'eqncounter'=>"0" ) );

SDV( $TrueLatex->CodeFolder,  $FarmD.'/cookbook/TrueLatex/' );
SDV( $TrueLatex->CacheFolder,  $FarmD.'/pub/latexcache/' );
SDV( $TrueLatex->CacheURL,  $PubDirUrl.'/latexcache/' );

// Possible values for RenderMethod:
//   "pdflatex:convert" , "latex-dvipdf:convert" , "xelatex:convert" , "xelatex-xdvipdfmx:convert" , "phpfun-foo:convert"
//   phpfun-foo: The PHP function foo($dirname, $filename) is called. Function should take care of creating $filename.".pdf" file.
SDV( $TrueLatex->RenderMethod,  "pdflatex:convert" );
SDV( $TrueLatex->UsePDFColors, false ); // If set to 'true', back/fore colors cannot be altered
SDV( $TrueLatex->BackgroundColor, "transparent" ); // "transparent" or "rrggbb" (hex)
SDV( $TrueLatex->FontColor, null ); // null or "rrggbb" (hex)

SDV( $TrueLatex->OutImageExt,  "png" );
SDV( $TrueLatex->AutoVerbose, true ); // If AutoVerbose is true, the Verbose markup option is ignored

SDV( $TrueLatex->EnableShorthand, true ); // Set this to false in order to disable shorthand

$TrueLatex->RenderMethod = "latex-dvipdf:convert";

// ---------------

$TrueLatex->ParamScopes = array( "", "::");

if (!file_exists($TrueLatex->CacheFolder))
    mkdir($TrueLatex->CacheFolder);
chmod($TrueLatex->CacheFolder, 0755);

// =======================================================================

// markup that handels LaTeX commands
Markup(
    'truelatex',
    'fulltext',
    '/\\(:latex(.*?):\\)(.*?)\\(:latexend:\\)/esi',
    "'<:block>'.Keep(TrueLatex_Render(PSS('$2'),PSS('$1')))");

// TrueLatex shorthand without any option
if ($TrueLatex->EnableShorthand)
    Markup(
        'truelatexShorthand',
        'fulltext',
        '/\\((\\$.*?\\$)\\)/esi',
        "'<:block>'.Keep(TrueLatex_Render(PSS('$1'),''))");

// =======================================================================

function TrueLatex_Render($LatexCode, $OptionString)
{
    global $TrueLatex;
    $TrueLatex->TemplateParams_temp = $TrueLatex->TemplateParams;
    $LatexCode = htmlspecialchars_decode($LatexCode);
    $templateParamString = ExtractOptions($OptionString);
    
    $TrueLatex->TrueLatexError = false;
    $TrueLatex->OutMsg = array("");
    
    $ImageFileNameString = md5($LatexCode.$templateParamString);
    $ImageFile = $TrueLatex->CacheFolder.$ImageFileNameString.'.'.$TrueLatex->OutImageExt;
    $img_path_parts = pathinfo($ImageFile);
    
    if ($TrueLatex->LocalOptions['ClearCache'])
        foreach (glob($TrueLatex->CacheFolder."*.".$TrueLatex->OutImageExt) as $filename)
            if (file_exists($filename))
                unlink($filename);
    
    $attemptedRender = false;
    if (!file_exists($ImageFile) || $TrueLatex->LocalOptions['ForceRender'])
    {
        if (file_exists($ImageFile))
            unlink($ImageFile);
        
        $attemptedRender = true;
        $TempTEXfile = TrueLatex_PrepareCode($LatexCode);  // Create temp-i.tex
        $tex_path_parts = pathinfo($TempTEXfile);
        
        // Create temp-i.pdf, temp-i.aux, temp-i.log, etc. :
        $TrueLatex->OutMsg[] = "<b>Parsing LaTeX:</b>\n";
        if (!$TrueLatex->TrueLatexError)
            $DocBaseName = TrueLatex_parseTeX($TrueLatex->CacheFolder, $tex_path_parts['filename']);
        else
            $TrueLatex->OutMsg[] = "Aborted!";
        
        // Create xxxxxxxxx...xxxx.png
        $TrueLatex->OutMsg[] = "\n";
        $TrueLatex->OutMsg[] = "<b>Extracting image:</b>\n";
        if (!$TrueLatex->TrueLatexError)
            //TrueLatex_extractPNG($TrueLatex->CacheFolder, $DocBaseName, $img_path_parts['basename']);
            //  echo "$TrueLatex->CacheFolder $DocBaseName ".$img_path_parts['basename'];
            {
            exec("convert $TrueLatex->CacheFolder".$DocBaseName." $TrueLatex->CacheFolder".$DocBaseName.".png");
            exec("convert -trim $TrueLatex->CacheFolder".$DocBaseName.".png $TrueLatex->CacheFolder".$img_path_parts['basename']); //FE
            }
        else
            $TrueLatex->OutMsg[] = "Aborted!";
        
        // Delete temp-i.*
        foreach (glob($tex_path_parts['dirname']."/".$tex_path_parts['filename'].".*") as $filename)
            if (file_exists($filename))
                //unlink($filename)
                ;
        
    }
    
    if (!file_exists($ImageFile) && !$TrueLatex->TrueLatexError && $attemptedRender)
    {
        $TrueLatex->TrueLatexError = true;
        $TrueLatex->OutMsg[] = "<b>ERROR in extracting image!</b>\n";
    }
    elseif (!$TrueLatex->TrueLatexError && $attemptedRender)
        $TrueLatex->OutMsg[] = "Done!";
        
    if (!$attemptedRender)
        $TrueLatex->OutMsg[] = "Using image file from cache. You may want to set ForceRender=true for debugging.";
    
    $ImageURL = $TrueLatex->CacheURL.$img_path_parts['basename'];
        $OutHTML = "";
    
    if (!$TrueLatex->LocalOptions['DisplayCode'] || $TrueLatex->LocalOptions['DisplayCodePlus'])
        $OutHTML = $OutHTML."<img class='TrueLatexImage' style='vertical-align:middle;' border=0 src=\"".$ImageURL."\" />";
        
    if ($TrueLatex->LocalOptions['DisplayCode'] || $TrueLatex->LocalOptions['DisplayCodePlus'])
        $OutHTML = $OutHTML."<div class='TrueLatexCode'><pre><code>".htmlspecialchars($LatexCode)."</code></pre></div>";
        
    if ($TrueLatex->LocalOptions['Verbose'] && !$TrueLatex->AutoVerbose)
        $OutHTML = $OutHTML."</br><div class='TrueLatexOutput'><pre><code>\n<b><u>TrueLatex:</u></b><br>".implode($TrueLatex->OutMsg,"\n   ")."</code></pre></div>";
    elseif ($TrueLatex->AutoVerbose && $attemptedRender)
    {
        $page = RetrieveAuthPage($pagename, 'edit', false, READPAGE_CURRENT);
        if (!(!$page))
            $OutHTML = $OutHTML."</br><div class='TrueLatexOutput'><pre><code>\n<b><u>TrueLatex:</u></b><br>".implode($TrueLatex->OutMsg,"\n   ")."</code></pre></div>";
    }
        
        
    return($OutHTML);
}


function ExtractOptions($OptionString)
{
    global $TrueLatex;
    
    $TrueLatex->LocalOptions['ForceRender'] = false;
    if (preg_match('/ForceRender[ \t]*=[ \t"\']*true[ \t"\']*/i', $OptionString))
        $TrueLatex->LocalOptions['ForceRender'] = true;
        
    $TrueLatex->LocalOptions['ClearCache'] = false;
    if (preg_match('/ClearCache[ \t]*=[ \t"\']*true[ \t"\']*/i', $OptionString))
        $TrueLatex->LocalOptions['ClearCache'] = true;
        
    $TrueLatex->LocalOptions['DisplayCode'] = false;
    if (preg_match('/DisplayCode[ \t]*=[ \t"\']*true[ \t"\']*/i', $OptionString))
        $TrueLatex->LocalOptions['DisplayCode'] = true;
        
    $TrueLatex->LocalOptions['DisplayCodePlus'] = false;
    if (preg_match('/DisplayCode[ \t]*\+[ \t]*=[ \t"\']*true[ \t"\']*/i', $OptionString))
        $TrueLatex->LocalOptions['DisplayCodePlus'] = true;
        
    $TrueLatex->LocalOptions['Verbose'] = false;
    if (preg_match('/Verbose[ \t]*=[ \t"\']*true[ \t"\']*/i', $OptionString))
        $TrueLatex->LocalOptions['Verbose'] = true;
    
    // Checking if any of the template parameters are supplied
    foreach ($TrueLatex->TemplateParams_temp as $thisParamName=>$thisParamVal)
    {
        if (preg_match('/^[\w]+$/i', $thisParamName, $theMatch)) // valid param name
        {
            $newVal = false;
            foreach ($TrueLatex->ParamScopes as $scopeprefix)
            {
                
                preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'[ \t]*=[ \t]*([^\s\'"]*)[ \t]*/i', $OptionString, $theValMatch);
                if (count($theValMatch)<1 || $theValMatch[1]=="")
                    preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'[ \t]*=[ \t]*"(.*?)"[ \t]*/i', $OptionString, $theValMatch);
                if (count($theValMatch)<1 || $theValMatch[1]=="")
                    preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'[ \t]*=[ \t]*\'(.*?)\'[ \t]*/i', $OptionString, $theValMatch);
            
                if (count($theValMatch)>=1) {
                    $newVal = $theValMatch[1];
                    break;
                }
                
                preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'[ \t]*\+=[ \t]*([^\s\'"]*)[ \t]*/i', $OptionString, $theValMatch);
                if (count($theValMatch)>=1) {
                    $theIntegerVal = intval($TrueLatex->TemplateParams[$thisParamName]);
                    $theChangeVal = intval($theValMatch[1]);
                    $newVal = strval($theIntegerVal+$theChangeVal);
                    break;
                }
                preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'[ \t]*-=[ \t]*([^\s\'"]*)[ \t]*/i', $OptionString, $theValMatch);
                if (count($theValMatch)>=1) {
                    $theIntegerVal = intval($TrueLatex->TemplateParams[$thisParamName]);
                    $theChangeVal = intval($theValMatch[1]);
                    $newVal = strval($theIntegerVal-$theChangeVal);
                    break;
                }
            
                if (preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'\+\+[ \t]*/i', $OptionString)) {
                    $theIntegerVal = intval($TrueLatex->TemplateParams[$thisParamName]);
                    $newVal = strval(++$theIntegerVal);
                    break;
                }
                if (preg_match('/[ \t]+'.$scopeprefix.$thisParamName.'--[ \t]*/i', $OptionString)) {
                    $theIntegerVal = intval($TrueLatex->TemplateParams[$thisParamName]);
                    $newVal = strval(--$theIntegerVal);
                    break;
                }
                
            }
            
            if ($newVal)
            {
                switch ($scopeprefix)
                {
                    case "":
                    $TrueLatex->TemplateParams_temp[$thisParamName] = $newVal;
                    break;
                    
                    case "::":
                    $TrueLatex->TemplateParams_temp[$thisParamName] = $newVal;
                    $TrueLatex->TemplateParams[$thisParamName] = $newVal;
                    break;
                }
            }
        }
    }
    
    return (implode($TrueLatex->TemplateParams_temp));
}


function TrueLatex_PrepareCode($LatexBody, $LatexHeader="")
{
    global $TrueLatex;
    
    foreach ($TrueLatex->StyleFile as $StyleFile)
    {
        $OriginalStyleFile = $TrueLatex->TemplateStyleFolder.$StyleFile;
        $CopiedStyleFile = $TrueLatex->CacheFolder.$StyleFile;
        if (!file_exists($CopiedStyleFile) || 
              !(filesize($OriginalStyleFile)==filesize($CopiedStyleFile)) || 
                !(filectime($OriginalStyleFile)==filectime($CopiedStyleFile)))
            copy($OriginalStyleFile, $CopiedStyleFile);
    }
        
    $TempFileTry = 0;
    do {
        $TempTEXfile = $TrueLatex->CacheFolder."temp-".$TempFileTry.".tex";
        $TempFileTry++;
    } while(file_exists($TempTEXfile) && $TempFileTry<100);
    
    $TEXfileContent = false;
    $TEXfileContent = file_get_contents($TrueLatex->TemplateStyleFolder.$TrueLatex->TemplateFile);
    if (!$TEXfileContent)
    {
        $TrueLatex->OutMsg[] = "<b>Error:</b> Could not open template file ".$TrueLatex->TemplateFile."\n";
        $TrueLatex->OutMsg[] = TrueLatexDiagnostic_file($TrueLatex->TemplateStyleFolder.$TrueLatex->TemplateFile);
        $TrueLatex->TrueLatexError = true;
    } else {
        $TEXfileContent = str_replace("<$-Body-$>", $LatexBody, $TEXfileContent);
        $TEXfileContent = str_replace("<$-Header-$>", $LatexHeader, $TEXfileContent);
        foreach ($TrueLatex->TemplateParams_temp as $thisParamName=>$thisParamVal)
            $TEXfileContent = str_replace("<$-".$thisParamName."-$>", $thisParamVal, $TEXfileContent);
    
        $couldWriteTemplate = false;
        $couldWriteTemplate = file_put_contents($TempTEXfile, $TEXfileContent);
        if(!$couldWriteTemplate)
        {
            $TrueLatex->OutMsg[] = "<b>Error:</b> Could not write file ".$TempTEXfile."\n";
            $TrueLatex->OutMsg[] = TrueLatexDiagnostic_file($TempTEXfile);
            $TrueLatex->TrueLatexError = true;
        }
    }
        
    return $TempTEXfile;
}

// --------------------

function TrueLatexDiagnostic_file($fname)
{
    return ( "Diagnostics:".
             "\n\t'".$fname."' exists? ".(file_exists($fname)?"yes":"no").
             "\n\tFile permission = ".substr(sprintf('%o', fileperms($fname)), -4).
             "\n\tPHP safe_mode = '".ini_get('safe_mode')."'\n" );
}

// =======================================================================

function TrueLatex_parseTeX($dirname, $filename)
{
    global $TrueLatex;
    $thisMethod = explode(':', $TrueLatex->RenderMethod);
    
    switch ($thisMethod[0])
    {
        case "pdflatex":
            $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                    "cd ".$dirname ." 2>&1 ; ".
                    "pdflatex ".$filename." 2>&1"
                    )));
            return $filename.'.pdf';
            
        case "latex-dvipdf":
            $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                    "cd ".$dirname ." 2>&1 ; ".
                    "latex ".$filename.".tex 2>&1 ; ".
                    "dvipdf ".$filename.".dvi 2>&1"
                    )));
            return $filename.'.pdf';
        
        case "xelatex":
            $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                    "cd ".$dirname ." 2>&1 ; ".
                    "xelatex ".$filename.".tex 2>&1"
                    )));
            return $filename.'.pdf';
        
        case "xelatex-xdvipdfmx":
            $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                    "cd ".$dirname ." 2>&1 ; ".
                    "xelatex -no-pdf ".$filename.".tex 2>&1 ; ".
                    "xdvipdfmx -v ".$filename.".xdv 2>&1"
                    )));
            return $filename.'.pdf';
            
        default:
            $thisSubMethod = explode('-', $thisMethod[0]);
            switch ($thisSubMethod[0])
            {
                case "phpfun":
                    $foo = $thisSubMethod[1];
                    $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", $foo($dirname, $filename)));
                    return $filename.'.pdf';
            }
    }
}


function TrueLatex_extractPNG($dirname, $name_in, $name_out)
{
    global $TrueLatex;
    $thisMethod = explode(':', $TrueLatex->RenderMethod);
    
    switch ($thisMethod[1])
    {
        case "convert":
            // trim
            $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                    "cd ".$dirname ." 2>&1; ".
                    "convert ".$name_in." -trim -blur 10x.3 ".$name_out." 2>&1"
                    )));
            if ($TrueLatex->UsePDFColors)
                break;
            // make transparent background
            $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                    "cd ".$dirname ." 2>&1; ".
                    "convert  ".$name_out." ".$name_out." -alpha Off  -compose Copy_Opacity  -composite  -channel A -negate ".
                            " -channel RGB -fill black -fuzz 100% -draw 'color 1,1 floodfill' ".$name_out." 2>&1"
                    )));
            // Fill RGB chanels (foreground color)
            if ($TrueLatex->FontColor)
                $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                        "cd ".$dirname ." 2>&1; ".
                        "convert  ".$name_out." -channel RGB -fuzz 50% -fill '#".$TrueLatex->FontColor."' -opaque black ".$name_out." 2>&1"
                        )));
            // Set background color
            if ($TrueLatex->BackgroundColor !== "transparent")
                $TrueLatex->OutMsg = array_merge($TrueLatex->OutMsg, explode("\n", shell_exec(
                        "cd ".$dirname ." 2>&1; ".
                        "convert  ".$name_out." -background '#".$TrueLatex->BackgroundColor."' -mosaic +matte -trim ".$name_out." 2>&1"
                        )));
            break;
    }
}

?>
