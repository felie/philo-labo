<?php if (!defined('PmWiki')) exit();
/*	Wikiwyg.php -- Wikiwyg integration module for PmWiki
 * Copyright 2006, 2007 Evan Prodromou <evan@prodromou.name>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/* Set recipe version. */

$RecipeInfo['wikiwyg']['version'] = '20070211';

# Keep the old browse

$WikiwygOldBrowse = $HandleActions['browse'];
$WikiwygOldEdit = $HandleActions['edit'];

$HandleActions['browse'] = 'WikiwygHandleBrowse';
$HandleActions['edit'] = 'WikiwygHandleEdit';

$HandleActions['render'] = 'WikiwygHandleRender';

function WikiwygHandleBrowse($pagename, $auth='read') {
    global $WikiwygOldBrowse;
    WikiwygAddHeaders($pagename);
	return $WikiwygOldBrowse($pagename, $auth);
}

function WikiwygHandleEdit($pagename, $auth='read') {
    global $WikiwygOldEdit;    
    WikiwygAddHeaders($pagename);
	return $WikiwygOldEdit($pagename, $auth);
}

function WikiwygHandleRender($pagename, $auth = 'read') {
	# FIXME: deal with site & group header & footer
	$text = str_replace("\r",'',stripmagic($_POST['text']));
	$html = MarkupToHTML($pagename, $text);

	header("200 OK");
    header("Content-type: text/html");
	header("Content-length: " . strlen($html));

	print $html;
}

function WikiwygAddHeaders($pagename) {
    global $EnablePathInfo, $EnableUpload, $PubDirUrl, $ScriptUrl,
      $LinkWikiWords, $UploadUrlFmt, $UploadPrefixFmt,
      $DefaultGroup, $DefaultName, $HTMLHeaderFmt, $Now;

	/* Setup a local pagename to pass into the JavaScript side. */

	$WikiwygPageName = $pagename;

	if (!$WikiwygPageName) {
		$WikiwygPageName = "$DefaultGroup.$DefaultName";
	}

	$WikiwygEnablePathInfo = ($EnablePathInfo) ? 1 : 0;
	$WikiwygEnableUpload = ($EnableUpload) ? 1 : 0;

	/* Stuff that gets stuck in the header. Note that some important configuration
	 * stuff is passed on to the JavaScript code through a JS variable 'pmwiki'.
	 */

	$HTMLHeaderFmt['wikiwyg'] = <<<END_OF_WIKIWYG_HEADERS

<link rel="stylesheet" type="text/css" href="$PubDirUrl/wikiwyg/css/wikiwyg.css" />
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg.js"></script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg/Util.js"></script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg/Toolbar.js"></script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg/Wikitext.js"></script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg/HTML.js"></script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg/Preview.js"></script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/lib/Wikiwyg/Wysiwyg.js"></script>
<script type="text/javascript">
  var pmwiki = {
	  pagename: '$WikiwygPageName',
	  scripturl: '$ScriptUrl',
	  pubpath: '$PubDirUrl',
	  wikiwords: $LinkWikiWords,
	  basetime: $Now,
	  pathinfo: $WikiwygEnablePathInfo,
	  uploads: $WikiwygEnableUpload,
	  uploadpath: '$UploadUrlFmt',
	  uploadprefix: '$UploadPrefixFmt',
  };
</script>
<script type="text/javascript" src="$PubDirUrl/wikiwyg/Wikiwyg.PmWiki.js"></script>

END_OF_WIKIWYG_HEADERS;
}
    
?>