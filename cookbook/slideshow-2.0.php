<?php
/*
     http://www.cyaneus.net
     By Cárlisson Galdino <bardo@swissinfo.org>
     This cookbook for PmWiki 2 uses S5 http://www.meyerweb.com/eric/tools/s5/
     Installing:
        * Download S5 and uncompress it to pub/s5 folder in your "Farm" directory
	* in local/config.php, include this file by:
	  if ($action == 'slideshow2') <--------------- change by FE, see below
	    { 
	    include_once("$FarmD/cookbook/slideshow-1.3.php"); 
	    $SlidesSkin="actual";
	    }
     History
     	1.2: added contribution by JonHaupt
     2: transformation by François Elie <francois@elie.org>
	the title is the first line of the page
	slides follows !!! headers and not !
	options 
	  $Toc1InSlides: a slide is produced at changes in !
	  $Toc2InSlides: a slide is produced at changes in !!
	  $TocInHeader:  the positions of slide is indicate at right in the header (his !!, his !)
	  $NumInToc: Toc in headers and Slides are numeroted (I -, II - for <h1>, and A. B. C. for <h2>)
	default options are true
     
*/

SDV($Toc1InSlides,true);
SDV($Toc2InSlides,true);
SDV($TocInHeader,true);
SDV($NumInToc,true);
SDV($roman,array('I','II','III','IV','V','VI','VII','VIII','IX','X'));

# Slideshow skin list
global $SlideShowSkinList; 
SDVA($SlideShowSkinList, array (
        'pmwiki' => 'pmwiki',
        'blue' => 'blue',
        'flower' => 'flower',
        'pixel' => 'pixel',
        'pretty' => 'pretty',
        ));

if (!isset($SlideSkin))
  $SlidesSkin = "actual";
# action=...&theme=... to specify a particular theme on display
if (isset($_GET['theme'])) 
    if (@$SlideShowSkinList[$_GET['theme']]) 
      $SlidesSkin = ($_GET['theme']);
    
SDV($HandleActions['slideshow2'],'HandleSlides2');   // François Elie

Markup('slide','_begin','/\(:RSS *(.+):\)/e',"RSS('\$1')");

SDV($SlideList,array());

SDV($SlideShowFmt, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>$Title</title>
<!-- metadata -->
<meta charset="UTF-8">
<meta name="generator" content="S5" />
<meta name="version" content="S5 1.1" />
<!-- configuration parameters -->
<meta name="defaultView" content="slideshow" />
<meta name="controlVis" content="hidden" />
<!-- style sheet links -->

<link rel="stylesheet" href="$FarmPubDirUrl/s5/ui/default/slides.css" type="text/css" media="projection" id="slideProj" />
<link rel="stylesheet" href="$FarmPubDirUrl/s5/ui/default/outline.css" type="text/css" media="screen" id="outlineStyle" />
<link rel="stylesheet" href="$FarmPubDirUrl/s5/skins/$SlidesSkin/skin.css" type="text/css" media="screen" id="themeStyle" />
<link rel="stylesheet" href="$FarmPubDirUrl/s5/ui/default/print.css" type="text/css" media="print" id="slidePrint" />
<link rel="stylesheet" href="$FarmPubDirUrl/s5/ui/default/opera.css" type="text/css" media="projection" id="operaFix" />
<link rel="stylesheet" href="$FarmPubDirUrl/wsplus/wsplus.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="/css/textesS5.css" type="text/css" media="screen"/>

<script language="javascript">function toggleFullScreen(elem) {
    // ## The below if statement seems to work better ## if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
    if ((document.fullScreenElement !== undefined && document.fullScreenElement === null) || (document.msFullscreenElement !== undefined && document.msFullscreenElement === null) || (document.mozFullScreen !== undefined && !document.mozFullScreen) || (document.webkitIsFullScreen !== undefined && !document.webkitIsFullScreen)) {
        if (elem.requestFullScreen) {
            elem.requestFullScreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullScreen) {
            elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        }
    } else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    }
}</script>

<script language=\'javascript\' type=\'text/javascript\'>
function view(question,item,color)
	{
	document.getElementById(\'R\'+question+\'F\').style.display=\'none\';
	document.getElementById(\'R\'+question+\'T\').style.display=\'none\';
	hdl=document.getElementById(\'R\'+question+color);
	hdl.style.display=\'block\';
	}

function result(id,nq)
	{
	frm=document.getElementById(\'form\'+id);
	sum=0;
	for (k=1;k<nq;k++)
		{
		ref=\'Q\'+k;
		fol=frm[ref];
		for (j=0;j<fol.length;j++)
			if (fol[j].checked)
				sum=sum+parseInt(fol[j].value);
		}
	document.getElementById(id).style.display=\'block\';
	//document.forms.quizz.validation.value=\'Vous totalisez \'+sum+\' points !\';
	frm.validation.value=\'Vous totalisez \'+sum+\' points !\';
	}
</script>


<style type=\'text/css\'><!--
  ul, ol, pre, dl, p { margin-top:0px; margin-bottom:0px; }
  code.escaped { white-space: nowrap; }
  .vspace { margin-top:1.33em; }
  .indent { margin-left:40px; }
  .outdent { margin-left:40px; text-indent:-40px; }
  a.createlinktext { text-decoration:none; border-bottom:1px dotted gray; }
  a.createlink { text-decoration:none; position:relative; top:-0.5em;
    font-weight:bold; font-size:smaller; border-bottom:none; }
  img { border:0px; }
  
  .rtl {direction:rtl; unicode-bidi:bidi-override;}
  .ltr {direction:ltr; unicode-bidi:bidi-override;}
  .rtl .indent, .rtl.indent, .rtl .outdent, .rtl.outdent {
    margin-left:0; margin-right: 40px;
  }
  
div.sectionedit { text-align:right;font-size:smaller;clear:both;}

  .indent1 {margin-left:1.25em;}
  .indent2 {margin-left:2.5em;}
  .indent3 {margin-left:3.75em;}
  .indent4 {margin-left:5em;}  
  .indent5 {margin-left:6.25em;}
  .indent6 {margin-left:7.5em;}

  .toc1 {margin-left:1em;}
  .toc2 {margin-left:2em;}
  .toc3 {margin-left:3em;}
  .toc4 {margin-left:4em;}  
  .toc5 {margin-left:5em;}
  .toc6 {margin-left:6em;}
 
  table.chesstable td.square1 { background-color: #cccccc; } 
  table.chesstable { border:1px solid #666666; } .editconflict { color:green; 
  font-style:italic; margin-top:1.33em; margin-bottom:1.33em; }

  table.markup { border:2px dotted #ccf; width:90%; }
  td.markup1, td.markup2 { padding-left:10px; padding-right:10px; }
  table.vert td.markup1 { border-bottom:1px solid #ccf; }
  table.horiz td.markup1 { width:23em; border-right:1px solid #ccf; }
  table.markup caption { text-align:left; }
  div.faq p, div.faq pre { margin-left:2em; }
  div.faq p.question { margin:1em 0 0.75em 0; font-weight:bold; }
  div.faqtoc div.faq * { display:none; }
  div.faqtoc div.faq p.question 
    { display:block; font-weight:normal; margin:0.5em 0 0.5em 20px; line-height:normal; }
  div.faqtoc div.faq p.question * { display:inline; }
   
    .frame 
      { border:1px solid #cccccc; padding:4px; background-color:#f9f9f9; }
    .lfloat { float:left; margin-right:0.5em; }
    .rfloat { float:right; margin-left:0.5em; }
a.varlink { text-decoration:none; }


<!-- embedded styles -->
<style type="text/css" media="all">
.imgcon {width: 525px; margin: 0 auto; padding: 0; text-align: center;}
#anim {width: 270px; height: 320px; position: relative; margin-top: 0.5em;}
#anim img {position: absolute; top: 42px; left: 24px;}
img#me01 {top: 0; left: 0;}
img#me02 {left: 23px;}
img#me04 {top: 44px;}
img#me05 {top: 43px;left: 36px;}
</style>
<!-- S5 JS -->
<script src="$FarmPubDirUrl/s5/ui/default/slides2.js" type="text/javascript"></script>

<script type="text/javascript" src="$FarmPubDirUrl/osmap-pmwiki.js"></script>

</head>
<body>

<div class="layout">
<div id="controls"><!-- DO NOT EDIT --></div>
<div id="currentSlide"><!-- DO NOT EDIT --></div>
<div id="header"></div>

<!--<div id="footer">
<h2>S5 + PmWiki</h2>
</div> -->

</div>
<div class="presentation">');

SDV($SlideSoloFmt,'
<div class="slide">
$SlideContent
</div>');
SDV($HandleSlideShowFmt,array(&$SlideShowFmt,&$SlideList,'</div></body></html>'));

function gettitles($level,$source,&$titles)
  {
  return preg_match_all("/\n$level([\ \w].*)/",$source,$titles);
  }
  
function splittree($level,$source)
  {
  return preg_split("/\n$level([\ \w].*)/", $source);
  }
  
function slide($pagename,$slide)
  {
  global $SlideSoloFmt,$SlideList,$FmtV;
  $FmtV['$SlideContent'] = MarkupToHTML($pagename, $slide);
  $SlideList[] = FmtPageName($SlideSoloFmt, $pagename);
  }
  
function hlist($t,$n,$level)
  {
  if ($level=='!!!!')
    $typenum='roman';
  else
    $typenum='galpha';
  for ($i=0;$i<sizeof($t);$i++)
    {
    $result.=$level;
    if ($i==$n-1)
      $result.="%blue%";
    $result.=numtitle($i,$typenum)."'''$t[$i]'''\n";
    }
  return $result;
  }
  
function numtitle($i,$type)
  {
  global $NumInToc,$roman;
  if (($NumInToc) and ($roman[$i]!=''))
    if ($type=='roman') 
      return $roman[$i]." - ";
    elseif ($type=='galpha')
      return chr(65+$i).". ";
  else 
    return '';
  }

function HandleSlides2($pagename, $auth = 'read') {
  global $HandleSlideShowFmt,$Toc1InSlides,$Toc2InSlides,$TocInHeader,$roman;
  $t = ReadTrail($pagename,$pagename);
  $page = RetrieveAuthPage($pagename, $auth, false, READPAGE_CURRENT);
  if (!$page) Abort("?cannot read $pagename");
  $cbgmt = $page['time'];
  $source = $page['text'];
  $gtitle=substr($source,0,strpos($source,"\n"));
  slide($pagename,'!'.$gtitle);
  $nblevel1=gettitles('!',$source,$titles1);; //to store titles of level 1 in $titles1
  $titles1=$titles1[0];
  foreach ($titles1 as &$title)
    $title=preg_replace('/!(.*)/','\1',trim($title));
  $part1=splittree('!',$source); // parts of level 1
  for ($i=0;$i<sizeof($part1);$i++) // for all part 1
    { 
    if (($i!=0) and $Toc1InSlides)
      slide($pagename,hlist($titles1,$i,'!!!!')); // title 1 announcement
    $nblevel2=gettitles('!!',$part1[$i],$titles2);; //to store titles of level 2 of this title of level 1
    $titles2=$titles2[0];
    foreach ($titles2 as &$title)
      $title=preg_replace('/!!(.*)/','\1',trim($title));
    $part2=splittree('!!',$part1[$i]); // parts of level 2
    for ($j=0;$j<sizeof($part2);$j++) // for all part 2
      {
      if (($j!=0) and $Toc2InSlides)
	slide($pagename,'!!!!'.numtitle($i-1,'roman').$titles1[$i-1]."\n".hlist($titles2,$j,'!!!!!'));
      $nblevel3=gettitles('!!!',$part2[$j],$titles3); //to store titles of level 3 of this title of level 2
      $titles3=$titles3[0];
      foreach ($titles3 as &$title) // remove !
	$title=preg_replace('/!!!(.*)/','%class=titleslide%\1',$title); // !!!->!
      $part3=splittree('!!!',$part2[$j]); // parts of level 3
      array_shift($part3);
      for ($k=0;$k<$nblevel3;$k++)
	{
	if (($i!=0) and $TocInHeader)// production of tree for maps
	  {
	  $pref1=numtitle($i-1,'roman');
	  $pref2=numtitle($j-1,'galpha');
	  $l0="%class=gtitle%$gtitle";
	  $l1="!!".$pref1.$titles1[$i-1]; 
	  $l2='';
	  if ($titles2[$j-1]!='')
	    $l2="!!!".$pref2.$titles2[$j-1];
	  $map="$l0\n$l1\n$l2"; // plan
	  }
	else
	  $map=''; // first page (cover)
	slide($pagename,$map.$titles3[$k]."\n".$part3[$k]);
	}
      }
    }
  PrintFmt($pagename,$HandleSlideShowFmt);
  exit();
}

