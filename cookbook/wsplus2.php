<?php

Markup('warning','block',"/^\(:warning (.*?):\)\s*$/e",
	">>warning<<\r\n$1>><<");
	
/* modèle
Markup('beamer','block',"/^\(:beamer[ 	]*(.*?) -- (.*?):\)\s*$/e",
	"beamer('$1','$2')");
	*/

Markup('epigraph','block',"/^\(:epigraph (.*?):\)\s*$/e",
        ">>epigraph<<\r\n$1>><<");
SDVA($WikiStyle['lpostit'], array('class'=>'frame lfloat sidenote postit'));

?>
