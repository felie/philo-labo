<?php

/* utilise wsplus */

Markup('texte','block',"/^\(:texte(.*?)--(.*?):\)\s*$/","<div class='texte'><p>$1</p></div>\r\n<div class='signature'><p>$2</p></div>\r\n");

Markup('tip','directives',"/^\(:tip(.*?):\)\s*$/","<div class='tip round lrindent'><p>$1</p></div>\r\n");

Markup('important','directives',"/^\(:important(.*?):\)\s*$/","<div class='round lrindent important'><p>$1</p></div>\r\n");

Markup('warning','directives',"/^\(:warning(.*?):\)\s*$/","<div class='round lrindent warning'><p>$1</p></div>\r\n");

Markup('sidenote','directives',"/^\(:sidenote(.*?):\)\s*$/","<div class='frame rfloat sidenote'><p>$1</p></div>\r\n");

Markup('postit','directives',"/^\(:postit ([^-]*?)--(.*?):\)\s*$/","<div class='frame rfloat sidenote postit'><p class='notetitle'>$1\r\n</p><p><p class='vspace'>$2\r\n</div></p></div>\r\n");
	

?>
