<?php if (!defined('PmWiki')) exit ();

/**
    Markup for embedding DailyMotion videos

    copyright 2015 AntonyTEmplier
    copyright 2007 Anno


    Build on code from swf.php copyright 2004 Patrick R. Michaud
    and from quicktime.php copyright 2006 Sebastian Siedentopf.
    This file is distributed under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This module enables embedding of DailyMotion videos into
    wiki pages. simply use (:daily whatever:) where "whatever" is
    replaced with the cryptic id that is attached after video/ of DailyMotion URLs.
    So for example if the URL to share a video is:

    http://www.dailymotion.com/video/x31eya4_the-hateful-eight-premier-trailer-jouissif-pour-le-dernier-tarantino_shortfilms

    then you'd write (:daily x31eya4:) or
    (:daily x31eya4_the-hateful-eight-premier-trailer-jouissif-pour-le-dernier-tarantino_shortfilms:).

*/

# Version date
$RecipeInfo['Dailymotion']['Version'] = '20150814';

# Default parameters
SDVA(
    $Dailymotion,
        array(
            'background' => '493D27',
            'foreground' => 'E8D9AC',
            'highlight' => 'FFFFF0'
        )
);

# (:daily xbulz0:)
if(function_exists('Markup_e')) {
    Markup_e('daily', '<img', "/\\(:daily (.*?)\\s*:\\)/", "ShowDaily(\$m[1])");
} else {
    Markup('daily', '<img', "/\\(:daily (.*?)\\s*:\\)/e", "ShowDaily('$1')");
}

function ShowDaily($url) {
    global $Dailymotion;
    $params = '';
    if (count($Dailymotion)) {
        $params = '?'.http_build_query($Dailymotion);
    }
    $out = '<iframe
    src="http://www.dailymotion.com/embed/video/'.$url.$params.'"
    width="560"
    height="315"
    frameborder="0"
    allowfullscreen></iframe>';
    return Keep($out);
}
