<?php 
//if (!defined('PmWiki')) exit();

/*****************************************************************************************
*                                                                                        *
*    gfsjquerychat provide connection to                                                 *
*    Gmail/Facebook Style jQuery Chat                                                    *
*    http://anantgarg.com/2009/05/13/gmail-facebook-style-jquery-chat/                   *   
*                                                                                        *
*    gplv2 François Elie 2016 (francois@elie.org)                                        *
*                                                                                        *
*    This program is free software: you can redistribute it and/or modify                *
*    it under the terms of the GNU General Public License as /published by                *
*    the Free Software Foundation, either version 3 of the License, or                   *
*    (at your option) any later version.                                                 *
*                                                                                        *
*    This program is distributed in the hope that it will be useful,                     *
*    but WITHOUT ANY WARRANTY; without even the implied warranty of                      *
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                       *
*    GNU General Public License for more details.                                        *
*                                                                                        *
*    You should have received a copy of the GNU General Public License                   *
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.               *
*                                                                                        *
*    BE CAREFUL: Gmail/Facebook is for non commercial usage                              *   
*    otherwise see cometchat.com                                                         *
*                                                                                        *
*****************************************************************************************/

$RecipeInfo['jquerychat']['Version'] = '2016-04-10';
$RecipeInfo['jquerychat']['Author'] = 'felie';

// le premier js est normalement pub/jqchat/chat/jsquery.js mais bon... il entre en conflit et fait disparaître les ascenseurs
$chat="/pub/chat";
$HTMLHeaderFmt['chat'] = '
<script src="/js/jquery-1.11.3.min.js"></script>
<script src="'.$chat.'/js/chat.js"></script>
<link type="text/css" rel="stylesheet" media="all" href="'.$chat.'/css/chat.css" />
<!--<link type="text/css" rel="stylesheet" media="all" href="'.$chat.'/css/screen.css" />-->
<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="'.$chat.'/css/screen_ie.css" />
<![endif]-->';

if (!isset($_SESSION['username']))
    $_SESSION['username']=$_COOKIE['username']; // déjà fait par le config.php de pmwiki
$loggedinrep=$_SERVER[DOCUMENT_ROOT]."/pub/chat/loggedin"; // to store loggedin signal
$signal="$loggedinrep/$Author"; // signal files are list of connectez people
if ($_GET[action]=='logout')
    unlink($signal); // delete the signal file
else    
    file_put_contents($signal,''); // put the signal file 

if (function_exists('Markup_e'))
  Markup_e("chats", "inline", "/\\(:chats:\\)/","chats()"); // a markup to see other, providing links to open chat with another connected user

# display connected people

function chats()
    {
    global $loggedinrep,$Author;
    $n=0;
    $iterator = new DirectoryIterator($loggedinrep);
    $result="<form style='display: inline;'><select style='background-color:orange;' name='present' size='1' onclick='javascript:chatWith(this.options[this.selectedIndex].value);'>";
    $entries=array();
    foreach ($iterator as $fileinfo) 
        if ($fileinfo->isFile()) 
            {
            $f=$fileinfo->getFilename();
            if ($f!=$Author and $f[0]!='.')
                if  (time()-$fileinfo->getMTime() > 20*60) // deconnexion quand 20mn d'inactivité
                    unlink("$loggedinrep/".$f);
                else
                    {
                    $n++;
                    $entry=$f;
                    $entries[].="<option style='min-width:100px;width:100px;' value='$entry'>$entry</option>";
                    }
            }
    asort($entries);
    $result.=implode('',$entries); 
    if ($n!=0)  
        return "<div style='display: inline;color:orange'>($n)</div>".$result."</select></form>";
    }
?>
