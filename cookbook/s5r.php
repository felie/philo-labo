<?php
/*
     http://www.cyaneus.net
     
     By C�rlisson Galdino <bardo@swissinfo.org>
     
     This cookbook for PmWiki 2 uses S5 reloaded (v 1.3b7) http://www.netzgesta.de/S5/
     
     Installing:
     
     Download S5 
        http://www.netzgesta.de/archive/S5_1.3beta7.zip
     uncompress it to pub/s5r folder in your "Farm" directory

     in /local/config.php, add:
     if ($action == 's5r')
        include_once('cookbook/s5r.php
*/

SDV($HandleActions['s5r'],'HandleSlidesS5R');  
Markup('slide','_begin','/\(:RSS *(.+):\)/e',"RSS('\$1')");

SDV($SlideList,array());

SDV($SlideShowFmt, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>***</title>
<!-- metadata -->
<meta name="generator" content="S5" />
<meta name="version" content="S5 1.29" />
<meta name="author" content="Eric A. Meyer" />
<meta name="company" content="Complex Spiral Consulting" />
<!-- meta extensions -->
<meta name="subject" content="S5 1.29 a.k.a. one4all" />
<meta name="creator" content="Christian Effenberger" />
<meta name="contributor" content="youcan[64]netzgesta[46]de" />
<meta name="contributor" content="Fran�ois Elie" />
<meta name="publisher" content="s5.netzgesta.de" />
<meta name="description" content="S5 1.29 is a very flexible and lightweight slide show system available for anyone to use (including transitions and scalable fonts and images), and the implementation in PmWiki (FE) is very useful" />
<meta name="keywords" content="S5, slide show, projection-mode, powerpoint-like, scala-like, keynote-like, incremental display, scalable fonts, scalable images, transitions, osf, xoxo, css, javascript, xhtml, public domain" />
<meta name="robots" content="index, follow" />
<meta name="revisit-after" content="7 days" />
<!-- meta temporary -->
<meta http-equiv="content-type" content="text/html; charset=iso8859-1" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<!-- configuration parameters -->
<meta name="defaultView" content="slideshow" />
<meta name="controlVis" content="visible" />
<!-- configuration extensions -->
<meta name="tranSitions" content="true" />
<meta name="fadeDuration" content="500" />
<meta name="incrDuration" content="250" />
<!-- configuration autoplay extension -->
<meta name="autoMatic" content="false" />
<meta name="playLoop" content="true" />
<meta name="playDelay" content="10" />
<!-- style sheet links -->
<link rel="stylesheet" href="ui/default/slides.css" type="text/css" media="projection" id="slideProj" />
<link rel="stylesheet" href="ui/default/outline.css" type="text/css" media="screen" id="outlineStyle" />
<link rel="stylesheet" href="ui/default/print.css" type="text/css" media="print" id="slidePrint" />
<link rel="stylesheet" href="ui/default/opera.css" type="text/css" media="projection" id="operaFix" />
<!-- embedded styles -->
<style type="text/css" media="all">
.imgcon {width: 100%; margin: 0 auto; padding: 0; text-align: center;}
#anim {width: 33%; height: 320px; position: relative;}
#anim img {position: absolute; top: 0px; left: 0px;}
.top {position: absolute; top: 0; left: 0;}
.red {color: red;}
.grey {color: gray;}
.bottom {position: absolute; bottom: 2em; right: 0em;}
.red {color: #C02;}
</style>
<!-- embedded JS -->
<script type="text/javascript">
<!--'."
var IE=navigator.appName=='Microsoft Internet Explorer'&&navigator.userAgent.indexOf('Opera')<1?1:0; if(IE) var IE7=window.XMLHttpRequest?1:0;
function png2gif(src,cls,width,height,alt) {
  var source = (isIE&&!isIE7?src+'.gif':src+'.png');
  document.writeln('<img src=\"' + source + '\" class=\"' + cls + '\" width=\"' + width + '\" height=\"' + height + '\" alt=\"' + alt + '\" \/>');
}
//-->
</script>".'
<!-- S5 JS -->
<script src="ui/default/slides.js" type="text/javascript"></script>
</head>
<body>

<div class="layout">
<div id="controls"><!-- DO NOT EDIT --></div>
<div id="currentSlide"><!-- DO NOT EDIT --></div>
<div id="header">
<script type="text/javascript">
<!-- // IE <= 6 hack
png2gif("pix/sun","scale top",256,256,"sun");
//-->
</script>
</div>
<div id="footer">
<h1>S5 Testbed</h1>'."
<h2>Your computer &#8226; Today's date</h2>".'
<script type="text/javascript">
<!-- // IE <= 6 hack
png2gif("pix/sunflower","scale bottom",102,200,"flower");
//-->
</script>
</div>
</div>
<div class=\'presentation\'>');

SDV($SlideSoloFmt,'

<div class=\'slide\'>
$SlideContent
</div>');

SDV($HandleSlideShowFmt,array(&$SlideShowFmt,&$SlideList,'</div></body></html>'));

function HandleSlidesS5R($pagename, $auth = 'read') {
  global $SlideShowFmt, $SlideSoloFmt,
    $SlideList,$RssItemFmt,
    $HandleSlideShowFmt,$FmtV,$ScriptUrl,$Group,$Name;
	 
  $t = ReadTrail($pagename,$pagename);
  $page = RetrieveAuthPage($pagename, $auth, false, READPAGE_CURRENT);
  if (!$page) Abort("?cannot read $pagename");
  $cbgmt = $page['time'];
  $source = $page['text'];
  $number_of_items = preg_match_all('/\n\!([\ \w].*)/', $source, $titles); // get the number of items and the dates
  $body_of_items = preg_split('/\n\!([\ \w].*)/', $source); // get the number of items and the dates
  $titles = $titles[0];
  for ($i = 0; $i < $number_of_items; $i++) {
    $FmtV['$SlideContent'] = MarkupToHTML($pagename,$titles[$i]. $body_of_items[$i + 1]);
 $SlideList[] = FmtPageName($SlideSoloFmt, $pagename);
 }   
  PrintFmt($pagename,$HandleSlideShowFmt);
  exit();
}

?>
