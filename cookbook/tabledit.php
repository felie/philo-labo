<?php if (!defined('PmWiki')) exit();
$RecipeInfo['TableEdit']['0.3'] = '20070408';
# Pierre ROUZEAU 7 april 2007 - Table edition 'tabledit' v0.3
# 0.3 rev. Amendments by Nick Bell (first line bug removal + presentation)
# contact:   pierre  ��t  rouzeau  d��t  net
#license  http://www.gnu.org/licenses/gpl.html GNU General Public License

$HTMLStylesFmt['tabledit']= "
div.tabledit {text-align:right;font-size:smaller;}
";
$TEdeftable = '(:table now border=1 width=80%:)\\\n(:cellnr:)\\\n(:cell:)'.
   '\\\n(:cell:)\\\n(:cellnr:)\\\n(:cell:)\\\n(:cell:)\\\n(:tableend:)\\\n';

SDV($GUIButtonDirUrlFmt,'$FarmPubDirUrl/guiedit');
SDV($TEEditFormNum,'1');

$GLOBALS['TEDisable'] = FALSE;
$GLOBALS['TEInInclude'] = FALSE;

Markup('tabledit', '<table', '/^\\(:(table\\d*(?:)?)(\\s.*?)?:\\)/ei', "TableEditCreateLink(\$pagename, '$2')");
Markup('notabledit', '<tabledit', '/\\(:(no)?tabledit:\\)/ei',"PZZ(\$GLOBALS['TEDisable']=('$1'=='no'))");
Markup('tedinclude', '<tabledit', '/\\(:tableinclude(in)?:\\)/ei',"PZZ(\$GLOBALS['TEInInclude']=('$1'=='in'))");

## (:include:) - beware, when sectionedit loaded, there is a competition to modify the include
Markup('include', '>if', '/\\(:include\\s+(\\S.*?):\\)/ei', "PRR(TEIncludeText(\$pagename, '$1'))");

function TEIncludeText($pagename,$inclspec) {
	if (function_exists('SectionEditIncludeText')) // take into account SectionEdit addon
		return "(:tableincludein:)\n".SectionEditIncludeText($pagename,$inclspec)."(:tableinclude:)";
	else	
		return "(:tableincludein:)\n".IncludeText($pagename,$inclspec)."(:tableinclude:)";
}	

/*** Convert table markup (:table ...:) into clickable editlinks*/
function TableEditCreateLink($pagename, $TEpar) {
	global $TECount, $ScriptUrl, $TEDisable, $TEInInclude, $InclCount, $EnablePost, $HandleAuth;
    if (!isset($TECount)) $TECount = 0;
	$old= Cells('table',PSS($TEpar));
	/*create editlink with anchor*/
	$editlink = FmtPageName("<a name='s$pagename".'_'."$TECount'></a><a href='\$PageUrl?action=tabledit&s=".
		$TECount."' rel='nofollow'>($[Table edit &#x2193;])</a>", $pagename);
	$out = "<div id='tbls$TECount' class='tabledit'>$editlink</div>".$old;
	if (substr($TEpar,1,3)== 'now') {
		$out = FmtPageName("<script language='javascript' type='text/javascript'>
		window.location='\$PageUrl?action=tabledit&s=$TECount'</script>".$old, $pagename);
		Lock(2);
		$page = RetrieveAuthPage($pagename, 'edit', true);
		if (!$page) Abort("?cannot edit $pagename"); 
		PCache($pagename,$page);
		$new = $page;
		$new['text'] = str_replace ('(:table now', '(:table', $new['text']);
		PostPage($pagename, $page, $new);
		Lock(0);
	}	
	if ((!$TEDisable)&&(!$TEInInclude) && CondAuth($pagename, 'edit')) { #  $EnablePost ??
		$TECount++;
		return $out;
	}
}

SDV($GUIButtons['tabledit'], array(1200, '', '', '',
  "<a href=\\\"javascript:insMarkup('$TEdeftable','',''); document.forms[$TEEditFormNum].elements['post'].click();\\\"><img src='$GUIButtonDirUrlFmt/table.gif' title='$[Insert table]'></a>"));

if ($action!='tabledit' && $action!='tableval') return;

SDV($HandleActions['tabledit'],'HandleTableEdit');
SDV($HandleActions['tableval'],'HandleTableVal');

function TEbrowser_detect () {
	$browser = '';
	$dom_browser = '';
	$navigator_user_agent = strtolower( $_SERVER['HTTP_USER_AGENT'] );
	if (stristr($navigator_user_agent, "msie")) {
		$browser = 'msie';
		$dom_browser = true;
	}
	elseif (stristr($navigator_user_agent, "gecko")) {
		$browser = 'mozilla';
		$dom_browser = true;
	}
	return $browser;
}

function TEexplode($pagename) {
	global $TEtbargs, $TEcells, $TEelargs, $TEelval, $TEnbcol, $TEnbrow, $TEindex, $TEnum, $TEtext;
	clearstatcache ();
	$page = RetrieveAuthPage($pagename,'edit'); 
	if (!page) Abort("?cannot edit $pagename");
	PCache($pagename,$page); # what is the use ? 
	$TEtext = str_replace ('�', '', $TEtext); // remove any '�' which may exists in the text...
	$TEtext = preg_replace ('/^\(:tableend(\s)?:\)/m', '�', $page['text']);
	preg_match_all ('/^\(:table[^�]*/m', $TEtext, $matches, PREG_SET_ORDER);
	$table=$matches[$TEnum][0]; // extracted table
	$TEindex = 0; // for the callback function
	$TEtext = preg_replace_callback('/^\(:table[^�]*/m', "TEselect_par", $TEtext);// set '#zp0Z#' instead of the table
	$TEtext = str_replace ('�', '(:tableend:)', $TEtext);
	$table = preg_replace ('/^\(:cell/Um', '(:cellnr', $table, 1); // if first cell not cellnr, replace by cellnr
	$table = str_replace ('(:cellnrnr', '(:cellnr', $table); // I shall improve former expression !
	$table = str_replace ("\x5C\x5C\n", '[[<<]]', $table);
	$table = str_replace ("\n", '�', $table);
	$table = str_replace ('[[<<]]', "\n", $table);
	$TEcells = explode('�', $table); // separate lines
	$TEcells[0] = str_replace ('(:table now', '(:table', $TEcells[0]); // remove the 'now' set for self loading
	preg_match ('/^\(:(table\d*(?:)?)(\s.*?)?:\)/i', $TEcells[0], $mt);
	$TEtbargs = ParseArgs($mt[2]);
	unset ($TEtbargs['#']);
	$icol=1;
	$irow=0;
	$TEnbcol=1;
	$itab=1;
	while ($el=$TEcells[$itab++]) {
		if (preg_match ('/^\(:cell(nr)?(\s.*?)?:\)(.*)?/sm', $el, $mt)) {
			if ($mt[1]=='nr') {
				if (--$icol > $TEnbcol) $TEnbcol=$icol;
				$irow++; 
				$icol=1;
			} 
	 		$TEelargs [$irow][$icol] = ParseArgs($mt[2]);
			unset ($TEelargs [$irow][$icol]['#']);
			$TEelval [$irow][$icol++] = $mt[3];
	 	}
		else {
			# there's a bug in table : do something ?
		}
	}
	if (--$icol > $TEnbcol) $TEnbcol=$icol; // Last row col number check 
	$TEnbrow = $irow;
}

function TEselect_par ($matches) {
	global $TEnum, $TEindex; 
	$TEindex++;	
	if (($TEnum+1)==$TEindex)
		return '#zp0Z#';
	else 
		return $matches[0];
}

function HandleTableEdit($pagename) {
	global $TEtbargs, $TEelargs, $TEelval, $TEnbcol, $TEnbrow, $TEnum, $TEcont, $PageStartFmt;
	$TEnum = @ $_GET['s'];
	$htarea = 3;
	if (TEbrowser_detect()=='mozilla') $htarea = 2;
	TEexplode($pagename);
	$prefixrad = "<input type='radio' name='align' value=";
	switch ($TEtbargs['align']) {
		case 'left': $tleft='checked';	break;
		case 'center': $tcenter='checked';	break;
		case 'right': $tright='checked';	break;
		default: $tdefault='checked';
	}
	$out= "
<form enctype='multipart/form-data' action='\$PageUrl?action=tableval&s=$TEnum$TEcont' method='post'>
<input type='hidden' name='action' value='tbledit' />
<table align='center' cellspacing='8'>
<tr>
<td> $[Rows]</td><td title='$[Number of rows]'><input name='nbrow' value='$TEnbrow' maxlength=3 size=2 /></td>
<td> $[Cell spacing]</td><td title='$[Cell padding in pixels]'><input name='cellsp' value='$TEtbargs[cellspacing]' maxlength=1 size=2 /></td>
<td> $[Width]</td><td title='$[Table width as % of browser width]'><input name='tbwidth' value='$TEtbargs[width]' maxlength=3 size=3 />%</td>
</tr><tr>
<td>$[Columns]</td><td title='$[Number of columns]'><input name='nbcol' value='$TEnbcol' maxlength=3 size=2 /></td>
<td> $[Cell padding]</td><td title='$[Cell padding]'><input name='cellpad' value='$TEtbargs[cellpadding]' maxlength=1 size=2 /></td>
<td>$[Border]</td><td title='$[Table border width in pixels]'><input name='border' value='$TEtbargs[border]' maxlength=1 size=3 />px</td>
</tr><tr>
<td align=center colspan=4 title='$[Table alignment]'>
$[Alignment]
$prefixrad'' $tdefault/> <em>$[default]</em>
$prefixrad'left' $tleft/> <em>$[left]</em>
$prefixrad'center' $tcenter/> <em>$[center]</em>
$prefixrad'right' $tright/> <em>$[right]</em>
</td>
</tr><tr>
<td align=center colspan=4>
<input type='submit' name='okedit' value='&nbsp;$[Save and edit]&nbsp;' title='$[Save table and continue editing]'/> &nbsp;&nbsp; 
<input type='submit' name='okreturn' value='&nbsp;$[Save and quit]&nbsp;' title='$[Save table and return to page]'/> &nbsp;&nbsp; 
<input type='submit' name='undo' value='&nbsp;&nbsp;$[Undo]&nbsp;&nbsp;'/> &nbsp;&nbsp;
<input type='submit' name='cancel' value='&nbsp;&nbsp;$[Cancel]&nbsp;&nbsp;' />
</td></tr>
</table>
<hr /><table align='center'><tr><td></td>";
	for ($icol=1; $icol<=$TEnbcol; $icol++) { # column headers
		$tl=''; $tc=''; $tr=''; $td='';
		$colalign [$icol] = @$TEelargs[1][$icol]['align'];
		switch ($colalign [$icol]) {
			case 'left': $tl='checked'; break;
			case 'center': $tc='checked'; break;
			case 'right': $tr='checked'; break;
			default: $td='checked';
		}
		$prad = "<input type='radio' name='c$icol' value=";
		$cwidth = $TEelargs[1][$icol][width];
		$out .= "\n<td align=center>
		<table width='100%'><tr>
		<td align=center title='$[Column width (% of table width)]'>$[Width:]<input name='width$icol' value='$cwidth' maxlength=3 size=3 />%</td>
		</tr><tr>
		<td align='center'><input type='submit' name='inscol$icol' value=' $[Ins] ' title='$[Insert a new column to the left of this one]'/>
		<input type='submit' name='delcol$icol' value=' $[Del] ' title='$[Delete this column]' onclick=\"return confirm('$[Are you sure you want to delete this column?]');\" /></td></tr>
		<tr><td align=center>$[Alignment]<br />$prad'' $td title='$[Default alignment]'/> |	$prad'left' $tl title='$[Align column left]'/>&larr;
		$prad'center' $tc title='$[Align column centre]'/>&rarr;$prad'right' $tr title='$[Align column right]'/></td></tr></table></td>\n";
	}
	$out .= "\n<td rowspan='".($TEnbrow+2)."'><input type='submit' name='inscol$icol' value='$[Add]' title='$[Add a new column on the right hand side of this table]'/></td>";
	$out.="</tr>";
	for ($irow=1; $irow<=$TEnbrow; $irow++) {
		$out.="<tr><td>
		<input type='submit' name='insrow$irow' value='$[Ins]' title='$[Insert a new row above this one]'/><br />
		<input type='submit' name='delrow$irow' value='$[Del]' title='$[Delete this row]' onclick=\"return confirm('$[Are you sure you want to delete this row?]');\"/></td>
		";
		for ($icol=1; $icol<=$TEnbcol; $icol++) {
			$val = $TEelval[$irow][$icol];
			$name = 'r'.$irow.'c'.$icol;
			$calign= $colalign [$icol];
			$twidth = max (intval(80/$TEnbcol), 15);
			$out .= "\n<td><textarea name='$name' rows='$htarea' cols='$twidth' wrap='virtual' align=$calign>$val</textarea></td>";
		}
		$out .= "\n</tr>";
	}
	$out .= "\n<tr><td colspan='" . ($TEnbrow + 1) . "' align='center'><input type='submit' name='insrow$irow' value='$[Add]' title='$[Add a new row to the bottom of this table]'/></td></tr>";
	$out.="\n</table></form>";
	$GLOBALS['PageHeaderFmt']=''; // Remove header
	$GLOBALS['PageLeftFmt']='';
	$GLOBALS['PageTitleFmt']='';
	$GLOBALS['PageActionFmt']='';
	PrintFmt($pagename, array($PageStartFmt,$out)); // send to browser
}

function HandleTableVal($pagename) {
	global $TEtable, $TEnewargs, $TEtbargs, $TEelargs, $TEnewelargs, $TEelval, $TEnbcol, $TEnbrow, $TEnum, $TEtext, $TEcont, $WorkDir;
	$TEnum = @$_GET['s'];
	TEexplode($pagename);
	$okedit = @$_POST["okedit"];
	$okreturn = @$_POST["okreturn"];
	if (@$_POST["cancel"]) {
		@copy("$WorkDir/$pagename,cancel", "$WorkDir/$pagename");
	}
	else if (@$_POST["undo"]) {
		@copy("$WorkDir/$pagename,undo", "$WorkDir/$pagename");
		$okedit=TRUE;
	}
	else{ 
		$nbrow = @$_REQUEST['nbrow'];
		$nbcol = @$_REQUEST['nbcol'];
		$TEnewargs[align] = @$_REQUEST['align'];
		$wd = @$_REQUEST['tbwidth'];
		if ($wd!='') $wd .= '%';
		$wd = str_replace('%%','%',$wd);
		$TEnewargs[width] =  $wd;
		$TEnewargs[cellspacing] = @$_REQUEST['cellsp'];
		$TEnewargs[cellpadding] = @$_REQUEST['cellpad'];
		$TEnewargs[border] = @$_REQUEST['border'];
		for ($icol=1; $icol <= $TEnbcol; $icol++) {// get the data in fields
			$wd = @$_REQUEST["width$icol"];
			if ($wd!='') $wd .= '%';
			$wd = str_replace('%%','%',$wd);
			$TEnewelargs[1][$icol]['width'] = $wd;
			$colalign = @$_REQUEST["c$icol"];
			for ($irow=1; $irow <= $TEnbrow; $irow++) {
				$TEnewelargs[$irow][$icol]['align'] = $colalign; // propagate alignment to the whole column
				$name = 'r'.$irow.'c'.$icol;
				@$TEelval[$irow][$icol] = @$_REQUEST[$name]; // old values are deleted
			}
		}
		for ($icol=1; $icol <= ($TEnbcol+1); $icol++) {// delete or insert col
			$name = "delcol$icol";
			$delcol = @$_POST[$name];
			if ($delcol) { // delete col
				for ($irow=1; $irow <= $TEnbrow; $irow++) {
					for ($i2col=$icol; $i2col <$TEnbcol; $i2col++) {
						unset ($TEelval[$irow][$i2col]);
						$TEelval[$irow][$i2col] = @$TEelval[$irow][$i2col+1];
						unset ($TEelargs[$irow][$i2col]);
						$TEelargs[$irow][$i2col] = @$TEelargs[$irow][$i2col+1];
					}
				}
				$nbcol = -- $TEnbcol;
				$okedit=TRUE;
				break;	
			}
			$name = "inscol$icol";
			$inscol = @$_POST[$name];
			if ($inscol) { // insert col
				for ($i2col=$TEnbcol; $i2col >= $icol; $i2col--) {
					for ($irow=1; $irow <= $TEnbrow; $irow++) {
						$TEelval[$irow][$i2col+1] = @$TEelval[$irow][$i2col];
						unset ($TEelval[$irow][$i2col]);
						$TEelargs[$irow][$i2col+1] = @$TEelargs[$irow][$i2col];
						unset ($TEelargs[$irow][$i2col]);
						$TEnewelargs[$irow][$i2col+1] = @$TEnewelargs[$irow][$i2col];
						unset ($TEnewelargs[$irow][$i2col]);
					}
				}
				$nbcol = ++ $TEnbcol;
				$okedit=TRUE;
				break;
			}				
		}		
		for ($irow=1; $irow <= ($TEnbrow+1); $irow++) { // delete or insert row
			$name = "insrow$irow";
			$insrow = @$_POST[$name];
			if ($insrow) { // insert row
				for ($i2row=$TEnbrow; $i2row >= $irow; $i2row--) {
					for ($icol=1; $icol <= $TEnbcol; $icol++) {
						$TEelval[$i2row+1][$icol] = @$TEelval[$i2row][$icol];
						unset ($TEelval[$i2row][$icol]);
						$TEelargs[$i2row+1][$icol] = @$TEelargs[$i2row][$icol];
						unset ($TEelargs[$i2row][$icol]);
						$TEnewelargs[$i2row+1][$icol]['align'] = $TEnewelargs[1][$icol]['align']; // not much optimised
					}
				}
				$nbrow = ++ $TEnbrow;
				$okedit=TRUE;
				break;
			}
			$name = "delrow$irow";
			$delrow = @$_POST[$name];
			if ($delrow) { // delete row
				for ($i2row=$irow; $i2row <$TEnbrow; $i2row++) {
					for ($icol=1; $icol <= $TEnbcol; $icol++) {
						unset ($TEelval[$i2row][$icol]);
						$TEelval[$i2row][$icol] = @$TEelval[$i2row+1][$icol];
						unset ($TEelargs[$i2row][$icol]);
						$TEelargs[$i2row][$icol] = @$TEelargs[$i2row+1][$icol];
					}
				}
				$nbrow = -- $TEnbrow;
				$okedit=TRUE;
				break;
			}
		}
		$TEnbcol = $nbcol;
		$TEnbrow = $nbrow;
		TEBuildTable ();
		Lock(2);
		$page = RetrieveAuthPage($pagename, 'edit', true);
		if (!$page) Abort("?cannot edit $pagename"); 
		PCache($pagename,$page);
		$new = $page;
		$new['text'] = str_replace ('#zp0Z#', $TEtable, $TEtext);
		$csum = FmtPageName('$[table modified]', $pagename);
#		dbt("csum:$csum",1);
		$new['csum'] = $csum;
		$new["csum:$Now"] = $csum;
		if (!@$_GET['cont']) 
			copy("$WorkDir/$pagename", "$WorkDir/$pagename,cancel");
		copy("$WorkDir/$pagename", "$WorkDir/$pagename,undo");
		PostPage($pagename, $page, $new);
		Lock(0);
	}
	$TEcont='&cont=ok';	
	if ($okedit) HandleTableEdit($pagename);
	else {
		@unlink ("$WorkDir/$pagename,cancel"); 
		@unlink ("$WorkDir/$pagename,undo"); 
		Redirect($pagename,"\$PageUrl#tbls$TEnum");
	}
}

function TEcleanargs ($prefix, $oldargs, $newargs) {
	$out.="(:$prefix";
	if (isset($newargs)) 
		foreach ($newargs as $key=>$val) {
			if ($val=='') {
				if (isset($oldargs[$key])) unset($oldargs[$key]);
			}	
			else $oldargs[$key] = $val;
		}
	if (isset($oldargs)) 
		foreach ($oldargs as $key=>$val) 
			$out .= ' '.$key."='".$val."'";
	$out.=':)';
	return $out;
}

function TEBuildTable () {
	global $TEtable, $TEnewargs, $TEtbargs, $TEelargs, $TEnewelargs, $TEelval, $TEnbcol, $TEnbrow;
	$TEtable = TEcleanargs ('table', $TEtbargs, $TEnewargs);
	for ($irow=1; $irow <= $TEnbrow; $irow++) {
		for ($icol=1; $icol <= $TEnbcol; $icol++) {
			$nr = $icol==1 ? 'nr' : '';
			$TEtable .= "\n".TEcleanargs ("cell$nr", $TEelargs[$irow][$icol], @$TEnewelargs[$irow][$icol]);
#			str_replace ("\n", '[[<<]]', @$TEelval[$irow][$icol]);
			$TEtable .= str_replace ("\n", "�", trim(@$TEelval[$irow][$icol])); // not \\ because strip remove backslashes
		}
	}
	$TEtable .= "\n"; #(:tableend:) is already in file
	$TEtable = stripmagic ($TEtable); // because POST add slashes
    $TEtable = str_replace ("\r", '', $TEtable);  // remove CR
	$TEtable = str_replace ('�', "\x5C\x5C\n", $TEtable); 
}

/* debug function
function dbt($par, $init=0) {
	if ($init!=0) 	$fp = fopen('cookbook/errortab.txt', "w");
	else 			$fp = fopen('cookbook/errortab.txt', "a+b");
	fwrite($fp, "\n".$par);
	fclose($fp);
} */