<?php if (!defined('PmWiki')) exit();
/*
   Author: Martin Fick
   Date: 2007-06-03
*/
define($MARKUP_SAME_PASS_VERSION, '1.0');

function MarkupSamePass($id, $when, $pats_reps, $opts='') {
  global $MARKUP_SAME_PASS;

  $MARKUP_SAME_PASS[$id]['id'] =  $ids;
  $MARKUP_SAME_PASS[$id]['pats_reps'] = $pats_reps;
  $MARKUP_SAME_PASS[$id]['opts'] = $opts;

  $eopts = "e$opts";
  $pats = array_keys($pats_reps);
  $pat = '/('. implode(')|(', $pats). ")/$eopts";

  $sub = "MarkupSamePassReplace(\$pagename, '$id', '\$0')";

  Markup($id, $when, $pat, $sub);
}

function MarkupSamePassReplace($pagename, $id, $text) {
  global $MARKUP_SAME_PASS;

  $opts = $MARKUP_SAME_PASS[$id]['opts'];
  foreach($MARKUP_SAME_PASS[$id]['pats_reps'] as $pat => $rep) {
    $text = preg_replace("/$pat/$opts", $rep, $text);
  }
  return $text;
}

