<?php

SDV($QuicktocPubDirUrl, '$FarmPubDirUrl/quicktoc');

Markup("quicktoc", "fulltext", 
  "/\\(:quicktoc:\\)/", 
  "<div id='quicktoc'><div id='quicktocframe' onclick='processquicktoc()'><div id='quicktocmsg'>(click to open)</div><h1 id='quicktocheading' class='closed'>Quick Page Table of Contents</h1><div id='quicktoccontents'><p><i>Scanning...</i></p></div></div></div>");

$HTMLHeaderFmt['quicktoc'] = 
  "<script language='javascript' type='text/javascript'
     src='\$QuicktocPubDirUrl/quicktoc.js'></script>
   <link rel='stylesheet' href='\$QuicktocPubDirUrl/quicktoc.css' 
     type='text/css' />";
?>