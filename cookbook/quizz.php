<?php if (!defined('PmWiki')) exit();
$RecipeInfo['LittleQuizz']['1']='2007-02-08';

Markup_e('quizz', 'block','/\\(:quizz(.*?):\\)/',"FmtQuizz(\$m[1],'onclick')");
Markup_e('quizzp', 'block','/\\(:quizzp(.*?):\\)/',"FmtQuizz(\$m[1],'onmouseover')"); // for presentation

$quizz_javascript='<script language=\'javascript\' type=\'text/javascript\'>
function view(question,item,color)
	{
	document.getElementById(\'R\'+question+\'F\').style.display=\'none\';
	document.getElementById(\'R\'+question+\'T\').style.display=\'none\';
	hdl=document.getElementById(\'R\'+question+color);
	hdl.style.display=\'block\';
	}

function result(id,nq)
	{
	frm=document.getElementById(\'form\'+id);
	sum=0;
	for (k=1;k<nq;k++)
		{
		ref=\'Q\'+k;
		fol=frm[ref];
		for (j=0;j<fol.length;j++)
			if (fol[j].checked)
				sum=sum+parseInt(fol[j].value);
		}
	document.getElementById(id).style.display=\'block\';
	//document.forms.quizz.validation.value=\'Vous totalisez \'+sum+\' points !\';
	frm.validation.value=\'Vous totalisez \'+sum+\' points !\';
	}
</script>';

$HTMLHeaderFmt['quizz'] = $quizz_javascript;

function fmtquizz($data,$mode) // deux mode! onclick, onmouseover (dans les présentations)
	{
        global $KPV,$KeepToken,$quizz_javascript;
	$quizz_question='<div style="font-size=3"><br/>Question %s - <b>%s</b><br/></div>';
	$quizz_reponse='<div style="font-size=3" id="%s-%s"><input '.$mode.'="view(\'%s\',\'%s\',\'%s\');" type="radio" name="Q%s" value="%s">%s - <b>%s</b></div>';
	$quizz_comment='<div id="R%s%s" style="background: %s; border: 1px solid black; display: none; padding: 2px; color: #000;">%s</div>';
        $data = preg_replace( "/$KeepToken(\\d+?)$KeepToken/e", '$KPV[$1]', $data);
        $data=explode("\n",$data); // fabrique le tableau de lignes
        list($id_quizz,$nom_du_quizz)=explode('=',$data[1]);
        //echo "$id_quizz=$nom_du_quizz";
        $nq=0;$nr=0;$final='';
        $quizz_debut='<h4>%s</h4><form name="quizz'.$id_quizz.'" id="form'.$id_quizz.'">';
        $result=sprintf($quizz_debut,$nom_du_quizz);
        $quizz_final="<p style='font-size=3'><br/><input type='button' name='validation' value='Resultat' ".'onclick="result(\''.$id_quizz.'\',%s)"'."><br/><br/><div id='$id_quizz' style='display: none;'>%s</div></p></form>";
        $data[1]=''; // je sais c'est sale mais array_shift merdoit
	foreach($data as $line) // pour chaque ligne
 			{
 			if (trim($line)=='') 
                            $mode='vide';
                        else if ($line[0]=='=') 
                            $mode='final';
                        else if ($line[1]=='+') 
                            $mode='bonne';
 			else if ($line[1]=='-') 
                            $mode='mauvaise';
 			else if ($line[0]==' ') 
                            $mode='reponse';
                        else $mode='question';
			switch($mode)
				{
				case 'vide':break;
				case 'question':
					{
					$nq++;$nr=0;
					$result.=sprintf($quizz_question,$nq,$line)."\n";
					break;
					}
				case 'reponse':
					{
					$nr++;
					$line=explode(' ',$line);
					$mdata=$line[1]; // par exemple 10+
					$texte=implode(' ',array_slice($line,2));
					if ($mdata[strlen($mdata)-1]=='+')
						$value='T';
					else
						$value='F';
					$ponderation=substr($mdata,0,strlen($mdata)-1);
					$result.=sprintf($quizz_reponse,"$id_quizz$nq",$nr,"$id_quizz$nq",$nr,$value,$nq,$ponderation,chr(ord('A')+$nr-1),$texte)."\n";
					break;
					}
				case 'bonne':
					{
					$value='T';
					$texte=trim(substr($line,2,strlen($line)-2));
					$result.=''.sprintf("$quizz_comment","$id_quizz$nq",$value,'lightgreen',$texte)."\n";
					break;
					}
				case 'mauvaise':
					{
					$value='F';
					$texte=trim(substr($line,2,strlen($line)-2));
					$result.=''.sprintf("$quizz_comment","$id_quizz$nq",$value,'#F78181',$texte)."\n";
					break;
					}
				case 'final':
					{
					$final.="<b>".substr($line,1,strlen($line)-1)."</b><br/>";
					break;
					}
				}
			}
	$result.=sprintf($quizz_final,$nq+1,$final)."\n";
	//return sprintf($quizz_javascript,$nq+1).
	return $result;
 }
/*
question
 10+ réponse (points et affiche quoi ?)
 12- réponse
 2+ réponse
 +texte si bonne réponse
 -texte si mauvaise réponser
*/

?>
