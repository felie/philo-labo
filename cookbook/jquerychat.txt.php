<?php if (!defined('PmWiki')) exit();

/*****************************************************************************************
*                                                                                        *
*    gfsjquerychat provide connection to                                                 *
*    Gmail/Facebook Style jQuery Chat                                                    *
*    http://anantgarg.com/2009/05/13/gmail-facebook-style-jquery-chat/                   *   
*                                                                                        *
*    gplv2 François Elie 2016 (francois@elie.org)                                        *
*                                                                                        *
*    This program is free software: you can redistribute it and/or modify                *
*    it under the terms of the GNU General Public License as published by                *
*    the Free Software Foundation, either version 3 of the License, or                   *
*    (at your option) any later version.                                                 *
*                                                                                        *
*    This program is distributed in the hope that it will be useful,                     *
*    but WITHOUT ANY WARRANTY; without even the implied warranty of                      *
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                       *
*    GNU General Public License for more details.                                        *
*                                                                                        *
*    You should have received a copy of the GNU General Public License                   *
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.               *
*                                                                                        *
*    BE CAREFUL: Gmail/Facebook is for non commercial usage                              *   
*    otherwise see cometchat.com                                                         *
*                                                                                        *
*****************************************************************************************/

$RecipeInfo['gfsjquerychat']['Version'] = '2016-04-10';
$RecipeInfo['gfsjquerychat']['Author'] = 'felie';

$_SESSION['username']=$Author;
$HTMLHeaderFmt['chat'] = '
<script type="text/javascript" src="$FarmPubDirUrl/jquerychat/chat/js/jquery.js"></script>
<script type="text/javascript" src="$FarmPubDirUrl/jquerychat/chat/js/chat.js"></script>
<link type="text/css" rel="stylesheet" media="all" href="$FarmPubDirUrl/jquerychat/chat/css/chat.css" />
<link type="text/css" rel="stylesheet" media="all" href="$FarmPubDirUrl/jquerychat/chat/css/screen.css" />
<!--[if lte IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="$FarmPubDirUrl/jquerychat/chat/css/screen_ie.css" />
<![endif]-->';

function jquerychat_str2file($s,$name)
  {
  //create the $name file 
  $f = fopen($name, 'w+');
  ftruncate($f, 0);
  fflush($f);
  fwrite($f,$s);
  fclose($f);
  }
  
$loggedinrep="$FarmPubDirUrl/jquerychat/loggedin"; // to store loggedin signals
$signal="$loggedinrep/$Author"; 
if ($_GET[action]=='logout')
    unlink($signal); // delete the signal file
else    
    jquerychat_str2file('',$signal); // touch the signal file

Markup_e("chats", "inline", "/\\(:chats:\\)/","chats()"); // a markup to see other, providing links to open chat with another connected user
function chats()
    {
    global $loggedinrep,$Author;
    $t='';
    if ($handle = opendir($loggedinrep)) 
        {
        while (false !== ($entry = readdir($handle)))
            {
            if ($entry != "." && $entry != ".." && $entry !=$Author) 
                {
                if($entry->isFile() && !$entry->isDot() && (time() - $entry->getMTime() > 7200)) // 2 hours without action ?
                    unlink($entry)
                else   
                    $t.="<a href=\"javascript:void(0)\" onclick=\"javascript:chatWith('$entry')\">$entry\n</a><br/>";
                }
            }
        closedir($handle);
        }
    return "<b>chats</b><br/>$t";
    }
include_once("$FarmPubDirUrl/jquerychat/chat/chat.php");