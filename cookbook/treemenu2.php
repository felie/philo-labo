<?php if (!defined('PmWiki')) exit();
/*  Copyright 2004 Karl Loncarek (karl@loncarek.de)
    This file is part of PmWiki; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  See pmwiki.php for full details.

    *History*
    * 2005-03-04 conceptional design
    * 2005-03-09 first really working version
*/


$HTMLHeaderFmt[] = "<script language='javascript' src='$PubDirUrl/treemenu/treemenu.js'></script>\n";

$HTMLStylesFmt['treemenu'] = "#treemenu{overflow:auto;white-space:nowrap;width:100%;}
treemenu,#treemenu *{font:11px Verdana;}
treemenu p{display:inline;}";

function BuildTreeMenu($s) {
  global $PubDirUrl;
  $s=str_replace("*","<br/>",$s);
  return "début $s bibifin";
};

Markup('treemenu2','inline',"/\(:treemenu2(.*?)treemenu2:\)/"," hi hi $1 ho ho");

?>
