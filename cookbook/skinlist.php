<?php

function SkinList($pagename) {
  global $FarmD, $PageSkinList;

  $skinlist = (array)$PageSkinList;
  foreach (array("pub/skins", "$FarmD/pub/skins") as $skindir) {
     $dp = @opendir($skindir);
     if (!$dp) continue;
     while (($skin = readdir($dp)) !== false) {
       if ($skin{0} == '.') continue;
       if (is_dir("$skindir/$skin")) $skinlist[$skin]++;
     }
     closedir($dp);
  }
  ksort($skinlist);
  $out = "* [[{\$FullName}?setskin= | default skin]]\n";
  foreach($skinlist as $skin => $c) 
    $out .= "* [[{\$FullName}?setskin=$skin | $skin]]\n";
  return $out;
}
  
Markup('skinlist', 'fulltext',
  '/\\(:skinlist:\\)/ie', 
  "SkinList(\$pagename)");


