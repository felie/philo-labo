<?php
/*  
TODO
- l'endroit où se trouve la page 
- inutile de refaire la page (cheksum dans le nom ?
- un tag de ses cours en attendant le drag'n drop
*/

// prépare le souce pmwiki pour le traitement par m2b vers beamer

// pour limiter ceux qui veulent faire des présentations en beamer à leurs propres pages.
//echo "$Author $pagename";

$page = RetrieveAuthPage($pagename, "read", true, READPAGE_CURRENT);

if (!$page) 
  Abort("?cannot source $pagename");
  foreach ($HTTPHeaders as $h) {
    $h = preg_replace('!^Content-type:\\s+text/html!i',
             'Content-type: text/plain', $h);
    header($h);
    }

$source=@$page['text'];

# transformation du source en un tableau de chaine
$sourcetablo=explode("\n",$source);

$titre=array_shift($sourcetablo);
if ($sourcetablo[0][0]=='(') // si la deuxième ligne est du genre (:toc:)...
 array_shift($sourcetablo);

array_unshift($sourcetablo,"% $titre","% $LongName","% ");

$source=implode("²",$sourcetablo);
//echo "$source";

// ici il faut remplacer les ressources par leur valeur pour m2b
// test avec un texte

function ressource($num)
	{
	if ($num[1]=='t')
		return getitem4m2b($num[2],true); // avec titre
	else
		return getitem4m2b($num[2],false);
	}

function ressource_longue($num)
        {
        return getitem4m2b($num[1],false,true);
        }

$source=preg_replace_callback('/\@f([0-9]*)/',"ressource_longue",$source); // habituellement le texte dans une iframe
$source=preg_replace_callback('/\@([t]*)([0-9]*)/',"ressource",$source);
$source=preg_replace('/>>tip<<²([^²]*)²([^>]*)>><</','a($1²$2)a²',$source);
$source=preg_replace('/>>warning<<²([^²]*)²([^>]*)>><</','e($1²$2)e²',$source);
$source=preg_replace('/>>important<<²([^²]*)²([^>]*)>><</','b($1²$2)b²',$source);
$source=str_replace('Lorem','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',$source);
$source=str_replace('AROBASE',"@",$source);

$source=str_replace('²',"\n",$source);

//echo "*$source*";
//exit;

$m2b="$FarmD/m2b/m2b -c ";

if ($f = fopen("$FarmD/m2b/$pagename.txt", "w")) 
  { fputs($f, "$source"); pclose($f); 
    $pgmline="$m2b -I /web/philo-labo $pagename"; 
    // exec($pgmline,$a,$b);
   exec($pgmline);

  }
//print_r($a);
//print($b);
//exit;

$filename = "$FarmD/m2b/$pagename.pdf";
$content = file_get_contents($filename);
header("Content-Disposition: inline; filename=$pagename.pdf");
header("Content-type: application/pdf");
header('Cache-Control: private, max-age=0, must-revalidate');
header('Pragma: public');
echo $content;
exit;
?>
