<?php if (!defined('PmWiki')) exit();

/*	=== RedirectIntermap ===
 *	Copyright 2008 Eemeli Aro <eemeli.aro@tkk.fi>
 *
 *	Allows redirects to defined intermap locations & implements
 *	silent 301 redirects
 *
 *	Based on Ryan Varick's CustomRedirects cookbook recipe.
 *
 *	To install, add the following line to your config file :
		include_once("$FarmD/cookbook/redirect-intermap.php");
 *
 *	For more information, please see the online documentation at
 *		http://www.pmwiki.org/wiki/Cookbook/RedirectIntermap
 *
 *	This program is free software; you can redistribute it and/or
 *	modify it under the terms of the GNU General Public License as
 *	published by the Free Software Foundation; either version 2 of
 *	the License, or (at your option) any later version.
 */

$RecipeInfo['RedirectIntermap']['Version'] = '2008-02-13';


// override the built-in (:redirect:) pattern
Markup('redirect', '<include',
  '/\\(:redirect\\s+(\\S.*?):\\)/ei',
  "RedirectMarkupExt(\$pagename, PSS('$1'))");

## override the built-in redirect function
## based on RedirectMarkup in pmwiki.php
function RedirectMarkupExt($pagename, $opt) {
  $k = Keep("(:redirect $opt:)");
  global $MarkupFrame, $EnableRedirectQuiet;
  if (!@$MarkupFrame[0]['redirect']) return $k;
  $opt = ParseArgs($opt);
  $to = @$opt['to']; if (!$to) $to = @$opt[''][0];
  if (!$to) return $k;
  if (preg_match('/^([^:]+:)(.+)$/', $to, $match))
    { RedirectIntermap( $pagename, $match[1], $match[2], @$opt['status'] ); return $k; }
  if (preg_match('/^([^#]+)(#[A-Za-z][-\\w]*)$/', $to, $match))
    { $to = $match[1]; $anchor = $match[2]; }
  $to = MakePageName($pagename, $to);
  if (!PageExists($to)) return $k;
  if ($to == $pagename) return '';
  if (@$opt['from']
      && !MatchPageNames($pagename, FixGlob($opt['from'], '$1*.$2')))
    return '';
  if (preg_match('/^30[1237]$/', @$opt['status']))
     header("HTTP/1.1 {$opt['status']}");

  // for 301 requests, do not append the query string if $EnableRedirectQuiet is set
  if ( IsEnabled($EnableRedirectQuiet, 0) && ($opt['status'] == '301') ) Redirect($to, "{\$PageUrl}");
  else Redirect($to, "{\$PageUrl}?from=$pagename$anchor");

  exit();
}

## redirect the browser to $path on intermap $imap
function RedirectIntermap( $pagename, $imap, $path, $status=FALSE ) {
	global $EnableRedirect, $RedirectDelay, $EnableStopWatch, $IMap, $EnableRedirectIntermap, $EnableRedirectExternal;

	if ( !IsEnabled($EnableRedirectIntermap, 1) || !isset($IMap[$imap]) ) return;
	if ( !IsEnabled($EnableRedirectExternal, 0) && ( !strncmp($path,'//',2) || ( strpos($path,'://') !== FALSE ) ) ) return;

	SDV($RedirectDelay, 0);
	clearstatcache();
	if ( $status && preg_match('/^30[1237]$/',$status) ) header("HTTP/1.1 $status");
	$url = PUE(str_replace('$1',$path,$IMap[$imap]));
	if ( IsEnabled($EnableRedirect,1) && ( !isset($_REQUEST['redirect']) || $_REQUEST['redirect'] ) ) {
		header("Location: $url");
		header("Content-type: text/html");
		echo "<html><head>
<meta http-equiv='Refresh' Content='$RedirectDelay; URL=$url' />
<title>Redirect</title></head><body></body></html>";
		exit;
	}
	echo "<a href='$url'>Redirect to $url</a>";
	if ( @$EnableStopWatch && function_exists('StopWatchHTML') ) StopWatchHTML($pagename, 1);
	exit;
}


