<?php if (!defined('PmWiki')) exit();

/**********************************************************************
   Copyright 2005 Douglas Stebila <douglas@stebila.ca>
   Based in part on TrackingMenu by Mike Ivanov
   Portions Copyright 2005 Mike Ivanov <mike70i@yahoo.com>
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
  
**********************************************************************/

$InsideEMenu = 0;
$EMenuGroup = '';

function BuildEMenu($name) {
  global $PubDirUrl, $HTMLHeaderFmt, $InsideEMenu;
  if ($name == 'emenu') {
    $InsideEMenu = 1;
    return "<:block><div class='emenu'>";
  }
  if ($name == 'emenuend') {
    $InsideEMenu = 0;
    return '<:block></div>';
  }
  return '<:block>';
}

function BuildEItem($star,$tail) {
  global $InsideEMenu, $EMenuGroup, $pagename, $DefaultPage;
  if( $InsideEMenu ) {
	if ($star == '*') {
      if( preg_match("/^\\s*\\[\\[([^|]*?)(\\|.*?)?\\]\\]/", $tail, $match ) ) {
        $pn = MakePageName( $DefaultPage, $match[1] );
        if( preg_match("/^([^.]*)/", $pn, $gn) ) {
		  $EMenuGroup = $gn[0];
		}
	  }
	  return $star. $tail;
	} else {
      if( preg_match("/^([^.]*)/", $pagename, $gn) ) {
		if ($EMenuGroup == $gn[0]) {
		  return $star . $tail;
		} else {
		  return;
		}
	  }
	}
  }
  return $star . $tail;
}

## precede link handlers
Markup('emenu','<links','/\\(:(emenu|emenuend)\\s?:\\)/e', "BuildEMenu('$1')");
Markup('^*E','>emenu','/(\\*+)(.*)/e', "BuildEItem('$1',PSS('$2'))");

