<?php if (!defined('PmWiki')) highlight_file(__FILE__);
if (!defined('PmWiki')) exit();
/** \file filesNotify.php
    \brief Extra PmWiki script for being notified about modified files

    Copyright 2007 Christian Ridderstrom (christian.ridderstrom@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.    

    Documentation: see the recipe page at
	http://pmwiki.org/wiki/Cookbook/FilesNotify

    Enable this script to automatically be sent emails when uploaded
    files are changed. This script relies on two external programs:
    'find' and 'diff'.

    Variables (in array $FilesNotify):
    'verbose'	- Integer controlling verboseness of this script
    'intervall'	- Minimum in seconds between updates of file listings
    'workDir'	- Location of file listings etc.
		  Default is $workdir/files
    'filesDir'	- Locate files to be checked if they have changed.
		  Default is 'uploads'.
    'find'	- Name of 'find'-command used to generate file listing
    'findOpts'	- Options to find command
    'headers'	- Optional headers to send with e-mail
    'mailto'	- Array with email addresses to send notification
    'subjectFmt'- Format string for message subject of notification
    'bodyFmt'	- Format string for message body of notification

*/
$RecipeInfo['FilesNotify']['Version'] = '2007-01-21';

SDVA($FilesNotify,
     array('verbose'	=> 0,
	   'intervall'	=> 3600*12,
	   'workDir'	=> '',	
	   'filesDir'	=> 'users',
	   'find'	=> 'find',
	   'findOpts'	=> "-printf '%TY-%Tm-%Td %TH:%TM  %-8s  %p\n'",
	   'diff'	=> 'diff',
	   'headers'	=> '',
	   'mailto'	=> array(),
	   'subjectFmt'	=> '$WikiTitle - modified files',
	   'bodyFmt'	=> "Modifications enregistrées:
\$FilesNotifyDiff
" ));

if($FilesNotify['workDir'] == '')
  $FilesNotify['workDir'] = $WorkDir.'/files';

# Make sure the work directory exists
mkdirp($FilesNotify['workDir']);

## For any browsing action, check if it is time to check the files
if($action == 'browse' || $action == '')
  if(FilesNotifyIsItTimeToUpdate())
    register_shutdown_function('FilesNotifyUpdate', $pagename, getcwd());


# Check if it is time for another check of the files
function FilesNotifyIsItTimeToUpdate() {
  global $FilesNotify, $Now;

  # The command creates 'files.0' when run.
  # Use this to get the time of whan the external command last ran
  $LastCmdTime = @filemtime($FilesNotify['workDir'].'/files.0');

  return $Now > $LastCmdTime + $FilesNotify['intervall'];
}

# 
function FilesNotifyUpdate($pagename, $dir='') {
  global $FilesNotify;
  $curdir = getcwd();
  if($dir) { flush(); chdir($dir); }

  Lock(2);			// Lock out other processes

  if(FilesNotifyIsItTimeToUpdate()) {
    $files0 = $FilesNotify['workDir'].'/files.0';
    $filesLatest = $FilesNotify['workDir'].'/files.latest';

    $cmd = $FilesNotify['find']
      .' '. $FilesNotify['filesDir']
      .' '. $FilesNotify['findOpts'] .' > '. $filesLatest;

    // Create listing of files
    FilesNotifyMsg(1, "Creating listing.\nExecuting: $cmd");
    system($cmd);

    if(! is_file($files0)) // Create empty files.0 if it does not yet exist
      touch($files0);

    // Create difference between files.latest and files.0
    $cmd = $FilesNotify['diff'] . " $filesLatest $files0"
      .' > '. $FilesNotify['workDir'].'/files.diff';
    FilesNotifyMsg(1, "Creating difference of listing.\nExecuting: $cmd");
    exec($cmd, $output, $status);

    if($status) {
      FilesNotifyRotateListings();
      filesNotifySendMail($pagename);
    }
  }
  Lock(0);			// Release lock

  chdir($curdir);		// Restore cwd just in case...
  return true;
}

# Rotate file listings so that:
#	files.8 -> files.9
#	files.7 -> files.8
#	...
#	files.latest -> files.0
function FilesNotifyRotateListings() {
  global $FilesNotify;

  FilesNotifyMsg(1, "File listing differ - will rotate listing logs");
  for($n = 9; $n > 0; $n -= 1) {
    clearstatcache();
    $filesOld = $FilesNotify['workDir']."/files.".($n-1);
    $filesOlder = $FilesNotify['workDir']."/files.".$n;
    if(is_file($filesOlder)) unlink($filesOlder);
    if(is_file($filesOld))
      rename($filesOld, $filesOlder); // Rename: files.(n-1) -> files.(n)
  }
  rename($FilesNotify['workDir'].'/files.latest',
	 $FilesNotify['workDir'].'/files.0');
}

#
# Send e-mail with difference between files.0 and files.1.
# This is what have changed since the last email was sent.
function FilesNotifySendMail($pagename) {
  global $FilesNotify;

  FilesNotifyMsg(2, "Preparing to send notification");
  $mailto = $FilesNotify['mailto'];

  $subject = FmtPageName($FilesNotify['subjectFmt'], $pagename);
  $body = FmtPageName($FilesNotify['bodyFmt'], $pagename);
  foreach ($mailto as $m) {
    $mbody = str_replace('$FilesNotifyDiff', FilesNotifyReadDiff(), $body);
    mail($m, $subject, $mbody, $FilesNotify['headers']);
    FilesNotifyMsg(2, "Notification sent to ".$m);
  }
}


# Read file with diffs and return the diff as a string
function FilesNotifyReadDiff() {
  global $FilesNotify;
  FilesNotifyMsg(3, "Reading files.diff before sending notification");
  $filesDiff = $FilesNotify['workDir'].'/files.diff';
  $s = file_get_contents($filesDiff);
  return $s;
}

#
# Help function for showing message if verboseness is greater than some value
# 
# Example:
# FilesNotifyMsg(1, "This is only shown if verboseness is >= 1");
#
function FilesNotifyMsg($verbose, $msg) {
  global $FilesNotify;
  if($FilesNotify['verbose'] >= $verbose)
    echo "<pre>".$msg."</pre>";
}

?>
