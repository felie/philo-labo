<?php if (!defined('PmWiki')) exit ();
# Simple inline Formatting

Markup("emph", "inline", "/\\\emph{(.*?)}/", "<em>$1</em>"); 
Markup("bf", "inline", "/\\\textbf{(.*?)}/", "<b>$1</b>");
Markup("it", "inline", "/\\\textit{(.*?)}/", "<i>$1</i>");
Markup("tt", "inline", "/\\\texttt{(.*?)}/", "<tt>$1</tt>");
Markup("code", "inline", "/{\\\ttfamily (.*?)}/", "<code>$1</code>");
Markup("url", "inline", "/\\\url{(.*?)}/", "<tt>$1</tt>");

# Simple block formatting
Markup("quotation", "block", "/\\\begin{quotation}(.*?)\\\end{quotation}/", "<blockquote>$1</blockquote>");
Markup("quote", "block", "/\\\begin{quote}(.*?)\\\end{quote}/", "<quote>$1</quote>");
Markup("h1", "block", "/\\\part{(.*?)}/", "<h1>$1</h1>");
Markup("h2", "block", "/\\\chapter{(.*?)}/", "<h2>$1</h2>");
Markup("h3", "block", "/\\\section{(.*?)}/", "<h3>$1</h3>");
Markup("h4", "block", "/\\\subsection{(.*?)}/", "<h4>$1</h4>");
Markup("h5", "block", "/\\\paragraph{(.*?)}/", "<h5>$1</h5>");
Markup("h6", "block", "/\\\subparagraph{(.*?)}/", "<h6>$1</h6>");

# Lists

Markup("itemize", "inline", "/\\\begin{itemize}/", "<ul>");
Markup("itemend", "inline", "/\\\end{itemize}/", "</ul>");
Markup("enumerate", "inline", "/\\\begin{enumerate}/", "<ol type=\"1\">");
Markup("enumend", "inline", "/\\\end{enumerate}/", "</ol>");
Markup("description", "inline", "/\\\begin{description}/", "<dl>");
Markup("descend", "inline", "/\\\end{description}/", "</dl>");
Markup("item", "inline", "/\\\item[[:space:]](.*?)\./", "<li>$1</li>");
Markup("descitem", "inline", "/\\\item\[(.*?)]{(.*?)}/", "<dt>$1</dt><dd>$2</dd>");

# Simple image display

Markup("figure", "inline", "/\\\begin{figure}\[H]/", "<div class=\"picture\">");
Markup("figend", "inline", "/\\\end{figure}/", "</div>");
Markup("image", "inline", "/\\\includegraphics\[scale=1]\{(.*?)}/", "<img src=\"/Site/uploads/$1\" / >");

?>
