<?php if (!defined('PmWiki')) exit();
/*  Copyright 2004 Karl Loncarek (karl@loncarek.de)
    This file is part of PmWiki; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  See pmwiki.php for full details.

    *History*
    * 2005-03-04 conceptional design
    * 2005-03-09 first really working version
*/


$HTMLHeaderFmt[] = "<script language='javascript' src='$PubDirUrl/treemenu/treemenu.js'></script>\n";

$HTMLStylesFmt['treemenu'] = "#treemenu{overflow:auto;white-space:nowrap;width:100%;}
#treemenu,#treemenu *{font:14px Verdana;}
#treemenu p{display:inline;}";

function BuildTreeMenu($name) {
  global $PubDirUrl;
  if ($name == 'treemenu') {
    return "<:block><div style='background-color:brown;' id='treemenu'>";
  };
  if ($name == 'treemenuend') {
    $html = "<:block>\n\n</div>\n";
    $html .="<script type='text/JavaScript'>\n<!--\ninit_treemenu('$PubDirUrl');\n//-->\n</script>\n";
    return $html;
  };
  return "<:block>";
};

Markup('treemenu','<block','/\\(:(treemenu|treemenuend)\\s?:\\)/e', "BuildTreeMenu('$1')");

?>