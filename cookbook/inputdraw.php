<?php if (!defined('PmWiki')) exit();

$RecipeInfo['InputDraw']['Version'] = '2009-05-07';

Markup('inputdraw', 'inline', "/\\(:inputdraw\\s+([^ ]*)\\s*([^ ]*)\\s*([^ ]*)\\s*([^ ]*)\\s*:\\)/e", "inputdraw_make('$1', '$2', '$3', '$4');");
Markup('inputdrawview', 'inline', "/\\(:inputdrawview\\s+([^ ]*)\\s*([^ ]*)\\s*([^ ]*)\\s*([^ ]*)\\s*:\\)/e", "inputdraw_view_make('$1', '$2', '$3', '$4');");

SDV($HandleActions['inputdraw'], 'HandleInputDraw');

function HandleInputDraw($pagename, $auth)
{
  global $FarmD, $UploadDir, $UploadPrefixFmt;

  if(!CondAuth($pagename, 'upload'))Redirect($pagename);

  $file_name = $_REQUEST[inputdraw_file_name];

  $uploadsPath = FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$file_name;

  $save_file = $FarmD.'/'.$uploadsPath;

  //die($save_file);

  $content = html_entity_decode($req[inputdraw_svg]);
  //die($content);

  $file = fopen($save_file, "w");
  fwrite($file, $content);

  //print_r($req);

  HandleBrowse($pagename);
}

function inputdraw_view_make($file_name, $width, $height, $background)
{
    GLOBAL $PubDirUrl, $pagename, $UploadDir, $UploadUrlFmt, $UploadPrefixFmt, $FarmD, $HTMLHeaderFmt;

  $block_html = "";

  if(!$width)$width = '100%';

  if(!$height)$height = '300';

  if(!$background)$background = "paper.jpg";

  if(!preg_match('/^http:/', $background)){
    
    $testfile = $FarmD."/".FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$background;
    
    if(file_exists($testfile)) { 
      $background = FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$background;
      if (!preg_match('/^http:/',$background)){
	$background = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/../'.$background;
      }
    }

    else {

      $testfile = $FarmD."/cookbook/InputDraw/".$background;
      if(file_exists($testfile)) {
	$background = $PubDirUrl."/InputDraw/".$background;
      }

    }

  }
  
  $pos = strrpos($file_name, '.');
  $file_id = ($pos == false) ? '' : strtolower(substr($file_name, 0, $pos));

  $uploadsUrl = FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$file_name;
  if (!preg_match('/^http:/',$uploadsUrl)){
    $uploadsUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/../'.$uploadsUrl;
  }

  $file_path = $uploadsUrl;

  $InputDrawSwfScript = "
<script src='".$PubDirUrl."/InputDraw/swfobject.js' type='text/javascript'></script>";

  $InputDrawScript = "
<script src='".$PubDirUrl."/InputDraw/inputdraw.js' type='text/javascript'></script>";

  $InputDrawBlock = "
<div id='${file_id}_inputdraw_place'></div>";

  $InputDrawInitScript = "
<script type='text/javascript'>
new InputDraw('".$PubDirUrl."/InputDraw/inputdraw.non-commercial.v1.5.swf', '${file_id}_inputdraw_place', {width:'${width}', height:'${height}', animation:60, src:'$file_path', background_image:'{$background}'});
</script>";

  $HTMLHeaderFmt['inputdraw'] = $InputDrawSwfScript.$InputDrawScript;

  $block_html = $block_html.$InputDrawBlock.$InputDrawInitScript;
  
  return Keep($block_html);  
}

function inputdraw_make($file_name, $width, $height, $background)
{
  GLOBAL $PubDirUrl, $pagename, $UploadDir, $UploadUrlFmt, $UploadPrefixFmt, $FarmD;

  if(!$width)$width = '100%';

  if(!$height)$height = '300';

  if(!$background)$background = "paper.jpg";

  if(!preg_match('/^http:/', $background)){

    $testfile = $FarmD."/".FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$background;
    
    if(file_exists($testfile)) { 
      $background = FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$background;
      if (!preg_match('/^http:/',$background)){
	$background = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/../'.$background;
      }
      
    }
    else {
      $testfile = $FarmD."/cookbook/InputDraw/".$background;
      if(file_exists($testfile)) {
	$background = $PubDirUrl."/InputDraw/".$background;
      }
    }
  }

  $block_html = "";

  $pos = strrpos($file_name, '.');
  $file_id = ($pos == false) ? '' : strtolower(substr($file_name, 0, $pos));

  $uploadsUrl = FmtPageName("$UploadDir$UploadPrefixFmt",$pagename)."/".$file_name;
  if (!preg_match('/^http:/',$uploadsUrl)){
    $uploadsUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/../'.$uploadsUrl;
  }

  $file_path = $uploadsUrl;

  $InputDrawSwfScript = "
<script src='".$PubDirUrl."/InputDraw/swfobject.js' type='text/javascript'></script>";

  $InputDrawScript = "
<script src='".$PubDirUrl."/InputDraw/inputdraw.js' type='text/javascript'></script>";

  $InputDrawBlock = "
<form name='${file_id}_inputdraw_form' action='".PageVar($pagename, '$PageUrl')."' method='post'>
<div id='${file_id}_inputdraw_place'></div>
<input type='hidden' id='${file_id}_inputdraw_svg' name='inputdraw_svg'/>
<input type='hidden' name='inputdraw_file_name' value='$file_name'/>
<input type='hidden' name='action' value='inputdraw' />
<input type='submit' value='Save'/>
</form>";

  $InputDrawInitScript = "
<script type='text/javascript'>
var inputdraw = new InputDraw('".$PubDirUrl."/InputDraw/inputdraw.non-commercial.v1.5.swf', '${file_id}_inputdraw_place', {id:'${file_id}_inputdraw_svg', width:'${width}', height:'${height}', animation:60, src:'$file_path', background_image:'${background}'});
</script>";

  $block_html = $block_html.$InputDrawSwfScript.$InputDrawScript.$InputDrawBlock.$InputDrawInitScript;
  
  return Keep($block_html);
}

?>