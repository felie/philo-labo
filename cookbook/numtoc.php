<?php if (!defined('PmWiki')) exit();
## Module NumToc Author:Pierre Rouzeau - V 0.7 - 2 dec. 2006 - Licence GPL

Markup('^!toc','>include','/^(!{1,6})(?:\s*)(.*)$/e', "MkNumTitle(strlen('$1'),PSS('$2'))");
Markup('^Q:toc','>include','/^Q:(.*)$/e', "MkQref(PSS('$1'))"); 
// another recipe gives more with directive toc
Markup('toc','directives','/\\(:(no)?toc:\\)/e', "RunToc('$1')");
Markup('indent','directives','/\\(:(no)?indent:\\)/e', "RunIndent('$1')");
Markup('num','directives','/\\(:(no)?num(?:\s+(.*?))?:\\)/e', "RunNum('$1',PSS('$2'))");

$MarkupFrameBase['posteval']['toc'] =
  "\$out=str_replace('{toc}','<div class=\'tocstyle\'>'.\$GLOBALS['Ntoc'].'</div>',\$out);";

$GroupFooterFmt .= '(:nonum:)(:noindent:)'; #(:notoc:)';

$levelmax=6;

define('NumToc',1);
	  
function MkNumTitle ($level, $text) {
  global $levelmax,$tcount, $CurLvl, $DefNum, $HdrIndStyle, $Ntoc, $NumHdr, $IndentHdr, $SetToc, $indentoffset;
  static $TcNb, $OrNb, $sp, $numoffset;
  if ($level>$levelmax) $level=$levalmax;
  if ($CurLvl>0 and $IndentHdr) $hdr = "</p></div>";
  else $hdr='';
  if (!isset($CurLvl)) $CurLvl=0;
  if ($CurLvl==0) {
    SDV ($DefNum, '1.1.1.1.1.1. ');
	SDV ($HdrIndStyle, 'text-indent:-1em;');
    $CurLvl++;
	$sp[1] = '';	
	for ($i=1; $i<7; $i++) {
	  $sp[$i+1] = $DefNum[$i+$i-1];
	  $OrNb[$i] = chr(ord($DefNum[$i+$i-2])-1); // dec don't work on alpha char
	  $TcNb[$i] = $OrNb[$i];
	}  
	$sp[7] = substr($DefNum,9);  // ?
  }
  $CurLvl = $level++;
  if ($NumHdr) {
    if (!isset($numoffset)) $numoffset = $CurLvl;
	$level-=$numoffset;
	$TcNb[$level] = IncNum($TcNb[$level]);
	for ($i=1; $i<=$level; $i++) $num.= $sp[$i].$TcNb[$i];	
	for ($i=$level+1; $i<7; $i++) $TcNb[$i]= $OrNb[$i];
    $num.= $sp[6];
	$pref= $num;
  }
  else 
    $pref = ""; // "- ";
  if ($IndentHdr) {
    if (!isset($indentoffset)) $indentoffset = $CurLvl-1;
	$idlvl = $CurLvl-$indentoffset;
	$indent = " style='$HdrIndStyle'";
	$classblk = "<div class='indent$idlvl'>";
  }	
  if ($SetToc) {
	$title = preg_replace ("/\\[\\[(?:#.*)?(?:.*?\\|)?(.*?)(-&gt;.*?)?\\]\\]/", "\\1", $text); //remove links
	$title = preg_replace ("/(%.*?%)([^%]*)/", "\\2", $title); //remove styling %...%
	$idtoc = $CurLvl-$indentoffset;
    $Ntoc.= "\n<span class='toc$idtoc'>
      $pref<a href='#ntoc".++$tcount."'>$title</a></span><br />";
	$reftoc = "[[#ntoc$tcount]]";  
  }  
  return ("$hdr\n<p>$classblk<h$CurLvl$indent>\n$reftoc&nbsp;$num&nbsp;$text</h$CurLvl><p>"); 
}

function IncNum ($num) {
  if (is_numeric($num)) 
    return ++$num; 
  else 
    return chr(ord($num)+1);
}

function MkQref ($text) {
  global $tcount, $CurLvl, $Ntoc, $SetToc, $indentoffset;
  if ($SetToc) {
	$idtoc = $CurLvl-$indentoffset;
    $Ntoc.= "\n<span class='toc$idtoc'>
      <a href='#ntoc".++$tcount."'>$text</a></span><br />";
	$anc = "[[#ntoc$tcount]]"; 
  }	
  return "Q:$anc$text";
} 

#function RunToc ($notoc) {
#  if ($GLOBALS['SetToc'] =($notoc!='no')) return '{toc}';
#}  


function RunToc ($notoc) {
  if ($GLOBALS['SetToc'] =($notoc!='no')) 
	return  '<script type="text/javascript"><!--
    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == \'block\')
          e.style.display = \'none\';
       else
          e.style.display = \'block\';
    }
//-->
</script><div id="xtoc"><a href="#" onclick="toggle_visibility(\'toc\');">Table des matières</a><div id="toc" style="display:none">{toc}</div></div>';
}

function RunIndent ($noindent) {
  global $IndentHdr;
  $wasindent = $IndentHdr;
  if (!$IndentHdr=($noindent!='no') and $wasindent) return '</p></div>';
}

function RunNum ($nonum, $def) {
  global $NumHdr, $DefNum, $GroupFooterFmt;
  $NumHdr =($nonum!='no');
  if ($def != '') $DefNum = $def;
}

$HTMLStylesFmt['nmhindent']= "
  .indent1 {margin-left:1.25em;}
  .indent2 {margin-left:2.5em;}
  .indent3 {margin-left:3.75em;}
  .indent4 {margin-left:5em;}  
  .indent5 {margin-left:6.25em;}
  .indent6 {margin-left:7.5em;}
";

$HTMLStylesFmt['nmhtoc']= "
  #xtoc {position:fixed;top:10%;right:0;opacity:1;background-color:ivory;}
  .toc1 {font-size:80%;margin-left:1em;}
  .toc2 {font-size:75%;margin-left:2em;}
  .toc3 {font-size:70%;margin-left:3em;}
  .toc4 {font-size:65%;margin-left:4em;}  
  .toc5 {font-size:60%;margin-left:5em;}
  .toc6 {font-size:55%;margin-left:6em;}
";
?>
