<?php if (!defined('PmWiki')) exit();
/*
This file is HTML5Audio.php; you
can redistribute it and/or modify it under the
terms of the GNU General Public License as
published by the Free Software Foundation
http://www.fsf.org either version 2 of the 
License, or (at your option) any later version.

Copyright 2010 GNUZoo (guru@gnuzoo.org)

	http://www.pmwiki.org/wiki/Profiles/GNUZoo

Please donate to the author:

	http://gnuzoo.org/GNUZooPayPal/
*/

$RecipeInfo['HTML5Audio']['Version'] = '1.0';

SDV($HTML5AudioDir, '/uploads/');

switch ($action) {
	case "edit"   :
	case "print"  :
	if (! @$_POST['preview']) break;
	case "browse" :
	case "slideshow2":
	default:
	Markup('HTML5Audio', '>aliasressourcetf', '/\\(:(html5audio)(\\s.*?)?:\\)/ie', "HTML5Audio('$1',PSS('$2'))");// avant c'était en fulltext
	break;
}

$html5audio_type=array(
    'ogg'=>'ogg',
    'oga'=>'ogg',
    'mp3'=>'mpeg',
    'wav'=>'x-wav',
    'au'=>'basic');
    
function HTML5Audio($name, $args='') {
        global $html5audio_type;
	$args=explode(' ',trim($args)); // first: filename
	$filename=$args[0];
	$extension=explode('.',$filename);
	$extension=$extension[sizeof($extension)-1];
	$extension=$html5audio_type[$extension];
	$args = ParseArgs($args[1]);
	$poster   = $args['poster'];
	$width    = $args['width'];
	$height   = $args['height'];
	if ($width  == '') $width  = 480;
	if ($height == '') $height = 360;
return Keep("<audio controls='controls'source src='$filename'  type='audio/$extension'/><br/>Your browser does not allow HTML5 audio.</audio>");
}
