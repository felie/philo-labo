
<?php if (!defined('PmWiki')) exit ();
/*  
Copyright by Nicolas Poulain 2007.  

You may use this code as you want, and change it as much as you want.  
The LaTeXMathML package offers many benefits over MathML, it also uses 
standard TeX commands, 

The LaTeXMathML package is available from http://www.maths.nottingham.ac.uk/personal/drw/lm.html .

In order to use this recipe:

1.  Download and install 
    1.1 the LaTeXMathML.php file into your PmWiki's cookbook/ directory
    1.2 the LaTeXMathML.js file into a new directory called LaTeXMathML/ into your PmWiki's pub/ directory

2.  Add the following line to a local customization file:
    include_once('cookbook/LaTeXMathML.php');

That's it!  The script adds $$ ... $$ and $...$ markups that
display Mathml from LaTeX syntax, the first form centers the 
equation, while the second generates the equation "inline".

TEX
- The math graphic for the GUI toolbar is available at
  http://www.pmichaud.com/pmwiki/pub/guiedit/math.gif .

*/

SDV($RecipeInfo['Cookbook.LaTeXMathML']['Version'], '20071004');

//  $JSMathUrl contains the url to the jsmath directory on the server.
//  Defaults to pub/jsMath/ .
SDV($LaTeXMathML, "$PubDirUrl/LaTeXMathML");

$HTMLHeaderFmt['jsMath'] = '
  <script type="text/javascript" src="'.$LaTeXMathML.'/LaTeXMathML.js"> </script>
';

// This line gives you LaTeX $$ $$ display equations in the center
// as it does with LaTeX.

Markup_e('$$', '<$','/\\$\\$(.*?)\\$\\$/',"Keep('<div align=\"center\">\$\displaystyle{'.\$m[1].'}\$</div>')");

//Markup('$$', '<{$',
//  '/\\$\\$(.*?)\\$\$/e',
//  "Keep('<div align=\"center\">\$\displaystyle{'.PSS('$1').'}\$</div>')");

//  This line gives you $ $ equations in line.  You can then use 
//  \displaystyle as normal to get pretty print equations inline.

Markup_e('$', 'directives','/\\$(.*?)\\$/',"Keep('<span>\$'.\$m[1].'\$</span>')");

//Markup('$', 'directives',
//  '/\\$(.*?)\\$/e',
//  "Keep('<span>\$'.PSS('$1').'\$</span>')");



