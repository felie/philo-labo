<?php if (!defined('PmWiki')) exit ();

/*  copyright 2006 Hans Bracker.

    This file is distributed under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This module enables image mapping in wiki pages.

    Usage example:
    (:imgmap mapname:)
    (:area href=Group.PageName shape=... coords=... :)
    (:area href=http://example.com/ shape=... coords=... :)
    ....
    (:imgmapend:)

    %usemap=#mapname%Attach:myimage.jpg

    Use distinct name as mapname for each image map (works like anchor)
    Necessary parameters for area:
        href=[Group.PageName|http://siteaddress]
        shape=[rect|circle|polygon|default]
        coords=number,number,number,number...
           for rectangle:x1,y1,x2,y2 (topleft corner, bottom right corner)
           for circle:x,y,r (left to centre, top to centre,radius)
           for polygon:x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,... (corners of polygon)
           for default: no coords. The whole image is the default.
    Optional parameters for area:
        title='title name' (shows as tooltip when moused over)
        alt='alternative text' (for browsers with no image support)
        tabindex=number
        accesskey=
        onclick='javascript commands' (needs $EnableOnclickJavascripting turned on)

*/
# Version date
$RecipeInfo['ImageMap']['Version'] = '2015-07-11';


# the onclick= parametyer is turned off by default, to prevent malicious javascripting,
# but can be enabled by setting it to 1.
SDV($EnableOnclickJavascripting, 0);

# add usemap as wiki style for inclusion in img tag:
$WikiStyleAttr['usemap'] = 'img';

# add imagemap markup
Markup('imgmap', '>block', '/\\(:imgmap (.*?)?\\s*?:\\)/', "<map name='$1'>");
Markup('imgmapend', '>block', '/\\(:imgmapend:\\)/', '</map>');

#Markup('area', 'directives', "/\\(:area (.*?:)(.*?)(\\s.*?)?\\s*?:\\)/e", "Keep(MapArea('$1','$2','$3'),'L')");
# Markup('area', 'directives', "/\\(:area (.*?)?\\s*?:\\)/e", "MapArea('$1')");
Markup_e('area', 'directives', "/\\(:area (.*?)?\\s*?:\\)/", "MapArea(\$m[1])");

function MapArea($args) {
    global $ScriptUrl, $EnablePathInfo, $EnableOnclickJavascripting;
    $arg = PSS(ParseArgs($args));
    if (isset($arg['href'])) {
        $tgt = $arg['href'];
        if($EnablePathInfo) $LinkUrl = $ScriptUrl."/".$tgt;
        else $LinkUrl = $ScriptUrl."?n=".$tgt;
        preg_match("/(http:)(.*?)/",$tgt,$m);
        if($m[1]) $LinkUrl = $tgt;
        preg_match("/(#)(.*?)/",$tgt,$m);
        if($m[1]) $LinkUrl = $tgt;
        }
    $out = "<area ";
    if (isset($arg['shape']))
        $out .= " shape='".$arg['shape']."'";
    if (isset($arg['coords']))
        $out .= " coords='".$arg['coords']."'";
        $out .= " href='".$LinkUrl."'";
    if (isset($arg['alt']))
        $out .= " alt='".$arg['alt']."'";
    if (isset($arg['title']))
        $out .= " title='".$arg['title']."'";
    if (isset($arg['tabindex']))
        $out .= " tabindex='".$arg['tabindex']."'";
    if (isset($arg['accesskey']))
        $out .= " accesskey='".$arg['accesskey']."'";
    if($EnableOnclickJavascripting) {
      if (isset($arg['onclick']))
         $out .= " onclick='".$arg['onclick']."'"; }
    $out .= " />";
    return Keep($out);
}

