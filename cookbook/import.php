<?php if (!defined('PmWiki')) exit();
/*  Copyright 2007 Patrick R. Michaud (pmichaud@pobox.com)
    This file is import.php; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  

    This script simplifies the process of importing text markup
    into PmWiki.  

    Plain text files (with or without wiki markup) are placed 
    in an "import directory" defined by the $ImportDir 
    variable (default "import/").  Each file to be imported
    must be named with a valid pagename and an optional '.txt'
    extension, but this can be changed via the $ImportFilePattern variable.

    Whenever this script detects that the import/ directory
    has changed, or at intervals given by $ImportFreq (in seconds),
    it scans the import/ directory to see if there are any new
    files to be imported.  When one is found, the script updates
    the corresponding page in wiki.d with the text of the file as 
    if it had been posted through the normal edit process.  The
    "post" is performed assuming authorship as given by
    the $ImportAuthor variable (default is 'ImportText').

    If there are a large number of files to be processed, then
    the import recipe will work for a maximum of $ImportTime
    seconds (default 15).  The remaining items will then be
    processed on subsequent calls to the script.

    The script keeps track of which files have previously been 
    imported so that it doesn't re-import it again later.
    As extra protection, if the $EnableImportRename variable
    is set (default is yes), then the script attempts to
    rename each input file with a timestamp suffix.

    The script also provides ?action=import to force immediate
    importing of any files in the import/ directory.  By default,
    ?action=import can only be invoked by someone with admin
    privileges.

    To install this script, copy it into the cookbook/
    directory and add the following lines to a customization
    file:

       # load script
       include_once('cookbook/import.php');
       # check for new files at least once per hour
       $ImportFreq = 3600;

*/

$RecipeInfo['ImportText']['Version'] = '2007-05-27';
#if ($VersionNum < 2001051)
#  Abort("import.php requires 2.2.0-beta51 or later.");

SDV($ImportDir, 'import');
SDV($ImportAuthor, 'ImportText');
SDV($ImportFreq, 0);
SDV($ImportFilePattern, "^($GroupPattern\.$NamePattern)(\.txt)?$");
SDV($LastImportFile, "$WorkDir/.lastimport");
SDVA($ImportSaveGlobals, array('Author' => 1, 'AuthorLink' => 1));
SDV($ImportExcludeEditFunctions, array('CheckBlocklist'));
SDV($ImportTime, 15);
SDV($ImportCount, 0);
SDV($ImportSkip, 0);

SDV($HandleActions['import'], 'HandleImport');
SDV($HandleAuth['import'], 'admin');

if ($action == 'browse') 
  register_shutdown_function('ImportCheck', getcwd());

function HandleImport($pagename, $auth = 'admin') {
  global $HandleImportFmt, $PageStartFmt, $PageEndFmt;
  $page = RetrieveAuthPage($pagename, $auth, true, READPAGE_CURRENT);
  if (!$page) Abort("Insufficient permissions for import");
  list($usec0, $sec0) = explode(' ', microtime());
  header('Content-type: text/plain');
  ImportUpdate(create_function('$x', 'print "$x\n"; flush();'));
  list($usec1, $sec1) = explode(' ', microtime());
  $time = sprintf("%.2f", $sec1 + $usec1 - $sec0 - $usec0);
  print FmtPageName("\nImported \$ImportCount pages in $time seconds\n", $pagename);
  flush();
}

function ImportCheck($cwd = NULL) {
  global $LastImportFile, $ImportFreq, $ImportDir;
  if ($cwd) { flush(); chdir($cwd); }

  $t = @filemtime($LastImportFile);
  $check = $ImportFreq && (time() > $t + $ImportFreq);
  if (!$check) 
    foreach((array)$ImportDir as $dir) 
      if (@filemtime($dir) > $t) { $check = true; break; }
  if ($check) ImportUpdate();
}

function ImportUpdate($logfn = 'StopWatch') {
  global $LastImportFile, $ImportDir, $ImportFilePattern, $EnableImportRename,
    $Now, $ImportSaveGlobals, $ImportAuthor, $Author, $AuthorLink,
    $EditFunctions, $ImportExcludeEditFunctions, $ImportTime, $ImportCount, $ImportSkip;
  $logfn("ImportUpdate: begin");
  $abort = ignore_user_abort(true);

  Lock(2);
  foreach($ImportSaveGlobals as $k => $v) 
    if ($v) $save[$k] = $GLOBALS[$k];
  $Author = $ImportAuthor;
  $AuthorLink = ($Author) ? "[[~$Author]]" : '?';
  $lastimport = array();  $nextimport = array();
  if (is_readable($LastImportFile)) 
    $lastimport = unserialize(file_get_contents($LastImportFile));
  @touch($LastImportFile);

  $editfn = array_diff($EditFunctions, (array)$ImportExcludeEditFunctions);
  $timeout = time() + $ImportTime;
  foreach((array)$ImportDir as $dir) {
    $dfp = @opendir($dir); if (!$dfp) continue;
    $logfn("ImportUpdate: scanning '$dir'");
    while ( ($fname = readdir($dfp)) !== false ) {
      if (!preg_match("/$ImportFilePattern/", $fname, $match)) continue;
      $pn = $match[1];
      $fpath = "$dir/$fname";
      clearstatcache(); $fmtime = filemtime($fpath);
      if ($fmtime <= @$lastimport[$fpath]) {
        $nextimport[$fpath] = $lastimport[$fpath];
        $logfn("ImportUpdate: already processed '$fpath'");
        continue;
      }
      if (time() > $timeout) 
        { $logfn("ImportUpdate: timeout $fpath"); $ImportSkip++; continue; }
      $logfn("ImportUpdate: processing '$fpath' (pagename=$pn)");
      if (IsEnabled($EnableImportRename, 1)) {
        $fpath2 = "$fpath,import-$Now";
        if (@rename($fpath, $fpath2)) {
          $logfn("ImportUpdate: renamed $fpath -> $fpath2");
          $fpath = $fpath2;
        }
      }
      $text = file_get_contents($fpath);
      $page = ReadPage($pn);
      $new = $page;
      $new['text'] = $text;
      UpdatePage($pn, $page, $new, $editfn);
      $ImportCount++;
      clearstatcache();
      $nextimport[$fpath] = filemtime($fpath);
    }
  }
  @unlink($LastImportFile);
  $fp = @fopen($LastImportFile, 'w');
  if ($fp) { 
    fputs($fp, serialize($nextimport)); 
    fclose($fp); 
    fixperms($LastImportFile);
    if ($ImportSkip) @touch($LastImportFile, 1167609600);    # 2007-01-01 UTC
  }
  foreach($ImportSaveGlobals as $k=>$v) 
    if ($v) $GLOBALS[$k] = $save[$k];
  $logfn("ImportUpdate: end ($ImportCount updated, $ImportSkip skipped)");
  ignore_user_abort($abort);
}
