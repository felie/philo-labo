<?php if (!defined('PmWiki')) exit ();

/*  copyright 2006 Hans Bracker. 
    copyright 2009 Ed Wildgoose - MailASail
    Build on code from swf.php copyright 2004 Patrick R. Michaud
    and from quicktime.php copyright 2006 Sebastian Siedentopf.
    This file is distributed under the terms of the GNU General Public 
    License as published by the Free Software Foundation; either 
    version 2 of the License, or (at your option) any later version.  

    This module enables embedding of Flash animations (.swf) files into
    wiki pages. 
    
    Width and height can be set with parameters inside the markup.
    Syntax: (:flash Attach:filename.swf [width=number] [height=number] :)
    Alt syntax: (:flash http://samplesite.com/path/filename.swf [width=number] [height=number] :)
    External resource (alt syntax embeddig with http://....) can be disabled
    by setting $EnableExternalResource = 0;
*/
// Version date
$RecipeInfo['Flash']['Version'] = '2008-02-10';


SDV($EnableExternalResource, 1);

Markup('flash', '<img', "/\\(:flash (.*?:)(.*?)(\\s.*?)?\\s*?:\\)/e", "ShowFlash('$1','$2','$3')");


function ShowFlash($imap, $path, $args) {
    global $UploadFileFmt, $pagename, $PageUrl, $PubDirUrl, $IMapLinkFmt, $EnableExternalResource;
    if ($EnableExternalResource==1 && $imap=='http:') {
        $exturl=1; 
        $FileUrl = 'http:'.$path;
    }
    else {
        $filepath = FmtPageName("$UploadFileFmt/".$path, $pagename);
	# Force the format of the generated link to be the bare URL and get the path to our upload
        $FileUrl = LinkUpload($pagename, $imap, $path, $path, $path, '$LinkUrl'); 
    }
    if (file_exists($filepath) || $exturl==1) {
        $args = ParseArgs($args);
     
        $dims = "";
        if (isset($args['width']))
            $dims .= " width=\"".$args['width']."\"";
        if (isset($args['height']))
            $dims .= " height=\"".$args['height']."\"";  

	$out = "\n<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" $dims id=\"csSWF\">\n";
        $out .= "  <param name=\"movie\" value=\"$FileUrl\" />\n";
        $out .= "  <param name=\"quality\" value=\"autohigh\" />\n";
        $out .= "  <param name=\"bgcolor\" value=\"#1a1a1a\" />\n";
        $out .= "  <param name=\"allowfullscreen\" value=\"true\" />\n";
        $out .= "  <param name=\"scale\" value=\"showall\" />\n";
        $out .= "  <param name=\"allowscriptaccess\" value=\"always\" />\n";
        $out .= "  <param name=\"flashvars\" value=\"autostart=false&thumb=$FileUrl.thumb.png&thumbscale=55&color=0x1A1A1A,0x1A1A1A\" />\n";
        $out .= "  <!--[if !IE]>-->\n";
        $out .= "    <object type=\"application/x-shockwave-flash\" data=\"$FileUrl\" $dims id=\"csSWF\">\n";
        $out .= "   <param name=\"quality\" value=\"autohigh\" />\n";
        $out .= "   <param name=\"bgcolor\" value=\"#1a1a1a\" />\n";
        $out .= "   <param name=\"allowfullscreen\" value=\"true\" />\n";
        $out .= "   <param name=\"scale\" value=\"showall\" />\n";
        $out .= "   <param name=\"allowscriptaccess\" value=\"always\" />\n";
        $out .= "   <param name=\"flashvars\" value=\"autostart=false&thumb=$FileUrl.thumb.png&thumbscale=55&color=0x1A1A1A,0x1A1A1A\" />\n";
        $out .= "  <!--<![endif]-->\n";
        $out .= "   <div id=\"noUpdate\">\n";
        $out .= "     <p>The video content presented here requires a more recent version of the Adobe Flash Player. If you are you using a browser with JavaScript disabled please enable it now. Otherwise, please update your version of the free Flash Player by <a href=\"http://www.adobe.com/go/getflashplayer\">downloading here</a>.</p>\n";
        $out .= "   </div>\n";
        $out .= "  <!--[if !IE]>-->\n";
        $out .= "  </object>\n";
        $out .= "  <!--<![endif]-->\n";
        $out .= "</object>\n";
  
        return Keep($out);
    }
    if($imap=='http:')  return "";  
    else return Keep($FileUrl);
}

