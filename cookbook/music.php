<?php
/*
   Author: Martin Fick
   Requires: content.php, markupsamepass.php and whichever filters
             you would like to use.
   Date: 2008-03-07

*/
define(MUSIC_VERSION, '1.3');

SDV($MUSIC_LIST, '-ppm,-ps');

ContentRegisterType('tab', 'text/plain', 'tab', $MUSIC_LIST, true);
ContentRegisterType('asciitab', 'text/plain', 'txt', $MUSIC_LIST, true);
ContentRegisterType('abctab', 'text/plain', 'abc', $MUSIC_LIST, true);
ContentRegisterType('abcm', 'text/plain', 'abc', $MUSIC_LIST, true);
ContentRegisterType('abc', 'text/plain', '', $MUSIC_LIST);

ContentRegisterType('midi', 'audio/x-midi', 'mid', $MUSIC_LIST);
ContentRegisterType('wav', 'audio/wav', 'wav', $MUSIC_LIST);
ContentRegisterType('mp2', 'audio/x-mp2', 'mp2', $MUSIC_LIST);
ContentRegisterType('mp3', 'audio/x-mp3', 'mp3', $MUSIC_LIST);
ContentRegisterType('flac', 'audio/x-flac', 'flac', $MUSIC_LIST);
ContentRegisterType('vorbis', 'application/ogg', 'ogg', $MUSIC_LIST);

ContentRegisterType('ps', 'application/postscript', 'ps', $MUSIC_LIST);
ContentRegisterType('pdf', 'application/pdf', 'pdf', $MUSIC_LIST);
ContentRegisterType('ppm', 'image/ppm', '', $MUSIC_LIST);
ContentRegisterType('gif', 'image/gif', '', $MUSIC_LIST);


ContentRegFSConverter("asciitab", "abctab", 'ascii2abctab < ${i} > ${o}');
ContentRegFSConverter("tab", "abctab", 'tab2abc ${i} > ${o}');

ContentRegFSConverter("abctab", "ps", 'abctab2ps -O ${o} ${i}');
ContentRegFSConverter("abcm", "ps", 'abcm2ps -O${o} ${i}');


ContentRegConverter("ps", "ppm", 'ContentConverter_ps2ppm');
ContentRegFSConverter("ppm", "gif", 'pnmcrop < ${i} | ppmtogif > ${o}');
ContentRegFSConverter("ps", "pdf", 'gs -q -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=${o} ${i}');


ContentRegFSConverter("abc",    "midi", 'abc2midi ${i} -o ${o}');
ContentRegFSConverter("abcm",   "midi", 'abc2midi ${i} -o ${o}');
ContentRegFSConverter("abctab", "midi", 'abc2midi ${i} -o ${o}');

ContentRegFSConverter("midi", "wav", 'timidity -Ow ${i} -o ${o}');
ContentRegFSConverter("wav", "mp2", 'toolame ${i} ${o}');
ContentRegFSConverter("wav", "mp3", 'lame ${i} ${o}');
ContentRegFSConverter("wav", "flac", 'flac ${i} -o ${o}');
ContentRegFSConverter("wav", "vorbis", 'oggenc ${i} -o ${o}');



if ( !function_exists('ContentConverter_ps2ppm')) {

function ContentConverter_ps2ppm($cp, $cnv, $intype, $outtype, $args, $data) {
  $dir = ContentDir($cp);
  $in = "$dir/input.ps";
  file_put_contents($in, $data);

  $cmd = "cd $dir ; gs -q -sDEVICE=ppmraw -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r96x96 -dNOPAUSE -dSAFER -sOutputFile=output%d.ppm $in";

  exec($cmd, $err);

  $err = implode("\n", $err); //  echo $err;

  $p = 1;
  $page = "$dir/output$p.ppm";
  while (file_exists($page)) {
    $pages[$p] = $page;
    $out[$p] = $p;
    $p++; $page = "$dir/output$p.ppm";
  }
  if($args != '')  $out = file_get_contents($pages[$args]);
  elseif($p == 2)  $out = file_get_contents($pages[1]);

  foreach($pages as $p=>$page) {
    $cp['tsegs'][count($cp['tsegs'])-1]['args'] = $p;
    ContentCachePutFile($cp, $page);
  }

  ContentRmTree($dir);
  return $out;
}

}
