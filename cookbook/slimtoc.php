<?php if (!defined('PmWiki')) exit();
/*
    The SlimTableOfContents adds support for automatically generating
    simple or numbered Table of Contents for a pmwiki page.

    Version 2009-02-26 (tested with PmWiki 2.2.0-stable)

    Special fit for work with sectionedit

    Copyright 2009 M.Scheierling (Tontyna) <eM@wahahn.de>
    Hammirs gemacht so gut wies ist - m�ge es funzen
    ansonsten kann jeder damit tun, was er will.
*/

/**********************
Usage:
-----
To create a Table Of Contents just add the following line on any wiki page:
    (:toc:)

To create a numbered Table Of Contents use the following markup:
    (:#toc:)

Requirements
------------
* Requires PmWiki 2.2.0 ??
* Requires Cookbook/SignalWhenMarkup
* Requires Cookbook/MarkupToUnstyled

Installation
------------
To use this recipe:
1. copy the slimtoc.css into pub/slimtoc/ folder
2. copy the slimtoc.php into the cookbook/ directory
3. copy the signalwhenmarkup.php into the cookbook/ directory
4. copy the markuptounstyled.php into the cookbook/ directory
5. add the following line to a local customization:
    include_once('cookbook/slimtoc.php');


Customization
-------------
These Customizations have to be set before including the script!

    $DefaultTocTitle = 'Contents';
Sets the caption of the TOC

....$TocLevelMax = 4;
defines the depth of headings to be included in TOC

The stylesheet / and JavaScript for toggling is included only once and on demand
per function SlimTOCIncludeHTMLHeaders()
Oh no, it's ALWAYS included. Cause if there is no (:toc:) on the main wiki page
but on e.g. a Sidebar it's too late to append anything to
$HTMLHeaderFmt
Ok. You can append, but without effect (HandleBrowse already generated $PageStart etc.)
Guess I'm gonna search for another Wiki, PmWiki is too .... messy

How it works
------------
When a (:toc:) Markup is detected the
  function SlimTOCBuildTOC($pagename,$number,$text)
is executed
$number tells whether it should build a numbered TOC
$text is the remaining text of the $page, in the state 'after nl1'

The function
1. splits this $text - into sections with !{1,6} headings
2. applies an anchor to each heading
3. inserts appropriate temporary (:tocnum:) Markup for numbered TOCs
4. Collects the TEXT of the heading for the TOC
   (with the help of function MarkupToUnstyled() from Cookbook/MarkupToUnstyled)
   appends it to a temporary list, remembers the level, ev. with (:tocnum:) Markup for numbered TOCs!
5. converts the collection into the toc-div
   [tocheader + javasrcipt to toggle toc , ul or ol with the items]
   and replaces the (:toc:) Markup with this TableOfContent

Output
------
HTML output of the TOC is:
<div class='toc'>
  <div style='cursor: pointer;' class='tocheader' onclick="javascript:toggleToc('tocid1');">
     <span style='cursor: pointer;' id="tocid1tog" class='toc_close'>
        <span>+</span>
     </span>
     <a name='toc1' id='toc1'></a>Contents
  </div>
  <div class='toclist' id='tocid1'>
    <ul>
      <li><a href='#tocanchor1'>Heading 1</a></li>
      <li><a href='#tocanchor2'>Heading 2</a></li>
      <li><a href='#tocanchor3'>Heading 3</a>
         <ul><li><a href='#tocanchor4'>Heading 3.1</a></li>
             <li><a href='#tocanchor5'>Heading 3.2</a></li>
         </ul>
      </li>
    </ul>
  </div>
</div>
The corresponding styles produce a nice Triangle by border-trick (got that from dokuwiki)
see toc.css

The javascript function toggleToc() will be added to the HTML via $HTMLHeaderFmt['toggle']
only when (:toc:) Markup iss detected.


HTML output of the TOC-anchor is:
<a name='tocanchor1' id='tocanchor1'></a>
It is inserted INSIDE the appropriate heading. After the opening <h#> tag

ToDo:
----
* caption
* level
* startclosed
* option "back-to-toc" in anchors

* watch the heading's text:
  there WILL BE markup I don't want in the TOC-list
  now improved by use of MarkupToUnstyled recipe

* show/hide TOC , evaluate/eat (:toc:) markup depending on action?
  if (($action == 'browse') || ($action == 'view')) || ($action=="print" || $action=="publish") {

**********************/

$RecipeInfo['SlimTableOfContents']['Version']='2009-02-26';

## needs to know about being in (:markup:) / control variable  $SignalMarkupMarkup
# so include cookbook SignalWhenMarkup!
include_once("$FarmD/cookbook/signalwhenmarkup.php");
## need cookbook MarkupToUnstyled for the TOC
require_once("$FarmD/cookbook/markuptounstyled.php");

/*** default values of global variables */
# caption of the TOC
SDV($DefaultTocTitle,'Contents');
# defines the depth of headings to be included in TOC
SDV($TocLevelMax,4);

/*** include JavaScript and css
   ALWAYS.
   Otherwise no chance for Sidebars
*/
SlimTOCIncludeHTMLHeaders();

/*** Internal variables, don't change */
# control structure
$TocLevelStack = array(0,0,0,0,0,0,0);
# when (:#toc:) markup is evaluated this will become true
$TocWithNumbers = false;
# counters for generating id's:
$TocIDCount = 0;      // TOC-id's
$TocAnchorCount = 0;  // anchor id's


/*** the markup */

$SlimTOCDone = 0;
## Markup (:[#]toc:)
# Q: why after 'nl1'?
# A: don't know enough about Markup Chain but this one works.
# Q: what's the Start (*) for?
# A: the Star is for (:*toc:)'s I inserted by testing PageTableOfContents and maybe For Future Use
Markup('toc','>nl1',
    '/\(:([#\*])?toc:\)(.*)$/se',
    "SlimTOCBuildTOC(\$pagename,'$1',PSS('$2'))");

## Markup (:[#]tocnum <numstring>:) for NumberedTOC to produce correct Numbering / cut off leading '0.'
Markup('tocnum','directives','/\\(:tocnum (.*?):\\)/e',"SlimTOCNumstring('$1')");


/**
 * function SlimTOCBuildTOC()
 * will be called when a (:toc:) Markup was detected
 * builds the TOC-div and prepares the temporary (:tocnum:) Markup for anchors
 */
function SlimTOCBuildTOC($pagename,$number,$text) {
    global $DefaultTocTitle, $TocLevelMax, $TocWithNumbers,
           $TocIDCount, $TocAnchorCount;
    if ($TocLevelMax < 1) $TocLevelMax = 1;
    if ($TocLevelMax > 6) $TocLevelMax = 6;

    # Good news: I don't have to care how often I'm called,
    # no problem with (:toc:)s in (:markup:)
    # Bad news: I MUST care - therea are problems with (:toc:)s in includes
    # just increment counter for TOC-ID
    $TocIDCount++;
    global $SlimTOCDone, $SignalMarkupMarkup;
    if (!$SignalMarkupMarkup){
      $SlimTOCDone++;
      if ($SlimTOCDone > 1) return '[hier toc gefressen]';
    }

    # reset level stack to 0
    SlimTOCLevelReset(1);
    # should we use Numbering?
    $TocWithNumbers = ($number=='#');;
    # include stylesheet and javascript once when needed.
// No. include always, otherwise no chance for tocs in Sidebars
// Q: who needs a TOC in the Sidebar?
// A: One never knows.
//    SlimTOCIncludeHTMLHeaders();

    $retval = '';
    $TmpList = array();
    /* check whether text contains headers and split them
       copied that regex from sectionedit without understanding it fully
     */
    $p = preg_split('/((?m:^)|(?<=:\)))((?=!{1,'.$TocLevelMax.'}[^!]))/', $text);
    /*check for special PHP version and fix strange problem with preg_split - von sectionedit
      anscheinend wird ein ! weggeveschpert
     */
    if (phpversion()=='4.1.2') {
      $p[(count($p)-1)] = '!'.$p[(count($p)-1)];
    }
    // analyze the splitters, extract heading text, build anchor
    for ($i = 0; $i < count($p); $i ++) {
      if (substr($p[$i],0,1) == '!'){
        # Hint: at the current stage of Markup Chain
        #       \n and (:nl:) can't be inserted in $text - they'll be eaten!

        # Q: how to extract complete headertext?
        # A: find first nl without Backslash to the left of it
        # Q: But if the heading has no nl at the end you won't catch it.
        # A: Right, but thats only the case if the heading is the last line on the page
        #    and now I added a '|$' so we catch it
        # Q: why doesn't that work within (:markup:)?
        # A: I don't know,, I dont want to know, I'll change it to the algorithm I use in sectionedittamed
        #    without the (:groupfooter:) stuff - we don't have (:groupfooter:) here anymore
        # Q: what's that space for you now append?
        # A: without that the replacement won't work in (:markup:), no, don't ask why,
        #    I haven't any idea.
        if ($i == count($p)-1){
          if (preg_match('/[^\n]$/',$p[$i])) $p[$i] .= " \n";
        }

        preg_match('/^(!{1,6})((?:(?:(?:.*)\\\\(?>(?:\\\\*))\n)*)(?:.*)(?:[^\\\\]))(\n)/', $p[$i], $match);
        # $1 holds the !!!, $2 is the remainder until/before the \n
        $headertext = $match[2];
        $level =strlen($match[1]);

        # adjust the level stack to current level
        SlimTOCLevelUp($level);

# shit, when there are multiple TOCS, merged from
        # for numbered TOC create the temporary (:tocnum .. :) Markup
        $tocnum = $TocWithNumbers ? '(:tocnum '.SlimTOCLevelString().':)' : '';
        # increment Anchor count for Anchor ID
        $TocAnchorCount++;
        $tocanchor = '#tocanchor'.$TocAnchorCount;

        # currently the anchor is inserted INSIDE the <h#>, i.e.: directly behind the !!!
        # maybe I'll add a 'Back-To-TOC' to it in the future
        # Anchor-Text:
        $anchor = "[[".$tocanchor."]]";
        $p[$i] = preg_replace('/^(!{1,6})(\s?)(.*)$/s','$1 '.$anchor. $tocnum.' $3',$p[$i]);

        /******** for future use:
        # to add it OUTSIDE / in front of it use something like that:
        $anchor = <div>[[#toc$i]]hier anker #$i [[#tocID|nach oben]]</div>\n";
        $p[$i] = preg_replace('/^(!{1,6})(\s?)(.*)$/s','$1 '.$tocnum.' $3',$p[$i]);
        $p[$i] = $anchor . "\n".$p[$i];
        **********/

        # process $headertext
        # was ist mit $headertext der L�nge 0?
        # wie krieg ich den header link-frei?
        # welches markup nervt auch noch?
        # remove links and other ugly markup
        $headertext = MarkupToUnstyled($pagename, PSS($headertext));
# fatal results when headertext contains ']]' !
# replace with Unicodes
$headertext = str_replace(']]','&#x005D;&#x005D;', $headertext);
#StopWatch("------------ das is der TOC-text: $headertext");
        # append to collection
        $TmpList[] = array("level" => $level,"text" => $tocnum." [[".$tocanchor."|$headertext]]");

      } // if substr(!)

      /* append processed splitter to return value */
      $retval .= $p[$i];
    }  // for $i

    # empty List = nothing to show
    if (count($TmpList)==0) return $retval;

    # now build the TOC
    $toclist='';
    $lastlevel=0;

    # use ul (*) or ol (#) items?
    $listchar = $TocWithNumbers ? '#' : '*';

    for ($i=0;$i<count($TmpList);$i++){
      $lastlevel++;
      $stars=SlimTOCStars($TmpList[$i]["level"], $listchar);
      # create sublists if needed
      $currlevel = strlen($stars);
      if ($currlevel > $lastlevel){
        for ($l=$lastlevel;$l<$currlevel;$l++){
          $toclist.=  str_pad('', $l , $listchar)."\n";
        }
      }
      $lastlevel = $currlevel;
      $toclist.= $stars. $TmpList[$i]["text"]."\n";
    } // for $i, $TmpList

    # wrap the list and add the tocheader, special styles/classes applied!
    $tocid = 'tocid'.$TocIDCount;
    $toctitle = '[[#toc'.$TocIDCount.']]'.$DefaultTocTitle."\n";
    $togglespan = "<span style='cursor: pointer;' id=\"{$tocid}tog\" class='toc_close'><span>+</span></span>";
    $tocdiv  = "<div class='toc'>\n<div style='cursor: pointer;' class='tocheader' onclick=\"javascript:toggleToc('$tocid');\">$togglespan$toctitle</div>\n" . "<div class='toclist' id='$tocid'>\n". $toclist ."</div></div>\n";
    # Thought I should use Keep() somehow, but it works...
    return $tocdiv."\n".$retval;
} // SlimTOCBuildTOC



/**
 * include stylesheet and javascript once, when needed
 * helper function for SlimTOCBuildTOC - no, have to call it once in maincontext
 */
function SlimTOCIncludeHTMLHeaders() {
    global $FarmD;
    global $TocIDCount, $HTMLHeaderFmt;
    static $inludedone = 1;
    if ($inludedone==1) {
      $inludedone++;

      $localcssfilename = 'pub/slimtoc/slimtocs.css';
      $farmcssfilename = "$FarmD/pub/slimtoc/slimtoc.css";
      if (file_exists($localcssfilename)) {
        $HTMLHeaderFmt[slimtoc] = '<link rel="stylesheet" href="$PubDirUrl/slimtoc/slimtoc.css" type="text/css" />';
      } elseif (file_exists($farmcssfilename)) {
        $HTMLHeaderFmt[slimtoc] = '<link rel="stylesheet" href="$FarmPubDirUrl/slimtoc/slimtoc.css" type="text/css" />';
      } else {
# when called by SlimTOCBuildTOC
# adding something to $HTMLStylesFmt
# $HTMLStylesFmt['slimtoc'] = "...."
# has no effect at all. That's pmwiki for you. No, I don't like how it retrieves pages...
# So I added my style to $HTMLHeaderFmt instead until I found that this only works
# when main wiki page has a (:toc:), i.e. during the first call from HandleBrowse to MarkupToHTML

# Now we are called from main context, so:
$HTMLStylesFmt[slimtoc] = "
<style type='text/css'><!--
div.toc { margin:1em 0 0 1em;font-size:80%;
    float:right; width:38%; clear:both;
    border: 1px dotted #cccccc;
    padding: 0.25em;
    background-color: #f7f7f7; }
div.tocheader{border:1px solid #8cacbb;
    background-color:#dee7ec;
    text-align:left;
    font-weight:bold;padding:3px;margin-bottom:2px;}
span.toc_open,
span.toc_close{
  border:0.4em solid #dee7ec;
  float:right;
  display:block;
  margin-left:0;
  margin-right:3px;
  margin-bottom:0;
}
span.toc_open{
   margin-top:0.4em;
   border-top-color:#000;
}
span.toc_close{
  margin-top:0;
  border-bottom-color:#000;
}
span.toc_open span,
span.toc_close span{ display:none;}

.toclist ol { list-style: none; }
.toclist ol li{text-indent:-2em}
--></style>
";
      }
# add the JavaScript
      $HTMLHeaderFmt['toggle'] = "<script type=\"text/javascript\">
function toggleToc(obj) {
    var elstyle = document.getElementById(obj).style;
    var text    = document.getElementById(obj + \"tog\");
    if (elstyle.display == 'none') {
        elstyle.display = 'block';
        text.innerHTML = '<span>&minus;</span>';
        text.className='toc_close';
    } else {
        elstyle.display = 'none';
        text.innerHTML = '<span>+</span>';
        text.className='toc_open';
    }
}
</script>";
    }
}  // SlimTOCIncludeHTMLHeaders

/**
 * increment level stack counter
 * helper function for SlimTOCBuildTOC
 */
function SlimTOCLevelUp($onlevel){
  global $TocLevelStack;
  if ($onlevel > 6) return;
  if ($onlevel < 1) return;
  $TocLevelStack[$onlevel]++;
  SlimTOCLevelReset($onlevel+1);
}


/**
 * reset level stack recursively to 0
 * helper function for SlimTOCBuildTOC
 */
function SlimTOCLevelReset($onlevel){
  global $TocLevelStack;
  if ($onlevel < 1) return;
  if ($onlevel > 6) return;
  $TocLevelStack[$onlevel] = 0;
  SlimTOCLevelReset($onlevel+1);
}


/**
 * build LevelString #.#.#
 * for the temporary (:tocnum .. :) Markup
 * use current level stack
 * helper function for SlimTOCBuildTOC
 */
function SlimTOCLevelString() {
  global $TocLevelStack;
  $retval='';
  $wantit = 0;
  $dot = '';
  for ($i = 6; $i > 0; $i--) {
    if (!$wantit && $TocLevelStack[$i] != 0) $wantit = 1;
    if ($wantit) {
      $retval = sprintf('%d',$TocLevelStack[$i]).$dot.$retval;
      $dot = '.';
    }
  }
  return $retval;
}

/**
 * create a string-of-stars of the appropriate length = list-depth
 * helper function for SlimTOCBuildTOC
 */
function SlimTOCStars($level, $listchar){
  global $TocLevelStack;
  $startlevel = 1;
  for ($i = 1; $i < count($TocLevelStack); $i ++) {
    if ($TocLevelStack[$i] != 0) {
      $startlevel = $i;
      break;
    }
  }
  $startlevel--;
  $retval = '';
  for ($i = $startlevel; $i < $level; $i ++) {
    $retval.= $listchar;
  }
  return $retval;
}

/**
 * convert the temporary Markup (:tocnum <numstring>:) to the correct Numbers
 * maybe we have to cut some of the outer left Zeroes (0.0) off
 * oh shit, global $TocLevelStack is really global,
      (includes with TOC will manipulate it to their own behalf)
 */
function SlimTOCNumstring($numstring){
  global $TocLevelStack;
  $startlevel = 1;
  for ($i = 1; $i < count($TocLevelStack); $i ++) {
    if ($TocLevelStack[$i] != 0) {
      $startlevel = $i;
      break;
    }
  }
  $startlevel--;
  $retval = '';
  $dummi = explode('.', $numstring);
  for ($i = $startlevel; $i < count($dummi); $i ++) {
    $retval.= (($i==$startlevel) ? '' : '.').$dummi[$i];
  }
  return $retval;
}