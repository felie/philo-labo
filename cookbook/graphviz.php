<?php

/*

Copyright (c) 2004, Paul Williams + François Elie
                    and other contributors as mentioned at
                    http://www.pmwiki.org/wiki/Cookbook/GraphViz

All rights reserved.

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright 
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright 
      notice, this list of conditions and the following disclaimer in the 
      documentation and/or other materials provided with the distribution.
    * The names of its contributors may be used to endorse or promote 
      products derived from this software without specific prior written 
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

$DotFiles = "/web/philo-labo/graphs";

# Dont change anything below here.

#========================================

Markup_e('graphviz','inline',"/\\(:graphviz(.*?):\\)/",
	"graphviz(\$m[1])");

function graphviz($dot_description) {
	global $KPV, $KeepToken;
	global $DotFiles;
	$dot = preg_replace( "/$KeepToken(\\d+?)$KeepToken/e", '$KPV[$1]', $dot_description);
	$dot = preg_replace( "/&gt;/", ">", $dot);
	$dot_filename = md5( $dot);
	if( ! file_exists( $DotFiles ."/". $dot_filename)) {
		$dot_file = fopen( $DotFiles ."/". $dot_filename.".dot","wb");
		fputs( $dot_file, $dot);
	}
	exec("cd $DotFiles;dot -Tpng $dot_filename.dot -o $dot_filename.png");
	return "<img src='/graphs/$dot_filename.png'/>";
}

?>