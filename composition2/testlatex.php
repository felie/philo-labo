<?php
include('phplatex.php');
$code='\textbf{Voici une démonstration que $\sqrt{2}$ est un nombre irrationnel}.

Elle repose sur le fait que tout nombre rationnel peut s\'écrire sous la forme $a/b$ où $a$ et $b$ sont premiers entre eux.
La preuve procède par l\'absurde.  

Supposons que $\sqrt{2}$ est rationnel, nous pourrions écrire: \begin{equation}\label{badassumption}\sqrt{2}=m/n\end{equation} où $m$ et $n$ sont premiers entre eux. 

Mettons au carré les deux côtés de l\'équation (\ref{badassumption}) et multiplions des deux côtés par $n^2$ pour obtenir \begin{equation}\label{eqn2}2n^2=m^2.\end{equation}
Évidemment $m^2$ est pair, and par conséquent $m$ aussi.  Donc $m=2k$ pour un certain entier $k$ et alors l\'équation (\ref{eqn2}) devient 

$$2n^2=4k^2$$. 

Divisons par $2$ pour trouver
$$
n^2 = 2k^2
$$
Le même raisonnement qui suivait de l\'équation (\ref{eqn2}) montre que $n$ doit être pair. 

En un mot: si l\'équation (\ref{badassumption}) conduit à ce que $m$ et $n$ sont tous les deux pairs, c\'est contradictoire avec la supposition que $m$ et $n$ sont premiers entre eux.

Nous en concluons que la supposition (\ref{badassumption}) ne peut être faite, et que $\sqrt{2}$ est irrationnel.';
echo "coucou";
echo texify($code);
echo "coucou";
?>