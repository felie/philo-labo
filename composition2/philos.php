<?php
// production d'un document notion-philosophemes-textes en arbre
// accès la base de donnée
define('ai_standalone',TRUE);
require_once('/web/philo-labo/philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('/web/philo-labo/philosophemes/ai.php');

function philosophemes() // produit un arbre dans les documents partagés des philosophème
    {
    $n=sql2array('select id,philosopheme from philosophemes order by philosopheme');
    foreach ($n as $cle=>$value)
        {
        //echo "$value[0] $value[1]<br/>";
        $id=$value[0];
        $p="PHI $value[1]";
        echo "$p<ul>";
        $t=sql2array("Select ressources.ressource from ressourcesphilosophemes,ressources where ressources.id=ressourcesphilosophemes.id_ressource and ressourcesphilosophemes.id_philosopheme=$value[0]");
        foreach ($t as $idt=>$txt)
            echo "<li>$txt[0]</li>";
        echo "</ul>";
        }

    }
    
philosophemes();
?>