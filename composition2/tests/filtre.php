<html>
<head>
<meta charset="UTF-8">
</head>
<body>
<?php
// cette fonction élimine les partie du texte qui ne sont pas activée par les filtres
$filtres=array(
    'avant'=>'A',
    'après'=>'Z',
    'questions'=>'E',
    'réponses'=>'R');
    
function filtrage($texte,$filtre)
    {
    global $filtres;
    foreach ($filtres as $k => $v)
        {
        if(strpos($filtre,$v) === false) // si le filtre n'est pas actif
            $texte=preg_replace("/<$k>.*<\/$k>/",'',$texte); // on vire la partie concernée
        else
          $texte=str_replace(array("<$k>","</$k>"),array('',''),$texte); // sinon nettoyage
        }
    echo "<br/> $filtre -------------> $texte";
    }
  
$texte='<avant>ceci est avant</avant><b> le texte </b><après>ceci est après</après>';

echo "<pre>";
echo "<br/>le texte de base: $texte";
echo filtrage($texte,'');
echo filtrage($texte,'A');
echo filtrage($texte,'Z');
echo filtrage($texte,'AZ');
echo "</pre>";
?>
</body>
