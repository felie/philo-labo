<?php

if (!isset($Author))
  require_once('../philosophemes/secure.php'); // pour récupérer $Author pour les droits en édition

$numres=$_GET[id]; // on coupe
$big=$_GET[big];
$suffixe='';
if ($big)
	$suffixe='BIG';
$numres=explode(' ',trim($numres))[0];
if (!is_numeric($numres));

require_once('ressources.php');

//$utilisations=simple_query("select count(*) from arbors where id_ressource=$numres");

$num_proprio=simple_query("select id_membre from ressources where id=$numres");
$num_membre=simple_query("select id from membres where membre='$Author'");
$nom=simple_query("select ressource from ressources where id=$numres");
$lien_delete='';
if ($num_membre==$num_proprio or $Author=='admin')
    $lien_delete="<div style='float:right'><a href=\"Javascript:Confirmation('Confirmez-vous la suppression de la ressource ?','Suppression annulée !','delete_ressource.php?id=$numres');\"><button class='favorite styled'
        type='button'>
    Détruire la ressource
</button></a></div>";

// mettre un bouton d'édition avec filtrage sur le groupe des éditeur ?

$header=$_GET['header'];

$type=simple_query("select nature from ressources where id=$numres");
if ($type=="texte")
    $goodies=" <a href='/Site/AideEditionTextes?skin=minimal'>Aide pour les notes marginales et la coloration de texte</a>";

$nature=nature_ressource($numres);
// le droit en édition pour tout le monde
//if (in_array($Author,array('felie','gginvert','lc','maryse','pleveau','theo','isabelle','valerie','phamel','admin')))

// par précaution on verra plus tard pour les saveurs...

$lien_modif="Si vous voyez une coquille, vous pouvez la <a style='text-align:middle' href=\"https://philo-labo.fr/composition/editer.php?id=$numres&skin=minimal\"><input style='font:bold 140% Arial;color:green;' type='button' value='corriger'></a> <a style='text-align:middle' href=\"https://philo-labo.fr/composition/editer.php?id=$numres&brut&skin=minimal\"><input style='font:bold 140% Arial;color:green;' type='button' value='correction brute'></a><b>$goodies</b><hr/>";
    
$correction_button="<a href=\"editer.php?id=$numres&brut&skin=minimal\"><button style='height:64;' class='favorite styled'
        type='button'>Corriger la ressource <b>$numres</b></button></a>";
$lien_delete='';
if ($num_membre==$num_proprio or $Author=='admin') // l'admin a tous les droits, sinon on ne peut détruire que ses propres ressources (d'ailleurs.. ?)
    $delete_button="<a href=\"Javascript:Confirmation('Confirmez-vous la suppression de la ressource ?','Suppression annulée !','delete_ressource.php?id=$numres');\"><button style='height:64;' class='favorite styled'
        type='button'>
    Détruire la ressource
</button></a>";

$en_tete="<table width=100%><tr><td width=20%>$lien_modif $correction_button</td><td><center><h2>$nom</h2></center></td><td align=right width=20%>$delete_button</td></tr></table><b>$goodies</b><hr/>";

echo '<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <link rel="stylesheet" href="/css/textes'.$suffixe.'.css" type="text/css" />
 </head>
 <body>
<script src="/js/jquery.min.js"></script>
<link rel="stylesheet" href="/css/gre.css">
<script src="/js/jquery.gre.fe.js"></script>
   <script language="Javascript" type="">
      function Confirmation(prompt,cancel,cible)
        { 
        resultat = confirm(prompt);
        if(resultat==1)
          window.location=cible;
        else
          alert(cancel);
        }
        </script>
  <div style="align:right">'.$en_tete.item_html($numres).'
  </div>
 </body>
</html>';
?>

