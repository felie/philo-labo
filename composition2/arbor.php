<?php
require_once('ressources.php'); // pour la fonction preference

/* les fonction json de php sont une merde, par exemple on ne peut pas y mettre de : */

function protect_comma($s)  {
  return str_replace(array(',',':'),array('&comma;','&colon;'),$s); } 

function unprotect_comma($s) { 
  return str_replace(array('&comma;','&colon;'),array(',',':'),$s); }

function delete_arbor($author,$doc)
  {
  $id_membre=simple_query("select id from membres where membre='$author'");
  $doc=str_replace("'","\'",$doc);
  $doc=str_replace('"',"\"",$doc);
  simple_query("delete from arbors where id_membre=$id_membre and document='$doc'");
  echo "document supprimé de la base de données";
  }

function protect_angainst_comma_and_colon($x)
  {
  $in=0;
  for ($i=0;$i<strlen($x);$i++)
    {
    switch ($x[$i]) 
      {
      case '"':
        $in=1-$in;
        $r.='"';
        break;
      case ':':
        if ($in==1)
            $r.='&colon;';
        else
            $r.=':';
        break;
      case ',':
        if ($in==1)
            $r.='&comma;';
        else 
            $r.=',';
        break;
      default:
        $r.=$x[$i];
      }
    }
  return $r;
  }
  
function json_decode_nice($json, $assoc = true)
  {
  global $envdoc;
  //echo "<br/>DEBUT json=$json<hr/>";
  $json = str_replace($envdoc,"",$json);
  $json = str_replace(array("\n","\r"),"\\n",$json);
  $json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
  $json = preg_replace('/(,)\s*}$/','}',$json);
  $json = preg_replace("/\\\'/","'",$json);
  //$json=addslashes($json);
  //echo "<br/>AVANT json=$json<hr/>";
  $json=str_replace('\\', '\\\\',$json); // pour le backslash
  //$json=str_replace('&mdash;','—',$json);
  // voir https://stackoverflow.com/questions/32056940/how-to-deal-with-backslashes-in-json-strings-php
  //echo serialize($json);
  $json=json_decode($json,$assoc,20);
  return $json;
  } 

function json2sql($json,$Author,$doc) // on récupére le json de ztree et on le met en base de données
  {
  //echo "dans jsqon2sql=$json"; //ok
    $json="{arbor:$json}";
    $json=protect_angainst_comma_and_colon($json);
    file_put_contents("/web/philo-labo/users/$Author/compositeur/mouchard3",$json);
    $b=json_decode_nice($json);
    $id_membre=simple_query("select id from membres where membre='$Author'");
    $doc=str_replace("'","\'",$doc);
    simple_query("delete from arbors where id_membre=$id_membre and document='$doc'");
    $tab=array();
    $b=$b['arbor'];
    if (sizeof($b)>0)
        foreach ($b as $l) 
                { 
                $l['name']=str_replace("  ","→",$l['name']);
                $tab[]=array($l['id'],$l['pId'],$l['name'],$l['icon'],$id_membre,$doc);
                } 
    array2sql('arbors',array('ego','father','name','category','id_membre','document'),$tab,'insert'); // mise dans la base
  return $b;
  }
  
function sql2json($author,$doc)
  {
  $id_membre=simple_query("select id from membres where membre='$author'");
  $doc=str_replace("'","\'",$doc);
  $nbr=simple_query("Select count(*) from arbors where id_membre='$id_membre' and document='$doc'");
  if ($nbr>0)
    {
    //file_put_contents('/web/philo-labo/users/felie/compositeur/pandoc/sql',simple_query("select name from arbors where id_membre='$id_membre' and document='$doc'"));
    $sql="Select ego,father,name,category from arbors where id_membre='$id_membre' and document='$doc'"; // le "S"elec t pour éviter la jointure automatique de id_ressource 
    return str_replace(array('&quot;','&colon;',"{},","'","→",'$'),array('’’',':','',"\'","→",'°°'),select2html($sql,'h',array('"id":%s','"pId":%s','"name":"%s"','"icon":"%s"'),array("[\n%s\n]",'{%s},','%s,')));
    }
  else
    return '{}';
  }
  
function json2arbor($j) //transdforme un json en tree
  {
  global $alert;
  //$alert.='json2arbor';
  $b=json_decode_nice($j);
  if (sizeof($b)>0)
    foreach ($b as $l) // construction du plan des ressources, pour diaporama ou beamer
        {
        $category=str_replace('/images/iconp/','',$l['icon']);
        $category=str_replace('.png','',$category);
        if (!preference('Affichage des numeros de ressources')) // on prend le numéro de la fin et on le met au début sans le @
            {
            //$alert='avant'.$l[name]."<br/>";
            $xname=explode(' ',strip_tags($l[name])); // on vire les tags
            $num=array_pop($xname);
            $alert.="num=$num<br/>";
            $is_ressource=($num[0]=='@');
            if ($is_ressource)
                $l[name]=substr($num.implode(' ',$xname),1);
            //$alert.="l[name]=$l[name]<br/>";
            }
        else
            $is_ressource=(is_numeric($l['name'][0]) and $l['icon']!='/images/iconp/vide.png'); // ressource 
        if ($is_ressource)
            $name="[$category]@".$l['name'];
        else
            if ($l['level']>=0 and ($category=='puce' or $category=='diapo')) // si c'est un niveau de plan
                $name=str_repeat('#',$l['level']+1).' '.$l['name']; 
            else
                $name=$l['name']; // level = -1 pour les contenus
        $plan.=$name."\n";
        }
  $plan=str_replace("→","  ",$plan); // pour les sous-items d'une liste
  $plan=str_replace('’’','"',$plan);
  $plan=str_replace("\n\n","\n",$plan);
  //$plan=str_replace('\\','\\\\',$plan);
  //$plan=str_replace("&comma;",",",$plan);
  return $plan;
  }
  
function nettoie($s)
    {
    $s=str_replace("\'","'",$s);
    $s=str_replace("&comma;",",",$s);
    $s=str_replace('    ','▸',$s); // surtout pour les espaces avant
    $s=str_replace('\\','\\\\',$s);
    return $s;
    }
  
function arbor2json($texte)
    {
    global $alert;
    //$alert='arbor2json';
    $texte2=strip_tags($texte);
    if ($texte2!=$texte)
        {
        $alert='Les tags html sont interdits en édition pour des raisons de sécurité. <br/>Le texte a été nettoyé.';
        $texte=$texte2;
        }
    $texte=str_replace(":",'&colon;',$texte); // le tree éditable contient des : ou des virgules
    $texte=str_replace(",",'&comma;',$texte);
    //$texte=str_replactrie(":","",$texte);
    $texte=str_replace("%list class=incremental%",'',$texte);
    $texte=str_replace("'''","**",$texte); // gras
    $texte=str_replace("''","*",$texte); // italiques
    $texte=str_replace("    • ","- ",$texte);
    $texte=str_replace("        ◦ ","  - ",$texte); // les copier/coller de puces
    $texte=str_replace("\n!!!!!","\n  - ",$texte);
    $texte=str_replace("\n!!!!","\n- ",$texte);
    $texte=str_replace("\n!!!","\n### ",$texte);
    $texte=str_replace("\n!!","\n## ",$texte);
    $texte=str_replace("\n!","\n# ","\n$texte"); // Un grand titre au tout début du document
    $texte=str_replace("\n\n","\n",$texte);
    //$texte=str_replace("'",'',$texte);
    $texte=str_replace('"',"’’",$texte); // laisser les guillemets détruit les documents à cause du json pourri
    $tablo=explode("\n",$texte); // convertit le texte en un tableau
    $array_pere=array(); // tableau des parents
    $prof=0;
    $prec_prof=0;
    $array_pere[1]=0;
    $lasttitre=null;
    $numero=0;
    $json=array();
    foreach ($tablo as &$ligne)
        if (trim($ligne)!='') // bof bof
        {
        $numero++; // incremente la ligne
        //echo "$numero - $ligne<br/>";
        if ($ligne[0]=='@') // cas d'une ressource sans catégorie
            {
            preg_match('/^@(\d+)(.*)$/',$ligne, $matches);
            $ress=$matches[1];
            $title=trim($matches[2]);
            $cat="[".simple_query("select nature from ressources where id=$ress")."]"; // ajout de la catégorie
            if ($title=="");
                $title=simple_query("select ressource from ressources where id=$ress"); // ajout du titre s'il n'y en a pas
            $ligne="$cat@$ress $title";
            }
        switch ($ligne[0]) 
            {
            case '#': // élement de plan
            $prof=strlen(preg_replace('/[^#]/','',$ligne));
            $ligne=preg_replace('/^(#*) /','',$ligne); //enleve l'entete de #
            $array_pere[$prof+1]=$numero;
            $pere=$array_pere[$prof]; // au lieu de prof-1
            //echo "$ligne - $prof - $pere <br/>";
            if ($prof==3)
                $iconlevel='diapo';
            else
                $iconlevel='puce';
            $json[].="{\"id\":\"$numero\", \"pId\":\"$pere\",\"name\":\"".trim($ligne)."\",\"icon\":\"/images/iconp/$iconlevel.png\",\"level\":\"".($prof-1)."\"}";
            $lasttitre=$numero;
            break;
            case '[': // ressources
            preg_match('/^\[([^\]]*)\]@(\d+)(.*)/',$ligne, $matches);
            $cat=$matches[1];
            $nress=$matches[2];
            $name=$matches[3];
            /*if ($prof<2)
                $alert.="<dl><dt><b>ligne $numero</b></dt><dd> Normalement une ressource s'affiche au niveau des diapos (niveau 3)<br/>
                Si vous souhaitez afficher une ressource au niveau 1 ou 2, il faut créer de 'faux titres' ## null ou # null et placer la ressource 'comme au niveau 3' pour berner le système</dd></dl>"; */
            $pere=$array_pere[$prof+1];
            if (!preference('Affichage des numeros de ressources')) // mise du numéro de ressource à la fin
                $item="{\"id\":\"$numero\",\"pId\":\"$pere\",\"name\":\"".rtrim(protect_comma($name))." ".cache("@$nress")."\",\"icon\":\"/images/iconp/$cat.png\",\"level\":\"$prof\"}";
            else
                $item="{\"id\":\"$numero\",\"pId\":\"$pere\",\"name\":\"$nress".rtrim(protect_comma($name))."\",\"icon\":\"/images/iconp/$cat.png\",\"level\":\"$prof\"}";
            $json[].=$item;
            $alert.="$item<br/>";
            break;
            default: // contenu de diapositive
                $json[].="{\"id\":\"$numero\",\"pId\":\"$lasttitre\",\"name\":\"".rtrim(protect_comma($ligne))."\",\"icon\":\"/images/iconp/vide.png\",\"level\":\"-1\"}";
            }
        }
    return nettoie("[".implode(',',$json)."]");
    }

?>
