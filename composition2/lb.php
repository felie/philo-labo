<?php

// mise à jour de la liste des documents d'un utilisateur dans le compositeur
// mise en évidence des documents qui sont partagés

require('../secure.php'); // charge $Author
require('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('../philosophemes/ai.php');

$repuser="/web/philo-labo/users/$Author/compositeur";

if (!isset($_GET['doc']))
  $doc=file_get_contents("$repuser/lastdoc");
else
  $doc=$_GET['doc'];

$i=simple_query("select id from membres where membre='$Author'");
if ($doc=='') // si pas de document, prendre le premier
  $doc=simple_query("select document from arbors where id_membres=$i order by document limit 1");

$select=$doc;
  
//liste déroulante des arbres de l'utilisateur dans la base de donnée avec un selecteur sur le document doc

// option 1: via la table arbors
//$listbox=sql2listbox("select document from arbors where id_membre=$i group by document",'document',$select);

// option 2 via les fichiers tree (plus solide!)
$liste=glob("/web/philo-labo/users/$Author/compositeur/*.tree");
$t=[];
$n=0;
foreach ($liste as $f)
    {
    $nomlocal=substr(str_replace("$repuser/",'',$f),0,-5);
    $t[$n][0]=$nomlocal;
    $t[$n][1]="$nomlocal";
    $n++;
    }
$listbox=listbox($t,'document');

// récupération des documents partagés
$partages=sql2array("select partage from partages where id_membre=$i");

$listbox=preg_replace("/<option value=\"([^\"]*)\">([^<]*)<\/option/",'<option value="\1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\2</option'   ,$listbox); 
$listbox=preg_replace("/selected=\"selected\">([^<]*)<\/option/",'selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\1</option'   ,$listbox);

$t='';
for ($i=0;$i<sizeof($partages);$i++)
    {
    $p=$partages[$i][0];
    $t.=$p;
    $listbox=str_replace(">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$p<",">&#10055;&nbsp;&nbsp;$p<",$listbox);
    }
    
echo "$listbox";

file_put_contents("$repuser/tableau",$listbox2);

file_put_contents("$repuser/lastdoc",$doc);

?>
