<?php

// echo "Ici vous rentrerez des préférences... par exemple si vous souhaitez que les polycopiés soient en noir et blanc (pour les images)";

require_once('../secure.php');
require_once('../config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('../philosophemes/ai.php');

//echo "Author=$Author";

// on suppriimer le résultat json de la recherche, parce que la préférence "affichage des numéros de ressources pourrait changer"
unlink("/web/philo-labo/users/$Author/compositeur/resultatsrecherche.json");

$content="<h1>Préférences de <i>$Author</i></h1>Attention: la case à cocher OK est sous le présent formulaire<br/>Si aucune discipline visible n'est cochée, le système considère qu'elles le sont toutes.".update2html("select id,preferences,id_discipline,disciplines_visibles from membres where membre='$Author'","h","=");
require('../philosophemes/ai_avsts2.php');

echo $content;

?>
