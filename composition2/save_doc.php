<?php

// reçoit des données et un nom d'un javascript, et sauvegarde les données dans ma table arbors (crée ou met à jour un document - un arbre de ressources)

require_once('../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('../philosophemes/ai.php');
require_once('arbor.php');

//nusleep (100); // au cas où une autre sauvegarde est en court
$LongName=file_get_contents("/web/philo-labo/users/$Author/longname");
$idAuthor=simple_query("select id from membres where membre='$Author'"); 

if (!isset($doc))
  $doc=$_POST['doc']; // nom du document passé
$narbre=$_POST[narbre];
file_put_contents("$repuser/lastdoc$narbre",$doc); // dernier document traité dans l'arbre de tel numéro (1 ou 2)   
    
$repuser="/web/philo-labo/users/$Author/compositeur";

if ($_POST[arbre]!='') // cas d'une création, d'une copie ou de l'enregistrement automatique après une modification en drag'n drop
    $j='['.implode(',',$_POST[arbre]).']'; // données passée en json en _POST
else // cas de l'enregistrement après édition à la main
    $j=arbor2json(strip_tags($_POST[texte]));
    
$tree=stripslashes(json2arbor($j));

$n=explode(' ',$doc);
$num=$n[0]; // numéro de la ressource dans la base de données

$nomlocal=preg_replace("/^(\d*)( *)\[$LongName\] /",'',$doc); // le numéro suivi d'un expace est facultatif, il n'est peut être pas encore créé

file_put_contents("$repuser/mouchard","*$doc*\n*$nomlocal*");

file_put_contents("$repuser/$nomlocal.json",$j); // enregistrement du json

file_put_contents("$repuser/$nomlocal.tree",$tree); // enregistrement du tree

$nom ="[$LongName] $nomlocal";
$tree=str_replace("\n",'\n',addslashes($tree));

// gestion de la base de données
if ($_POST[from]=='create' or $_POST[from]=='save_as') // création
    {
    $q="insert into ressources (ressource,id_membre,nature,texte) values ('$nom',$idAuthor,'composite','$tree')";
    $r=simple_query("insert into ressources (ressource,id_membre,nature,texte) values ('$nom',$idAuthor,'composite','$tree')");
    $id=simple_query("select id from ressources where ressource='$nom'");
    // ajout au début du catalogue
    $catalogue=file_get_contents("$repuser/Catalogue.tree");
    file_put_contents("$repuser/Catalogue.tree","[composite]@$id $nom\n$catalogue"); // l'ajoute au début du Catalogue
    simple_query("update ressources set texte='[composite]@$id $nom\n$catalogue' where ressource='[$LongName] Catalogue'"); // maj catalogue en base se donnée 
    //ajoute à la table arbors pour compatibilité avec la v1
    simple_query("insert into arbors (document,id_membre) values ('$nomlocal',$idAuthor)");
    }
else // mise à jour
    simple_query("update ressources set texte='$tree' where id=$num and nature='composite'"); // par sécurité le nature='composite'

file_put_contents("$repuser/temoin2","$r $q");
    
if (!$firstaccess==1)
    echo "Document <b>$doc</b> de <b>$Author</b> enregistré<hr/>$alert";
    
?>
