<?php

/* fonctions qui sont partagées entre le wiki et la base de donnée
   seules les ressouces du domaine public peuvent être affichées dans la zone publique
*/

include_once('/web/philo-labo/cookbook/detect_mobile.php');
//include_once('/web/philo-labo/philosophemes/secure.php');
include_once('ressources.php');
include_once('../local/log.php');

$popups_grand_format=preference('Popups grand format');
//echo "grand format=$popups_grand_format";

// function nomwiki($i)
//   {
//   $i=str_replace(array('(',')','"',"'",'/','-','_'),array('','','',"",' ',' ',' '),$i);
//   return preg_replace('/[ ](\S)/','\1',ucwords($i));
//   }

// enlève la peau des patates
function economiseur($t)
  {
  $texte=explode("\n",$t);
  $avale=true;
  $result='';
  foreach ($texte as $ligne)
    {
    if( strstr($ligne, "<!-- BeginTopNavButtons -->")) $avale=false; 
    if( strstr($ligne, "<!-- EndTopNavButtons -->")) $avale=true; 
    if( strstr($ligne, "<!-- BeginBottomNavButtons -->")) $avale=false; 
    if( strstr($ligne, "<!-- EndBottomNavButtons -->")) $avale=true; 
    if ($avale)
      $result.=$ligne;
    }
  return $result;
  }

$nature_select='<select name="nature[]" id="test-select" multiple="multiple">
        <option selected value="pdf" data-section="Tout/ebooks">pdf</option>
        <option selected value="texte" data-section="Tout/textes">textes</option>
        <option selected value="image" data-section="Tout/Images">images</option>
        <option selected value="graphe" data-section="Tout/Images">graphe</option>
        <option selected value="graphique" data-section="Tout/Images">graphique</option>
        <option selected value="dialogue" data-section="Tout/Images">dialogue</option>
        <option selected value="lilypond" data-section="Tout/Images">lilypond</option>
        <option selected value="exo-de-maths" data-section="Tout/Exercices">Exercices de maths</option>
        <option selected value="JCross" data-section="Tout/Exercices/hotpatatoes">JCross</option>
        <option selected value="JQuizz" data-section="Tout/Exercices/hotpatatoes">JQuizz</option>
        <option selected value="JCloze" data-section="Tout/Exercices/hotpatatoes">JCloze</option>
        <option selected value="JMix" data-section="Tout/Exercices/hotpatatoes">JMix</option>
        <option selected value="JMatch" data-section="Tout/Exercices/hotpatatoes">JMatch</option>
        <option selected value="document" data-section="Tout/ebooks">documents</option>
        <option selected value="epub" data-section="Tout/ebooks">epub</option>
        <option selected value="mobi" data-section="Tout/ebooks">mobi</option>
        <option selected value="sujet-question" data-section="Tout">sujets-question</option>
        <option selected value="sujet-texte" data-section="Tout/textes">sujets-texte</option>
        <option selected value="site" data-section="Tout">sites</option>
        <option selected value="LearningsApps" data-section="Tout/Exercices">LearningsApps</option>
        <option selected value="youtube" data-section="Tout/Multimedia/Video">youtube</option>
        <option selected value="dailymotion" data-section="Tout/Multimedia/Video">dailymotion</option>
        <option selected value="vimeo" data-section="Tout/Multimedia/Video">vimeo</option>
        <option selected value="mp4" data-section="Tout/Multimedia/Video">mp4</option>
        <option selected value="mp3" data-section="Tout/Multimedia">mp3</option>
        <option selected value="Quizz" data-section="Tout/Exercices">Quizz</option>
        <option selected value="composite" data-section="Tout" data-description="Ressources composées de ressources">composite</option>
        <option selected value="h5p" data-section="Tout/Exercices" data-description="Ressources h5p">H5P</option>
      </select>';
  
// formulaire de recherche multicritères
$form_search='<div class="machin">
  <form method="POST" action="%s">
      <input name="chaine" value="" size="30">
      <input type="submit" style="display:inline" name="" value="OK"/>'.
      $nature_select.'
  </form>
    <script type="text/javascript">
      $("#test-select").treeMultiselect({ enableSelectAll: false, sortable: true, hideSidePanel:true, startCollapsed:true  });
    </script>
  </div>';
     
$numdrag="<div draggable='true' ondragstart='drag(event)' id='%1\$s'>%1\$s</div>";
$lienpop2='<a href="#" onClick="openDialog(\'%s\')">%s</a>';
$lienpop="<a class='alink' href='/philosophemes/ress.php?id=%s'>%s</a>";
//$tag='<a target="boulot" href="ventiler.php?nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme&id=%1$s"><img src="/images/phi.png"></a>';
$tag='<a class="alink" title="" href="/philosophemes/ventiler.php?nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme&id=%1$s"><img src="/images/phi.png"></a>';
$ephi=edit_().'<b>%1$s</b>'.$tag;
$edit="<a href=\"?action=update&table={table}&amp;id=%1\$s\"><img border=0 alt=\"update\" src=\"images/edit.png\"/></a>";
$getress='<a href="#" onClick="openDialog(\'%1$s\',\'/philosophemes/ress.php?id=%1$s\')" onMouseover="openDialog(\'%1$s\',\'/philosophemes/ress.php?id=%1$s\')>%1$s</a>';
//$getress='<a class="alink" title="%1$s" href="ress.php?id=%1$s">%1$s</a>';

function liste_sous_condition($condition)
  {
  global $getress,$tag;
  return select2html('Select ressources.id,ressources.id,ressources.id,ressources.id,ressources.id,ressources.ressource,auteurs.auteur,ressources.nature,membres.membre from ressources'.jn(ressource,auteur).jn(ressource,membre).$condition,',h',array('d',edit_(),$getress,$tag),array("<div><table class='t' id='tire-specs'>%s</table></div>","<tr>%s</tr>","< >%s</td>"));
  }
  
/*
function jolitexte($t) // fait le découpage en paragraphe
  {
  $t="<p>$t</p>";
  $t=str_replace("\n\n","</p><p>",$t);
  return $t;
  }*/
  
// résultat de la recherche dans la table des ressources, avec filtrage supplémentaires sur les ressources perso et dans les disciplines visibles des préférences
 function resultatrecherche($_edit=false,$_delete=false,$_tag=false,$json=false)
  {  
  global $_POST,$lienpop,$numdrag,$tag,$ephi,$edit,$href_del,$Z,$Author;
  $perso=preference('Chercher seulement dans mes ressources');
  $disciplines=simple_query("select disciplines_visibles+0 from membres where membre='$Author'");  // récupération du nombre codant les disciplines visibles  
  if (!isset($_POST[chaine]))
    return '';
  $chaine=$_POST[chaine];
  $recherche=$chaine;
  $body="$chaine</br>";
  $chaine=str_replace("'","\"",$chaine);
  $chaine=str_replace('"',"\"",$chaine);
  $chaine='"" '.$chaine;
  $chaine=explode('"',$chaine);
  //echo "<pre>";print_r($chaine);echo "</pre>";
  $like='';   
  foreach ($chaine as $c)
    {
    if ($c!='')
      {
      if ($c[0]==' ')
        {
        $c=explode(' ',$c);
        //print_r($c);
        foreach ($c as $cc)
          {
          if ($cc!='')
            {
            if (ctype_digit($cc))
	      $like.="and ((ressources.id=$cc) or (annee=$cc))";
	    else
            //echo " **$cc** ";
	      $like.="and concat(ressources.id,' ',ressource,' ',texte,' ',auteur,' ',reference,' ',tags,' ',notions,' ',note,' ',lieu,' ',annee)  like '%$cc%'";
            }
           }
        }
      else
        {
        //echo " *$c* ";
        if (ctype_digit($c))
	  $like.="and ((ressources.id=$c) or (annee=$c))";
	else
	  $like.="and concat(ressources.id,' ',ressource,' ',texte,' ',auteur,' ',reference,' ',tags,' ',notions,' ',note,' ',lieu,' ',annee)   like '%$c%'";
        }
      }
    }
  $nature=$_POST[nature];
  if (sizeof($nature)>0)
    {
    foreach ($nature as &$n)
      $n="'$n'";
    $options=implode(',',$nature);
    $chaine=str_replace("'","\'",$chaine);
    //print_r($options);
    $masksup='<table style="border-width: 0px;"><tr style="border-width: 0px;">';
    if ($_delete) $masksup.="<td style='border-width: 0px;'>$href_del</td>";
    if ($_edit) $masksup.="<td style='border-width: 0px;'>$edit</td>";
    if ($_tag) $masksup.="<td style='border-width: 0px;'>$tag</td>";
    $masksup.="<td style='cursor:move;border-width: 0px;'>$numdrag</td>";
    $masksup.='</tr></table>';
    $filtre_disciplines='';
    if ($disciplines!=0) // si c'est 0 c'est si c'était tout
      $filtre_disciplines="$disciplines & disciplines+0 and ";
    $filtre_perso='';
    if ($perso)
        $filtre_perso='id_membre='.simple_query("select id from membres where membre='$Author'")." and ";
    $condition=" where $filtre_perso $filtre_disciplines nature IN ($options) $like order by ressource";
    if (!$json)	
      {
      $q="Select ressources.id,ressources.id,ressources.nature,ressources.id,ressources.ressource,auteurs.auteur from ressources".jn(ressource,auteur)."$condition";
      $body.=select2html($q,'h',array($masksup,$tag,'<img src="/images/iconp/%s.png">',$lienpop,'%s'),$Z,array('id','phi','nat','ressource','auteur'));
      }
    else
      {
      //$q="Select ressources.id,ressources.id,ressources.ressource,auteurs.auteur,ressources.nature from ressources".jn(ressource,auteur)."$condition";
      //$body=select2html($q,'h',array("{ id:~~~@%s~~~, pId:0, name:~~~%s %s - %s~~~,icon:~~~/images/iconp/%s.png~~~},"),array('%s','%s','%s'));
      // on enlève maintenant l'id 
      $q="Select ressources.id,ressources.ressource,auteurs.auteur,ressources.nature from ressources".jn(ressource,auteur)."$condition";
      if (preference("Affichage des numeros de ressources"))  // on le numéro au début
        {
        $q="Select ressources.id,ressources.ressource,auteurs.auteur,ressources.nature from ressources".jn(ressource,auteur)."$condition";
        $body=select2html($q,'h',array("{ id:xxx, pId:0, name:~~~%s %s %s~~~,icon:~~~/images/iconp/%s.png~~~},"),array('%s','%s','%s'));
        }
      else // on met le numéro à la fin avec @ devant
        {
        $q="Select ressources.ressource,auteurs.auteur,ressources.id,ressources.nature from ressources".jn(ressource,auteur)."$condition";
        $body=select2html($q,'h',array("{ id:xxx, pId:0, name:~~~%s %s ".cache('@%s')."~~~,icon:~~~/images/iconp/%s.png~~~},"),array('%s','%s','%s'));
        }
      $body=str_replace('"',"''",$body); // protéger les guillemets pour aviter que ça perturbe le json
      $nombre=0;
      $body=str_replace('~~~','"',$body,$nombre);
      $body='{ id:xxx, pId:0, name:"il y a '.($nombre/4).' résultats",icon:"/images/iconp/vide.png"},'.$body;
      $body=str_replace('- ",icon','",icon',$body); // bidouille pour éviter le - quand l'auteur est vide
      $body=incremental($body);
      file_put_contents("/web/philo-labo/users/$Author/compositeur/requete",$q);
      }
    writelog("$Author\trecherche\t$recherche");
    }
  else
    $body.="aucune catégorie de ressource sélectionnée";
  return $body;
  }

function incremental($line)
  {
  global $cle;
  $cle=0;
  return preg_replace_callback(
	'|xxx|',
	function ($matches) {
                global $cle;
                $cle=$cle+1;
		return $cle;
	},
	$line
        );
  }
/*  
$youtube='<iframe class="youtube-player" type="text/html" width="640" height="385"
  src="https://www.youtube.com/embed/%s" frameborder="0">
</iframe>';
*/
$youtube='<iframe class="youtube-player" type="text/html" width="640" height="385"
  src="%s" frameborder="0">
</iframe>';

function getitem($id,$relax=1)
    {
    return item_wiki($id,$relax);
    }

function montre($i,$j)
  {
  echo "$i $j<br/>";
  }

function getitemt($id,$relax=0) // adhoc mais généralisable
    {
      $titre=simple_query("select ressource from ressources where id=$id");
      return "<h4>$titre</h4>".getitem($id,$relax);
    }

function getitemtt($id,$relax=0) // adhoc mais généralisable
    {
      return simple_query("select ressource from ressources where id=$id");
    }


function getitemf($id)
  {
  return("<iframe width=100% height=600 frameborder=0 src='/philosophemes/texte.php?id=$id'></iframe>");
  }
  
function getitemtf($id) // adhoc mais généralisable
    {
      $titre=simple_query("select ressource from ressources where id=$id");
      return "<big>$titre</big><p/><iframe width=95% height=600 frameborder=3 src='/philosophemes/texte.php?id=$id'></iframe>";
    }
    
function getitemx($id,$relax=1)
    {
    return item_popup($id,$relax);
    }
    
function getitem4m2b($id, $titre=false,$long=false) // adhoc mais généralisable
    {
    global $Author,$youtube;
    $titre='';
    if ($titre)
      $titre="### ".simple_query("select ressource from ressources where id=$id")."\n\n";
    $server='https://'.$_SERVER['HTTP_HOST']; //"https://philo-labo.fr";
    //echo "Author=$Author";
      $nature=simple_query("select nature from ressources where id=$id");
      switch ($nature){
          case 'image':$url=simple_query('select url from ressources where id='.$id);
			return "imgp(".$url.")\n\n";	
          case 'texte':
	    if ($long)	
 		$texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("ext_pdf($id pages=-,nup=1,scale=1<SDL>%s<SDL>>%s *%s*<SDL>)ext<SDL>"),array('%s','%s','%s'));
	    else
                {
            	$texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("t(%s<SDL>%s *%s*)t <SDL>"),array('%s','%s','%s'));
                $texte=str_replace('<SDL>',"\n",$texte);
                }
            return $titre.$texte."\n\n";
            break;
       }
   }  

// reçoit un tableau de numero de ressources textes, la taille de la fonte, la hauteur admise
// renvoie un tableau d'images

// règle 1: si un texte est déjà présent, un autre ne pourra s'ajouter à une case que s'il est complet
// règle 2: si une ressource n'est pas un texte, elle est renvoyée dans une case

function aff_tab($var)
{   
    echo "<pre>";
    $output=print_r($var,true);
    echo htmlspecialchars($output);
    echo "</pre>";
} 
  
// function aff_tab($var, $return = false) {
//   $r = aff_tab(htmlspecialchars(print_r($var, true)));
//   if ($return) return $r;
//   else echo $r;
// }

function split_text($texte,$n)
  {
  global $Author;
  // écrit texte dans un répertoire utilisateur
$texte=str_replace(array(' ;',' ?',' !',' :'),array(';','?','!',':'),$texte);
//$texte=preg_replace('/([^\ ])\"/',"$1\"",$texte);
//$texte=preg_replace('/(\"[^\ ])/',"\"$1",$texte);
$tex='
\documentclass[a5paper]{article}
\usepackage{setspace}
\usepackage{lmodern}
\usepackage{times}


\usepackage[landscape,left=0cm,right=0cm,top=0cm,bottom=0cm]{geometry}% pour les marges

\usepackage[utf8]{inputenc}% pour écrire en utf8 dans les fichiers
\usepackage[T1]{fontenc}% pour les accents dans les pdf
\usepackage{aeguill}
\usepackage{needspace}

\setlength{\parindent}{2cm}
\setlength{\parskip}{-1cm}
%\renewcommand{\baselinestretch}{1.2}


\thispagestyle{empty} % pas de numérotation de page
\usepackage{nopageno}

\newcommand{\auteur}[1]{\nopagebreak[4]\\\\\vspace{40pt}\hfill\rmfamily\Huge\textsc{#1}}
\newcommand{\texte}[1]{\fontsize{24}{28}\rmfamily{#1}\par}%
\newcommand{\reference}[1]{\nopagebreak[4]\par\vspace{30pt}\hfill\rmfamily\Huge{\textit{#1}}}

\clubpenalty=9996
\widowpenalty=9999
\brokenpenalty=4991
\predisplaypenalty=10000
\postdisplaypenalty=1549
\displaywidowpenalty=1602
\usepackage[all]{nowidow}


\begin{document}
'
.$texte
.'
\end{document}
';
$rep="/web/philo-labo/users/$Author";
file_put_contents("$rep/split.tex",$tex);
  // y execute latex dessus
// echo 'before latex<br/>';
exec("cd $rep;latex --interaction=nonstopmode split 2>&1",$output);
//printr($output);
$output='';
  // y execute dvinpg
// echo 'before dvipng<br/>';
//exec("cd $rep;rm texte$n*");
exec("cd $rep;dvipng split -D 300 -bg Transparent -o texte$n-%d.png 2>&1",$output);
//printr($output);
// echo 'after dvipng<br>';
// echo "le numéro du texte est $n<br/>";
  // retourne la liste des images
  $i=1;
  $res=array();
  while (file_exists("$rep/texte$n-$i.png")){
     $res[]="users/$Author/texte$n-$i.png";
     $i+=1;
     }
  return $res; // renvoie un tableau d'images
  }
  
function splitter($x) // liste de ressources
  {
//   print_r($x);
//   echo "<br/>";
  foreach ($x as $o) // vérifier s'il n'y a que des textes
    {
    $nature=simple_query("select nature from ressources where id=$o");
    if ($nature!='texte')
      return ""; // il y a une ressource qui n'est pas un texte, on ne splitte pas
    }
  $img=array();
  foreach ($x as $o)
    {
    $texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$o",'h',array("\\texte{%s}\\auteur{%s}\\reference{%s}"),array('%s','%s','%s'));
    $a=split_text($texte,$o);
    $img[]=$a;
    }
  return $img; //renvoie un tableau de tableau d'images
  }
  
// pour l'ajout des ressources

$natures=array('Texte','ExoDeMaths','SujetTexte','Pdf','Epub','Mobi','Document','SujetQuestion','Site','Youtube','Dailymotion','Vimeo','Mp4','Mp3','Image','H5P','Graphe','Graphique','Dialogue','Lilypond','Midi','Music');
function add_ressources()
    {
    global $natures;
    $result='';
    foreach ($natures as $nature)
          {
          $img=strtolower($nature);
          //$titre=str_replace('_','',$nature);
          $result.="<option value=\"$nature\" style=\"background-image:url('/images/icons/24x24/$img.png');\">$nature</option>";
          }
    return $result;
    }
$addressources="\n<div style='clear:both;float:left;display:inline' class=\"drop-down\">    
	   <select name=\"options\">".add_ressources()."</select>     
</div>
<!-- pour le menu déroulant d'ajout des ressources-->
<style>
.drop-down { 
 position: relative;  
 display: inline-block;  
 width: auto;
 text-decoration:none;
 margin:0;
 padding:0;
 margin-top: 0;   
 font-family: Arial;  
 font-style: normal;
 }      
 .drop-down select {   
  display: none;    
 }      
 .drop-down .select-list {   
 position: absolute;
 /*width:100%;*/
 top: 0;      
 left: 0;     
 z-index: 1;    
 margin:0px;
 margin-top: 30px;
 padding: 0;           
 }      
 .drop-down .select-list li {   
 display: none;
list-style:none; 
 }    
 .drop-down .select-list li span {  
 display: inline-block;
 margin:-2px;
 min-height: 28px;
 height:28px;
 min-width: 220%;      
 /*width: 100%;*/        
 padding: 4px 5px 4px 40px; 
 background-color: lightgray;     
 background-position: left 10px center;   
 background-repeat: no-repeat;   
  font-family: Arial;  
 font-style: normal;
 font-weight: normal;
 font-size: 16px;       
 text-align: left;       
 color: black;        
 opacity: 1;      
 box-sizing: border-box;  
 }     
 .drop-down .select-list li span img:hover,  
 .drop-down .select-list li span img:focus {     
 opacity: 1; 
 background-color:orange;
 }
 .drop-down .select-list li span:hover,  
 .drop-down .select-list li span:focus {     
 opacity: 1; 
 background-color:orange;
 }
a:hover {background-color:transparent;}
</style>

<script>
jQuery().ready(function() {  
/* Custom select design */    
jQuery('.drop-down').append('<div style=\"display:inline-block;text-decoration:none\" class=\"button\"></div>');    
jQuery('.drop-down').append('<ul class=\"select-list\"></ul>');    
jQuery('.drop-down select option').each(function() {  
var bg = jQuery(this).css('background-image');    
jQuery('.select-list').append('<li class=\"clsAnchor\"><span value=\"' + jQuery(this).val() + '\" onclick=\"add_ressource(\''+jQuery(this).text()+'\');\" class=\"' + jQuery(this).attr('class') + '\" style=background-image:' + bg + '><big><big>&nbsp;Ajouter ' + jQuery(this).text() + ' à la base</big></big></span></li>');   
});    
jQuery('.drop-down .button').html('<span style=background-image:' + jQuery('.drop-down select').find(':selected').css('background-image') + '>' + '</span>' + '<a href=\"javascript:void(0);\" class=\"select-list-link\"><img margin=0 width=22 src=\"/images/menucompo/add.png\"></a>');   
jQuery('.drop-down ul li').each(function() {   
if (jQuery(this).find('span').text() == jQuery('.drop-down select').find(':selected').text()) {  
jQuery(this).addClass('active');       
}      
});     
jQuery('.drop-down .select-list span').on('click', function()
{          
var dd_text = jQuery(this).text();  
var dd_img = jQuery(this).css('background-image'); 
var dd_val = jQuery(this).attr('value');   
jQuery('.drop-down .button').html(' ' + '<a href=\"javascript:void(0);\" class=\"select-list-link\"><img width=24 style=\"text-decoration:none\" src=\"/images/menucompo/add.png\"></a>');      
jQuery('.drop-down .select-list span').parent().removeClass('active');    
jQuery(this).parent().addClass('active');     
$('.drop-down select[name=options]').val( dd_val ); 
$('.drop-down .select-list li').slideUp();     
});       
jQuery('.drop-down .button').on('click','a.select-list-link', function()
{      
jQuery('.drop-down ul li').slideToggle(10);  
});     
/* End */       
});
</script>
";

// code pour les notifications et les popup dans le wiki et le compositeur
function code_pour_popup()
    { global $popups_grand_format;
if ($popups_grand_format)
return '<div class="jqmWindow" id="dialog1" style="left:0vw;top:0px;bottom:0px;width:98vw;border-width:1px;border-radius:0px;">
<a style="text-decoration:none" href="#" class="jqmClose"><img style="float: right;position: relative; bottom: -5px;" alt=\'fermer\' src="/images/close.png"/></a>
<div id=\'content1\' style="overflow-y:scroll;width:94vw;height:95vh;margin:5px;"></div>
</div>
<!-- pour popupf() avec iframe -->
<div class="jqmWindow" id="dialogf" style="left:0vw;top:0px;bottom:0px;width:98vw;border-width:1px;border-radius:0px;"> <!--background-color: rgba(169, 169, 169, 0.0);">-->
<a style="text-decoration:none" href="#" class="jqmClose"><img style="float:right;position:relative; top:0px;" alt=\'fermer\' src="/images/close.png"/></a>
<div style="text-align:center;"><iframe border=1 src="" scrolling="yes" class=\'contentf\' id=\'contentf\' style="width:95vw;height:92vh;margin:0px;"></iframe></div>
</div>'.
"<script src=\"/js/jqmodal/jqModal.js\"></script>
<script src=\"/js/xnotify.js\" ></script>
<script>
function add_ressource(type) {// Ajouter une ressource (fonction générique, on passe le type à la fonction) 
		  xnotify({body:\"Ajouter une ressource de type \"+type,duration:1});
                  popupf(\"/Ai/Ajouter\"+type+\"?skin=minimal\"); // pour affichage dans une boite modale centrale
		}
function popup(url){
                  $('#content1').html(''); // effacement
                  //$('#content1').load(url);
                  //$('#dialog1').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
                  $('#dialog1').jqm({ajax:encodeURI(url),target:'#content1',ajaxText:'Patientez...',closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
		}
		function popupf(url){
                    var iframe=document.getElementById(\"contentf\");
                    iframe.contentWindow.document.body.innerHTML = \"\"; // pour vider une iframe!!!
                    iframe.src=url;
                    $('#dialogf').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
		}
</script>";
else
return '<div class="jqmWindow" id="dialog1" style="left:20vw;top:10vh;bottom:10vh;width:60vw;border-width:1px;border-radius:0px;">
<a style="text-decoration:none" href="#" class="jqmClose"><img style="float: right;position: relative; bottom: -5px;" alt=\'fermer\' src="/images/close.png"/></a>
<div id=\'content1\' style="overflow-y:scroll;width:57vw;height:75vh;margin:5px;"></div>
</div>
<!-- pour popupf() avec iframe -->
<div class="jqmWindow" id="dialogf" style="left:20vw;top:10vh;bottom:10vh;width:50vw;border-width:1px;border-radius:0px;"> <!--background-color: rgba(169, 169, 169, 0.0);">-->
<a style="text-decoration:none" href="#" class="jqmClose"><img style="float:right;position:relative; top:0px;" alt=\'fermer\' src="/images/close.png"/></a>
<div style="text-align:center;"><iframe border=1 src="" scrolling="yes" class=\'contentf\' id=\'contentf\' style="width:48vw;height:75vh;margin:0px;"></iframe></div>
</div>'.
"<script src=\"/js/jqmodal/jqModal.js\"></script>
<script src=\"/js/xnotify.js\" ></script>
<script>
function add_ressource(type) {// Ajouter une ressource (fonction générique, on passe le type à la fonction) 
		  xnotify({body:\"Ajouter une ressource de type \"+type,duration:1});
                  popupf(\"/Ai/Ajouter\"+type+\"?skin=minimal\"); // pour affichage dans une boite modale centrale
		}
function popup(url){
                  $('#content1').html(''); // effacement
                  //$('#content1').load(url);
                  //$('#dialog1').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
                  $('#dialog1').jqm({ajax:encodeURI(url),target:'#content1',ajaxText:'Patientez...',closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
		}
		function popupf(url){
                    var iframe=document.getElementById(\"contentf\");
                    iframe.contentWindow.document.body.innerHTML = \"\"; // pour vider une iframe!!!
                    iframe.src=url;
                    $('#dialogf').jqm({closeOnEsc:true,overlay:50,modal:true,trigger:false}).jqmShow({overlay: 70});
		}
</script>";
    }
    
  
?>
