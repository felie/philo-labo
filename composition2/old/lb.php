<?php

# mise en évidence des fichiers partagés

# require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once ('../philosophemes/ai.php'); // pour la fonction listbox utlisée plus loin
require_once ('../philosophemes/secure.php');

$repuser="/web/philo-labo/users/$Author/compositeur";

if (!isset($_GET['doc']))
  $doc=file_get_contents("$repuser/lastdoc");
else
  $doc=$_GET['doc'];

// récupération des documents utilisateur  
$trees = glob("$repuser/*.tree");

$ndoc=0;
$documents=array();
foreach ($trees as &$d)
    {
    $name=substr($d,strlen($repuser)+1,-5);
    $documents[$ndoc][0]=$name;
    $documents[$ndoc][1]=$name; 
    $ndoc++;
    }

$listbox=listbox($documents,'documents',$doc);
// faire de la place pour le signe de partage
$listbox=preg_replace("/<option value=\"([^\"]*)\">([^<]*)<\/option/",'<option value="\1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\2</option'   ,$listbox); 
$listbox=preg_replace("/selected=\"selected\">([^<]*)<\/option/",'selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\1</option'   ,$listbox);

// récupération des documents partagés
$LongName=file_get_contents("/web/philo-labo/users/$Author/longname");
$rep="/web/philo-labo/partage/\[$LongName\]";
$trees = glob("$rep*.tree");

// mettre les étoiles aux documents partagés
foreach ($trees as $d)
  {
  $p=substr($d,strlen($rep)-1,-5);
  file_put_contents("/web/philo-labo/users/$Author/compositeur/partages",$p);
  $listbox=str_replace(">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$p<",">&#10055;&nbsp;&nbsp;$p<",$listbox);
  }

echo $listbox;

?>
