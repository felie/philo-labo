<?php
require_once('../../../../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require('../../../../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('../../../../philosophemes/ai.php');
require_once('../../../../philosophemes/philo-labo.php'); // fonctions spécifiques à philo-labo

// construit le formulaire de recherche qui sera affiché
$recherches=sprintf($form_search,'?action=searchengine&json=true');

// enregistre le résultats de la recherche en json
if ($_GET['action']=='searchengine')
 file_put_contents('resultat.json',"[\n".str_replace("'","\'",resultatrecherche(true,true,true,$_GET['json']))."\n]");

// pour le formulaire de choix du document - le submit du formulaire est bloqué par event.prevent.Default()
$selection_document="<form id='selection_document'>".sql2listbox('select document from arbors where id_membre=16 group by document','document').'<button id="selection">Charger</button></form>';

// le submit du formulaire est bloqué par event.prevent.Default() voir plus bas
$creation='<form id="formulaire_de_creation"><input id="doc_a_creer" type="text" size="10" name="doc"><button id="create">Créer ou mettre à jour</button></form>';

?>
<!DOCTYPE html>
<HTML>
<HEAD>
	<TITLE> ZTREE DEMO - multiTree</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="../../../css/demo.css" type="text/css">
	<link rel="stylesheet" href="../../../css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="../../../js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="../../../js/jquery.ztree.exedit.js"></script>
	<script>
          $(document).ready(function(){
            $("#create").click(function(){
	      var doc = document.getElementById("doc_a_creer").value;
              if (doc == '')
                doc = document.getElementById("selection_document").elements[0].value;
              var zTree = $.fn.zTree.getZTreeObj("doc");
              nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray (nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 str = '{id:'+nodes_array[i].id+',pId:'+nodes_array[i].pId+',name:"'+nodes_array[i].name.replace(/\'/g,"\\'")+'"}';
                 tempResult.push(str);
                 }
              $.post("save_doc.php",
                {
                arbre:tempResult,
		doc:doc,
                },           function(data,status){
                alert(data);
              });
            });
          });
        </script>
	<script>
	  $(document).ready(function(){
	    $("#tbutton").click(function(){
              var zTree = $.fn.zTree.getZTreeObj("panier");
	      nodes = zTree.getNodes();
	      var nodes_array = zTree.transformToArray (nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 str = '{id:'+nodes_array[i].id+',pId:'+nodes_array[i].pId+',name:"'+nodes_array[i].name.replace(/\'/g,"\\'")+'"}';
                 tempResult.push(str);
                 }
	      $.post("save_doc.php",
        	{
		arbre:tempResult,
		doc:"panier",
        	},              function(data,status){
            	alert(data);
              });
            });
          });
	</script>
       <script>
          $(document).ready(function(){
            $("#selection").click(function(){
	    var doc = document.getElementById("selection_document").elements[0].value; // récupération du nom du document à charger
            var settingChargement = {
                        async: {
                                enable: true,
                                type:"get",             
                                url:"read_doc.php?doc="+doc,  // le script php produit un json à manger par le javascript
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest"},
                                dataFilter: filter
                        },
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick
                        },
                };
            $.fn.zTree.destroy("doc"); // on le détruit
            $.fn.zTree.init($("#doc"), settingChargement); // on le recrée avec la nouvelles données
	//   alert('chargement de '+doc);
           });
          });
        </script>
	<SCRIPT type="text/javascript">
		var settingRecherche = {
			async: {
				enable: true,
				type:"get",		
				url:"read_json.php?json=resultat.json",
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				simpleData: {
				enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				onClick: myOnClick
			},
		};
                var settingPanier = {
                        async: {
                                enable: true, 
                                type:"get",             
                                url:"read_doc.php?doc=panier",
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest"},
                                dataFilter: filter
                        },
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick
                        },
                };
                var settingDocument = {
                        edit: {
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick
                        },
                };
		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		                childNodes[i].open = true; // pour expand par defaut
			}
			return childNodes;
		}

                function myOnClick(event, treeId, treeNode) {
                        document.getElementById('iframeid').src="/philosophemes/ressif.php?id="+treeNode.id;
                }

		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return targetNode ? targetNode.drop !== false : true;
		}
		var zvide=[];
		$(document).ready(function(){
			$.fn.zTree.init($("#panier"), settingPanier);
			$.fn.zTree.init($("#recherche"), settingRecherche);
			$.fn.zTree.init($("#doc"), settingDocument,zvide);
			$('#formulaire_de_creation').submit(function(event) {
    				event.preventDefault();
			});
			$('#selection_document').submit(function(event) {
                                event.preventDefault();
                        });

		});
	</SCRIPT>
</HEAD>

<BODY> 
  <table width=100% height=600>
    <td width=24% >
	<div align=center><b>Ressource</b><iframe id='iframeid' style="width:400px;height:640px;margin:5px"></iframe></div>
    </td>
    <td width=24%>
	<div align=center style="background-color:lightblue"><b>Moteur</b><?php echo "<div style='width:350px;'>$recherches</div>"; ?><p/></div>
	<div align=center><div style="width:300px;margin:5px"><b>Résultats</b><ul id="recherche" class="ztree" style="width:300px;height:500px"></ul></div></div>
    </td>
    <td width=24%>
	<div align=center><div style="width:300px;margin:5px"><b>Panier</b> <button id="tbutton">Enregistrer</button><ul id="panier" class="ztree" style="width:300px;height:600px"></ul></div></div>
    </td>
    <td width=24%>
	<div align=center><div style="width:300px;margin:5px"><b>document</b>
<?php echo $creation; ?>
<?php echo $selection_document; ?>

<ul id="doc" class="ztree" style="width:300px;height:554px"></ul></div></div>
    </td>
</table>
</BODY> 
</HTML>
