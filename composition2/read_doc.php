<?php
// va lire un document dans la base de données et le donne à manger au javascript

require_once('../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('../philosophemes/ai.php');

$repuser="/web/philo-labo/users/$Author/compositeur";
$LongName=file_get_contents("/web/philo-labo/users/$Author/longname");
$idAuthor=simple_query("select id from membres where membre='$Author'");

require_once('arbor.php');

$doc=$_GET[doc]; // nom du  document passé

// complete avec le numéro si le numéro manque - c'est le cas pour la création d'un document ou l'enregistrer sous
if (!is_numeric($doc[0]))
    $doc=simple_query("select id from ressources where ressource='$doc' and id_membre=$idAuthor").' '.$doc;
    
$n=explode(' ',$doc);
$num=array_shift($n);
$nomlocal=implode(' ',$n);
$nomlocal=trim(str_replace("[$LongName] ",'',$nomlocal));

$is_my_composite=simple_query("select id from ressources where id=$num and nature='composite' and id_membre=$idAuthor");
$q="select id from ressources where id=$num and nature='composite' and id_membre=$idAuthor";

file_put_contents("$repuser/nomlocal","$num *$nomlocal* *$is_my_composite* *$q*");

if ($is_my_composite=='')
    $s=simple_query('select texte from ressources where id='.$num); // lit dans la base de données
else // fichier normal de l'utilisateur dans son répertoire - lecture par priorité
    $s=file_get_contents("$repuser/$nomlocal.tree"); // chargement du tree

file_put_contents("$repuser/nomlocaltree",$s);

file_put_contents("$repuser/lastdoc$_GET[narbre]",$doc);// enregistrement du dernier document, pour rechargement 
echo arbor2json($s);
file_put_contents("$repuser/nomlocaljson",arbor2json($s));
file_put_contents("$repuser/nomcomplet",$doc);
    
?>
