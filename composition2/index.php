<?php    
// id_content pour charger une fenetre modale (pour l'ardoise de plan)
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ini_set("display_errors", 1);

require_once('timer.php');
require_once('../secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require_once('../config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once ('../philosophemes/ai.php');
require_once('philo-labo2.php'); // fonctions spécifiques à philo-labo
require_once('../cookbook/jquerychat.php');
//require_once('phplatex.php');
$idAuthor=simple_query("select id from membres where membre='$Author'");
$LongName=file_get_contents("/web/philo-labo/users/$Author/longname");

function create_if_necessary($dir) // create a directory if necessary
    {
    $action=(!file_exists($dir));
    if ($action) 
        mkdir($dir);
    return $action;
    }

$repuser="/web/philo-labo/users/$Author/compositeur";
create_if_necessary($repuser); // normalement inutile

if (!file_exists("$repuser/Test.tree"))
    file_put_contents("$repuser/Test.tree","Peuplez votre document en glisser-déposer\Et éditez-le pour le plan");

// Si le Catalogue n'existe pas 
// détruit les documents de l'utilisateur
// et les recrée
if (!file_exists("$repuser/Catalogue.tree")) // regarde si on rentre en v2
    {
    echo "Premier accès à la v2 - construction du catalogue<br/>";
    simple_query("delete from ressources where nature='composite' and id_membre=$idAuthor");
    unlink("$repuser/lastdoc1");
    unlink("$repuser/lastdoc2");
    $trees=glob("$repuser/*.tree"); // récupére tous les documents
    $liste='';
    foreach ($trees as $d) 
        {
        $nom="[$LongName] ".substr(str_replace("$repuser/",'',$d),0,-5); // nom du document
        $contenu=addslashes(file_get_contents($d));
        //echo "insert into ressources (ressource,id_membre,nature,texte) values ('$nom',$idAuthor,'composite',\"$contenu\")";
        simple_query("insert into ressources (ressource,id_membre,nature,texte) values ('$nom',$idAuthor,'composite',\"$contenu\")");
        // récupère l'$id de la ressource
        $id=simple_query("select id from ressources where ressource='$nom' and nature='composite' and id_membre=$idAuthor");
        // la met dans la liste
        //echo $id.' '.
        $liste.='[composite]@'.$id.' '.$nom."\n";
        }
    file_put_contents("$repuser/Catalogue.tree",$liste);
    simple_query("insert into ressources (ressource,id_membre,nature,texte) values ('[$LongName] Catalogue',$idAuthor,'composite',\"$liste\")");
    }
else // vérification que tous les documents tree sont dans le Catalogue quelque part
    {
    $cat=simple_query("select texte from ressources where ressource='[$LongName] Catalogue'");
    $liste=[];
    preg_replace_callback(
        "/\[$LongName\] (.*)/",
        function ($matches) // on met tous les tree dans le tableau liste
            {
            global $liste;
            return $liste[].="$matches[1]";
            }
        ,$cat); 
    //file_put_contents("$repuser/liste",serialize($liste));
    $tree=glob("$repuser/*tree");
    $not=[];
    foreach ($tree as $t) // pour chaque fichier tree
        {
        $t=str_replace("$repuser/",'',$t);
        $t=substr($t,0,-5);
        if (!in_array($t,$liste))
            {
            $not.=$t;
            }
        file_put_contents("$repuser/liste",serialize($not));
        }
    }
    
$Cat=simple_query("select id from ressources where id_membre=$idAuthor and ressource='[$LongName] Catalogue'");
//echo "Le catalogue porte l'id $Cat</br>";
$Cat="$Cat [$LongName] Catalogue"; // le nom du catalogue


// formulaire de recherche mais en ajax pour chargement dans l'arbre sans rechargement

if (isset($_POST['chaine']))
    file_put_contents("$repuser/search",$_POST['chaine']); // si la page est rechargée, la valeur sera rechargée
$recherche_precedente=file_get_contents("$repuser/search");

    // enregistre le résultats de la recherche en json
//if ($_GET['action']=='searchengine')
// file_put_contents('resultat.json',"[\n".str_replace("'","\'",resultatrecherche(true,true,true,$_GET['json']))."\n]");
if ($_POST['action']=='searchengine')
 file_put_contents("$repuser/resultat.json","[\n".str_replace(array("'",'&mdash;'),array("\'",'—'),resultatrecherche(true,true,true,$_POST['json']))."\n]"); // get json

require('icon_partage.php'); // recupére la valeur de l'icone de partage (partager ou ne pas partager selon l'état du document actuel

$lastdoc1=file_get_contents("$repuser/lastdoc1");
if (trim($lastdoc1)=='')
    {
    $lastdoc1=$Cat; // par défaut le calalogue
    file_put_contents('lastdoc1',$lastdoc1);
    }

$lastdoc2=file_get_contents("$repuser/lastdoc2");
if (trim($lastdoc2)=='')
    $lastdoc2=$Cat; // par défaut le catalogue

//echo "lastdoc1=$lastdoc1<br/>";
//echo "lastdoc2=$lastdoc2<br/>";

function def_button($onclick,$title,$text,$icon,$id='')
    {
    return "<div class='skinnytip' style='display:inline' data-options='borderColor:#99CCFF,backColor:#D5EAFF' onclick=\"$onclick\" data-title='$title'  data-text='$text'><img height=26 width=26 src='/images/menucompo/$icon.png'/></div>";
    }
    
// édition 
function back_button($n)
    { return def_button("back($n);",'Reculer','Ouvre le document précédent','back'); }
function home_button($n) 
    { global $Cat; return def_button("change_doc('$Cat',$n,1);",'Catalogue',"Ouvre le document <i>Catalogue</i> contenant tous vos documents.",'cat'); }
function forward_button($n) 
    { return def_button("forward(1);",'Avancer','Ouvre le document suivant','forward'); }
function vide_button() 
    { return '<img width=20 src="/images/menucompo/vide.png"/>'; }
function edit_button($n) 
    { return def_button("editer($n);",'Éditer','Ouvre le document pour le modifier à la main','edit'); }
function delete_button($n) 
    { return def_button("deletedoc($n);",'Supprime le document','Une confirmation va vous être demandée, mais attention...à manipuler avec précaution pour éviter les moments de solitude.','delete'); }
function creation_button($n) 
    { return def_button("create($n);",'Créer un nouveau document','Le document sera créé avec quelques lignes dedans que vous pourrez éditer','new'); }
function save_as_button($n) 
    { return def_button("save_as($n);",'Enregistrer sous','Enregistrer sous un autre nom, qui sera rechargé ici','copy'); }
function edition_buttons($n)
    { return back_button($n).home_button($n).forward_button($n).vide_button().creation_button($n).save_as_button($n).edit_button($n).delete_button($n); }

// exports
function pdfs_button($n) 
    { return def_button("export_avec_popup($n,'scanpdfs')","Affiche les pdfs","Affiche les pdfs correspondant aux composites de ce documents.",'pdfs'); }
function beamer_button($n) 
    { return def_button("export_sans_popup($n,'beamer')","Diaporama en pdf","La génération du diaporama peut prendre un peu de temps si les textes sont nouveaux.",'beamer'); }
function poly_button($n) 
    { return def_button("export_sans_popup($n,'poly_joli')","Extraire un polycopié","Extraire les textes (et les images en option par défaut) dans un polycopié en pdf. Les lignes sont les mêmes que dans le diaporama. Très pratique pour que les étudiants puissent s'y retrouver dans le poly quand le texte leur est projeté",'poly'); }
function outline_button($n) 
    { return def_button("export_sans_popup($n,'outline')","Extraire le plan de l'arbre","Exporte le plan de l\'arbre en pdf, avec juste le titre des ressources",'outline'); }
function brut_button($n) 
    { return def_button("export_avec_popup($n),'brut')","export brut","Exporte de façon à ce que vous puissiez le copier et le coller dans un autre document. Mais vous pouvez aussi utiliser le drag'n drop!",'brut'); }
function export_maths_e($n) 
    { return def_button("export_sans_popup($n,'maths_enonce');","Énoncé de mathématiques","Exporte un énoncé de mathématiques",'mathse');}
function export_maths_c($n) 
    { return def_button("export_sans_popup($n,'maths_correction');","Corrigé de mathématiques","Exporte un corrigé de mathématiques",'mathsc');}
function S5_button($n) 
    { return def_button("export_sans_popup($n,'S5');","Diaporama S5","<b>EN TRAVAUX</b> Produit un diaporama jouable dans le navigateur, avec les textes découpés si nécessaires en plusieurs diapos, gère les textes, les images et les videos youtube pour l'instant","S5");}
function html5_button($n) 
    { return def_button("export_sans_popup($n,'html5');","Export en html pour les traitements de textes","Produit un export en html5, que vous pouvez copier dans votre traitement de texte préféré, qui est, j'en suis sûr, Libreoffice",'html5'); }
function markdown_button($n) 
    { return def_button("export_avec_popup($n,'markdown_joli');","Export en markdown","Si vous ne connaissez pas le format markdown, vous devriez jeter un coup d\oeil au concept, et au passage, à pandoc, un couteau suisse surpuissant de conversion entre des formats de documents, écrit par John McFarlane, qui est professeur de philosophie à Berkeley",'markdown'); }
function export_buttons_simple($n)
    { return beamer_button($n).poly_button($n).brut_button($n); }
function export_buttons_all($n)
     { return export_buttons_simple($n).vide_button().outline_button($n).S5_button($n).html5_button($n).markdown_button($n).vide_button().export_maths_e($n).export_maths_c($n); }
    
function arbre($n)
    {
    return
    "<div class='entetecompo' style='min-height:5vh'>".edition_buttons($n).vide_button().export_buttons_all($n).
    "</div>
    <div style='float:left;height:24px' id=nomdoc$n>
    </div>  
    <ul id='doc$n' class='ztree arbre'></ul>";
    }
    
$sharing_button1='<div class="skinnytip" id="icon_partage1" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Partage/Cesse le partage" 
    data-text="Bascule. Permet de partager un document, ou cesser de le faire.">'.$icon_partage.'
</div>';
$sharing_button2='<div class="skinnytip" id="icon_partage2" style="display:inline;" data-options="borderColor:#99CCFF,backColor:#D5EAFF" 
    data-title="Partage/Cesse le partage" 
    data-text="Bascule. Permet de partager un document, ou cesser de le faire.">'.$icon_partage.'
</div>';
    
$evolution_button1='
    <div class="skinnytip" style="height:21px;vertical-align:middle;float:right;display:inline;margin-left:0px;margin-right:2px" data-options="borderColor:#99CCFF,backColor:#D5EAFF" onclick="popupf(\'https://philo-labo.fr/?n=Ian.Evolution?skin=minimal\')"
    data-title="Gestion des évolutions du compositeur"
    data-text="Demandes d\'évolutions.">
    <img height=21 alt="icone gabarits" src="/images/menucompo/gabaritwhite.png"/>
    </div>';
$sujet_form="<div class=\"skinnytip\" style=\"float:top;display:inline;margin-left:0px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"
                        data-title=\"Production d'un sujet\"
                        data-text=\"Deux numéros de questions et le numéro d'un texte, et cela vous produit le pdf\">
                        <form method=\"get\" target=\"_blank\" action=\"https://philo-labo.fr/composition/gabarit0.php\"><input name=\"s1\" type=\"text\" size=1><input name=\"s2\" type=\"text\" size=1><input name=\"s3\" type=\"text\" size=1><input type=\"submit\" value=\"sujet\"></form>
                </div>";

    
$menus_complets=preference('Interface avec toutes les icones');
$affnumress=preference('Affichage des numeros de ressources');

function dMenu($n)
    { return
"<div id='dMenu$n'>
	<ul>
		<li style=\"background-color:lightblue;\"><b>Opérations locales</b></li>
		<li id=\"m_add2\" onclick=\"addTreeNode('doc$n');\">Ajouter un noeud</li>
		<li id=\"m_del2\" onclick=\"removeTreeNode($n);\">Supprimer ce noeud</li>
		<li id=\"m_rename2\" onclick=\"editTreeNode('doc$n');\">Renommer ce noeud</li>
		<li id=\"m_lpdf\" onclick=\"traiter_le_document($n,'lpdf');\">Ce texte en pdf divisé en diapos</li>
		<li id=\"m_lpng\" onclick=\"traiter_le_document($n,'pdf');\">Ce texte en pdf</li>
		<li id=\"p_lpng\" onclick=\"traiter_le_document($n,'poly1');\">Cette ressource en pdf pleine page</li>
		<li id=\"p_lpng\" onclick=\"export_sans_popup($n,'poly1');\">Tout l'arbre en pdf pleine page</li>
		<li id=\"p_lpng\" onclick=\"traiter_le_document($n,'polyA5');\">Cette ressource en A5</li>
		<li id=\"p_lpng\" onclick=\"export_sans_popup($n,'polyA5');\">Tout l'arbre en A5</li>
		<li style=\"background-color:lightblue;\"><b>Opérations globales</b></li>
		<li id=\"expandAll\" onclick=\"zTree$n.expandAll('doc$n');\">Développer l'arbre</li>
		<li id=\"collapseAll\" onclick=\"zTree$n.expandAll('doc$n');\">Envelopper l'arbre</li>
		<li id=\"m_edit\" onclick=\"editer($n);\">Editer ce document (expérimental)</li>
		<li style=\"background-color:lightblue;\"><b>Exportation de l\"arbre</b></li>
		<li id=\"m_arbreedit\" onclick=\"export_avec_popup($n,'arbre éditable');\">arbre éditable</li>
		<li id=\"m_outline\" onclick=\"export_sans_popup($n,'outline_html');\">plan en html</li>
		<li id=\"m_brut\" onclick=\"export_avec_popup($n,'brut');\">brut</li>
		<li id=\"m_markdown\" onclick=\"export_avec_popup($n,'markdown_joli');\">Markdown joli</li>
		<li id=\"m_html\" onclick=\"export_avec_popup($n,'html');\">html</li>
		<li id=\"m_docx\" onclick=\"export_sans_popup($n,'docx');\">export en docx</li>
		<li id=\"p_synchro\" onclick=\"export_sans_popup($n,'poly1');\">poly1</li>
        <li id=\"p_synchro\" onclick=\"export_sans_popup($n,'polynew');\">polynew</li>
		<li id=\"p_synchro\" onclick=\"export_sans_popup($n,'poly_joli');\">poly_joli</li>
		<li id=\"m_pmwiki\" onclick=\"export_avec_popup($n,'pmwiki');\">PmWiki à copier dans une nouvelle page</li>
		<li id=\"m_s5\" onclick=\"export_avec_popup($n,'S5');\">S5 rustique</li>
		<li id=\"m_dys\" onclick=\"export_sans_popup($n,'poly_dys');\">Poly dys</li>
		<li id=\"m_poly_joli\" onclick=\"export_sans_popup($n,'poly_joli');\">Poly joli</li>
		<li id=\"m_beamer\" onclick=\"export_sans_popup($n,'beamer');\">Beamer [obsolète] (veiller patienter)</li>
        <li id=\"m_poly1\" onclick=\"export_sans_popup($n,'poly1');\">Poly (après Beamer) normal</li>
		<li id=\"m_poly2\" onclick=\"export_sans_popup($n,'poly2');\">Poly (après Beamer) 2 col</li>
		<li id=\"m_polyA5\" onclick=\"export_sans_popup($n,'polyA5');\">Poly paysage A4 - 2xA5</li>
		<li id=\"m_synchro\" onclick=\"export_sans_popup($n,'synchro2');\">Poly synchro 2col</li>
                <li id=\"m_pdf\" onclick=\"export_sans_popup($n,'pdf');\">pdf direct</li>
	</ul>
</div>";
}

?>
<!DOCTYPE html>
<HTML lang="fr">
<HEAD>
	<TITLE>Le compositeur de philo-labo</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="/ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<link rel="stylesheet" href="/js/jqmodal/jqModal.css" type="text/css">
	<link rel="stylesheet" href="/css/textes.css" type="text/css" />
	<link rel="stylesheet" href="/css/compositeur.css" type="text/css">
	
<!-- pour les popup en alink -->
<?php

echo "
</script>
<script src='/js/jquery-1.6.4.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
<script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    width:930px;border-width:3px;border-radius: 10px;
    border:3px solid black;
    }
 .ui-dialog-shadow { 
/*-webkit-box-shadow: 6px 6px 6px 0 #000000;
box-shadow: 6px 6px 6px 0 #000000;*/
 }
  .ui-dialog-titlebar {
  height:128;
  background:white;
  border:0;
  }
.ui-dialog-titlebar-close { 
  background: url('/js/images/ui-icons_888888_256x240.png') repeat scroll -93px -128px rgba(1, 2, 0, 0);
  //background: url('/images/close.png') repeat scroll -100px -200px rgba(1, 2, 0, 0);
}
  </style>
	<script>
	jq132(document).ready(function() {
               jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div>Patientez un instant...</div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('alt'),
					width: 930,
					height:600
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href')+'');
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";
?>

<?php echo $HTMLHeaderFmt['chat'];?>
	<!--<script src="/ztree/js/jquery-1.4.4.min.js"></script>-->
	<script src="/js/jquery-1.8.0.min.js"></script>
        <!--<script src="/js/jqmodal/jqModal.js"></script>-->
		<!--<script src="/js/jqmodal/jqDnRfe.js"></script>-->
	<script src="/ztree/js/jquery.ztree.core.js"></script>
	<script src="/ztree/js/jquery.ztree.excheck.js"></script>
	<script src="/ztree/js/jquery.ztree.exedit.js"></script>
	<!--<script src="dragdroptouch.min.js"></script>-->
	<script src="/js/jquery.tree-multiselect.min.js"></script>
	
<?php if (!preference('Ne pas afficher les informations au survol')) echo '<script src="/js/skinnytip.js"></script>';?>
	        
    <link rel="stylesheet" href="/css/jquery.tree-multiselect.min.css"> 
	<script>
        function gabarits(){
            popupf("gabarits.php");
            }
        function functionOK(){
                alert('Utilisez le bouton OK pour soumettre votre requête au moteur de recherche du compositeur');
                }
        function formrecherchesubmit(){
                 $.post('resultats_recherche.php', // Un script PHP que l'on va créer juste après
                    {
                    chaine : $('#chaine').val(),  // Nous récupérons la valeur de nos inputs que l'on fait passer à connexion.php
                    filtre : $('#test-select').val()
                    },
                    function(data){ // Cette fonction ne fait rien encore, nous la mettrons à jour plus tard
                        if (data == 'Success'){
                                $.fn.zTree.init($("#doc0"), settingRecherche);
                            }
                        },
                        'text' // Nous souhaitons recevoir "Success" ou "Failed", donc on indique text !
                        );
                    }
        $('#submit_recherche').click(function(){ // fonction de recherche
                formrecherchesubmit();
                });
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds){
                break;
                    }
                }
              }
        function deletedoc(narbre){
              var doc = document.getElementById('nomdoc'+narbre).innerHTML;
              if (doc=='Catalogue')
                {
                alert('Vous ne pouvez pas supprimer votre catalogue!');
                exit();
                }
              var username = document.getElementById('Author').innerHTML;
              if (confirm('Confirmez-vous la destruction de '+doc+' ?')) {
                 xnotify({body:'Effacement du fichier '+doc});
                 $.get("delete.php?doc="+doc,
                  {doc:doc,}, 
                  function(data,status)
                    {
                    xnotify({body:data,duration:1});
                    });
                 $('#firstdoc').load('firstdoc.php'); // récupère le premier document
                 alert(doc+' a été supprimé');
                 change_doc(Cat,narbre,1);
                 var doc = document.getElementById('nomdoc'+(3-narbre)).innerHTML; // mise à jour du catalogue
                 if (doc=='Catalogue')
                    change_doc(Cat,(3-narbre),0); // l'autre arbre
                 }
              };
        $("#create1").click(function(){
                create(1);
                });
            function create(narbre){
                var doc=prompt('Nom du fichier à créer:','');
                if ((doc == '') || (doc.includes('"')) || (doc.includes('/')) || (doc.charAt(0)==' '))
                    {
                    alert('Nom incorrect, vide ou contenant /," ou commençant par un un espace.');
                    //xnotify({body:'Nom incorrect, vide ou contenant "'});
                    exit();
                    }
                if (doc.includes("'"))
                    alert("Votre nom de document contient le signe ', il sera remplacé par lepar le signe ’");
                doc=doc.replace("'","’"); // guillements exotiques bof bof
                var str='{id:"doc_1",pId:"null",name:"Peuplez votre document en glisser-déposer",icon:"/images/iconp/vide.png",level:"0"},{id:"doc_2",pId:"null",name:"Éditez-le pour en indiquer le plan",icon:"/images/iconp/edit.png",level:"0"},{id:"doc_3",pId:"null",name:"et supprimer ces trois lignes",icon:"/images/iconp/vide.png",level:"0"}';
                var tempResult= [];
                tempResult.push(str);
                doc='['+LongName+'] '+doc;
                $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                narbre:narbre,
                from:'create',
                }, 
                function(data,status)
                    {
                    xnotify({body:'Création du document <b>'+doc+'</b> s\'il n\'existe pas déjà.',duration:1});
                    });
                sleep(1000);
                //alert('dans create '+doc);
                change_doc(doc,narbre,1);
                //alert('après change');
                var doc = document.getElementById('nomdoc'+(3-narbre)).innerHTML; // mise à jour du catalogue
                if (doc=='Catalogue')
                    change_doc(doc,3-narbre,0);
                };
        function save_as(narbre) {
	      var doc = prompt('Enregistrer le document courant sous:','');
	      doc=doc.replace("'","’");
               if ((doc == '') || (doc.includes('"')))
                {
                alert('Nom incorrect, vide ou contenant "');
                exit();
                }
              if ($.fn.zTree.getZTreeObj('doc'+narbre).getNodeByTId('doc_1') == null)
                {
                xnotify({body:'Création impossible: arbre vide'});
                exit();
                }
              var zTree = $.fn.zTree.getZTreeObj('doc'+narbre);
              nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray (nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
              doc='['+LongName+'] '+doc;
              $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                narbre:narbre,
                from:'save_as',
                }, 
                function(data,status)
                {
                xnotify({body:'Le fichier '+doc+' a été créé',duration:1});
                change_doc(doc,narbre,1); // rechargement
                });
            };
           function maj_doc(narbre){ 
              var doc = document.getElementById('nomdoc'+narbre).innerHTML;
              alert(doc);
              var zTree = $.fn.zTree.getZTreeObj('doc'+narbre);
              var nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray(nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 { 
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
              $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                narbre:narbre,
                },           
                function(data,status){
                xnotify({body:'sauvegarde du document '+doc,duration:1});
              });
              // $.fn.zTree.init($("#doc"), settingDocument,zvide); 
            };
        var settingRecherche = {
			async: {     
				enable: true,
				type:"get",		
				url:"read_json.php?json=resultatsrecherche.json",
				autoParam:["id", "name=n", "level=lv"],
				otherParam:{"otherParam":"zTreeAsyncTest"},
				dataFilter: filter
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false,
				drag: 
                    { 
                    isMove:true // seulement des copies, pas de déplacement (euh non en fait!)
                    }
			},
			data: {
				simpleData: {
				enable: true
				}
			},
			callback: {
				beforeDrag: beforeDrag,
				beforeDrop: beforeDrop,
				onDrop: onDrop,
				onClick: myOnClick,
				onRightClick: OnRightClick,
			},
            view: {
                dblClickExpand: false,
                nameIsHTML:true,
                showLine:false,
                showTitle:false,
                },
		};
        var settingDocument = {
                async: {
                        enable: true, 
                        type:"get",             
                        url:"read_doc.php?arbre=&doc="+'<?php echo $_GET['lastdoc'];?>',    
                        autoParam:["id", "name=n", "level=lv"],
                        otherParam:{"otherParam":"zTreeAsyncTest"},
                        dataFilter: filter
                },
                edit: {
                        enable: true,
                        showRemoveBtn: false,
                        showRenameBtn: false
                },
                data: {
                        simpleData: {
                        enable: true,
                        drag: 
                            {
                            isMove:false
                            }
                        }
                },
                callback: {
                        beforeDrag: beforeDrag,
                        beforeDrop: beforeDrop,
                        onClick: myOnClick,
                        onDrop: onDrop,
                        onRightClick: OnRightClick,
                },
                view: {
                        dblClickExpand: false,
                        nameIsHTML:true,
                        showLine:false,
                        showTitle:false,
                },
        };
		function filter(treeId, parentNode, childNodes) {
			if (!childNodes) return null;
			for (var i=0, l=childNodes.length; i<l; i++) {
				childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		                childNodes[i].open = true; // pour expand par defaut
			}
			return childNodes;
		}
        function onDrop(event,arbre,treeNodes,x,y,z) // reprise du code de la function maj_doc
            { 
             var narbre=arbre.substring(3,4);
             if (narbre==0)
                {
                //alert('sortie');
                exit();
                }
            var doc = document.getElementById('nomdoc'+narbre).innerHTML; // récupération du nom de document
            //alert(doc);
              var zTree = $.fn.zTree.getZTreeObj(arbre);
              var nodes = zTree.getNodes();
              var nodes_array = zTree.transformToArray(nodes);
              var tempResult = [];
              var str = "";
              for(var i=0;i<nodes_array.length;i++)
                 {
                 a=nodes_array[i];
                 str = '{id:"'+a.tId+'",pId:"'+a.parentTId+'",name:"'+a.name.replace(/\'/g,"\\'")+'",icon:"'+a.icon+'",level:"'+a.level+'"}';
                 tempResult.push(str);
                 }
              $.post("save_doc.php",
                {
                arbre:tempResult,
                doc:doc,
                narbre:narbre,
                },           
                function(data,status){
                xnotify({body:'Mise à jour du document <b> '+doc+'</b>',duration:1});
              });
              //alert(tempResult);
              // si l'autre fenêtre contient le même document il est mis à jour
              var doc2 = document.getElementById('nomdoc'+(3-narbre)).innerHTML; // mise à jour du catalogue dangereux!!! copie le moteur de recherche parfois
              //alert(doc+' '+doc2);
              if (doc==doc2)
                {
                alert('La fenêtre '+(3-narbre)+' va être mise à jour');
                change_doc(doc2,3-narbre,0);
                }
            }
        function strip_tags(s)
            {
            return s.replace(/(<([^>]+)>)/ig,'');
            }
		function isNumeric(n) {
 			 return (!isNaN(parseFloat(n)) && isFinite(n));
		}
        function myOnClick(event, treeId, treeNode) { // affichage d'une resources dans une fenêtre modale centrale, que l'on peut refermer avec ESC
        	var num=name2n(treeNode.name);
        	var visu=name2visu(treeNode.name);
        	var narbre=treeId.substring(3,4);
        	//alert('myonclick --> num='+num+' visu='+visu+' narbre='+narbre);
			if (treeNode.icon=='/images/iconp/composite.png') // si c'est un objet composite
                {
                //alert(n[0]);
                if (narbre==0) // colonne de recherche -> dans doc2 
                    change_doc(visu,2,1); // affichage dans la colonne de droite pour un composite
                else
                    {
                    //alert('avant');
                    change_doc(visu,narbre,1); // affichage dans la même colonne (cas des colonnes du milieu et de droite)
                    //alert('après');
                    }
                }
            else
                {
                if (isNumeric(num))
                    { 
                    var url="ressif.php?id="+num+' '+visu; // treeNode.name (le id qui commence le name sera extrait au début de ressif.php
                    popupf(url);
                    }
                else
                    alert('ce n\'est pas une ressource mais un élement de ressource');
                }
        }
		function beforeDrag(treeId, treeNodes) {
			for (var i=0,l=treeNodes.length; i<l; i++) {
				if (treeNodes[i].drag === false) {
					return false;
				}
			}
			return true;
		}
		function beforeDrop(treeId, treeNodes, targetNode, moveType) {
			return targetNode ? targetNode.drop !== false : true;
		}
		function OnRightClick(event, treeId, treeNode) {
                        var zTree = $.fn.zTree.getZTreeObj(treeId);
                        if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
                                //zTree.cancelSelectedNode();
                                showRMenu(treeId,"root", event.clientX, event.clientY);
                        } else if (treeNode && !treeNode.noR) {
                                zTree.selectNode(treeNode);
                                showRMenu(treeId,"node", event.clientX, event.clientY);
                        }
		}
				function showRMenu(treeId,type, x, y) {
                        if (treeId=='doc0') 
                          {
                          $("#rMenu ul").show();
                          if (type=="root") {
                                  $("#m_del").hide();
                                  $("#m_edit").hide();
                          } else {
                                  $("#m_del").show();
                                  $("#m_edit").show();
                          }
                          rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
                          }
                        if (treeId=='doc1') {
                          $("#dMenu1 ul").show();
                          if (type=="root") {
                                  $("#m_del").hide();
                                  $("#m_edit").hide();
                          } else {
                                  $("#m_del").show();
                                  $("#m_edit").show();
                          }
                          dMenu1.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
                        }
                        if (treeId=='doc2') {
                          $("#dMenu2 ul").show();
                          if (type=="root") {
                                  $("#m_del").hide();
                                  $("#m_edit").hide();
                          } else {
                                  $("#m_del").show();
                                  $("#m_edit").show();
                          }
                          dMenu2.css({"top":y+"px", "left":x+"px", "visibility":"visible"});
                        }
			$("body").bind("mousedown", onBodyMouseDown);
		}
		function hideRMenu() {
			if (rMenu) rMenu.css({"visibility": "hidden"});
			if (dMenu1) dMenu1.css({"visibility": "hidden"});
			if (dMenu2) dMenu2.css({"visibility": "hidden"});
			$("body").unbind("mousedown", onBodyMouseDown);
		}
		function onBodyMouseDown(event){
			if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
				rMenu.css({"visibility" : "hidden"});
			}
			if (!(event.target.id == "dMenu1" || $(event.target).parents("#dMenu1").length>0)) {
				dMenu1.css({"visibility" : "hidden"});
			}
			if (!(event.target.id == "dMenu2" || $(event.target).parents("#dMenu2").length>0)) {
				dMenu2.css({"visibility" : "hidden"});
			}
		}
		var addCount = 1;
		function addTreeNode(treeId) {
			hideRMenu();
			var zTree = $.fn.zTree.getZTreeObj(treeId);
			var newNode = { name:"Élément " + (addCount++)};
			if (zTree.getSelectedNodes()[0]) {
				//newNode.checked = zTree.getSelectedNodes()[0].checked;
				zTree.addNodes(zTree.getSelectedNodes()[0], newNode);
			} else {
				zTree.addNodes(null, newNode);
			}
		}
// 		function resetTree() {
// 			hideRMenu();
// 			$.fn.zTree.init($("#panier"), setting, zNodes);
// 		}
		function editTreeNode(treeId) {
			var zTree = $.fn.zTree.getZTreeObj(treeId),
			nodes = zTree.getSelectedNodes(),
			treeNode = nodes[0];
			hideRMenu();
			if (nodes.length == 0) {
				alert("Sélectionnez d'abord un élément...");
				return;
			}
			zTree.editName(treeNode);
		};
		function removeTreeNode(treeId) {
			hideRMenu();
			maj_doc(treeId);
			var zTree = $.fn.zTree.getZTreeObj(treeId)
			var nodes = zTree.getSelectedNodes();
			if (nodes && nodes.length>0) 
                {
				if (nodes[0].children && nodes[0].children.length > 0) 
                    {
					var msg = "Si vous détruisez ce noeud, vous détruirez aussi tout ce qui lui est subordonné. \n\nMerci de confirmer";
					if (confirm(msg)==true){
						zTree.removeNode(nodes[0]);
					}
				} 
            else 
                {
                zTree.removeNode(nodes[0]);
				}
			}
		}
		function voir_pile(narbre)
            {
            var n=parseInt(narbre);
            alert('pile '+narbre+' '+pile[n]+' '+pos_pile[n]);
            } 
        function back(narbre)
            {
            var n=parseInt(narbre);
            //voir_pile(n);
            if (pos_pile[n]>0)
                {
                pos_pile[n]--;
                change_doc(pile[n][pos_pile[n]],n,0);
                }
            else
                xnotify({body:'Rien avant',duration:1});
            }
        function forward(narbre)
            {
            var n=parseInt(narbre);
            //voir_pile(n);
            if (pos_pile[n]<pile[n].length-1)
                {
                pos_pile[n]++;
                change_doc(pile[n][pos_pile[n]],n,0);
                }
            else
                xnotify({body:'Rien après',duration:1});
            }
        function name2n(name){ // à partir d'un nom de ressource contenant le numéro (visible ou pas), rend le numéro de la ressource
            // il y a deux formes
            // numero nom (si affichage des numéro est actif)
            // nom @nom (si affichage des numéro est inactif)
            name=strip_tags(name); //vire les tags HTML 
        	var n = name.split(' ');
        	if (affnumress==1) // initialisée au début 
                var num=n[0]; 
            else
                {
                var num=n[n.length-1];
                num=num.substring(1);
                }
            return parseInt(num); // retourne le numéro de la ressource
            }
        function name2visu(name){ // à partir d'un nom de ressource, renvoie le nom qui devra s'afficher
            return strip_tags(name); // pour l'instant
            }
        function change_doc(doc,narbre,empile) // charge le document doc dans la colonne du milieu (settingChargement pour doc)
                 {
                 num=name2n(doc);
                 name=name2visu(doc); // pour affichage
                 if (empile==1)
                    {
                    var n=parseInt(narbre);
                    pile[n].push(strip_tags(doc)); // seul endroit où l'on écrit dans le tableau de pile
                    pos_pile[n]++;
                    }
                document.getElementById('nomdoc'+narbre).innerHTML=name;
                //alert('changement de document '+doc+'dans '+narbre);
                 var settingChargement = {
                        async: {
                                enable: true,
                                type:"get",             
                                url:"read_doc.php?doc="+encodeURIComponent(doc),  // le script php produit un json à manger par le javascript
                                autoParam:["id", "name=n", "level=lv"],
                                otherParam:{"otherParam":"zTreeAsyncTest","narbre":narbre},
                                dataFilter: filter
                        },
                        edit: { 
                                enable: true,
                                showRemoveBtn: false,
                                showRenameBtn: false,
                                drag: 
                                    { 
                                    isMove:true // seulement des copies, pas de déplacement (euh non en fait)
                                    }
                        },
                        data: {
                                simpleData: {
                                enable: true
                                }
                        },
                        callback: {
                                beforeDrag: beforeDrag,
                                beforeDrop: beforeDrop,
                                onClick: myOnClick,
                                onDrop: onDrop,
                                onRightClick: OnRightClick,
                        },
                        view:{
                             nameIsHTML:true,
                             showLine:false,
                             showTitle:false,
                             }
                    };
                //if (arbre='#partage')
                //    settingChargement.callback.onDrop=null;
                $.fn.zTree.destroy('doc'+narbre); // on le détruit ('doc') le substr enlève le #
                $.fn.zTree.init($('#doc'+narbre), settingChargement); 
                //sleep(1000);
                $("#icon_partage").load('icon_partage.php?doc='+doc+'&appelajax=ok');
                }
		function traiter_le_document(narbre,format) {
                    hideRMenu();
                    var zTree = $.fn.zTree.getZTreeObj('#doc'+narbre);
                    var nodes = zTree.getSelectedNodes();
                    traite_document(nodes[0].name,format);
            }
        async function partager(narbre) {
		  var LongName = document.getElementById("LongName").innerHTML;
		  //var LongName=LongName.replace(' ','\ ');
		  var user = document.getElementById("Author").innerHTML;
		  //alert(LongName);
		  var doc = document.getElementById('nomdoc'+narbre).innerHTML; // récupère le nom du doc
		  alert(doc);
		  if (confirm('Confirmez-vous le partage du plan '+doc+' ?')) {
                    xnotify({body:'Partage du plan '+doc});
                    $('#content1').load("partager.php?LongName="+encodeURIComponent(LongName)+"&ok=ok&doc="+encodeURIComponent(doc)+"&user="+username,function() // callback 
                        {
                        sleep(1000);
                        document.getElementById('icon_partage').innerHTML="<div style='display:inline;margin-right:5px' onclick=\"cesserdepartager();\" title='Cesser de partager le document'><div id='sharing' style='display:inline'><img height=21 alt='icone ne pas partager' src='/images/menucompo/noshare.png'/></div></div>";
                        });
                    }
                 }
        async function cesserdepartager(narbre) {
		  var LongName = document.getElementById("LongName").value;
		  var user = document.getElementById("Author").value;
		  var doc = document.getElementById('nomdoc'+narbre).innerHTML; // récupère le nom du doc
		  if (confirm('Confirmez-vous que vous cessez de partager le plan '+doc+' ?')) {
                    xnotify({body:'Arrêt du partage du plan '+doc});
                    $('#content1').load("partager.php?LongName="+encodeURIComponent(LongName)+"&ok=nok&doc="+encodeURIComponent(doc)+"&user="+username,function() // callback
                        {
                        sleep(1000);
                        document.getElementById('icon_partage').innerHTML="<div style='display:inline;margin-right:5px' onclick=\"partager();\" title='Partager le document'><div id='sharing' style='display:inline'><img height=21 alt='icone partager' src='/images/menucompo/share.png'/></div></div>";
                        });
                    }
                 }
// 		function add_ressource(type) {// Ajouter une ressource (fonction générique, on passe le type à la fonction) 
// 		  xnotify({body:"Ajouter une ressource de type "+type,duration:1});
//                   popupf("/Ai/Ajouter"+type+"?skin=minimal"); // pour affichage dans une boite modale centrale
// 		}
		function export_avec_popup(narbre,format) { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
                doc = document.getElementById('nomdoc'+narbre).innerHTML; // récupère le nom du doc
                xnotify({body:format+" "+doc,duration:1});
                popup("version_echo.php?format="+format+"&doc="+encodeURI(doc));
            }
		function export_sans_popup(narbre,format) { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
                doc = document.getElementById('nomdoc'+narbre).innerHTML; // récupère le nom du doc
                xnotify({body:format+" "+doc+' en préparation',duration:1});
                document.getElementById('contentf').src="version_echo.php?format="+format+"&doc="+encodeURI(doc); // pour exécution
            }
		function traite_document(name,format) { // action sur un élément dont e nom (contenant le numéro est donné
		  xnotify({body:format+" "+name,duration:1});
                  document.getElementById('contentf').src="version_echo.php?solo=oui&format="+format+"&doc="+encodeURI(name.replace("'",'')); // pour exécution
                  //popupf("version_echo.php?solo=oui&format="+format+"&doc="+encodeURI(name)); // pour exécution
		}
		function editer(narbre) { // production du document en divers format - affichage dans une fenêtre modale, sauf pour beamer
            doc = document.getElementById('nomdoc'+narbre).innerHTML; // récupère le nom du doc
            //alert("éditer "+doc+"dans "+narbre);
            document.getElementById('contentf').src="edit_doc.php?doc="+doc; // pour affichage dans une boite modale centrale 
            // une ruse pour éviter d'utiliser les promesses - c'est la fenêtre modale qui met à jour l'arbre
            settingDocument.async.url="read_doc.php?doc="+doc;  // chargement du document
            $('#dialogf').jqm({onHide: function(hash) 
                { hash.w.hide() && hash.o && hash.o.remove(); 
                  $.fn.zTree.init($("#doc"+narbre),settingDocument,zvide);
                  var doc2 = document.getElementById('nomdoc'+(3-narbre)).innerHTML; // mise à jour du catalogue
                  //alert(doc+'<-->'+doc2);
                  if (doc==doc2)
                    {
                    alert('La fenêtre '+(3-narbre)+' va être mise à jour');
                    change_doc(doc2,3-narbre,0);
                    }
                }, closeOnEsc: true,overlay:50 ,modal:true,}).jqmShow({overlay: 70});
            }
		var zvide=[];
		$(document).ready(function(){ // initialisations
            affnumress=document.getElementById('affnumress').innerHTML; // pour gérer l'affichage des numéros de ressources en javascript dans myonclick et ondrop
            LongName=document.getElementById('LongName').innerHTML;
            Cat=document.getElementById('Cat').innerHTML;
            pile=[]; // les piles de documents pour gérer la navigation
            pos_pile=[];
            pile[1]=[];
            pos_pile[1]=-1;
            pile[2]=[];
            pos_pile[2]=-1;
            SkinnyTip.init();
			$.fn.zTree.init($("#doc0"), settingRecherche);
			//$.fn.zTree.init($("#doc"), settingDocument,zvide); 
			change_doc('<?php echo $lastdoc1;?>','1',1); // initialisation avec le dernier document utilisé dans la fenêtre du milieu
			change_doc('<?php echo $lastdoc2;?>','2',1); // initialisation avec le dernier document utilisé dans la fenêtre de droite
			rMenu = $("#rMenu");
			zTree1 = $.fn.zTree.getZTreeObj("doc1"); // utile ??
			zTree2 = $.fn.zTree.getZTreeObj("doc2");
			dMenu1 = $("#dMenu1");
			dMenu2 = $("#dMenu2");
		});
</script>
<style>
div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
div#rMenu ul li{
	margin: 1px 0;
	padding: 0 5px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #DFDFDF;
}
div#dMenu1 {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
div#dMenu1 ul li{
	margin: 1px 0;
	padding: 0 5px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #DFDFDF;
}
div#dMenu2 {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
div#dMenu2 ul li{
	margin: 1px 0;
	padding: 0 5px;
	cursor: pointer;
	list-style: none outside none;
	background-color: #DFDFDF;
}
</style>
</HEAD>
<BODY> 
<div style="transform:rotate(-90deg);transform-origin:left;opacity:0.2;position:absolute;bottom:10px;left:30vw;z-index:1000;font-size:400%;" ><b>recherche</b></div>
<div style="transform:rotate(-90deg);transform-origin:left;opacity:0.2;position:absolute;bottom:10px;left:63vw;z-index:1000;font-size:400%;" ><b>document</b></div>
<div style="transform:rotate(-90deg);transform-origin:left;opacity:0.2;position:absolute;bottom:10px;left:96vw;z-index:1000;font-size:400%;" ><b>document</b></div>
<div id="test"></div>
<div id='export'></div>
<!--<button id="testai">Testai</button><a href="#" class="jqModal">view</a>-->

<!--<div align=center style="left:0px;top:0px"><iframe id='iframeid' style="width:400px;height:640px;margin:5px" ></iframe></div>-->

<?php 
// chargement du code javascript pour les popup et popupf
echo code_pour_popup(); 
?>

<?php

$information_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;margin-right:1px;float:right;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/Site/InfoCompositeur?skin=iansclean');\"
                    data-title=\"Informations\"
                    data-text=\"Quelques informations sur le compositeur. Crédits, etc.\">
                    <img height=21 alt=\"icone export brut\" src=\"/images/menucompo/info.png\"/>
                </div>";
$quit_button="<div class=\"skinnytip\" style=\"float:right;margin-top:0px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" 
                    data-title=\"Déconnexion\"
                    data-text=\"Il est prudent de se déconnecter pour éviter que quelqu'un passant derrière vous usurpe votre identité ici.\">
                    <a href=\"https://philo-labo.fr?action=logout\"><img style=\"height:21px;float:right;margin-right:5px;display:inline;\" src=\"/images/logout.png\"></a>
                </div>";
$preferences_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;float:left;display:inline;margin-left:0px;margin-right:2px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popup('preferences.php');\"
                            data-title=\"Préférences\"
                            data-text=\"Vos préférences\">
                            <img height=21 src='/images/menucompo/prefwhite.png'>
                    </div>";
$toddo_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;float:left;display:inline;margin-left:0px;margin-right:2px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/outils/kanban2/');\"
                            data-title=\"Toddo\"
                            data-text=\"Ouvre votre gestionnaire de tâches.\">
                            <img height=21 src='/images/menucompo/todo.png'>
                    </div>";
$help_button="<div class=\"skinnytip\" style=\"height:21px;vertical-align:middle;float:right;display:inline;margin-left:0px;margin-right:2px\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/Public/AideDuCompositeur?skin=minimal');\"
                            data-title=\"Aide\"
                            data-text=\"Affiche l'aide du compositeur.\">
                            <img height=21 src='/images/menucompo/bouee32.png'>
                    </div>";
$timer_button="<div class=\"skinnytip\" style=\"margin-right:3px;float:right;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"
           data-title=\"Un timer pour la correction des copies\"
           data-text=\"Avec moyenne, pause etc...\">
           <img onclick=\"popupf('/outils/timer/timer.html');\" width=20 src='/images/menucompo/timerwhite.png'>
        </div>";
$goodies="<div class=\"skinnytip\" style=\"float:right;margin-top:1px;margin-bottom:5px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/outils/kanban/eisen.php?doc=compositeur');\"
            data-title=\"Eisenhower\"
            data-text=\"Une matrice d'Eisenhower pour organiser son travail sur le compositeur.\">
            <b>&nbsp;&nbsp;E&nbsp;&nbsp;</b>
        </div>
         <div class=\"skinnytip\" style=\"float:right;margin-top:1px;margin-bottom:5px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/outils/kanban/kanban.php?doc=compositeur');\"
            data-title=\"Kanban\"
            data-text=\"Un kanban pour organiser son travail sur le compositeur.\">
            <b>&nbsp;&nbsp;K</b>
        </div>
        <div class=\"skinnytip\" style=\"float:right;margin-top:1px;margin-bottom:5px;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\" onclick=\"popupf('/outils/kanban2');\"
            data-title=\"Kanban2\"
            data-text=\"Le kanban v2 pour organiser son travail sur le compositeur.\">
            <b>&nbsp;&nbsp;K2</b>
        </div>";
$wiki_button="<div class=\"skinnytip\" style=\"margin:-1px;padding:0px;float:left;\" data-options=\"borderColor:#99CCFF,backColor:#D5EAFF\"
           data-title=\"Retourner à la page d'accueil de philo-labo\"
           data-text=\"En particulier vers le wiki, les e-books, etc.\">
           <a href=\"/\"><img width=18 src=\"/images/menucompo/home.png\"/></a>&nbsp;
           </div>";
$cc_button="<img onclick=\"popup('/composition/licence.html');\" style=\"height:21px;vertical-align:middle;float:right;display:inline\" src=\"/images/cc-by-nc-sa-p.png\"/>";
           
?>  
<div class="compositeur" style="border:0px dotted black;">
<table style="width:100%">
 <tr>
  <td style="width:33%;border:0px dotted black">
   <div class="zone">
    <div class="titre_zone"><?php echo "$wiki_button$timer_button";?><?php if ($menus_complets) echo $goodies;?><center>recherche</center>
        
    </div>
    <div class="entetecompo">
      <div style="margin:0px;text-align:left;background-color:lightblue"><!--Pensez à faire une MAJ de votre document avant...<br/>S'il n'y a pas de résultats, c'est qu'il y en a trop. Affinez la recherche.-->
        <form style='float:left;background-color:lightblue;display:inline-block;width:100%' style='margin:0px' id="formrecherche" onKeyPress="if (event.keyCode == 13){event.preventDefault();formrecherchesubmit()}">
            <input type="hidden" name="action" value="searchengine">
            <input type="hidden" name="json" value="true">
            <?php echo $addressources;?>
            <center><input name="chaine" id="chaine" value="<?php echo $recherche_precedente; ?>" style="width:85%"></center>
            <!--<input type="button" id="submit_recherche" style="display:inline" name="submit" value="OK"/>-->
          <?php echo $nature_select; ?>
        </form>
        <script>
        $("#test-select").treeMultiselect({ enableSelectAll: false, sortable: true, hideSidePanel:true, startCollapsed:true  });
        </script>
      </div>
    <ul id="doc0" class="ztree arbre"></ul>
   </div>
  </td>

   <td style="width:33%;border:0px dotted black">
        <div class="zone">
                <div class="titre_zone"><?php echo "[$Author] $preferences_button $toddo_button";?>
                    <div style="float:right;margin-right:5px;color:white"><?php echo chats();?>
                    </div>
                </div>
                <?php echo arbre(1); ?>
        </div>
  </td> 
  <td style="width:33%;border:0px dotted black">
        <div class="zone">
            <div class="titre_zone">documents<?php echo "$quit_button$cc_button$information_button$evolution_button$help_button";?>
            </div>
            <?php echo arbre(2); ?>
        </div>
  </td>
 </tr>
</table>
</div>

<div id="rMenu">
	<ul>
		<li style="background-color:lightblue;"><b>Opérations locales</b></li>
		<!--<li id="m_add2" onclick="addTreeNode('doc');">Ajouter un noeud</li>-->
		<li id="m_lpdf" onclick="traiter_le_document('0','lpdf');">Ce texte en pdf divisé en diapos</li>
		<li id="m_lpng" onclick="traiter_le_document('0','pdf');">Ce texte en pdf</li>
		<li id="p_lpng" onclick="traiter_le_document('0','poly1');">Cette ressource en pdf pleine page</li>
		<li id="p_lpng" onclick="export_sans_popup('0','poly1');">Tout l'arbre en pdf pleine page</li>
    </ul>
</div>
<?php echo dMenu(1);echo dMenu(2);?>
<div id="firstdoc" style="display: inline ;visibility:hidden;"></div>
<div id="affnumress" style="display: inline ;visibility:hidden;"><?php echo $affnumress;?></div>
<div id="Author" style="display: inline ;visibility:hidden;"><?php echo $Author;?></div>
<div id="LongName" style="display: inline ;visibility:hidden;"><?php echo $LongName;?></div>
<div id="lastdoc1" style="display: inline ;visibility:hidden;"><?php echo $lastdoc1;?></div>
<div id="lastdoc2" style="display: inline ;visibility:hidden;"><?php echo $lastdoc2;?></div>
<div id="Cat" style="display: inline ;visibility:hidden;"><?php echo $Cat;?></div>
</BODY> 
</HTML>
<?php
//echo temps('lecteur du compositeur');
?>
