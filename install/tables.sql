-- Adminer 3.3.3 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `academies`;
CREATE TABLE `academies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `academie` tinytext NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `academies` (`id`, `academie`) VALUES
(0,	''),
(1,	'Poitiers'),
(2,	'OrlÃ©ans-Tours'),
(3,	'France entiÃ¨re'),
(4,	'Toulouse'),
(5,	'Montpellier'),
(6,	'Lyon'),
(7,	'Lille'),
(21,	'CrÃ©teil');

DROP TABLE IF EXISTS `adresses`;
CREATE TABLE `adresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) NOT NULL,
  `nature` enum('learningsapps','video','audio','prezi','ebook') NOT NULL,
  `url` varchar(200) NOT NULL,
  `id_licence` int(11) NOT NULL,
  `id_philosopheme` int(11) NOT NULL,
  `id_membre_verseur` int(11) NOT NULL,
  `id_membre_valideur` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `arbors`;
CREATE TABLE `arbors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ego` int(11) NOT NULL,
  `father` int(11) NOT NULL,
  `id_ressource` int(11) NOT NULL,
  `name` tinytext NOT NULL,
  `category` tinytext NOT NULL,
  `id_membre` int(11) NOT NULL,
  `document` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `auteur2s`;
CREATE TABLE `auteur2s` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auteur2` tinytext NOT NULL,
  `prenoms` tinytext NOT NULL,
  `naissance` year(4) NOT NULL,
  `mort` year(4) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `auteurs`;
CREATE TABLE `auteurs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auteur` tinytext NOT NULL,
  `prenoms` tinytext NOT NULL,
  `apropos` set('mort depuis plus de 70 ans','ecrit en francais') NOT NULL,
  `publicdomain` enum('oui','non') NOT NULL DEFAULT 'non',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `auteurstest`;
CREATE TABLE `auteurstest` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auteur` varchar(50) NOT NULL,
  `prenoms` tinytext NOT NULL,
  `apropos` set('mort depuis plus de 70 ans','ecrit en francais') NOT NULL,
  `publicdomain` enum('oui','non') NOT NULL DEFAULT 'non',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL DEFAULT '',
  `to` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  `sent` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recd` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `to` (`to`),
  KEY `from` (`from`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `contributions`;
CREATE TABLE `contributions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ressource` int(11) NOT NULL,
  `id_membre` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `developpements`;
CREATE TABLE `developpements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_membre` int(11) NOT NULL,
  `ladate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `developpement` tinytext NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_membre` (`id_membre`),
  CONSTRAINT `developpements_ibfk_1` FOREIGN KEY (`id_membre`) REFERENCES `membres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `disciplines`;
CREATE TABLE `disciplines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discipline` tinytext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `disciplines` (`id`, `discipline`) VALUES
(1,	'Arts plastiques'),
(2,	'Arts appliques'),
(3,	'Biochimie - gÃ©nie biologique'),
(4,	'Ã‰conomie et gestion - administration et ressources humaines'),
(5,	'Ã‰conomie et gestion - finance et contrÃ´le'),
(6,	'Ã‰conomie et gestion - marketing'),
(7,	'Ã‰conomie et gestion - systÃ¨me d\\\'information'),
(8,	'Ã‰ducation physique et sportive'),
(9,	'GÃ©ographie'),
(10,	'Grammaire'),
(11,	'Histoire'),
(12,	'Basque'),
(13,	'Catalan'),
(14,	'Allemand'),
(15,	'Anglais'),
(16,	'Arabe'),
(17,	'Espagnol'),
(18,	'HÃ©breu'),
(19,	'Italien'),
(20,	'Langue et culture japonaises'),
(21,	'Lettres classiques'),
(22,	'Lettres modernes'),
(23,	'Mathematiques'),
(24,	'Musique'),
(25,	'Philosophie'),
(26,	'Physique-chimie option chimie'),
(27,	'Physique-chimie option physique'),
(28,	'Sciences de la vie, de la Terre et de l\\\'Univers'),
(29,	'Sciences Ã©conomiques et sociales'),
(30,	'Sciences de l\\\'ingÃ©nieur - ingÃ©nierie des constructions'),
(31,	'Sciences de l\\\'ingÃ©nieur - ingÃ©nierie Ã©lectrique'),
(32,	'Sciences de l\\\'ingÃ©nieur - ingÃ©nierie informatique'),
(33,	'Sciences de l\\\'ingÃ©nieur - ingÃ©nierie mÃ©canique'),
(34,	'Sciences mÃ©dico-sociales');

DROP TABLE IF EXISTS `fichiers`;
CREATE TABLE `fichiers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(100) NOT NULL,
  `nature` enum('hotpatatoes  Jcross','hotpatatoes  JQuizz','hotpatatoes  JCloze','hotpatatoes  JMix','hotpatatoes  JMatch','ebook') CHARACTER SET utf8 NOT NULL,
  `url` varchar(200) NOT NULL,
  `id_licence` int(11) NOT NULL,
  `source` varchar(200) NOT NULL,
  `id_philosopheme` int(11) NOT NULL,
  `id_membre_verseur` int(11) NOT NULL,
  `id_membre_valideur` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `gabarits`;
CREATE TABLE `gabarits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_membre` int(11) NOT NULL,
  `gabarit` tinytext NOT NULL,
  `nature` enum('DEBUT','SEPARATION','texte','sujet-question','sujet-texte','image','FIN') NOT NULL DEFAULT 'texte',
  `format` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_membre` (`id_membre`),
  CONSTRAINT `gabarits_ibfk_1` FOREIGN KEY (`id_membre`) REFERENCES `membres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `licences`;
CREATE TABLE `licences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `licence` varchar(30) NOT NULL,
  `description` tinytext NOT NULL,
  `documentation` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `licences` (`id`, `licence`, `description`, `documentation`) VALUES
(0,	'',	'',	''),
(1,	'GFDL',	'',	''),
(2,	'CC BY-SA',	'Partage dans les mÃªmes conditions',	'http://creativecommons.org/licenses/by-sa/3.0/fr/'),
(3,	'Domaine public',	'',	''),
(4,	'CC BY',	'Attribution',	'http://creativecommons.org/licenses/by/3.0/fr/'),
(5,	'CC BY-ND',	'Attribution + Pas de Modification',	'http://creativecommons.org/licenses/by-nd/3.0/fr/'),
(9,	'CC BY-NC-ND',	'Attribution + Pas dâ€™Utilisation Commerciale + Pas de Modification',	'http://creativecommons.org/licenses/by-nc-nd/3.0/fr/'),
(11,	'CC BY-NC-SA',	'Attribution + Pas dâ€™Utilisation Commerciale + Partage dans les mÃªmes conditions',	'http://creativecommons.org/licenses/by-nc-sa/3.0/fr/'),
(14,	'CC BY-NC',	'Attribution + Pas dâ€™Utilisation Commerciale',	'Attribution + Pas dâ€™Utilisation Commerciale');

DROP TABLE IF EXISTS `membres`;
CREATE TABLE `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `membre` varchar(25) NOT NULL,
  `preferences` set('Polys avec images','Images en noir et blanc dans les polys','Mode incremental pour les diaporamas','Numeros de lignes','Chercher seulement dans mes ressources','Nom sur la couverture des diaporamas','Date sur la couverture des diaporamas','Interface avec toutes les icones','Sommaire dans les polys','Ressources dans les sommaires','Titres dans les polys') DEFAULT 'Polys avec images,Numeros de lignes,Ressources dans les sommaires',
  `id_discipline` int(11) NOT NULL DEFAULT '25',
  `disciplines_visibles` set('Arts plastiques','Arts appliques','Biochimie - genie biologique','Economie et gestion - administration et ressources humaines','Economie et gestion - finance et controle','Economie et gestion - marketing','Economie et gestion - systeme d information','Education physique et sportive','Geographie','Grammaire','Histoire','Basque','Catalan','Allemand','Anglais','Arabe','Espagnol','Hebreu','Italien','Langue et culture japonaises','Lettres classiques','Lettres modernes','Mathematiques','Musique','Philosophie','Physique-chimie option chimie','Physique-chimie option physique','Sciences de la vie - de la Terre et de l Univers','Sciences economiques et sociales','Sciences de l ingenieur - ingenierie des constructions','Sciences de l ingenieur - ingenierie electrique','Sciences de l ingenieur - ingenierie informatique','Sciences de l ingenieur - ingenierie mecanique','Sciences medico-sociales') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_discipline` (`id_discipline`),
  CONSTRAINT `membres_ibfk_1` FOREIGN KEY (`id_discipline`) REFERENCES `disciplines` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `notions`;
CREATE TABLE `notions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notion` tinytext NOT NULL,
  `order` tinyint(4) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `notions` (`id`, `notion`, `order`) VALUES
(1,	'La conscience, l\'inconscient',	0),
(8,	'La perception',	0),
(9,	'L\'existence, le temps',	0),
(10,	'Le dÃ©sir, autrui',	0),
(11,	'La vÃ©ritÃ©',	0),
(12,	'La raison, la croyance',	0),
(14,	'La dÃ©monstration',	0),
(16,	'La matiÃ¨re, l\'esprit',	0),
(17,	'Le vivant',	0),
(18,	'La sociÃ©tÃ©, l\'Ã‰tat',	0),
(19,	'La justice, le droit',	0),
(20,	'La culture, l\'histoire',	0),
(21,	'Le travail, la technique',	0),
(22,	'La libertÃ©',	0),
(23,	'Le devoir, la morale',	0),
(25,	'Le bonheur',	0),
(31,	'La conscience, l\'inconscient',	0),
(32,	'L\'existence, le temps',	0),
(33,	'Le dÃ©sir, autrui',	0),
(34,	'La raison, la croyance',	0),
(35,	'La matiÃ¨re, l\'esprit',	0),
(36,	'La sociÃ©tÃ©, l\'Ã‰tat',	0),
(37,	'La justice, le droit',	0),
(38,	'La culture, l\'histoire',	0),
(39,	'Le travail, la technique',	0),
(40,	'Le devoir, la morale',	0),
(42,	'',	0);

DROP TABLE IF EXISTS `partages`;
CREATE TABLE `partages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `partage` tinytext NOT NULL,
  `id_membre` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_membre` (`id_membre`),
  CONSTRAINT `partages_ibfk_1` FOREIGN KEY (`id_membre`) REFERENCES `membres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `philos`;
CREATE TABLE `philos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `philo` tinytext NOT NULL,
  `texte` text NOT NULL,
  `id_auteur` bigint(20) NOT NULL,
  `reference` tinytext NOT NULL,
  `id_philosopheme` int(11) NOT NULL,
  `tags` tinytext NOT NULL,
  `notions` tinytext NOT NULL,
  `note` text NOT NULL,
  `origine` tinytext NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `philosophemes`;
CREATE TABLE `philosophemes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `philosopheme` varchar(50) NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `philosopheme` (`philosopheme`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `philosophemes` (`id`, `philosopheme`) VALUES
(116,	''),
(48,	'Admiration et curiosite'),
(79,	'Agir et produire'),
(9,	'alienation et liberte'),
(288,	'AllÃ©gorie'),
(27,	'Alter ego'),
(41,	'Ami et copain et camarade'),
(184,	'AmitiÃ©s'),
(351,	'arbre mÃ©taphore'),
(280,	'arithmÃ©tique et raison'),
(106,	'art et art contemporain'),
(57,	'Art et chance'),
(188,	'Art et crÃ©ation'),
(58,	'Art et jeu'),
(195,	'Art et passions'),
(24,	'Art et perception de la rÃ©alitÃ©'),
(194,	'Art et reprÃ©sentation de l\'esprit'),
(193,	'art et sensible'),
(93,	'Art et temps'),
(313,	'art et vÃ©ritÃ©'),
(200,	'art sacrÃ©-art profane'),
(5,	'Artiste et artisan'),
(43,	'Artiste et philosophe'),
(140,	'Artistes (exemples)'),
(353,	'arts et sciences'),
(128,	'AthÃ©isme et superstition'),
(74,	'AutoritÃ© et autoritarisme'),
(331,	'avoir le droit'),
(17,	'Ã‰tat de nature'),
(234,	'ÃŠxemple du bÃ¢ton de l\'agent'),
(278,	'Ã©vidence et vÃ©ritÃ©'),
(207,	'barbarie et connaissance'),
(253,	'bavarder et dire'),
(287,	'bÃªtise'),
(152,	'Besoins et nÃ©cessitÃ©'),
(222,	'bonheur et politique'),
(28,	'Bonheur et solitude'),
(269,	'causalitÃ©'),
(212,	'Chatiment et punition'),
(332,	'chicane et droit'),
(235,	'Choix'),
(71,	'Choix et dÃ©libÃ©ration'),
(250,	'cinÃ©ma'),
(290,	'cinÃ©ma droit justice'),
(302,	'CinÃ©ma et autrui'),
(297,	'cinÃ©ma et culture'),
(310,	'cinÃ©ma et dÃ©sir'),
(296,	'cinÃ©ma et existence'),
(338,	'CinÃ©ma et histoire'),
(299,	'cinÃ©ma et inconscient'),
(293,	'cinÃ©ma et langage'),
(311,	'CinÃ©ma et libertÃ©'),
(291,	'cinÃ©ma et morale'),
(346,	'cinÃ©ma et nature'),
(289,	'cinÃ©ma et notions'),
(292,	'cinÃ©ma et perception'),
(309,	'cinÃ©ma et politique'),
(294,	'cinÃ©ma et rÃ©el'),
(298,	'cinÃ©ma et sociÃ©tÃ©'),
(325,	'CinÃ©ma et temps'),
(340,	'cinÃ©ma et travail'),
(275,	'CitÃ©-Ã©conomie'),
(216,	'Citoyen'),
(337,	'cogito'),
(349,	'Commerce'),
(285,	'communautÃ©'),
(172,	'Communiquer est une nÃ©cessitÃ©'),
(149,	'communiquer et dialoguer'),
(99,	'Comportement et habitude'),
(88,	'Condition humaine/nature humaine'),
(105,	'conscience /cerveau'),
(314,	'conscience et jugement'),
(303,	'conscience et temps'),
(135,	'Conscience morale'),
(78,	'Constater des faits -interprÃ©ter'),
(334,	'contrat de droit et contrat de dupe'),
(32,	'Copier-imiter-exprimer'),
(308,	'corps'),
(112,	'Corruption et argent'),
(244,	'coutume'),
(178,	'Croire et savoir'),
(86,	'Croyance et crÃ©dulitÃ©'),
(158,	'Croyance et foi'),
(256,	'culture'),
(307,	'DÃ©finir l\'inconscient'),
(103,	'DÃ©mocratie et aristocratie'),
(62,	'DÃ©mocratie et despotisme'),
(204,	'DÃ©mocratie et fraternitÃ©'),
(153,	'DÃ©monstration et vÃ©ritÃ©'),
(69,	'DÃ©montrer et montrer'),
(68,	'DÃ©montrer et prouver'),
(320,	'dÃ©sir de reconnaissance'),
(80,	'DÃ©sir et besoin'),
(37,	'DÃ©sir et bonheur'),
(39,	'DÃ©sir et ennui'),
(321,	'dÃ©sir et libertÃ©'),
(211,	'DÃ©sir et manque'),
(319,	'dÃ©sir et volontÃ©'),
(107,	'Despotisme et bonheur'),
(109,	'Despotisme et dÃ©sir'),
(108,	'Despotisme et sÃ©paration des pouvoirs'),
(66,	'Destin et hasard'),
(300,	'devoir'),
(345,	'Dialogue et conversation'),
(143,	'Dialoguer/dÃ©battre'),
(155,	'Dieu est mort'),
(104,	'DiffÃ©rence homme/animal'),
(304,	'dire le temps'),
(279,	'discontinuitÃ© du temps des sciences'),
(259,	'DiversitÃ© culturelle'),
(241,	'Divertissement'),
(147,	'domination et habitus'),
(154,	'Doute et vÃ©ritÃ©'),
(233,	'Droit de vote et voter pour ses droits'),
(324,	'Droit et vengeance'),
(277,	'Droit naturel'),
(224,	'Droits de l\'homme et du citoyen'),
(215,	'Ecriture'),
(355,	'Education et musique'),
(348,	'Education et RÃ©publique'),
(40,	'EgalitÃ© et identitÃ©'),
(177,	'EgalitÃ© et libertÃ©'),
(335,	'EgoÃ¯sme'),
(139,	'Enfance et naÃ¯vetÃ©'),
(168,	'EquivocitÃ© du langage'),
(52,	'Erreur et illusion'),
(315,	'esprit critique'),
(110,	'Etre maÃ®trisÃ© et se maÃ®triser'),
(45,	'Exemple-cas particulier'),
(312,	'exemples'),
(354,	'existence de Dieu'),
(126,	'ExpÃ©rience et connaissance'),
(49,	'ExpÃ©rience et expÃ©rimentation'),
(96,	'ExpÃ©rience et habitude'),
(70,	'ExpÃ©rience et observation'),
(199,	'fanatisme'),
(283,	'FinalitÃ©'),
(243,	'Finitude'),
(170,	'Fondements de la vie monastique'),
(47,	'Force et puissance'),
(63,	'Foule et peuple'),
(81,	'GÃ©nÃ©rositÃ© chrÃ©tienne-gÃ©nÃ©rositÃ© de l\'actio'),
(329,	'goÃ»t esthÃ©tique'),
(231,	'guerre'),
(352,	'hasard'),
(327,	'histoire et narration'),
(192,	'Histoire et passions'),
(237,	'honnÃªte homme'),
(94,	'HypothÃ¨se et supposition'),
(252,	'IdÃ©ologie et histoire'),
(305,	'identitÃ© et changement'),
(163,	'Ignorance'),
(268,	'illusion de la conscience'),
(179,	'Illusion et dÃ©sir'),
(239,	'Illusions de la morale'),
(249,	'image et cinÃ©ma'),
(92,	'Image et concept'),
(242,	'Imagination, erreur, illusion'),
(138,	'Inconscient et rÃªve'),
(97,	'Induction-DÃ©duction'),
(61,	'InquiÃ©tude et doute'),
(127,	'Insociable sociabilitÃ©'),
(230,	'Instinct et libre-arbitre'),
(55,	'Instinct et raison'),
(157,	'Instruire et apprendre'),
(333,	'InterprÃ©ter le droit'),
(95,	'Inventer et dÃ©couvrir'),
(326,	'inventions techniques et scientifiques'),
(339,	'jalousie'),
(267,	'jardin paysage'),
(133,	'Je est un mot neutre'),
(258,	'Jeu sÃ©rieux'),
(213,	'Justice divine'),
(245,	'justice et force'),
(26,	'L\'instant et la durÃ©e'),
(11,	'L\'usage du \"Je\" et des dÃ©ictiques'),
(98,	'La haine de la raison'),
(167,	'La lettre et l\'esprit'),
(217,	'La loi et la rÃ¨gle'),
(6,	'La mÃ©canique et le vivant'),
(191,	'La reconnaissance'),
(18,	'La technique comme mÃ©taphore'),
(124,	'langage et action'),
(214,	'langue et passions'),
(343,	'lÃ©gende'),
(175,	'Le beau et l\'utile'),
(50,	'Le corps et l\'Ã¢me'),
(20,	'Le droit et la force'),
(85,	'Le gÃ©nie'),
(8,	'Le machinisme et la machine'),
(111,	'Le mÃ©chant'),
(344,	'le monde'),
(347,	'le monstrueux'),
(38,	'Le mot et l\'idÃ©e'),
(44,	'Le musÃ©e'),
(144,	'Le mythe de l\'intÃ©rioritÃ©'),
(29,	'Le rÃ©el et l\'illusion'),
(197,	'le sens de l\'histoire'),
(46,	'Lecture suivie'),
(22,	'Les anciens et les modernes'),
(187,	'Les fins de l\'art'),
(87,	'Les imaginations'),
(189,	'Les leÃ§ons de l\'histoire'),
(281,	'les masses'),
(125,	'les mot et les choses'),
(36,	'Les mots et les choses'),
(236,	'LibertÃ© d\'indiffÃ©rence'),
(196,	'LibertÃ© et Ã©ducation'),
(151,	'LibertÃ© et contrainte'),
(350,	'libertÃ© et licence'),
(171,	'LibertÃ© et lois de la nature'),
(341,	'libertÃ© et mauvaise foi'),
(31,	'LibertÃ© et obÃ©issance'),
(60,	'LibertÃ© et volontÃ©'),
(145,	'LibertÃ© politique'),
(272,	'Logique et mathÃ©matiques'),
(75,	'Machiavel et Montesquieu'),
(148,	'Main et outil'),
(282,	'matiÃ¨re et pensÃ©e'),
(238,	'maxime morale'),
(65,	'MÃ©canisme et vie'),
(54,	'MÃ©taphore de l\'abeille'),
(64,	'MÃ©taphore du thÃ©Ã¢tre'),
(218,	'MÃ©taphore du tissage'),
(206,	'MÃ©taphore mÃ©dicale'),
(246,	'misÃ¨re et faiblesse'),
(255,	'moi et autrui'),
(113,	'Monarchie et despotisme'),
(223,	'Morale et politique'),
(176,	'Mort et dÃ©sespoir'),
(322,	'mort et dÃ©sir'),
(323,	'mort et existence'),
(265,	'mythe et interprÃ©tation'),
(210,	'Mythes'),
(100,	'NÃ©cessitÃ© de penser'),
(336,	'nihilisme et vide'),
(123,	'obÃ©issance et libertÃ©'),
(101,	'Observer - s\'observer'),
(19,	'Oeuvre et effet'),
(91,	'Opinion et jugement'),
(76,	'Opinion et prÃ©jugÃ©'),
(83,	'Outil-machine'),
(330,	'Pacte social'),
(198,	'Parole et Ãªtre'),
(121,	'particulier/gÃ©nÃ©ral'),
(102,	'Partie et Tout'),
(208,	'Passion et raison'),
(142,	'Paternalisme'),
(51,	'Perception-sensation'),
(150,	'Persuader et convaincre'),
(174,	'Peuple et Tyrannie'),
(295,	'peur et angoisse'),
(190,	'Philosophie'),
(264,	'philosophie et mythes'),
(84,	'PitiÃ© et morale'),
(240,	'pitiÃ© et piÃ©tÃ©'),
(156,	'politique et Ã©conomique'),
(77,	'Politique et histoire'),
(232,	'Pouvoir, puissance, capacitÃ©'),
(164,	'Pragmatisme'),
(317,	'prÃ©jugÃ©'),
(306,	'PrÃ©sent Avenir Futur'),
(328,	'progrÃ¨s'),
(141,	'Psychanalyse'),
(247,	'RAISON'),
(82,	'Raison-raisonnement-raisonnable'),
(129,	'Raisons de la croyance'),
(251,	'RÃ©versibilitÃ© du temps'),
(33,	'RÃªve et rÃªverie'),
(248,	'religion'),
(181,	'Religion et crainte'),
(169,	'Religion et enfermement'),
(160,	'Religion et illusion'),
(165,	'Religion et loi'),
(201,	'Religion et morale'),
(159,	'Religion et mort'),
(173,	'Religion et pouvoir'),
(202,	'Religion et psychanalyse'),
(183,	'Religion et sacrifice'),
(209,	'Religion et science'),
(180,	'Religion et sociÃ©tÃ©'),
(42,	'ReprÃ©sentation et imitation'),
(166,	'Sacrifice d\'Isaac et interprÃ©tation'),
(162,	'Savoir et politique'),
(35,	'Savoir et pouvoir'),
(161,	'Sens des mots'),
(59,	'Sensation-rÃ©flexion'),
(56,	'Signe signal symbole'),
(146,	'sociologie'),
(205,	'Soumission et esclavage'),
(270,	'sublime'),
(219,	'sujets dissertation politique'),
(186,	'Sujets-questions sur l\'art'),
(203,	'Sujets-questions sur la religion'),
(114,	'superstition et crainte'),
(131,	'Superstition et finalitÃ©'),
(130,	'Superstition et piÃ©tÃ©'),
(53,	'Technique et maÃ®trise'),
(89,	'Technique et politique'),
(23,	'Technique et science'),
(276,	'temps et Ã©ternitÃ©'),
(72,	'Totalitarisme'),
(137,	'Travail et aliÃ©nation'),
(182,	'Travail et crÃ©ation de soi'),
(284,	'travail et propriÃ©tÃ©'),
(73,	'Tyrannie et Etat de droit'),
(134,	'UtilitÃ© /dÃ©sintÃ©ressement'),
(136,	'Utopie'),
(286,	'vagabondage'),
(316,	'vÃ©ritÃ© et mÃ©taphysique'),
(132,	'vÃ©ritÃ© et sincÃ©ritÃ©'),
(30,	'Vie intÃ©rieure et vie extÃ©rieure'),
(257,	'vivant'),
(67,	'VolontÃ© et nÃ©cessitÃ©');

DROP TABLE IF EXISTS `remarques`;
CREATE TABLE `remarques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_membre` int(11) NOT NULL,
  `ladate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `remarque` tinytext NOT NULL,
  `description` text NOT NULL,
  `reponse` text NOT NULL,
  `statut` enum('soumis','en cours','clos') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_membre` (`id_membre`),
  CONSTRAINT `remarques_ibfk_1` FOREIGN KEY (`id_membre`) REFERENCES `membres` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `ressources`;
CREATE TABLE `ressources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ressource` tinytext NOT NULL,
  `texte` mediumtext NOT NULL,
  `questions` text NOT NULL,
  `id_auteur` bigint(20) NOT NULL,
  `reference` tinytext NOT NULL,
  `serie` tinytext NOT NULL,
  `lasession` tinytext NOT NULL,
  `lieu` tinytext NOT NULL,
  `annee` tinytext NOT NULL,
  `tags` tinytext NOT NULL,
  `notions` tinytext NOT NULL,
  `note` text NOT NULL,
  `origine` tinytext NOT NULL,
  `url` tinytext NOT NULL,
  `source` tinytext NOT NULL,
  `id_licence` int(11) NOT NULL,
  `nature` enum('JCross','JQuizz','JMix','JCloze','JMatch','document','epub','pdf','texte','sujet-question','sujet-texte','LearningApps','youtube','dailymotion','vimeo','mp4','mp3','Quizz','image','composite','h5p','graphe','graphique','dialogue','lilypond','midi','music','mobi') NOT NULL DEFAULT 'texte',
  `id_membre` int(11) NOT NULL DEFAULT '0',
  `fork` int(11) NOT NULL,
  `ladate` int(11) NOT NULL,
  `debut` int(5) NOT NULL,
  `fin` int(5) NOT NULL,
  `page_couv` tinyint(4) NOT NULL,
  `disciplines` set('Arts plastiques','Arts appliques','Biochimie - genie biologique','Economie et gestion - administration et ressources humaines','Economie et gestion - finance et controle','Economie et gestion - marketing','Economie et gestion - systeme d information','Education physique et sportive','Geographie','Grammaire','Histoire','Basque','Catalan','Allemand','Anglais','Arabe','Espagnol','Hebreu','Italien','Langue et culture japonaises','Lettres classiques','Lettres modernes','Mathematiques','Musique','Philosophie','Physique-chimie option chimie','Physique-chimie option physique','Sciences de la vie - de la Terre et de l Univers','Sciences economiques et sociales','Sciences de l ingenieur - ingenierie des constructions','Sciences de l ingenieur - ingenierie electrique','Sciences de l ingenieur - ingenierie informatique','Sciences de l ingenieur - ingenierie mecanique','Sciences medico-sociales') NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `ressourcesphilosophemes`;
CREATE TABLE `ressourcesphilosophemes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_philosopheme` int(11) NOT NULL,
  `id_ressource` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `save_ressources`;
CREATE TABLE `save_ressources` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ressource` tinytext NOT NULL,
  `texte` text NOT NULL,
  `questions` text NOT NULL,
  `id_auteur` bigint(20) NOT NULL,
  `reference` tinytext NOT NULL,
  `serie` tinytext NOT NULL,
  `lasession` tinytext NOT NULL,
  `lieu` tinytext NOT NULL,
  `annee` tinytext NOT NULL,
  `tags` tinytext NOT NULL,
  `notions` tinytext NOT NULL,
  `note` text NOT NULL,
  `origine` tinytext NOT NULL,
  `url` tinytext NOT NULL,
  `source` tinytext NOT NULL,
  `id_licence` int(11) NOT NULL,
  `nature` enum('JCross','JQuizz','JMix','JCloze','JMatch','document','epub','pdf','texte','sujet-question','sujet-texte','LearningApps','youtube','dailymotion','vimeo','mp4','mp3','Quizz','image','composite','h5p') NOT NULL DEFAULT 'texte',
  `id_membre` int(11) NOT NULL DEFAULT '0',
  `fork` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `series`;
CREATE TABLE `series` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serie` tinytext NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session` tinytext NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sources`;
CREATE TABLE `sources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` text NOT NULL,
  `description` tinytext NOT NULL,
  `url` tinytext NOT NULL,
  `volume` tinyint(4) NOT NULL DEFAULT '1',
  `cvolume` text NOT NULL,
  `quality` tinyint(4) NOT NULL DEFAULT '1',
  `cquality` text NOT NULL,
  `freedom` tinyint(4) NOT NULL DEFAULT '1',
  `cfreedom` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sujets`;
CREATE TABLE `sujets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sujet` tinytext,
  `id_serie` int(11) DEFAULT NULL,
  `id_session` int(11) DEFAULT NULL,
  `id_academie` int(11) DEFAULT NULL,
  `annee` tinytext,
  `id_notion` int(11) DEFAULT NULL,
  `origine` tinytext NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `textes`;
CREATE TABLE `textes` (
  `id` int(11) NOT NULL,
  `texte` text,
  `nature` tinytext CHARACTER SET latin1 NOT NULL,
  `id_auteur` int(11) NOT NULL,
  `serie` tinytext CHARACTER SET latin1,
  `session` tinytext CHARACTER SET latin1,
  `lieu` tinytext CHARACTER SET latin1,
  `annee` tinytext CHARACTER SET latin1,
  `questions` text CHARACTER SET latin1 NOT NULL,
  `id_licence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `validations`;
CREATE TABLE `validations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_ressource` int(11) NOT NULL,
  `id_membre` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-08-24 19:56:05

