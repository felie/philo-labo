## INSTALLATION

### Configuration conseillée

Ubuntu 16.04 (ou 18.04)
Apache 2
PHP 5.6  (voir https://tecadmin.net/install-php5-on-ubuntu/)
MySQL 5.5.62 

### Configuration Apache

Ne pas oublier de rendre le serveur web propriétaire des fichiers
Classiquement par:
'''
sudo chown www-data.www-data -R *
'''

Le système philo-labo a besoin d'être à la racine d'un serveur.
si vous laissez les fichiers ainsi
/var/www/html/philo-labo 
Il vous faudra créer un serveur virtuel avec Apache par exemple
todo:donner des explications

### Base de données

adminer est un outil excellent (voir http://www.ubuntuboss.com/how-to-install-adminer-on-ubuntu-18-04/)

* créez une base de données mysql vide
* peuplez-la avec le fichier tables.sql
* renseignez le fichier config.php (dans la racine) pour que les connexions puissent être établies

### Envoi de mail

Envoi de mail pour l'inscription automatique -> voir la documentation de postfix

### Administration et utilisateurs

Dans la version installable, il n'y a qu'un utilisateur: admin, et sans mot de passe

Voir la documentation de userauth2, dans le cookbook de PmWiki pour la mécanique des droits



