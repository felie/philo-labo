<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

	<head> 
		<title>Démonstration jQuery MultiSelect</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

                <script src="/ai/js/jquery.js" type="text/javascript"></script>
		<script src="/ai/js/jquery.dimensions.js" type="text/javascript"></script>
		<script src="/ai/js/jqueryMultiSelect.js" type="text/javascript"></script>

		<link href="/ai/js/jqueryMultiSelect.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript">
			
			$(document).ready( function() {
				
				// Default options
				$("#control_1, #control_3, #control_4, #control_5").multiSelect();
				
				// With callback
				$("#control_6").multiSelect( null, function(el) {
					$("#callbackResult").show().fadeOut();
				});
				
				// Options displayed in comma-separated list
				$("#control_7").multiSelect({ oneOrMoreSelected: '*' });
				
				// 'Select All' text changed
				$("#control_8").multiSelect({ selectAllText: 'Coche les tous!' });
				
				// Show test data
				$("FORM").submit( function() {
					var results = $(this).serialize().replace(/&/g, '\n');
					results = decodeURI(results);
					alert(results);
					return false;
				});
				
			});
			
		</script>
		
		<style type="text/css">
			HTML {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 12px;
			}
			
			H2 {
				font-size: 14px;
				font-weight: bold;
				margin: 1em 0em .25em 0em;
			}
			
			P {
				margin: 1em 0em;
			}
		</style>
</head>
<body>


<form action="" method="post">
			
			<h1>jQuery MultiSelect</h1>

<select id="control_5" name="control_5[]" multiple="multiple" size="5">
					<option value=""></option>
					<option value="option_1" selected="selected">Option 1</option>
					<option value="option_2" selected="selected">Option 2</option>

					<option value="option_3" selected="selected">Option 3</option>
					<option value="option_4">Option 4</option>
					<option value="option_5">Option 5</option>
					<option value="option_6">Option 6</option>
					<option value="option_7">Option 7</option>
				</select>

<p>
				<input type="submit" value="Voir le résultat" />
			</p>
			
		</form>
		
	</body>
</html>