<?php 

require_once('secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
    
require_once('sql_config.php');
require_once('ai.php');
    
function associe($id,$nn,$t1,$t2)  // t1 ressources t2 philosopheme
        {
        $tb1=$t1."s";$tb2=$t2."s";
        $sel=' selected="selected"';
        $item="<option %s value='%s'>%s</option>";
        $max=sql2array("select id,$t2 from $t2"."s",'h'); // tous les possibles
        $sql="select $tb2.id from $nn,$tb2,$tb1 where $nn.id_$t1=$tb1.id and $nn.id_$t2=$tb2.id and $tb1.id=$id"; // déjà présents
        $check=sql2seq($sql);
        //echo "max<pre>";print_r($max);
        //echo "</pre>check<pre>";print_r($check);echo "</pre>";
        $selections='';
        for ($i=1;$i<sizeof($max)+1;$i++)
            {
            if (in_array($max[$i-1][0],$check))
                $seld=$sel;
            else
                $seld="";
            $selections.=vsprintf($item,array($seld,$max[$i-1][0],$max[$i-1][1]));
            }
        return "<center><form id='form_insert' method='POST'><select name='$t2"."[]' class='multiselect' multiple='multiple' style='height:300px;'>".$selections."</select><center><div onclick=\"associe();\">OK</div></center><input type='hidden' name='$t1' value='$id'></form><div id='result'>lieu d'écriture</div></center>";
        }


$scripts.="
<script src='/js/jquery-1.3.2.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    }
 .ui-dialog-shadow { 
-webkit-box-shadow: 12px 12px 16px 0 #000000;
box-shadow: 12px 12px 16px 0 #000000;
 }
  .ui-dialog-titlebar {
  height:0;
  background:ivory;
  border:0;
  }
  </style>
	<script type='text/javascript'>
	jq132(document).ready(function() {
		jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div></div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('title'),
					width: 800
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href'));
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";
        
$scripts.="
<link href='/ai/css/multi-select.css' media='screen' rel='stylesheet' type='text/css'>
<script src='/ai/js/jquery.tinysort.js' type='text/javascript'></script>
<script src='/ai/js/jquery.js' type='text/javascript'></script>
<script src='/ai/js/jquery.multi-select.js' type='text/javascript'></script>
<script src='/ai/js/jquery.quicksearch.js' type='text/javascript'></script>";
$scripts.='
<script type="text/javascript">
function associe()
{
    $.ajax({
        url: "associe.php",
        type: "POST",
        data: {
            ssd: "yes",
            data: $("#form_insert").serialize()
        },
        success: function(data) {
        $("#result").html(data);
        }
    });
}
</script>
<script type="text/javascript">
          $(document).ready(function () {
              $(".multiselect").multiSelect();
          });
</script>'; 

$content.="<h3>Affectation de philosophèmes - faites votre choix</h3>";

$id=$_GET[id];$t1=$_GET[t1];$t2=$_GET[t2];$nn=$_GET[nn];

$content.=simple_query("select $t1 from $t1"."s where id=".$id)."<p/>"; 

$content.=associe($id,$nn,$t1,$t2);

$ai_indent=true;
require('ai_avsts2.php');
echo $content;
?>
