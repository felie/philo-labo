<?php  // base de données pour philo-labo

require_once('secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici

define('ai_standalone',TRUE);
require('sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('ai.php');
require_once('philo-labo.php'); // fonctions spécifiques à philo-labo

$_GET[admin]=false;

$recherches=sprintf($form_search,'?action=searchengine');

$body=$recherches;

if ($_GET['action']='searchengine')
    $body.=resultatrecherche(true,true,true);


$title=str_replace('_',' ',$action);
$nb_ress=simple_query('select count(*) from ressources');
$logo="<img src='logo.png' alt='logo' /><br/>[$Author] / <b>$nb_ress</b> ressources<br/>$recherches";
$header="Philosophèmes";
$content.="<div id='centr'><p>$body</p></div>";
$ai_indent=0;

$scripts.='<script src="../js/jquery-1.11.3.min.js"></script>
           <script src="../js/jquery-ui.min.js"></script>';

// pour la saisie multiple
$scripts.='<script src="../ai/multiselect2/src/jquery.tree-multiselect.js"></script>
    <style>
      * {
        font-family: sans-serif;
      }
      .machin 
        {
        display:block;
        width:200px;
        }
    </style>
    <link rel="stylesheet" href="../ai/multiselect2/dist/jquery.tree-multiselect.min.css"/>
    '; 


// pour les popup 
/*
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js'></script>
  <link rel='stylesheet' type='text/css' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css'>
  */
$scripts.="
<script src='/js/jquery-1.3.2.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    }
 .ui-dialog-shadow { 
-webkit-box-shadow: 12px 12px 16px 0 #000000;
box-shadow: 12px 12px 16px 0 #000000;
 }
  .ui-dialog-titlebar {
  height:0;
  background:ivory;
  border:0;
  }
  </style>
	<script type='text/javascript'>
	jq132(document).ready(function() {
		jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div></div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('title'),
					width: 800
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href'));
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";

$ai_indent=false;
require('ai_avsts2.php');
echo $content;
?>
