<?php  // base de données pour philo-labo

# à remplacer par une requête à config.php qui s'interromp avant de passer la main à pmwiki.php
require_once('secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici

// restriction d'accès (au cas où les gens essaie de rentrer par l'url)
if (!in_array($Author,array('felie','admin','maryse')))
	{
	echo "Vous n'avez pas assez de droits pour vous rendre dans le gestionnaire de donn&eacute;es<br/><br/>
	- Vous disposez du moteur de recherche<br/>
	- Vous pouvez remonter des ressources gr&acirc;ce aux formulaires accessibles dans la barre d'icones<br/>";
	exit;
}

// jusque là - système spécifique pour le SSO avec PmWiki

$pathwiki="/web/philo-labo";

$default_action='Liste aléatoire';
//$default_action='Hit parade';

define('ai_standalone',TRUE);
require('sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('ai.php');
require_once('philo-labo.php'); // fonctions spécifiques à philo-labo

// Si nouveau membre, l'ajouter à la table des membres 
if (simple_query("select membre from membres where membre='$Author'")!="$Author")
  simple_query("insert into membres (membre) values('$Author')");

$_GET[admin]=false;

$actions=array();

if ($_GET[admin])
$actions=array(
'get csv',
);

/*function form_r($nature)
  {
  return '<form enctype="multipart/form-data" method="GET">'.$nature.' <input type="hidden" name="nature" value="'.$nature.'"><input type="hidden" name="action" value="search"><input maxlength="255" size="10" name="chaine" value="" /><input type="submit" name="ai_form_submit" value="OK"/></form>';
  }*/
  
$recherches=sprintf($form_search,'?action=searchengine');

$actions=array_merge($actions,array(
'test|creephilo.php|pop',
'Accueil',
'Les 100 dernières ressources',
'Liste aléatoire',
'<div style=\'background:darkblue;color:white\'>Textes</div>',
'Rechercher des textes',
'Script-wiki pour un auteur',
'<div style=\'background:darkblue;color:white\'>Philosophèmes</div>',
'Liste des notions',
'Ajouter un philosophème',
'Classer les philosophèmes',
'Tri par philosophèmes',
'Tri par notions',
'Tri par notions alpha',
'Mise à jour Philosophèmes->Wiki',
'<div style=\'background:darkblue;color:white\'>Ajouter des ressources</div>',
'Ajouter un auteur',
//'Editer les auteurs',
'Ajouter une ressource | /Ai/AjouterRessources',
'<div style=\'background:darkblue;color:white\'>Gérer les ressources</div>',
'Ressource -> philosophèmes',
'Philosophème -> ressources',
'Ressources x philosophèmes',
'<div style=\'background:darkblue;color:white\'>Licences et droits</div>',
'licences et droits',
'<div style=\'background:darkblue;color:white\'>Structure de la base</div>',
'Structure de la base',
//'<div style=\'background:darkblue;color:white\'>Ne pas toucher - tests</div>',
//'Injecter un fichier csv|getcsv.php',
));

function add_consult($objet,$a,$b)
  {
  return "<h1>Ajouter $objet</h1>".insert2html("select $a from $objet"."s limit 1")."<h1>Liste des $objet". "s</h1>".select2html("select id,$b from $objet"."s order by $objet ASC",'h');  
  }
  
/*function ecrire_texte($id)
    {
    global $Z;
    $defp="<p align=justify style='text-indent:1cm'>";
    $t=select2html("select texte from philos where philos.id=$id",'v',$Z,array('%s','%s','%s'));
    $ar=select2html("select id_auteur,reference from philos".jn(philo,auteur)." where philos.id=$id",'h',array("<p align=right><b><big>%s</big></b><br/>%s</p>"),array('%s','%s','%s'));
    $t=$defp.str_replace("<br/>","</p>$defp",$t)."</p>";
    $fph = fopen("/web/ains-wiki/philosophemes/philos/ph$id",'w+'); //création du fichier
    fputs($fph,$t.$ar);
    fclose($fph);
    }*/
    
function ecrire_texte2($id) // attention *** adresse fausse ins ???
    {
    global $Z;
    $defp="<p align=justify style='text-indent:1cm'>";
    $t=select2html("select texte from ressources where ressources.id=$id",'v',$Z,array('%s','%s','%s'));
    $ar=select2html("select id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("<p align=right><b><big>%s</big></b><br/>%s</p>"),array('%s','%s','%s'));
    $t=$defp.str_replace("<br/>","</p>$defp",$t)."</p>";
    $fph = fopen("/web/ains-wiki/philosophemes/philos/ph$id",'w+'); //création du fichier
    fputs($fph,$t.$ar);
    fclose($fph);
    }

function getressbyp($id)
    {
    return select2html("Select ressources.id,ressource,auteur,nature from ressources,ressourcesphilosophemes,auteurs where ressourcesphilosophemes.id_ressource=ressources.id and ressourcesphilosophemes.id_philosopheme=$id and auteurs.id=ressources.id_auteur",'h'); 
    }
function getressbypj($id)
  {
  $liste=sql2array("select ressources.id from ressources,ressourcesphilosophemes where ressourcesphilosophemes.id_ressource=ressources.id and ressourcesphilosophemes.id_philosopheme=$id");
  foreach ($liste as $item)
    $result.=getitem($item[0]);
  return $result;
  }
  
function associe($id,$nn,$t1,$t2)  // t1 ressources t2 philosopheme
        {
        $tb1=$t1."s";$tb2=$t2."s";
        $sel=' selected="selected"';
        $item="<option %s value='%s'>%s</option>";
        $max=sql2array("select id,$t2 from $t2"."s",'h'); // tous les possibles
        $sql="select $tb2.id from $nn,$tb2,$tb1 where $nn.id_$t1=$tb1.id and $nn.id_$t2=$tb2.id and $tb1.id=$id"; // déjà présents
        $check=sql2seq($sql);
        //echo "max<pre>";print_r($max);
        //echo "</pre>check<pre>";print_r($check);echo "</pre>";
        $selections='';
        for ($i=1;$i<sizeof($max)+1;$i++)
            {
            if (in_array($max[$i-1][0],$check))
                $seld=$sel;
            else
                $seld="";
            $selections.=vsprintf($item,array($seld,$max[$i-1][0],$max[$i-1][1]));
            }
        return "<form id='form_insert' method='POST'><select name='$t2"."[]' class='multiselect' multiple='multiple' style='height:300px;'>".$selections."</select><center><div onclick='associe();'><b>OK</b></div></center><input type='hidden' name='$t1' value='$id'></form><div id='result'></div>";
        }
      
//$tag='<a class="alink" title="" href="ventiler.php?nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme&id=%1$s"><img src="/images/phi.png"></a>';
$tag='<a target="boulot" href="ventiler.php?nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme&id=%1$s"><img src="/images/phi.png"></a>';
$ephi=edit_().'<b>%1$s</b>'.$tag;
$voir='<a href="?action=voirRes&id=%s"><img src="/images/oeil.png"></a>';  
$getress='<a class="alink" title="%1$s" href="ress.php?id=%1$s">%1$s</a>';

switch ($action) {
  case 'Accueil':
    $body.=file_get_contents('info.html');
    break;
  case 'searchengine':
    $body=resultatrecherche(true,true,true,$_GET['json']);
    break;
  case 'get csv':
    $body.='<a href="getcsv.php">get csv</a>';
    break;
  case 'Ajouter un auteur': 
    $body.=add_consult('auteur','auteur','auteur,publicdomain');
    break;
  case 'Ajouter un item':
    $body.="<h1>Creéer un philosophème</h1>".insert2html("select * from philos limit 1",'v',array('s','A'),$Z,$Z,'?action=voir&id=last&nouveau=true');
    break;
  case 'Liste aléatoire':
    $body.="<h1>Liste aléatoire de 100 ressources</h1><h2>cliquer pour affecter des philosophèmes</h2>";
    $body.= liste_sous_condition(" order by RAND() limit 100");
    break;
  case 'Textes du domaine public':
    $body.="Il y a au moins <b>".simple_query("select count(*) from ressources".jn(ressource,auteur)." where nature like 'texte%' and auteurs.publicdomain='oui' or id_licence!=0")."</b> textes du domaine public dans la base";
    break;
  case 'voirRes':
    $id=$_GET['id'];
    if ($id=='last')
        $id=$ai_lastID;
    //if ($_GET[nouveau])
      //ecrire_texte($id);
    $body.=select2html("select * from ressources where id=$id",'v',array($ephi));
    break;
  case 'voir';
  case 'voirTexte':
    $id=$_GET['id'];
    if ($id=='last')
        $id=$ai_lastID;
    if ($_GET[nouveau])
        ecrire_texte2($id);
    $body.=select2html("select ressources.id,ressources.id,ressource,texte,id_auteur,reference,tags,notions,note from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array($ephi,'<h2><a ?action=voirTexte&id=%s>%s</a></h2>','<p align="justify">%s</p>','<p align=right><big><b>%s</b></big></br>%s</p>','tags: %s<br/>','notions:%s<br/>','note: %s<br/>'),array('%s','%s<hr/>','%s'));
  break;
  case 'voirFichier':
    $id=$_GET['id'];
    if ($id=='last')
        $id=$ai_lastID;
    $body.=select2html("select id,id,ressource,url,source,nature from ressources where id=$id",'h',array('d',"$ephi",'<h2><a ?action=voirFichier&id=%1$s>%1$s</a></h2>'));
  break;
  case 'Script-wiki pour un auteur':
  $body.='<h2>Produire le script pour quel auteur ?<h2><form enctype="multipart/form-data" method="POST" action="?action=script"><input maxlength="255" size="20" name="chaine" value="" /><input type="submit" name="ai_form_submit" value="OK"/></form>';
  break;
  case 'script':
  $auteur=$_POST['chaine'];
  $body.=select2html("Select ressources.id,ressources.ressource,ressources.id from ressources".jn(ressource,auteur)." where auteurs.auteur='$auteur' order by ressource",'h',array('h','* [x] %s ph%s'),array('%s','%s<br/>','%s'));
  break;
  case 'Rechercher des textes':
      $body.='<form enctype="multipart/form-data" method="GET"><input type="hidden" name="action" value="searchTextes"><input maxlength="255" size="20" name="chaine" value="" /><input type="submit" name="ai_form_submit" value="OK"/></form>';
      break;
  case 'Rechercher des textes (liste)':
      $body.='<form enctype="multipart/form-data" method="GET"><input type="hidden" name="action" value="search2liste"><input maxlength="255" size="20" name="chaine" value="" /><input type="submit" name="ai_form_submit" value="OK"/></form>';
      break;
  case 'searchTextes':
    $chaine=$_GET['chaine'];
    $chaine=str_replace("'","\'",$chaine);
    $body.=select2html('Select ressources.id,ressources.id,ressources.id,ressources.ressource,ressources.texte,auteurs.auteur,ressources.reference,ressources.tags,ressources.notions,ressources.note from ressources'.jn(ressource,auteur)." where concat( ressource, texte, auteur,reference, tags, notions, note ) like '%$chaine%' order by auteur",',h',array($ephi,'<big>%s</big>','<h2><a href=?action=voirTexte&id=%s>%s</a></h2>','<p align="justify">%s</p>','<p align=right><b>%s</b></br>%s</p>','tags: %s<br/>','notions:%s<br/>','note: %s<br/>'),array('%s','%s<hr/>','%s'));
    break;
  case 'search':
    $chaine=$_GET['chaine'];
    $nature=$_GET['nature'];
    $chaine=str_replace("'","\'",$chaine);
    $body.=select2html('Select ressources.id,ressources.ressource,auteurs.auteur from ressources'.jn(ressource,auteur)." where nature='$nature' and concat( ressource, texte, auteur,reference, tags, notions,annee,lieu,note ) like '%$chaine%' order by auteur",',h',array($ephi));
    break;
  case 'search2liste':
    $chaine=$_GET['chaine'];
    $chaine=str_replace("'","\'",$chaine);
    $body.=select2html('Select ressources.id,ressources.id,ressources.id,ressources.ressource,auteurs.auteur from ressources'.jn(ressource,auteur)." where concat( ressource, texte, auteur,reference, tags, notions, note ) like '%$chaine%' order by auteur",',h',array('<big>%s</big>',$voir.'<a target="boulot" href=ventiler.php?id=%1$s&nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme>%2$s</a>'));
    break;
  case 'searchRes':
    $chaine=$_GET['chaine'];
    $chaine=str_replace("'","\'",$chaine);
    $body.="<h1>Cliquez sur la ressource <br/><br/>pour lui affecter des philosophèmes</h1>";
    $body.=select2html("select id,id,ressource,nature from ressources where ressource like '%$chaine%'",'h',array('<a href="?action=voirRes&id=%s"><img src="/images/oeil.png"></a>','<a target="boulot" href=ventiler.php?id=%s&nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme>%s</a>'));
    break;
  case 'Les 100 dernières ressources':
    $body.='<h1>Les 100 dernières ressources</h1>';
    $body.=liste_sous_condition(" order by id desc limit 100");
    break;
  case 'update':
    $id=$_GET[id];
    $table=$_GET[table];
    $retour=urldecode($_SERVER['HTTP_REFERER']);
    $body.=update2html("select * from $table where id=".$id,'v',$Z,$Z,$Z,$retour);
    //ecrire_texte($id);
    break;
  case 'Classer les philosophèmes':
    $body.="<h2>Cliquez sur le philosophème <br/><br/>que vous voulez affecter à une ou plusieurs notions</h2>".select2html('select id,philosopheme from philosophemes','h',array("<a target='boulot' href='ventiler.php?nn=philosophemesnotion2s&t1=philosopheme&t2=notion2&id=%s'>%s</a>"));
    break;
  case 'Tri par philosophèmes':
    $body.=select2html("select id_philosopheme,id_notion2 from philosophemesnotion2s".jn(philosophemesnotion2,philosopheme).jn(philosophemesnotion2,notion2)." order by philosopheme",'h');
    break;
  case 'Tri par notions':
    $body.=select2html("select id_notion2,id_philosopheme from philosophemesnotion2s".jn(philosophemesnotion2,philosopheme).jn(philosophemesnotion2,notion2)." order by id_notion2",'h');
    break;
  case 'Liste des notions':
    $body.=select2html("select id,notion2 from notion2s",'h');
    break;
  case 'Tri par notions alpha':
    $body.=select2html("select id_notion2,id_philosopheme from philosophemesnotion2s".jn(philosophemesnotion2,philosopheme).jn(philosophemesnotion2,notion2)." order by notion2",'h');
    break;
  case 'ventiler':
    $id=$_GET[id];$t1=$_GET[t1];$t2=$_GET[t2];
    $body.="<h2>".simple_query("select $t1 from $t1"."s where id=".$id)."</h2>";
    $body.=associe($id,$t1."s$t2"."s",$t1,$t2);
    break;
  case 'ventilerRes':
    $id=$_GET[id];$t1='ressource';$t2='philosopheme';
    $body.="<h2>".simple_query("select $t1 from $t1"."s where id=".$id)."</h2>";
    $body.=associe($id,$t1."s$t2"."s",$t1,$t2);
    break;
  case 'Ajouter un philosophème':
    $body.="<h2>Ajouter un philosophème</h2>".insert2html("select * from philosophemes limit 1",'v').select2html("select * from philosophemes",'h','e');
    break;
  case 'Ajouter un texte':
    $body.="<h1>Ajouter un texte</h1>".insert2html("select id,ressource,texte,id_auteur,reference,tags,notions from ressources limit 1",'v','hsA',$Z,$Z,'?action=voirRes&id=last&nouveau=true');
    break;
  case 'Verser un fichier (exo,ebook)':
    $body.="<h2>Versement d'un fichier</h2><ul><li>Pour les fichiers hotpatatoes, faire remonter l'export html dans le champ 'url' et si possible le code source aussi</li><li>Pour les ebooks, le source est un fichier éditable dans un format ouvert: .odt, markdown, etc...</li><p/>";
    $body.=insert2html("select id,ressource,url,source,nature,id_licence from ressources limit 1",'v','hsFF',$Z,$Z,'?action=voirFichier&id=last&nouveau=true');
    break;
  case 'Signaler une ressource (lien)':
    $body.="<h2>Signalement d'une ressource externe</h2>";
    $body.=insert2html("select id,ressource,url,nature,id_licence from ressources limit 1",'v','hss');
    break;
  case 'Ressource -> philosophèmes':
     $body.="<h1>Ressources à affecter<br/><br/> à des philosophèmes</h1>La recherche est faite seulement sur le nom de la ressource pour l'instant";
     $body.='<form enctype="multipart/form-data" method="GET"><input type="hidden" name="action" value="searchRes"><input maxlength="255" size="20" name="chaine" value="" /><input type="submit" name="ai_form_submit" value="OK"/></form>';
      break;
  case 'Philosophème -> ressources';
    $body.="<h1>Cliquez sur le philosophème<br/><br/>sur lequel vous souhaitez des ressources</h1>".select2html("select id,philosopheme from philosophemes",'h',array('<a href="?action=searchphi&id=%s&nom=%1$s">%1$s</a>'));
    break;
  case 'Mise à jour Philosophèmes->Wiki':
    $liste=sql2array('select * from philosophemes');
    foreach ($liste as $philo)
      {
      //echo "créer le fichier Philosophemes.".$philo[1]." avec dedans !(:philosopheme $philo[0]:)\n(:rpp $philo[0]:)<br/>";
      //$philo[1]=str_replace(array('/','-','_'),array(' ',' ',' '),$philo[1]);
      $file=nomwiki($philo[1]); //(preg_replace('/[ ](\S)/','\1',ucwords($philo[1])));
      touch("$pathwiki/wiki.d/Philosophemes.$file");
      str2file("!(:philosopheme $philo[0]:)\n(:rpp $philo[0]:)","$pathwiki/import/Philosophemes.$file");
      }
    $body= "Penser à faire faire un ?action=import par un administrateur";
    //print_r($liste);
    // fabriquer la page dans import avec dedans !(:philosopheme 32:)(:rpp 32:)
    break;
  case 'Voir tous les textes':
    $body.="<h1>Ressource textuelles</h1><h2>cliquer pour affecter des philosophèmes</h2>";
    $body.=select2html("Select ressources.id,ressource,auteur from ressources".jn(ressource,auteur)." where nature='texte' order by auteur",'h',array($voir.'<a target="boulot" href=ventiler.php?id=%1$s&nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme>%2$s</a>'));
    break;
  case 'Voir les ressources non textuelles':
    $body.="<h1>Ressource non textuelles</h1><h2>cliquer pour affecter des philosophèmes</h2>";
    $body.=select2html("select id,id,ressource,nature from ressources where !(nature like '%texte%') order by nature",'h',array($voir,'<a target="boulot" href=ventiler.php?id=%s&nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme>%s</a>'),$Z,array('id','ressource','nature'));
    break;
  case 'Ressources x philosophèmes':
    $body.="<h1>Toutes les associations</h1>".array2html(sql2array("select philosopheme,ressource,nature from ressourcesphilosophemes ".jn(ressourcesphilosopheme,ressource).jn(ressourcesphilosopheme,philosopheme)." order by philosopheme",'h','h'));
    break;
  case 'searchphi':
    $id=$_GET[id];$nom=urldecode($_GET[nom]);
    $body.="<h1>$nom</h1>";
    $body.=array2html(sql2array("select ressources.id,nature,ressource,id_auteur,reference,url,id_licence from ressources,ressourcesphilosophemes where ressourcesphilosophemes.id_philosopheme=$id and ressourcesphilosophemes.id_ressource=ressources.id")); 
    break;
   case 'Natures de ressources':
    $body.=insert2html("select * from natures limit 1").select2html("select id,nature,description from natures",'h','e');
    break;
   case 'licences et droits':
    $body.=insert2html("select * from licences limit 1").select2html("select id,licence,description,documentation from licences",'h','e');
    break;
   case 'Structure de la base':
    $body.="<h1>Structure indicative cible</h1><img src='/images/database.png'><hr/>
ressources(titre,nature)<br/>
philosophemes(philosopheme)<br/>
notions(notion)<br/>
philosophemes-ressources(id_philosopheme,id_ressource)<br/>
validation(id_ressource,id_membre)<br/>
membres(membre)<br/>
contibutions(id_ressource,id_membre)<br/>
philosophemes-notions(id_philosopheme,id_notion)<br/>";
   break;
   default:
    $body.='pas encore fait :) ... ou vous avez cliqué sur un titre de rubrique';
    }

$title=str_replace('_',' ',$action);
$nb_ress=simple_query('select count(*) from ressources');
$logo="<img src='logo.png' alt='logo' /><br/>[$Author] / <b>$nb_ress</b> ressources<br/>$recherches";
$header="Philosophèmes";
$content.="<div id='centr'><p>$body</p></div>";
$ai_indent=0;

$scripts.='
<script>
function associe()
{
    $.ajax({
        url: "http://philo-labo.fr/philosophemes/associe.php",
        type: "POST",
        data: {
            ssd: "yes",
            data: $("#form_insert").serialize()
        },
        success: function(data) {
        $('."'#result').html(data);
        }
    });
}
</script>";

$scripts.='<script src="../js/jquery-1.11.3.min.js"></script>
           <script src="../js/jquery-ui.min.js"></script>';

// pour la saisie multiple
$scripts.='<script src="../ai/multiselect2/src/jquery.tree-multiselect.js"></script>
    <style>
      * {
        font-family: sans-serif;
      }
      .machin 
        {
        display:block;
        width:200px;
        }
    </style>
    <link rel="stylesheet" href="../ai/multiselect2/dist/jquery.tree-multiselect.min.css"/>
    '; 

// pour les popup 
/*
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/jquery-ui.min.js'></script>
  <link rel='stylesheet' type='text/css' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/base/jquery-ui.css'>
  */
$scripts.="
<script src='/js/jquery-1.3.2.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    }
 .ui-dialog-shadow { 
-webkit-box-shadow: 12px 12px 16px 0 #000000;
box-shadow: 12px 12px 16px 0 #000000;
 }
  .ui-dialog-titlebar {
  height:0;
  background:ivory;
  border:0;
  }
.ui-dialog-titlebar-close {
  background: url('../js/images/ui-icons_888888_256x240.png') repeat scroll -93px -128px rgba(1, 2, 0, 0);
}

  </style>
	<script type='text/javascript'>
	jq132(document).ready(function() {
		jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div></div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('title'),
					width: 800
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href'));
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";

//$content.="<a class='alink' href='/philosophemes/creephilo.php'>test</a>";
$ai_indent=false;
require('ai_avsts2.php');
echo $content;
?>
