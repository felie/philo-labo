<?php  // base de données pour philo-labo

require_once('secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici

define('ai_standalone',TRUE);
require('sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require ('ai.php');
require_once('philo-labo.php'); // fonctions spécifiques à philo-labo

$_GET[admin]=false;

$recherches=sprintf($form_search,'?action=searchengine');

$body=$recherches;

if ($_GET['action']='searchengine')
    $body.='<div class="domBtnDiv"><div id="dom_2" class="categoryDiv">'.resultatrecherche(true,true,true).'</div></div>';


$title=str_replace('_',' ',$action);
$nb_ress=simple_query('select count(*) from ressources');
$logo="<img src='logo.png' alt='logo' /><br/>[$Author] / <b>$nb_ress</b> ressources<br/>$recherches";
$header="Philosophèmes";
$content.="<div id='centr'><p>$body</p></div>";
$ai_indent=0;

$scripts.='<script src="../js/jquery-1.11.3.min.js"></script>
            <script src="../js/jquery-ui.min.js"></script>';

// pour la saisie multiple
$scripts.='<script src="../ai/multiselect2/src/jquery.tree-multiselect.js"></script>
    <style>
      * {
        font-family: sans-serif;
      }
      .machin 
        {
        display:block;
        width:200px;
        }
    </style>
    <link rel="stylesheet" href="../ai/multiselect2/dist/jquery.tree-multiselect.min.css"/>
    '; 


$scripts.="
<script src='/js/jquery-1.3.2.min.js'></script>
<script>var jq132 = jQuery.noConflict();</script>
 <script src='/js/jquery-ui-1.7.1.min.js'></script>
  <link rel='stylesheet' type='text/css' href='/js/jquery-ui.css'>
    <style>
  .ui-dialog {
    overflow:visible;
    }
 .ui-dialog-shadow { 
-webkit-box-shadow: 12px 12px 16px 0 #000000;
box-shadow: 12px 12px 16px 0 #000000;
 }
  .ui-dialog-titlebar {
  height:0;
  background:ivory;
  border:0;
  }
  </style>
	<script type='text/javascript'>
	jq132(document).ready(function() {
		jq132('.alink').each(function() {
			var \$link = jq132(this);
			var \$dialog = jq132('<div></div>')
				.dialog({
					autoOpen: false,
					title: \$link.attr('title'),
					width: 800
				});
			\$link.click(function() {
				\$dialog.dialog('open');
				\$dialog.load(\$link.attr('href'));
				jq132('.ui-dialog').addClass('ui-dialog-shadow');
				return false;
			});
		});
	});
	</script>";	

// les scripts pour ztree

$scripts.='<link rel="stylesheet" href="../ztree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="../ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="../ztree/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="../ztree/js/jquery.ztree.core.js"></script>
	<!--<script type="text/javascript" src="../ztree/js/jquery.ztree.excheck.js"></script>-->
	<script type="text/javascript" src="../ztree/js/jquery.ztree.exedit.js"></script>
        </script>
	<SCRIPT type="text/javascript">
		<!--
		var     MoveTest = {
			errorMsg: "Error!...Please drag it to the correct category!",
			curTarget: null,
			curTmpTarget: null,
			noSel: function() {
				try {
					window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
				} catch(e){}
			}, 
			dragTree2Dom: function(treeId, treeNodes) {
				return !treeNodes[0].isParent;
			},
			prevTree: function(treeId, treeNodes, targetNode) {
				return !targetNode.isParent && targetNode.parentTId == treeNodes[0].parentTId;
			},
			nextTree: function(treeId, treeNodes, targetNode) {
				return !targetNode.isParent && targetNode.parentTId == treeNodes[0].parentTId;
			},
			innerTree: function(treeId, treeNodes, targetNode) {
				return targetNode!=null && targetNode.isParent && targetNode.tId == treeNodes[0].parentTId;
			},
			dragMove: function(e, treeId, treeNodes) {
				var p = null, pId = \'dom_\' + treeNodes[0].pId;
				if (e.target.id == pId) {
					p = $(e.target);
				} else {
					p = $(e.target).parent(\'#\' + pId);
					if (!p.get(0)) {
						p = null;
					}
				}

				$(\'.domBtnDiv .active\').removeClass(\'active\');
				if (p) {
					p.addClass(\'active\');
				}
			},
			dropTree2Dom: function(e, treeId, treeNodes, targetNode, moveType) {
				var domId = "dom_" + treeNodes[0].getParentNode().id;
					var zTree = $.fn.zTree.getZTreeObj("treeDemo");
					zTree.removeNode(treeNodes[0]);

					var newDom = $("span[domId=" + treeNodes[0].id + "]");
					if (newDom.length > 0) {
						newDom.removeClass("domBtn_Disabled");
						newDom.addClass("domBtn");
					} else {
						$("#" + domId).append("<span class=\'domBtn\' domId=\'" + treeNodes[0].id + "\'>" + treeNodes[0].name + "</span>");
					}
					MoveTest.updateType();
				} 
			},
			dom2Tree: function(e, treeId, treeNode) {
				var target = MoveTest.curTarget, tmpTarget = MoveTest.curTmpTarget;
				if (!target) return;
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"), parentNode;
				if (treeNode != null && treeNode.isParent && "dom_" + treeNode.id == target.parent().attr("id")) {
					parentNode = treeNode;
				} else if (treeNode != null && !treeNode.isParent && "dom_" + treeNode.getParentNode().id == target.parent().attr("id")) {
					parentNode = treeNode.getParentNode();
				}

				if (tmpTarget) tmpTarget.remove();
				if (!!parentNode) {
					var nodes = zTree.addNodes(parentNode, {id:target.attr("domId"), name: target.text()});
					zTree.selectNode(nodes[0]);
				} else {
					target.removeClass("domBtn_Disabled");
					target.addClass("domBtn");
					alert(MoveTest.errorMsg);
				}
				MoveTest.curTarget = null;
				MoveTest.curTmpTarget = null;
			},
			updateType: function() {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
				nodes = zTree.getNodes();
				for (var i=0, l=nodes.length; i<l; i++) {
					var num = nodes[i].children ? nodes[i].children.length : 0;
					nodes[i].name = nodes[i].name.replace(/ \(.*\)/gi, "") + " (" + num + ")";
					zTree.updateNode(nodes[i]);
				}
			},
			bindDom: function() {
				$(".domBtnDiv").bind("mousedown", MoveTest.bindMouseDown);
			},
			bindMouseDown: function(e) {
				var target = e.target;
				if (target!=null && target.className=="domBtn") {
					var doc = $(document), target = $(target),
					docScrollTop = doc.scrollTop(),
					docScrollLeft = doc.scrollLeft();
					target.addClass("domBtn_Disabled");
					target.removeClass("domBtn");
					curDom = $("<span class=\'dom_tmp domBtn\'>" + target.text() + "</span>");
					curDom.appendTo("body");

					curDom.css({
						"top": (e.clientY + docScrollTop + 3) + "px",
						"left": (e.clientX + docScrollLeft + 3) + "px"
					});
					MoveTest.curTarget = target;
					MoveTest.curTmpTarget = curDom;

					doc.bind("mousemove", MoveTest.bindMouseMove);
					doc.bind("mouseup", MoveTest.bindMouseUp);
					doc.bind("selectstart", MoveTest.docSelect);
				}
				if(e.preventDefault) {
					e.preventDefault();
				}
			},
			bindMouseMove: function(e) {
				MoveTest.noSel();
				var doc = $(document), 
				docScrollTop = doc.scrollTop(),
				docScrollLeft = doc.scrollLeft(),
				tmpTarget = MoveTest.curTmpTarget;
				if (tmpTarget) {
					tmpTarget.css({
						"top": (e.clientY + docScrollTop + 3) + "px",
						"left": (e.clientX + docScrollLeft + 3) + "px"
					});
				}
				return false;
			},
			bindMouseUp: function(e) {
				var doc = $(document);
				doc.unbind("mousemove", MoveTest.bindMouseMove);
				doc.unbind("mouseup", MoveTest.bindMouseUp);
				doc.unbind("selectstart", MoveTest.docSelect);

				var target = MoveTest.curTarget, tmpTarget = MoveTest.curTmpTarget;
				if (tmpTarget) tmpTarget.remove();

				if ($(e.target).parents("#treeDemo").length == 0) {
					if (target) {
						target.removeClass("domBtn_Disabled");
						target.addClass("domBtn");
					}
					MoveTest.curTarget = null;
					MoveTest.curTmpTarget = null;
				}
			},
			bindSelect: function() {
				return false;
			}
		};

		var setting = {
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false,
				drag: {
					prev: MoveTest.prevTree,
					next: MoveTest.nextTree,
					inner: MoveTest.innerTree
				}
			},
			data: {
				keep: {
					parent: true,
					leaf: true
				},
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeDrag: MoveTest.dragTree2Dom,
				onDrop: MoveTest.dropTree2Dom,
				onDragMove: MoveTest.dragMove,
				onMouseUp: MoveTest.dom2Tree
			},
			view: {
				selectedMulti: false
			}
		};

		var zNodes =[
			{ id:1, pId:0, name:"PLANT", isParent: true, open:true},
			{ id:2, pId:0, name:"ANIMAL", isParent: true, open:true},
			{ id:20, pId:2, name:"Elephant"},
			{ id:29, pId:2, name:"Shark"},
			{ id:10, pId:1, name:"Cabbage"},
			{ id:19, pId:1, name:"Tomato"}
		];

		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			MoveTest.updateType();
			MoveTest.bindDom();
		});
	</SCRIPT>
	<style type="text/css">
.dom_line {margin:2px;border-bottom:1px gray dotted;height:1px}
.domBtnDiv {display:block;padding:2px;border:1px gray dotted;background-color:powderblue}
.categoryDiv {display:inline-block; width:335px}
.domBtn {display:inline-block;cursor:pointer;padding:2px;margin:2px 10px;border:1px gray solid;background-color:#FFE6B0}
.domBtn_Disabled {display:inline-block;cursor:default;padding:2px;margin:2px 10px;border:1px gray solid;background-color:#DFDFDF;color:#999999}
.dom_tmp {position:absolute;font-size:12px;}
.active {background-color: #93C3CF}
	</style>';

$content.='<div class="zTreeDemoBackground left">
  <ul id="treeDemo" class="ztree"></ul>
</div>';

// fin pour ztree 

$ai_indent=false;
require('ai_avsts2.php');
echo $content;
?>
