<?php

require('sql_config.php');
require('ai.php');

?>
<html>
  <head>
    <title>Remontée d'un fichier csv vers la base de donnée</title>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  </head>
  <body>
  <h1>Remontée d'un fichier csv vers la base de donnée</h1>
  <!-- Le type d'encodage des données, enctype, DOIT être spécifié comme ce qui suit -->
<form enctype="multipart/form-data" action="csv2sql.php" method="post">
  <!-- MAX_FILE_SIZE doit précéder le champ input de type file -->
  <h2>1. Indiquez la cible</h2> Attention, la première ligne sera considérée comme contenant des données.<br/>
  Table cible : 
  <? echo listbox(sql2list('SHOW TABLES'),'table'); ?>
  <br/>
  Nouvelle table ? Indiquez le nom souhaité
  <input type name='newtable' type='text' > (les champs seront les en-têtes du fichier csv)
  <h2>2. Indiquez la source</h2>
  <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
  <!-- Le nom de l'élément input détermine le nom dans le tableau $_FILES -->
  Sélectionnez le fichier : <input name="userfile" type="file" /><br/>
  <h2>3. Exécutez la remontée</h2>
  <input type="submit" value="Envoyer le fichier" />
</form>
  </body>
</html>