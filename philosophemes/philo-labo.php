<?php

/* fonctions qui sont partagées entre le wiki et la base de donnée
   seules les ressouces du domaine public peuvent être affichées dans la zone publique
*/

include_once('/web/philo-labo/cookbook/detect_mobile.php');
include_once('/web/philo-labo/composition/ressources.php');

// function nomwiki($i)
//   {
//   $i=str_replace(array('(',')','"',"'",'/','-','_'),array('','','',"",' ',' ',' '),$i);
//   return preg_replace('/[ ](\S)/','\1',ucwords($i));
//   }

// enlève la peau des patates
function economiseur($t)
  {
  $texte=explode("\n",$t);
  $avale=true;
  $result='';
  foreach ($texte as $ligne)
    {
    if( strstr($ligne, "<!-- BeginTopNavButtons -->")) $avale=false; 
    if( strstr($ligne, "<!-- EndTopNavButtons -->")) $avale=true; 
    if( strstr($ligne, "<!-- BeginBottomNavButtons -->")) $avale=false; 
    if( strstr($ligne, "<!-- EndBottomNavButtons -->")) $avale=true; 
    if ($avale)
      $result.=$ligne;
    }
  return $result;
  }

// formulaire de recherche multicritères
$form_search='<div class="machin">
  <form method="POST" action="%s">
      <input name="chaine" value="" size="30">
      <input type="submit" style="display:inline" name="" value="OK"/>
      <select name="nature[]" id="test-select" multiple="multiple">
        <option selected value="pdf" data-section="Tout/ebooks">pdf</option>
        <option selected value="texte" data-section="Tout/textes">textes</option>
        <option selected value="image" data-section="Tout/Images">images</option>
        <option selected value="graphe" data-section="Tout/Images">graphe</option>
        <option selected value="graphique" data-section="Tout/Images">graphique</option>
        <option selected value="JCross" data-section="Tout/Exercices/hotpatatoes">JCross</option>
        <option selected value="JQuizz" data-section="Tout/Exercices/hotpatatoes">JQuizz</option>
        <option selected value="JCloze" data-section="Tout/Exercices/hotpatatoes">JCloze</option>
        <option selected value="JMix" data-section="Tout/Exercices/hotpatatoes">JMix</option>
        <option selected value="JMatch" data-section="Tout/Exercices/hotpatatoes">JMatch</option>
        <option selected value="document" data-section="Tout/ebooks">documents</option>
        <option selected value="epub" data-section="Tout/ebooks">epub</option>
        <option selected value="sujet-question" data-section="Tout">sujets-question</option>
        <option selected value="sujet-texte" data-section="Tout/textes">sujets-texte</option>
        <option selected value="LearningsApps" data-section="Tout/Exercices">LearningsApps</option>
        <option selected value="youtube" data-section="Tout/Multimedia/Video">youtube</option>
        <option selected value="dailymotion" data-section="Tout/Multimedia/Video">dailymotion</option>
        <option selected value="vimeo" data-section="Tout/Multimedia/Video">vimeo</option>
        <option selected value="mp4" data-section="Tout/Multimedia/Video">mp4</option>
        <option selected value="mp3" data-section="Tout/Multimedia">mp3</option>
        <option selected value="Quizz" data-section="Tout/Exercices">Quizz</option>
        <option selected value="composite" data-section="Tout" data-description="Ressources composées de ressources">composite</option>
        <option selected value="h5p" data-section="Tout/Exercices" data-description="Ressources h5p">H5P</option>
      </select>
  </form>
    <script type="text/javascript">
      $("#test-select").treeMultiselect({ enableSelectAll: false, sortable: true, hideSidePanel:true, startCollapsed:true  });
    </script>
  </div>';
     
$numdrag="<div draggable='true' ondragstart='drag(event)' id='%1\$s'>%1\$s</div>";
$lienpop2='<a href="#" onClick="openDialog(\'%s\')">%s</a>';
$lienpop="<a class='alink' href='/philosophemes/ress.php?id=%s'>%s</a>";
//$tag='<a target="boulot" href="ventiler.php?nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme&id=%1$s"><img src="/images/phi.png"></a>';
$tag='<a class="alink" title="" href="/philosophemes/ventiler.php?nn=ressourcesphilosophemes&t1=ressource&t2=philosopheme&id=%1$s"><img src="/images/phi.png"></a>';
$ephi=edit_().'<b>%1$s</b>'.$tag;
$edit="<a href=\"?action=update&table={table}&amp;id=%1\$s\"><img border=0 alt=\"update\" src=\"images/edit.png\"/></a>";
$getress='<a href="#" onClick="openDialog(\'%1$s\',\'/philosophemes/ress.php?id=%1$s\')" onMouseover="openDialog(\'%1$s\',\'/philosophemes/ress.php?id=%1$s\')>%1$s</a>';
//$getress='<a class="alink" title="%1$s" href="ress.php?id=%1$s">%1$s</a>';

function liste_sous_condition($condition)
  {
  global $getress,$tag;
  return select2html('Select ressources.id,ressources.id,ressources.id,ressources.id,ressources.id,ressources.ressource,auteurs.auteur,ressources.nature,membres.membre from ressources'.jn(ressource,auteur).jn(ressource,membre).$condition,',h',array('d',edit_(),$getress,$tag),array("<div><table class='t' id='tire-specs'>%s</table></div>","<tr>%s</tr>","<td>%s</td>"));
  }
  
/*
function jolitexte($t) // fait le découpage en paragraphe
  {
  $t="<p>$t</p>";
  $t=str_replace("\n\n","</p><p>",$t);
  return $t;
  }*/
  
 function resultatrecherche($_edit=false,$_delete=false,$_tag=false,$json=false)
  {  
  global $_POST,$lienpop,$numdrag,$tag,$ephi,$edit,$href_del,$Z;
  if (!isset($_POST[chaine]))
    return '';
  $chaine=$_POST[chaine];
  $body="$chaine</br>";
  $chaine=str_replace("'","\"",$chaine);
  $chaine=str_replace('"',"\"",$chaine);
  $chaine='"" '.$chaine;
  $chaine=explode('"',$chaine);
  //echo "<pre>";print_r($chaine);echo "</pre>";
  $like='';   
  foreach ($chaine as $c)
    {
    if ($c!='')
      {
      if ($c[0]==' ')
        {
        $c=explode(' ',$c);
        //print_r($c);
        foreach ($c as $cc)
          {
          if ($cc!='')
            {
            if (ctype_digit($cc))
	      $like.="and ((ressources.id=$cc) or (annee=$cc))";
	    else
            //echo " **$cc** ";
	      $like.="and concat(ressources.id,' ',ressource,' ',texte,' ',auteur,' ',reference,' ',tags,' ',notions,' ',note,' ',lieu,' ',annee)  like '%$cc%'";
            }
           }
        }
      else
        {
        //echo " *$c* ";
        if (ctype_digit($c))
	  $like.="and ((ressources.id=$c) or (annee=$c))";
	else
	  $like.="and concat(ressources.id,' ',ressource,' ',texte,' ',auteur,' ',reference,' ',tags,' ',notions,' ',note,' ',lieu,' ',annee)   like '%$c%'";
        }
      }
    }
  $nature=$_POST[nature];
  if (sizeof($nature)>0)
    {
    foreach ($nature as &$n)
      $n="'$n'";
    $options=implode(',',$nature);
    $chaine=str_replace("'","\'",$chaine);
    //print_r($options);
    $masksup='<table style="border-width: 0px;"><tr style="border-width: 0px;">';
    if ($_delete) $masksup.="<td style='border-width: 0px;'>$href_del</td>";
    if ($_edit) $masksup.="<td style='border-width: 0px;'>$edit</td>";
    if ($_tag) $masksup.="<td style='border-width: 0px;'>$tag</td>";
    $masksup.="<td style='cursor:move;border-width: 0px;'>$numdrag</td>";
    $masksup.='</tr></table>';
    $condition=" where nature IN ($options) $like order by auteur";
    
    if (!$json)	
      {
      $q="Select ressources.id,ressources.id,ressources.nature,ressources.id,ressources.ressource,auteurs.auteur from ressources".jn(ressource,auteur)."$condition";
      $body.=select2html($q,'h',array($masksup,$tag,'<img src="/images/iconp/%s.png">',$lienpop,'%s'),$Z,array('id','phi','nat','ressource','auteur'));
      }
    else
      {
      $q="Select ressources.id,ressources.ressource,auteurs.auteur,ressources.nature from ressources".jn(ressource,auteur)."$condition";
      $body=select2html($q,'h',array("{ id:xxx, pId:0, name:~~~%s %s - %s~~~,icon:~~~/images/iconp/%s.png~~~},"),array('%s','%s','%s'));
      $body=str_replace('"',"''",$body); // protéger les guillemets pour aviter que ça perturbe le json
      $nombre=0;
      $body=str_replace('~~~','"',$body,$nombre);
      $body='{ id:xxx, pId:0, name:"il y a '.($nombre/4).' résultats",icon:""},'.$body;
      $body=str_replace('- ",icon','",icon',$body); // bidouille pour éviter le - quand l'auteur est vide
      $body=incremental($body);
      //file_put_contents("/web/philo-labo/users/$Author/compositeur/nombre_resultats",$nombre);
      }
    }
  else
    $body.="aucune catégorie de ressource sélectionnée";
  return $body;
  }

function incremental($line)
  {
  global $cle;
  $cle=0;
  return preg_replace_callback(
	'|xxx|',
	function ($matches) {
                global $cle;
                $cle=$cle+1;
		return $cle;
	},
	$line
        );
  }
/*  
$youtube='<iframe class="youtube-player" type="text/html" width="640" height="385"
  src="https://www.youtube.com/embed/%s" frameborder="0">
</iframe>';
*/
$youtube='<iframe class="youtube-player" type="text/html" width="640" height="385"
  src="%s" frameborder="0">
</iframe>';

function getitem($id,$relax=1)
    {
    return item_html($id,$relax);
    }

function montre($i,$j)
  {
  echo "$i $j<br/>";
  }

// 'hotpatatoes  Jcross','hotpatatoes  JQuizz','hotpatatoes  JCloze','hotpatatoes  JMix','hotpatatoes  JMatch','document','epub','pdf','texte','sujet-question','sujet-texte','LearningsApps','youtube','dailymotion','vimeo','mp4','mp3'

// affichage dans le wiki

// function getitem($id,$relax=1) // adhoc mais généralisable
//     {
// echo $Author;
//     global $Author,$youtube;
//     $server='https://'.$_SERVER['HTTP_HOST']; //"https://philo-labo.fr";
//     //echo "Author=$Author";
//     if ($relax or ($Author!='') or (simple_query('select id_licence from ressources where id='.$id)!=0)) 
//       {
//       $nature=simple_query("select nature from ressources where id=$id");
//       $questions='';
//       switch ($nature){
//           case 'sujet-texte':
//             $questions=simple_query("select questions from ressources where id=$id");
//             $questions=str_replace('QUESTIONS','QUESTIONS',$questions); 
//             $questions=preg_replace('/(.°)/','<p/>\1',$questions);
//             $questions=preg_replace('/ (.\))/','<p/>\1',$questions);
//             // continue avec le chargement du texte
//           case 'texte':
//             $texte=select2html("select texte from ressources where id=$id",'h',array("<div class='texte'><p>%s</p>"),array('%s','%s','%s'));
//             $refs=select2html("select id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("<div class='auteur'>%s</div><div class='reference'>%s</div></div>"),array('%s','%s','%s'));
//             $texte=str_replace("\n",'</p><p>',$texte);
//             return "$texte$refs$questions";
//             break;
//           case 'sujet-texte':
//             $texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("<div class='texte'>%s<div class='auteur'>%s</div><div class='reference'>%s</div></div>"),array('%s','%s','%s'));
//             $questions=simple_query("select questions from ressources where id=$id");
//             $questions=str_replace('QUESTIONS','QUESTIONS',$questions); 
//             $questions=preg_replace('/(.°)/','<p/>\1',$questions);
//             $questions=preg_replace('/ (.\))/','<p/>\1',$questions);
//             $texte=str_replace("\n",'</p><p>',$texte);
//             return $texte.$questions;
//             break;
//           case 'composite':
//           case 'Quizz':
//             return select2html("select texte from ressources where id=$id",'h',$Z,array('%s','%s','%s'));
//             break;
//           case 'sujet-question':
//             return simple_query('select ressource from ressources where id='.$id);
//             break;
//           case 'image':
//             return '<img src="'.simple_query('select url from ressources where id='.$id).'"/>';
//             break;
//           
//           case 'Jcross':
//           case 'JQuizz':
//           case 'JCloze':
//           case 'JMix':
//           case 'JMatch':
//             $nature="hotpatatoes";
//           case 'LearningApps':
//             return "[=<a class='alink' href='/philosophemes/ress.php?id=$id'><img src='/images/icons/$nature.png'></a>=]";
//             break;
// 	  case 'pdf':
// 		$ressource=simple_query('select ressource from ressources where id='.$id);
//                 $filename=simple_query('select url from ressources where id='.$id); // récupération de l'adresse du fichier pdf
//                 $url_download = $filename;//str_replace(' ',"\ ",$filename);
//                 return "<a href='$url_download'><img src='/images/icons/pdf.png'>$ressource</a>";
//                 break;
//           case 'document':
//           case 'epub':
//             return '<a href="'.simple_query('select url from ressources where id='.$id)."\"><img src='/images/icons/$nature.png'></a>";
//             break;
//           case 'vimeo':
//             $url=simple_query('select url from ressources where id='.$id);
//             $VIDEO_ID=str_replace('https://vimeo.com/','',$url);
//             return '<iframe src="//player.vimeo.com/video/'.$VIDEO_ID.'?portrait=0&color=333" width="480" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
//             break;
//           case 'mp4':
//             $url=simple_query('select url from ressources where id='.$id);
// 	    if ($url[0]=="/") // fichier local	
// 		$url=substr($url,0,strlen($url)-4);
//             return "(:html5video filename='$url':)";
// 	/*	}
// 	    else
// 		{
// 		echo $url;
// 		return "<video controls src='$url'>Echec de lecture</video>";
// 		}*/
//             break;
//           case 'mp3':
//             return "(:html5audio ".simple_query('select url from ressources where id='.$id)." :)";
//             break;
//           case 'dailymotion':
//             $url=simple_query('select url from ressources where id='.$id);
//             $code=str_replace('http://dai.ly/','',$url);
//             return '<iframe frameborder="0" width="480" height="360"  src="//www.dailymotion.com/embed/video/'.$code.'">allowfullscreen</iframe>';
//              break;
//           case 'youtube':
//             $url=simple_query('select url from ressources where id='.$id);
// 	    $tablo=explode('/',$url); 
//             $youtube_ID=array_pop($tablo);
// 	    return '<iframe class="youtube-player" type="text/html" width="640" height="385" src="//www.youtube.com/embed/'.$youtube_ID.'" frameborder="0"></iframe>';
//             break;
//           case 'h5p':
//             $numero=simple_query("select reference from ressources where id=$id");
//             return "<iframe src='https://h5p.org/h5p/embed/$numero' width='490' height='231' frameborder='0'>
//             </iframe><script src='https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js' charset='UTF-8'></script>";
//             break;
//           default:
//           return "[Ressource n°$i de nature <b>$nature</b> non encore affichable getitem]";
//           }
//       }
//     else
//       return '[droits sur la ressource non vérifiés]';
//     }
//     
function getitemt($id,$relax=0) // adhoc mais généralisable
    {
      $titre=simple_query("select ressource from ressources where id=$id");
      return "<h4>$titre</h4>".getitem($id,$relax);
    }

function getitemtt($id,$relax=0) // adhoc mais généralisable
    {
      return simple_query("select ressource from ressources where id=$id");
    }


function getitemf($id)
  {
  return("<iframe width=100% height=600 frameborder=0 src='/philosophemes/texte.php?id=$id'></iframe>");
  }
  
function getitemtf($id) // adhoc mais généralisable
    {
      $titre=simple_query("select ressource from ressources where id=$id");
      return "<big>$titre</big><p/><iframe width=95% height=600 frameborder=3 src='/philosophemes/texte.php?id=$id'></iframe>";
    }
    
function getitemx($id,$relax=1)
    {
    return item_html($id,$relax);
    }
  
function getitem4m2b($id, $titre=false,$long=false) // adhoc mais généralisable
    {
    global $Author,$youtube;
    $titre='';
    if ($titre)
      $titre="### ".simple_query("select ressource from ressources where id=$id")."\n\n";
    $server='https://'.$_SERVER['HTTP_HOST']; //"https://philo-labo.fr";
    //echo "Author=$Author";
      $nature=simple_query("select nature from ressources where id=$id");
      switch ($nature){
          case 'image':$url=simple_query('select url from ressources where id='.$id);
			return "imgp(".$url.")\n\n";	
          case 'texte':
	    if ($long)	
 		$texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("ext_pdf($id pages=-,nup=1,scale=1<SDL>%s<SDL>>%s *%s*<SDL>)ext<SDL>"),array('%s','%s','%s'));
	    else
                {
            	$texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("t(%s<SDL>%s *%s*)t <SDL>"),array('%s','%s','%s'));
                $texte=str_replace('<SDL>',"\n",$texte);
                }
            return $titre.$texte."\n\n";
            break;
       }
   }  

// reçoit un tableau de numero de ressources textes, la taille de la fonte, la hauteur admise
// renvoie un tableau d'images

// règle 1: si un texte est déjà présent, un autre ne pourra s'ajouter à une case que s'il est complet
// règle 2: si une ressource n'est pas un texte, elle est renvoyée dans une case

function aff_tab($var)
{   
    echo "<pre>";
    $output=print_r($var,true);
    echo htmlspecialchars($output);
    echo "</pre>";
} 
  
// function aff_tab($var, $return = false) {
//   $r = aff_tab(htmlspecialchars(print_r($var, true)));
//   if ($return) return $r;
//   else echo $r;
// }

function split_text($texte,$n)
  {
  global $Author;
  // écrit texte dans un répertoire utilisateur
$texte=str_replace(array(' ;',' ?',' !',' :'),array(';','?','!',':'),$texte);
//$texte=preg_replace('/([^\ ])\"/',"$1\"",$texte);
//$texte=preg_replace('/(\"[^\ ])/',"\"$1",$texte);
$tex='
\documentclass[a5paper]{article}
\usepackage{setspace}
\usepackage{lmodern}
\usepackage{times}


\usepackage[landscape,left=0cm,right=0cm,top=0cm,bottom=0cm]{geometry}% pour les marges

\usepackage[utf8]{inputenc}% pour écrire en utf8 dans les fichiers
\usepackage[T1]{fontenc}% pour les accents dans les pdf
\usepackage{aeguill}
\usepackage{needspace}

\setlength{\parindent}{2cm}
\setlength{\parskip}{-1cm}
%\renewcommand{\baselinestretch}{1.2}


\thispagestyle{empty} % pas de numérotation de page
\usepackage{nopageno}

\newcommand{\auteur}[1]{\nopagebreak[4]\\\\\vspace{40pt}\hfill\rmfamily\Huge\textsc{#1}}
\newcommand{\texte}[1]{\fontsize{24}{28}\rmfamily{#1}\par}%
\newcommand{\reference}[1]{\nopagebreak[4]\par\vspace{30pt}\hfill\rmfamily\Huge{\textit{#1}}}

\clubpenalty=9996
\widowpenalty=9999
\brokenpenalty=4991
\predisplaypenalty=10000
\postdisplaypenalty=1549
\displaywidowpenalty=1602
\usepackage[all]{nowidow}


\begin{document}
'
.$texte
.'
\end{document}
';
$rep="/web/philo-labo/users/$Author";
file_put_contents("$rep/split.tex",$tex);
  // y execute latex dessus
// echo 'before latex<br/>';
exec("cd $rep;latex --interaction=nonstopmode split 2>&1",$output);
//printr($output);
$output='';
  // y execute dvinpg
// echo 'before dvipng<br/>';
//exec("cd $rep;rm texte$n*");
exec("cd $rep;dvipng split -D 300 -bg Transparent -o texte$n-%d.png 2>&1",$output);
//printr($output);
// echo 'after dvipng<br>';
// echo "le numéro du texte est $n<br/>";
  // retourne la liste des images
  $i=1;
  $res=array();
  while (file_exists("$rep/texte$n-$i.png")){
     $res[]="users/$Author/texte$n-$i.png";
     $i+=1;
     }
  return $res; // renvoie un tableau d'images
  }
  
function splitter($x) // liste de ressources
  {
//   print_r($x);
//   echo "<br/>";
  foreach ($x as $o) // vérifier s'il n'y a que des textes
    {
    $nature=simple_query("select nature from ressources where id=$o");
    if ($nature!='texte')
      return ""; // il y a une ressource qui n'est pas un texte, on ne splitte pas
    }
  $img=array();
  foreach ($x as $o)
    {
    $texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$o",'h',array("\\texte{%s}\\auteur{%s}\\reference{%s}"),array('%s','%s','%s'));
    $a=split_text($texte,$o);
    $img[]=$a;
    }
  return $img; //renvoie un tableau de tableau d'images
  }
  


?>
