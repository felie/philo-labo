<html>
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<LINK rel="stylesheet" type="text/css" href="ai.css">
</head>
<body>

<?php

// todo: avec les type en ligne deux ? pour construire la table avec les types de champs directement ?

require('sql_config.php');
require('ai.php');
//print_r($_POST);
//print_r($_FILES);
$target_dir = "$ai_base/upload/";

$target_file=getfilefromform('userfile');

$data=csv2array($target_file);
$nbr_chps=sizeof($data[0]); // nombre de champs  présent dans lecvs

//print_r($_POST);

if ($_POST['newtable']!='') 
  {
  // récupération des en-tête du fichier csv
  $table=$_POST['newtable'];
  echo "création de la table <b>$table</b> avec les champs suivants:";
  // création de la table
  $champ=array();
  printr($data[0]);
  $query="CREATE TABLE IF NOT EXISTS $table (\nid int NOT NULL auto_increment,\nprimary key (id),\n";
  for ($i=0;$i<sizeof($data[0]);$i++)
    $champ[]=$data[0][$i]." tinytext";
  $query.=implode($champ,",\n")."\n)";
  //echo "<code>".$query."<code>";
  db_query($query);
  }
else  
  {
  $fields=$data[0]; // c'est ce qui est sélectionné ?
  $table=$_POST['table'][0];
  }

echo "<pre>";print_r($fields);echo "</pre>";
  
echo "remontée des données dans la table <b>$table</b><br/>";

//$fields=sql2list("show fields from ".$table);

//printr($fields);

//echo "nombre de champs dans le fichier csv : $nbr_chps<br/>";
echo "Nombre de champs concernés dans la table de la base de données : ".(sizeof($fields)-1).".<br/>"; //???
echo "Nombre de lignes dans le fichier remonté : ".sizeof($data).".<br/>";

if ($nbr_chps>sizeof($fields)-1)
  echo 'ATTENTION, nombre de champs du fichier csv supérieur au nombre de champs dans la table';

//$field_name=array();
//for ($i=1;$i<=$nbr_chps;$i++) // on récupére les premiers noms de champs sauf le id!
//  $field_name[$i-1].=$fields[$i][0];
//printr($field_name);

$field_name=$fields;

echo "Outre <b>$table</b>, les tables affectées sont: ";

if ($_POST['newtable']=='')
  $begin=0;
else 
  $begin=1;

for ($i=$begin;$i<sizeof($field_name);$i++)
  if (substr($field_name[$i],0,3)=='id_') 
    {
    //echo 'traitement du champ pointeur '.$field_name[$i].' pour récupérer/ajouter les index<br/>';
    $chp=target_table($field_name[$i]);
    echo "<b>$chp</b> ";
    //echo "table cible où récupérer les index: $chp"."s<br/>";
    for ($j=0;$j<sizeof($data);$j++) // pour chaque ligne de ce champ qui pointe sur une autre table
      $data[$j][$i]=id_of($data[$j][$i],$chp,true); // on remplace par l'id (en le créant si nécessaire
    }
     
//printr($data);

// injection des données dans la table

//print_r($field_name);

array2sql($table,$field_name,$data,'insert',1); 

echo '<br/>La relecture des données donne:<div class="t">'.select2html("select * from $table",'h').'</div>';

?>
</body>
</html>