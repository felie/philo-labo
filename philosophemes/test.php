  <!DOCTYPE html5>
<html>
  <head>
    <title>Tree Multiselect test</title>
    <meta charset="UTF-8">
    <script src="./lib/jquery-1.11.3.min.js"></script>
    <script src="./lib/jquery-ui.min.js"></script>
    <script src="../src/jquery.tree-multiselect.js"></script>

    <style>
      * {
        font-family: sans-serif;
      }
      .machin 
        {
        display:block;
        width:200px;
        }
    </style>
    <link rel="stylesheet" href="../dist/jquery.tree-multiselect.min.css">
  </head>
  
<?php
$numtexte=$_GET[id];
require_once('sql_config.php');
require_once('ai.php');
//require_once('philo-labo.php');
//$ai_debug=true;

$result = sql2array("SHOW COLUMNS FROM ressources LIKE 'nature'");
$result=$result[0][1];
if ($result) {
    $option_array = explode("','",preg_replace("/(enum|set)\('(.+?)'\)/","\\2", $result));
}
foreach ($option_array as $i)
  echo "$i.<br/>";

?>

<body>
    <div class='machin'>
    <select id="test-select" multiple="multiple">
      <option value="blueberry" data-section="Smoothies">Blueberry</option>
      <option value="strawberry" data-section="Smoothies">Strawberries</option>
      <option value="peach" data-section="Smoothies">Peach</option>
      <option value="milk tea" data-section="Smoothies/Bubble Tea">Milk Tea</option>
      <option value="green apple" data-section="Smoothies/Bubble Tea">Green Apple</option>
      <option value="passion fruit" data-section="Smoothies/Bubble Tea" data-description="The greatest flavor" selected="selected">Passion Fruit</option>
    </select>
    </div>

    <script type="text/javascript">
      $("#test-select").treeMultiselect({ enableSelectAll: true, sortable: true, hideSidePanel:true });
    </script>
  </body>
</html>
