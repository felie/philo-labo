<?php

// reçoit des données et un nom d'un javascript, et sauvegarde les données dans ma table arbors (crée ou met à jour un document - un arbre de ressources)

require_once('../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('../philosophemes/ai.php');
require_once('arbor.php');

//nusleep (100); // au cas où une autre sauvegarde est en court

if (!isset($doc))
    $doc=$_POST['doc']; // nom du document passé
    
if (($doc)=="panier")
  $envdoc="panier_";
else 
  $envdoc="doc_";
  
$repuser="/web/philo-labo/users/$Author/compositeur";

$id_author=simple_query("select id from membres where membre='$Author'");
$base_existe=simple_query("select id from arbors where id_membre=$id_author and document='$doc'");

if ($base_existe!='' and $_POST['from']=='create')
    {
    echo "Le document existe déjà dans la base de données.";
    exit(); // le fichier existe déjà
    }

file_put_contents("$repuser/lastdoc",$doc);
if ($_POST[arbre]!='')
    $a='['.implode(',',$_POST[arbre]).']'; // données passée en json en _POST
else
    $a=file_get_contents("$repuser/$doc.json"); // chargement du doc pour enregistrement dans la base de données à partir du json produit par formulaire
$a=str_replace('▸','  ',$a); // rétablissement des espaces
//echo "contenu du json<br/>$a<br/>"; //ok
// 1 - écriture dans la base de donnée  
file_put_contents($repuser."/mouchard1",serialize($a));

$b=json2sql($a,$Author,$doc); // $b contient le $json à traiter par la suite

file_put_contents($repuser."/mouchard2",serialize($b));

// 2 - écriture dans un fichier tree
//echo json2arbor($b);
file_put_contents("$repuser/$doc.tree",json2arbor($b));
if (!$firstaccess==1)
    echo "Document <b>$doc</b> de <b>$Author</b> enregistré";
//echo "<pre>".file_get_contents("$repuser/$doc.tree")."</pre>";

// 3 - écritre d'un fichier json
//file_put_contents("$repuser/$doc.json",sql2json($Author,$doc)); // pour contrôler le fichier
//echo "Document $doc.json de $Author enregistré<br/>";

if ($recharge=='ok')
  header('Location: https://philo-labo.fr/composition/');
?>
