<?php
// va lire un document dans la base de données et le donne à manger au javascript

require_once('../philosophemes/secure.php'); // vérifie qu'on est connecté au pmwiki sinon on se fait jeter avant d'arriver ici
require_once('../philosophemes/sql_config.php'); // plusieurs bases peuvent utiliser ai sur la même machine
require_once('../philosophemes/ai.php');
require_once('arbor.php');

$share=$_GET['share'];

$repuser="/web/philo-labo/users/$Author/compositeur";
file_put_contents("$repuser/temoin",'coucou');

function nettoie($s)
    {
    $s=str_replace("\'","'",$s);
    $s=str_replace("&comma;",",",$s);
    $s=str_replace('    ','▸',$s); // surtout pour les espaces avant
    $s=str_replace('\\','\\\\',$s);
    return $s;
    }

if ($share!=1) // cas normal, on prend le répertoire utilisateur
    {
    $doc=$_GET['doc']; // nom du  document passé
    if ($doc!='panier')
        file_put_contents("$repuser/lastdoc",$doc);// enregistrement du dernier document, pour rechargement après l'exécution du moteur de recherche
    $s=sql2json($Author,$doc); // lecture dans la base de données, écriture du json pour vérification
    file_put_contents("$repuser/$doc.json",$s); // pour vérification au passage
    file_put_contents("$repuser/mouchard.json",$s); // pour vérification au passage
    echo nettoie($s);
    }
else // cas du chargement d'un document de partage
    {   
    $doc=$_GET['doc'];
    file_put_contents("$repuser/partage_actuel",$doc); // avec le repuser précédent
    $ciblesymb=readlink("/web/philo-labo/partage/$doc.tree"); // cible du lien symbolique qui se trouve dans le répertoire des documents partagés
    file_put_contents("$repuser/mouchard",$ciblesymb); // avec le repuser précédent
    $decoupe=explode("/",$ciblesymb);
    $Author=$decoupe[4];
    $decoupe=explode("] ",$doc);
    $doc=$decoupe[1];
    $s=sql2json($Author,$doc); // lecture dans la base de données, écriture du json pour vérification
    file_put_contents("$repuser/$doc.json",$s); // pour vérification au passage
    //file_put_contents("$repuser/mouchard.json",$s); // pour vérification au passage
    echo nettoie($s);
    }

?>
