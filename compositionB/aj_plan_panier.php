<?php

require_once('../philosophemes/secure.php');
require_once('../philosophemes/sql_config.php');
require_once('../philosophemes/ai.php');
require_once('arbor.php');

error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ini_set("display_errors", 1);

$doc=$_POST[doc];
$texte=$_POST[texte];
$texte=str_replace(":",'&colon;',$texte); // le tree éditable contient des : ou des virgules
$texte=str_replace(",",'&comma;',$texte);
//$texte=str_replace(":","",$texte);
$texte=str_replace("%list class=incremental%",'',$texte);
$texte=str_replace("'''","**",$texte); // gras
$texte=str_replace("''","*",$texte); // italiques
$texte=str_replace("    • ","- ",$texte);
$texte=str_replace("        ◦ ","  - ",$texte); // les copier/coller de puces
$texte=str_replace("\n!!!!!","\n  - ",$texte);
$texte=str_replace("\n!!!!","\n- ",$texte);
$texte=str_replace("\n!!!","\n### ",$texte);
$texte=str_replace("\n!!","\n## ",$texte);
$texte=str_replace("\n!","\n# ","\n$texte"); // Un grand titre au tout début du document
//$texte=str_replace("\n** ","\n  - ","$texte"); // les puces
//$texte=str_replace("\n* ","\n- ","$texte"); // les puces
/*
$texte=preg_replace_callback( // mettre les titres en minuscules
        "/# ([ a-zA-Z0-9áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ\-’'.:;§%\?!\*]*)/", // les titres
        function ($matches) 
            {
            $x=trim($matches[1]);
            $deb=$x[0]; // la première lettre
            $x=substr($x,1); // sans la première lettre
            $x=strtolower($x); // mise en minuscule
            $x=str_replace(array('Œ','É', 'È', 'Ê', 'Ë', 'À', 'Â', 'Î', 'Ï', 'Ô', 'Ù', 'Û'),array('œ','é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û'),$x); // mise en minuscule des accents
            return "# $deb$x";
            }
        ,$texte); 
*/
$recharge=$_POST[recharge];
$repuser="/web/philo-labo/users/$Author/compositeur";
$texte=str_replace("\n\n","\n",$texte);
//$texte=str_replace("'",'',$texte);
$texte=str_replace('"',"’’",$texte); // laisser les guillemets détruit les documents à cause du json pourri
$tablo=explode("\n",$texte); // convertit le texte en un tableau
$array_pere=array(); // tableau des parents
$prec_prof=0;
$array_pere[1]=null;
$lasttitre=null;
$numero=0;
$json=array();
foreach ($tablo as &$ligne)
    if (trim($ligne)!='') // bof bof
    {
    $numero++; // incremente la ligne
    //echo "$numero - $ligne<br/>";
    if ($ligne[0]=='@') // cas d'une ressource sans catégorie
      {
      preg_match('/^@(\d+)(.*)$/',$ligne, $matches);
      $ress=$matches[1];
      $title=trim($matches[2]);
      $cat="[".simple_query("select nature from ressources where id=$ress")."]"; // ajout de la catégorie
      if ($title=="");
        $title=simple_query("select ressource from ressources where id=$ress"); // ajout du titre s'il n'y en a pas
      $ligne="$cat@$ress $title";
      }
    switch ($ligne[0]) 
      {
      case '#': // élement de plan
      $ligne=str_replace("# ",'',$ligne);
      $ligne=str_replace("#",' ',$ligne);
      $prof=strlen($ligne)-strlen(ltrim($ligne))+1;
//       if ($prof<$prec_prof)
//           $array_prof[$prof]=$numero; // devient parent
//       if ($prof>$prec_prof) 
//           $array_prof[$prec_prof]=$lasttitre; //le père devient parent
      if ($prof>$prec_prof) // même s'il y en a qui sautent
           for ($i=$prec_prof+1;$i<=$prof;$i++)
             $array_pere[$i]=$lasttitre; 
      $pere="doc_".$array_pere[$prof]; // au lieu de prof-1
      if ($pere=="doc_0")
          $pere='null';
      $json[].="{\"id\":\"doc_$numero\", \"pId\":\"$pere\",\"name\":\"".trim($ligne)."\",\"icon\":\"/images/iconp/puce.png\",\"level\":\"".($prof-1)."\"}";
      $prec_prof=$prof;
      $lasttitre=$numero;
      break;
      case '[': // ressources
      preg_match('/^\[(.*)\]@(.*)/',$ligne, $matches);
      $cat=$matches[1];
      $name=$matches[2];
      $json[].="{\"id\":\"doc_$numero\",\"pId\":\"$lasttitre\",\"name\":\"".rtrim(protect_comma($name))."\",\"icon\":\"/images/iconp/$cat.png\",\"level\":\"".($prof)."\"}";
      break;
      default: // contenu de diapositive
      $json[].="{\"id\":\"doc_$numero\",\"pId\":\"$lasttitre\",\"name\":\"".rtrim(protect_comma($ligne))."\",\"icon\":\"/images/iconp/vide.png\",\"level\":\"-1\"}";
      }
    }
$le_json="[".implode(',',$json)."]";

//echo "<br/>le json $le_json<br/>";  

file_put_contents("$repuser/$doc.json",$le_json); /// **** addslashes ???
$recharge='';
require('save_doc.php'); // pour enregistrement dans la base
?>
