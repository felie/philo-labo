<?php

// Afficher les erreurs à l'écran
ini_set('display_errors', 1);
// Enregistrer les erreurs dans un fichier de log
ini_set('log_errors', 1);
// Nom du fichier qui enregistre les logs (attention aux droits à l'écriture)
ini_set('error_log', dirname(__file__) . '/log_error_php.txt');
// Afficher les erreurs et les avertissements
error_reporting(E_ERROR);

#error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);
ini_set("display_errors", 1);

// accès la base de donnée
define('ai_standalone',TRUE);
require_once("$_SERVER[DOCUMENT_ROOT]/philosophemes/sql_config.php"); // plusieurs bases peuvent utiliser ai sur la même machine
require_once("$_SERVER[DOCUMENT_ROOT]/philosophemes/ai.php");
//require_once('philo-labo2.php');
require_once('Slimdown.php');
require_once('phplatex.php');

function diaposolo($x)
    { return "### fww\n".$x.' fww '."\n### \n";}

$rl_actual='L'; // alterne droite et gauche (pour les images flottantes)
function rl()
    { 
    global $rl_actual;
    //if (preference('Numeros de lignes'))
    //    return('R');
    if ($rl_actual=='R')
      $rl_actual='L';
    else
      $rl_actual='R';
    return $rl_actual;
    }
    
    
function affimage($image,$legende,$mode="latex",$nature='')
    {
    global $unecolonne,$paysage;
    if ($mode=='latex')
        //return Beamer::render('\begin{figure}[!h]\centering\includegraphics[width=1\linewidth,height=1.5\linewidth,keepaspectratio]{/web/philo-labo'.$image.'}\captionsetup{labelformat=empty}\caption{'.$legende.'}\end{figure}');
         if ($unecolonne)
            return Beamer::render('\vspace{20pt}\begin{wrapfigure}{'.rl().'}{0.\textwidth}\centering\includegraphics[width=\linewidth,height=1.3\linewidth,keepaspectratio]{/web/philo-labo'.$image.'}\captionsetup{labelformat=empty}\caption{'.$legende.'}\end{wrapfigure}');
         else
            if ($paysage=='p')
                return Beamer::render('\vspace{20pt}\begin{wrapfigure}{'.rl().'}{0.45\linewidth}\centering\includegraphics[width=\linewidth,keepaspectratio]{/web/philo-labo'.$image.'}\captionsetup{labelformat=empty}\caption{'.$legende.'}\end{wrapfigure}');
            else
                return Beamer::render('\begin{figure}[!h]\centering\includegraphics[width=1\linewidth,keepaspectratio]{/web/philo-labo'.$image.'}\captionsetup{labelformat=empty}\caption{'.$legende.'}\end{figure}');
    else
        return "<figure><img width=600 src=\"$image?time=".filemtime("/web/philo-labo$image")."\"><figcaption>$legende</figcaption></figure>";
    }
    
function ladate($id)
    {
    return simple_query("select ladate from ressources where id=$id");  //récupération de la date de modification de la ressource
    }
    
function dialogue($id,$mode='latex')
    {
    $dial='/data/dialogue';
    $img="/web/philo-labo$dial";
    $ladate=ladate($id);
    //file_put_contents("$img/date$id",(filemtime("$img/$id.png")<$ladate));
    $legende=simple_query("select ressource from ressources where id=$id");
    if (!file_exists("$img/$id.png") or filemtime("$img/$id.png")<$ladate) // si le fichier existe et qu'il est à jour
        {
        unlink("$img/$id.png");
        $source=simple_query("select texte from ressources where id=$id");
        $source=str_replace(array("&gt;","&lt;",'&mdash;','--'),array('>','<','--','--'),$source);
        file_put_contents("$img/$id",$source);
        exec("cd $img;mscgen -T png $id 2> error"); // attention aux conflits
        }
    if (!file_exists("$img/$id.png")) // si la création du fichier image a échoué, afficher les erreurs
        return file_get_contents("$img/error");
    else
        return affimage("$dial/$id.png",$legende,$mode,'dialogue',1).$erreurs;    
    }
    
function lilypond($id,$mode='latex')
    {
    $partoches='/data/partition';
    $img="/web/philo-labo$partoches";
    $ladate=ladate($id);
    file_put_contents("$img/date$id",(filemtime("$img/$id.png")<$ladate));
    $legende=simple_query("select ressource from ressources where id=$id");
    if (!file_exists("$img/$id.png") or filemtime("$img/$id.png")<$ladate) // si le fichier existe et qu'il est à jour
        {
        unlink("$img/$id.png");
        $source=simple_query("select texte from ressources where id=$id");
        $source=str_replace(array("&gt;","&lt;",'&mdash;','--'),array('>','<','--','--'),$source);
        file_put_contents("$img/$id.ly","\\version '2.16.2'\n\header {\ntagline = \"\"  % removed\n}\n$source"); // le code lylipond dans un fichier
        exec("cd $img;lilypond --png  -dresolution=300 $id.ly;convert $id.png -bordercolor white -trim +repage -border 10x10 a$id.png;mv a$id.png $id.png");
        }
    return affimage("$partoches/$id.png",$legende,$mode,'lilypond').$erreurs;    
    }
    
function graphique($id,$mode="latex") // la première ligne indique le type du graphique, le reste ce sont les données en csv, le fichier n'est créé que s'il n'existe pas ou qu'il n'est pas à jour
    {
    global $Author;
    $imgw='/data/graphique';
    $img="/web/philo-labo$imgw";
    $gabarits='/web/philo-labo/data/gabarits_graphique';
    $ladate=ladate($id);
    $legende=simple_query("select ressource from ressources where id=$id");
    if (!file_exists("$img/$id.png") or filemtime("$img/$id.png")<$ladate) // si le fichier existe et qu'il est à jour
        {
        unlink("$img/$id.png");
        $source=simple_query("select texte from ressources where id=$id");
        //$source=str_replace( "&gt;", ">", $source); // en attendant de savoir pourquoi ça merde
        $repuser="/web/philo-labo/users/$Author/compositeur";
        $source=explode("\n",$source);
        $type=trim(array_shift($source)); // et dépile le tableau
        $source=implode("\n",$source); // $source contient le csv
        $gabarit=file_get_contents("$gabarits/$type.tex"); // récupération du document source
        if (in_array(substr($type,0,5),array('arbre','chess','go','pyram'))) // on injecte le code
             {
             if (substr($type,0,5)==='arbre') // il faut exécuter brackets
                {
                file_put_contents("$repuser/data.csv",$source); // enregistrement des données dans un fichier csv
                exec("cd $repuser;$gabarits/brackets data.csv");
                $source=file_get_contents("$repuser/data.csv");
                $source=str_replace(array('é','è','à','ù','ê'),array('\\\'e','\grave{e}','\grave{a}','\grave{u}','\^{e}'),$source);
                }
             $gabarit=str_replace("CODE",$source,$gabarit);
             }
        else
            {
            file_put_contents("$repuser/data.csv",$source); // enregistrement des données dans un fichier csv
            $gabarit=str_replace("FILE","data.csv",$gabarit);
            }
        file_put_contents("$repuser/gabarit.tex",$gabarit);
        if ($type=='pyramide' or $type=="go") // ne marche pas avec xelatex
            exec("cd $repuser;rm gabarit.pdf;xelatex --interaction=nonstopmode --shell-escape gabarit.tex;pdftk gabarit.pdf cat 1 output 1.pdf;convert -quality 100 -density 300 1.pdf gabarit.png;mv gabarit.png $img/$id.png"); // produit le graphique
        else    
            exec("cd $repuser;rm gabarit.pdf;pdflatex --interaction=nonstopmode --shell-escape gabarit.tex;pdftk gabarit.pdf cat 1 output 1.pdf;convert -quality 100 -density 300 1.pdf gabarit.png;mv gabarit.png $img/$id.png"); // produit le graphique
        }
        return affimage("$imgw/$id.png",$legende,$mode,'graphique'); 
    }

    
function getitemText($id)
    {
    return simple_query("select texte from ressources where id=$id");
    }
    
function getitemAuthor($id)
    {
    return simple_query("select auteurs.auteur from ressources ".jn(ressource,auteur)." where ressources.id=$id");
    }

function nature_ressource($id)
    {
    return simple_query("select nature from ressources where id=$id");
    }

function nom_ressource($id)
  {
  return simple_query("select ressource from ressources where id=$id");
  }

function url_abs($url)
  {
  global $_SERVER;
  $ScriptUrl = "https://".$_SERVER['HTTP_HOST'];
  if (!strstr($url, 'http://'))
    $url="$ScriptUrl/$url";
  return $url;
  }

function expandcomposite($id)
    {
    $nature=simple_query("select nature from ressources where ressources.id=$id");
    if ($nature=='composite')
        return select2html("select texte from ressources where ressources.id=$id",'h',array('%s'),array('%s','%s','%s'));
    else
        return "@$id";
    } 
  
function get_nature($id)
    {
    return simple_query("select nature from ressources where id=$id");
    }
    
function split_text_pdf($id,$filtre,$split=1) // produit un split du texte dans le répertoire de l'utilisateur /tmp/pdf$id.pdf et les range dans /splits
    {
    global $Author;
    //echo "recalcul de $id-$filtre en pdf<br/>";
    $repuser="/web/philo-labo/users/$Author/compositeur";
    $source=item_poly($id,$filtre);
    file_put_contents("$repuser/tmp/concattextes.tex",$source); // écriture de la concaténation de texte dans un fichier
    exec("cd $repuser;mkdir tmp;cd tmp;rm chunkpdf$id$filtre*;/web/philo-labo/m2b/generate_splitted_text.sh $Author chunkpdf$id$filtre;cp concattextes.tex /web/philo-labo/splits/$id$filtre.tex;mv chunkpdf$id$filtre.pdf /web/philo-labo/splits"); // traitement de ce fichier par un script dédié
    if ($split==1) // split (sinon on travaille avec le chunckpdf entier)
        exec("cd /web/philo-labo/splits;rm $id$filtre-*;pdftk chunkpdf$id$filtre.pdf burst output $id$filtre-%d.pdf"); //splitte le texte
    }
    
function split_text_dvi($id,$filtre) // produit un split du texte en un certain nombre de fichier png dans le répertoire de l'utilisateur tmp/$id%.png et les range dans /splits
    {
    global $Author;
    $splits="/web/philo-labo/splits";
    $repuser="/web/philo-labo/users/$Author/compositeur";
    $ladate=simple_query("select ladate from ressources where id=$id"); // récupération de la date de modification de la ressource
    //$compare=(filemtime("$splits/$id-1.png")<$ladate);
    //file_put_contents("/web/philo-labo/splits/date","\n$compare $ladate ".filemtime("$splits/$id-1.png"),FILE_APPEND);
    if (!file_exists("$splits/$id$filtre-1.png") or filemtime("$splits/$id$filtre-1.png")<$ladate) // si le fichier existe et qu'il est à jour  ***
    // et ne le modifie que si l'enregistrement dans la base de donnée est plus récent que le fichier (mise à jour nécessaire.
        {
        //echo "recalcul de $id-$filtre en dvi<br/>";
        $source=item_poly($id,$filtre);
        //echo "<code>$source</code>";
        file_put_contents("$repuser/tmp/concattextes.tex",$source); // écriture de la concaténation de texte dans un fichier
        exec("cd $repuser;mkdir tmp;cd tmp;rm $splits/$id$filtre-*;rm $id$filtre*;/web/philo-labo/m2b/generate_splitted_text_dvi.sh $Author $id$filtre;find . -size 90c -exec rm {} \;;cp $id$filtre*.png $splits"); // traitement de ce fichier par un script dédié on est obligé d'enlever des petites images merdiques de 90 octets qui font 1 caractères...
        }
    }
    
function item_wiki($id,$filtre='') // dans le wiki 
    {
    global $Author,$youtube;
    $server='https://'.$_SERVER['HTTP_HOST']; //"https://philo-labo.fr";
    //echo "Author=$Author";
    if ($Author!='' or (simple_query('select id_licence from ressources where id='.$id)!=0)) 
      {
      $nature=simple_query("select nature from ressources where id=$id");
      $questions='';
      switch ($nature){
          case 'sujet-texte':
            $questions=simple_query("select questions from ressources where id=$id");
            $questions=str_replace('QUESTIONS','QUESTIONS',$questions); 
            $questions=preg_replace('/(.°)/','<p/>\1',$questions);
            $questions=preg_replace('/ (.\))/','<p/>\1',$questions);
            // continue avec le chargement du texte
          case 'texte':
            $texte=select2html("select texte from ressources where id=$id",'h',array("<div class='texte'><p>%s</p>"),array('%s','%s','%s'));
            $refs=select2html("select id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("<div class='auteur'><p>%s</p></div><div class='reference'><p>%s</p></div></div>"),array('%s','%s','%s'));
            $texte=str_replace("\n",'</p><p>',$texte);
            return "$texte$refs$questions";
            break;
          case 'composite':
          case 'Quizz':
            return select2html("select texte from ressources where id=$id",'h',$Z,array('%s','%s','%s'));
            break;
          case 'sujet-question':
            return simple_query('select ressource from ressources where id='.$id);
            break;
          case 'image':
            return '<img src="'.simple_query('select url from ressources where id='.$id).'"/>';
            break;
          
          case 'Jcross':
          case 'JQuizz':
          case 'JCloze':
          case 'JMix':
          case 'JMatch':
            $nature="hotpatatoes";
          case 'LearningApps':
            return "[=<a class='alink' href='/philosophemes/ress.php?id=$id'><img src='/images/icons/$nature.png'></a>=]";
            break;
	  case 'pdf':
		$ressource=simple_query('select ressource from ressources where id='.$id);
                $filename=simple_query('select url from ressources where id='.$id); // récupération de l'adresse du fichier pdf
                $url_download = $filename;//str_replace(' ',"\ ",$filename);
                return "<a href='$url_download'><img src='/images/icons/pdf.png'>$ressource</a>";
                break;
          case 'document':
          case 'epub':
            return '<a href="'.simple_query('select url from ressources where id='.$id)."\"><img src='/images/icons/$nature.png'></a>";
            break;
          case 'vimeo':
            $url=simple_query('select url from ressources where id='.$id);
            $VIDEO_ID=str_replace('https://vimeo.com/','',$url);
            return '<iframe src="//player.vimeo.com/video/'.$VIDEO_ID.'?portrait=0&color=333" width="480" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
            break;
          case 'mp4':
            $url=simple_query('select url from ressources where id='.$id);
	    if ($url[0]=="/") // fichier local	
		$url=substr($url,0,strlen($url)-4);
            return "(:html5video filename='$url':)";
	/*	}
	    else
		{
		echo $url;
		return "<video controls src='$url'>Echec de lecture</video>";
		}*/
            break;
          case 'mp3':
            return "(:html5audio ".simple_query('select url from ressources where id='.$id)." :)";
            break;
          case 'dailymotion':
            $url=simple_query('select url from ressources where id='.$id);
            $code=str_replace('http://dai.ly/','',$url);
            return '<iframe frameborder="0" width="480" height="360"  src="//www.dailymotion.com/embed/video/'.$code.'">allowfullscreen</iframe>';
             break;
          case 'youtube':
            $youtube_ID=get_youtube_id($id);
            return '<iframe class="youtube-player" type="text/html" width="640" height="385" src="//www.youtube.com/embed/'.$youtube_ID.'" frameborder="0"></iframe>';
            break;
          case 'h5p':
            $numero=simple_query("select reference from ressources where id=$id");
            return " <iframe style='max-height:624px;scrolling:yes;' src='https://h5p.org/h5p/embed/$numero' width=100% height='624px' frameborder='0'>
            </iframe><!--<script src='https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js' charset='UTF-8'></script>-->";
            break;
          case 'graphe':
          case 'graphique':
            return item_html($id);
          default:
          return "[Ressource n°$i de nature <b>$nature</b> non encore affichable]";
          }
      }
    else
      return '[droits sur la ressource non vérifiés]';
    }
    
// affichage hors du wiki - auparavant getitemx 
function item_popup($id,$filtre='') // adhoc mais généralisable - le même pour le wiki et le compositeur
    {
    return item_html($id,$filtre);
    }

function dansfiltre($s,$filtre)    
    {
    return (strpos($filtre, $s) !== FALSE); 
    }

function item_beamer($id,$filtre='',$titre='') // diaporamas par beamer en pdf
    {
    global $Author,$Z;
    if (preference('Numeros de lignes') and !dansfiltre('n',$filtre)) // on ne va pas l'ajouter s'il y est déjà
      $filtre.='n';
    $repuser="/web/philo-labo/users/$Author/compositeur/tmp";
    $ladate=ladate($id);
    $nature=get_nature($id);
    switch ($nature)
        {
        case 'sujet-texte':
        case 'texte':
            //$texte=Beamer::render($texte); ???
            $splits="/web/philo-labo/splits";
            if ((strpos($filtre,'t') === false) or ($titre=='')) // pas d'affichage du titre
                {
                $new='';
                if (!file_exists("$splits/$id$filtre-1.pdf") or filemtime("$splits/$id$filtre-1.pdf")<$ladate)
                    split_text_pdf($id,$filtre); 
// inutile on charge le texte en une fois
                $i=1;
                $suffixe='w'; // la première diapo est centrée verticalement, les autres verticalement en haut
                while (file_exists("$splits/$id$filtre-$i.pdf"))
                    {
                    $res.="\n### fw$suffixe\n$new\includepdf[pages=-]{"."$splits/$id$filtre-$i.pdf"."}\nfw$suffixe\n### \n"; 
                    $suffixe='t';
                    $i=$i+1;
                    }
                 //$res="\n### TeXtE\n\includepdf[pages=-]{"."$splits/chunkpdf$id$filtre.pdf}\nTeXtE";
                 }
            else 
                {
                $new='';
                if (!file_exists("$splits/$id$filtre-1.png") or filemtime("$splits/$id$filtre-1.png")<$ladate)
                    split_text_dvi($id,$filtre);
                $i=1;
                while (file_exists("$splits/$id$filtre-$i.png")) 
                    {
                    $res.="\n### fww $titre\n$new\\vspace{-0.2cm}\includegraphics[height=0.75\\textwidth,keepaspectratio]{"."$splits/$id$filtre-$i.png"."}\nfww\n### \n"; 
                    $i=$i+1;
                    }
                }
            return $res;
        case 'image':
          //$legende=simple_query("select ressource from ressources where ressources.id=$id");
          $legende=$titre;
          $url=simple_query("select url from ressources where ressources.id=$id");
          if (($url[0])=='/') 
            $url="http://philo-labo.fr$url"; // cas des images locales
          exec("cd /web/philo-labo/tmp/;wget \"$url\" -O bimage$id;convert bimage$id bimage$id.png"); // téléchargement et conversion de l'image en png par sécurité
          $url="/web/philo-labo/tmp/bimage$id.png";
          if (dansfiltre('s',$filtre)) // image dans legende
            return diaposolo('\begin{figure}\begin{center}\includegraphics[height=0.95\textheight,width=0.95\textwidth,keepaspectratio]{'.$url.'}\end{center}\end{figure}');
          else
            if (dansfiltre('t',$filtre))
             return "### $legende\n".'\begin{figure}\begin{center}\includegraphics[height=0.85\textheight,width=0.95\textwidth,keepaspectratio]{'.$url.'}\end{center}\end{figure}';
            else
              return diaposolo('\begin{figure}\begin{center}\includegraphics[height=0.9\textheight,width=0.95\textwidth,keepaspectratio]{'.$url.'}\legend{\Large '.$legende.'}\end{center}\end{figure}');
        //case 'vimeo':
        case 'dailymotion':
        case 'youtube':
        case 'graphe':
	    case 'graphique':
        case 'dialogue':
        case 'lilypond':
          return diaposolo(item_poly($id,'','','1')); // largeur totale
        case vimeo:
          return select2html("select url,ressource,nature from ressources where id=$id",'v',array("\href{\detokenize{%s}}{%s \includegraphics[width=20,height=20]{/web/philo-labo/images/icons/24x24/%s.png}}"),$Z,array('','',''));
        break;
        case 'site':
	      return select2html("select url,ressource,nature from ressources where id=$id",'v',array("\href{\detokenize{%s}}{%s \includegraphics[width=20,height=20]{/web/philo-labo/images/icons/24x24/%s.png}}"),$Z,array('','',''));
	      break;
        default:
        case 'epub':
        case 'pdf':
        case 'document':
            return select2html("select url,ressource,nature from ressources where id=$id",'v',array("\href{\detokenize{https://philo-labo.fr/%s}}{%s \includegraphics[width=20,height=20]{/web/philo-labo/images/icons/24x24/%s.png}}"),$Z,array('','',''));
        default:
            return "### $nature\nCe type d'objet n'est pas encore géré pour beamer\n";
        }
    }
    
function item_in_slide($action,$id,$filtre='',$titre='') // diaporamas par beamer en pdf
    {
    global $Author,$Z;
    $nature=get_nature($id);
    switch ($nature)
        {
        case 'sujet-texte':
        case 'texte':
            return '[texte?]';
        case 'image':
          $url=simple_query("select url from ressources where ressources.id=$id");
          if (($url[0])=='/') 
            $url="http://philo-labo.fr$url"; // cas des images locales
          exec("cd /web/philo-labo/tmp/;wget \"$url\" -O bimage$id;convert bimage$id bimage$id.png"); // téléchargement et conversion de l'image en png par sécurité
          $url="/web/philo-labo/tmp/bimage$id.png";
          if ($action=='c')
            return '\includegraphics[height=\textwidth,width=\textwidth,keepaspectratio]{'.$url.'}';
          else
            return '\includegraphics[height=0.5\textwidth,width=0.5\textwidth,keepaspectratio]{'.$url.'}';
        default:
            return "[objet $id]";
        }
    }
    
function item_S5($id,$filtre='',$titre='') // diaporamas en s5 (html)
    {
    global $Author;
    $repuser="/web/philo-labo/users/$Author/compositeur/tmp";
    $nature=get_nature($id);
    switch ($nature)
        {
        case 'sujet-texte':
        case 'texte':
            split_text_dvi($id,$filtre); // divise le texte en dvi puis en png
            $res="";
            $i=1;
            while (file_exists("/web/philo-labo/splits/$id$filtre-$i.png")){
                $res.="### $titre\n<center><img width=\"600\" style='left:0;margin:0;width:600;' src='https://philo-labo.fr/splits/$id$filtre-$i.png'></center>\n";
            $i+=1;
            }
            return $res; // retourne la liste des images-texte à afficher
            break;
        case 'image':
          $texte=select2html("select url,ressource from ressources where ressources.id=$id",'h',array("<center><img height=\"500\" style=\"max-width:790;max-height:500;width:790;height:500;\" src=\"http://philo-labo.fr%s\"><center><center>%s</center>"),array('%s','%s','%s'));
          return $texte; // pour passer les dimensions d'une image
          break;
        case 'youtube':
          $youtube_ID=get_youtube_id($id);
          $debut=simple_query("select debut from ressources where id=$id");
          $fin=simple_query("select fin from ressources where id=$id");
          $ressource=simple_query("select ressource from ressources where id=$id");
          $url2="http://www.youtube.com/watch?v=$youtube_ID&feature=player_embedded&autoplay=1&loop=1&start=$debut&end=$fin";
          $url="//www.youtube.com/embed/$youtube_ID";
          //return "### $ressource\n<a target='_blank' href='$url2'>lien</a><br/><center><object class=\"youtube-player\" type=\"text/html\" width=\"800\" height=\"600\" data=\"$url\" frameborder=\"0\"></object><center>";
          return "<center><a target='_blank' href='$url2'><img src='http://img.youtube.com/vi/$youtube_ID/hqdefault.jpg'></a><br/>Clic sur l'image puis tapez f pour mettre en plein écran.<br/>Ctrl-w pour revenir.</center>";
          break;  
        default:
            return item_html($id);
        }
    }
    
function preference($pref)
    {
    global $Author;
    if ($pref=='theme')
        return simple_query("select theme from themes,membres where membre='$Author' and themes.id=id_theme");
    if ($pref=='colortheme')
        return simple_query("select colortheme from colorthemes,membres where membre='$Author' and colorthemes.id=id_colortheme");
    return simple_query("select count(*) from membres where membre='$Author' and FIND_IN_SET('$pref',preferences)>0");
    }
    
    
function item_poly_joli($id,$filtre='',$titre='') // polycopiés en pdf
    {
    global $Author,$unecolonne;
    $nature=get_nature($id);
    //echo "filtre dans item_poly=$filtre<br/>";
    switch ($nature)
        {
        case 'sujet-question':
            return '\hspace{-2cm}$\rhd$ '.simple_query("select ressource from ressources where id=$id").'\newline ';
        case 'sujet-texte': // pas de question dans les poly ???
        /*case 'texte':
          $testnumber='';
          if (preference('Numeroter les textes'))
            $textnumber='\stepcounter{textnumber}\insertbox{\circled{\arabic{textnumber}}}\vspace{-6pt}';
          $texte=select2html("select id_auteur,ressource,texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array('\addcontentsline{toc}{subsection}{[texte] %s: %s }\par %s\par\nolinenumbers\nobreak\begin{minipage}{0.5\textwidth-1.5cm}\begin{flushright}\vspace{0.3cm}\textsc{%s}\par %s\end{flushright}\end{minipage}\vspace{0.3cm}'),array("%s","%s","%s"));
          $texte='\par\noindent '.$texte;
          $texte=str_replace('<latex>','  \nolinenumbers ',$texte); // le latex sera interprété automatiquement, sans recours à php latex, évidement je laisse des espaces avant \nolinenumber pour que l'écriture du premier paragraphe s'écrive *avant* (voir if(preference)...)
          $truffe='\par';
          if (preference('Numeros de lignes') or dansfiltre('n',$filtre)) // numéros de lignes
            $truffe.='\resetlinenumber\linenumbers';
          if (preference('Numeroter les textes'))
            $truffe=$textnumber.$truffe;
          $texte=preg_replace('/\\\\par (.)/',$truffe.' $1',$texte, 1);
          //$texte=preg_replace('/\\\\par (.)/',$textnumber.'\par\resetlinenumber\linenumbers $1',$texte, 1);
          $texte=str_replace("\n","\n\n",$texte);
          $texte=str_replace("&mdash;",'\textemdash~',$texte);
          //$texte=str_replace("\\\\",'\vspace{0.5cm}\par ',$texte);
          $texte=filtrage($texte,$filtre);
          $texte=Beamer::render($texte);
          $texte=guillemets_latex($texte);
          //file_put_contents('/web/philo-labo/users/felie/compositeur/concattextes.txt',$texte); //inutile ?
          //*** $texte=str_replace('\par '."\r".'\par ','\par\leavevmode\par ',$texte); // et non \n !!! perdu 2h
          return $texte;*/
        case 'texte':
          if ($unecolonne)
            $signature='\nopagebreak[4]\par\nopagebreak[4]\nolinenumbers\begin{minipage}{\textwidth-1cm}\par\begin{flushright}\vspace{0.1cm}\textsc{%s}\par %s\end{flushright}\end{minipage}\newline ';
          else
            $signature='\nopagebreak[4]\par\nopagebreak[4]\nolinenumbers\begin{minipage}{\linewidth-1.1cm}\nolinenumbers\par\begin{flushright}\vspace{0.1cm}\textsc{%s}\par %s\end{flushright}\end{minipage}\newline ';
          $testnumber='';
          if (preference('Numeroter les textes'))
            $textnumber='\stepcounter{textnumber}\hspace{10pt}\insertbox{\circled{\arabic{textnumber}}}\nopagebreak[4]\vspace{-6pt}\nopagebreak[4]';
          $texte=select2html("select id_auteur,ressource,texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array('\addcontentsline{toc}{subsection}{[texte] %s: %s }\par %s'.$signature),array("%s","%s","%s"));
          $texte='\par\noindent '.$texte;
          $texte=str_replace('<latex>','  \nolinenumbers ',$texte); // le latex sera interprété automatiquement, sans recours à php latex, évidement je laisse des espaces avant \nolinenumber pour que l'écriture du premier paragraphe s'écrive *avant* (voir if(preference)...)
          $truffe='\par';
          if (preference('Numeros de lignes') or dansfiltre('n',$filtre)) // numéros de lignes
            $truffe.='\resetlinenumber\linenumbers';
          if (preference('Numeroter les textes'))
            $truffe=$textnumber.$truffe;
          $texte=preg_replace('/\\\\par (.)/',$truffe.' $1',$texte, 1);
          //$texte=preg_replace('/\\\\par (.)/',$textnumber.'\par\resetlinenumber\linenumbers $1',$texte, 1);
          $texte=str_replace("\n","\n\n",$texte);
          $texte=str_replace("&mdash;",'\textemdash~',$texte);
          //$texte=str_replace("\\\\",'\vspace{0.5cm}\par ',$texte);
          $texte=filtrage($texte,$filtre);
          $texte=Beamer::render($texte);
          $texte=guillemets_latex($texte);
          //file_put_contents('/web/philo-labo/users/felie/compositeur/concattextes.txt',$texte); //inutile ?
          //*** $texte=str_replace('\par '."\r".'\par ','\par\leavevmode\par ',$texte); // et non \n !!! perdu 2h
          return $texte;
        default:return item_poly($id,$filtre='',$titre='');
          }
        }
        
function includevideo($file,$url,$ressource,$largeur,$nature)
  {
  //return '\begin{figure}[!ht]\begin{center}\includegraphics[height=0.6\textwidth,width='.$largeur.'\textwidth,keepaspectratio]{'.$file.'}\captionsetup{labelformat=empty}\caption{['.$nature.'] \href{'.$url.'}{'.$ressource.'}}\end{center}\end{figure}';
  return '\begin{figure}[!ht]\begin{center}\includegraphics[height=0.6\textwidth,width='.$largeur.'\textwidth,keepaspectratio]{'.$file.'}\captionsetup{labelformat=empty}\caption{\href{'.$url.'}{'.$ressource.'}}\end{center}\end{figure}';
  }
    
function item_poly($id,$filtre='',$titre='',$largeur='0.45') // polycopiés en pdf
    {
    global $Author,$sujet;
    $nature=get_nature($id);
    //echo "filtre dans item_poly=$filtre<br/>";
    switch ($nature)
        {
        case 'sujet-question':
            return '\hspace{-2cm}$\rhd$ '.simple_query("select ressource from ressources where id=$id").'\newline ';
        case 'sujet-texte': // pas de question dans les poly ???
        case 'texte':
          $texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array('\par %s\par\nolinenumbers\nobreak\begin{minipage}{\textwidth-2cm}\begin{flushright}\vspace{0.4cm}\textsc{%s}\par %s\end{flushright}\vspace{0.6cm}\end{minipage}'),array("%s","%s","%s"));
          if (preference('Numeros de lignes') or ($sujet==1)) // faire un reset du numero de ligne après la première lettre du texte
            $texte=preg_replace('/par (.)/','par\resetlinenumber\linenumbers $1',$texte, 1);
          $texte=str_replace("\n","\n\n",$texte);
          $texte=str_replace("&mdash;",'\textemdash~',$texte);
          $texte=str_replace("\\\\",'\vspace{0.5cm}\par ',$texte);
          $texte=filtrage($texte,$filtre);
          $texte=Beamer::render($texte); // le latex n'aura pas de numero de ligne (on met du nolinenumber pour <latex> et <dlatex>)
          $texte=guillemets_latex($texte);
          $texte=preg_replace('/^#(.*)$/','\section{\1}',$texte);
          $texte=preg_replace('/^##(.*)$/','\subsection{\1}',$texte);
          $texte=preg_replace('/^###(.*)$/','\subsubsection{\1}',$texte);
          //*** $texte=str_replace('\par '."\r".'\par ','\par\leavevmode\par ',$texte); // et non \n !!! perdu 2h
          return $texte;
        case 'image':
          if (preference('Polys avec images')!=1) // préférences
            return '';
          $legende=simple_query("select ressource from ressources where ressources.id=$id");
          $url=simple_query("select url from ressources where ressources.id=$id");
          if (($url[0])=='/') 
            $url="http://philo-labo.fr/$url"; // cas des images locales
          if (preference('Images en noir et blanc dans les polys')==1)
            exec("cd /web/philo-labo/tmp/;wget \"$url\" -O image$id;convert -colorspace Gray -modulate 140 image$id image$id.png"); // téléchargement et conversion de l'image en png par sécurité
          else
            exec("cd /web/philo-labo/tmp/;wget \"$url\" -O image$id;convert image$id image$id.png");
          return affimage("/tmp/image$id",$legende,'latex','image');
        case 'youtube':
          $youtube_ID=get_youtube_id($id);
          $debut=simple_query("select debut from ressources where id=$id");
          $fin=simple_query("select fin from ressources where id=$id");
          $ressource=simple_query("select ressource from ressources where id=$id");
          $file="/web/philo-labo/data/youtube/$youtube_ID.jpg";
          if (!file_exists($file))
            exec("wget https://img.youtube.com/vi/$youtube_ID/hqdefault.jpg -O $file");
          $url="https://www.youtube.com/embed/$youtube_ID?start=$debut&end=$fin";
          if (preference('Polys avec images des videos avec liens'))
            return includevideo($file,$url,$ressource,$largeur,$nature);
          break;
        case 'dailymotion':
          $url=simple_query("select url from ressources where id=$id");
          $ressource=simple_query("select ressource from ressources where id=$id");
          $tablo=explode('/',$url);
          $dailymotion_id=array_pop($tablo);
          $file="/web/philo-labo/data/dailymotion/$dailymotion_id.jpg"; 
          if (!file_exists($file))
            exec("wget https://www.dailymotion.com/thumbnail/video/$dailymotion_id -O $file");
          return includevideo($file,$url,$ressource,$largeur,$nature);
        case 'vimeo':
          $url=simple_query("select url from ressources where id=$id");
          $ressource=simple_query("select ressource from ressources where id=$id");
          $tablo=explode('/',$url);
          $vimeo_ID=array_pop($tablo);
          $file="/web/philo-labo/data/vimeo/$vimeo_ID.jpg";
          if (!file_exists($file))
            {
            $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$vimeo_ID.php"));
            $urlfile=$hash[0]['thumbnail_large'];//            
            exec("wget $urlfile -O $file");
            }
          return includevideo($file,$url,$ressource,$largeur,$nature);
        case 'graphe':
          $nom=simple_query("select ressource from ressources where id=$id");
          $dot=simple_query("select texte from ressources where id=$id");
          $dot=str_replace( "&gt;", ">",$dot);
          $dot_filename = md5( $dot);
          $DotFiles="/web/philo-labo/data/graphs";
          if( ! file_exists( $DotFiles ."/". $dot_filename)) {
            $dot_file = fopen( $DotFiles ."/". $dot_filename.".dot","wb");
            fputs( $dot_file, "digraph toto { $dot }");
                    }  
          exec("cd $DotFiles;dot -Tpng $dot_filename.dot -o $dot_filename.png");
          $url="/data/graphs/$dot_filename.png";
          return affimage($url,$nom,'latex','graphe');
        case 'graphique':
            return graphique($id);
        case 'dialogue':
            return dialogue($id);
        case 'lilypond':
            return lilypond($id);
        case 'site':
	      return select2html("select url,ressource,nature from ressources where id=$id",'v',array("\href{\detokenize{%s}}{%s \includegraphics[width=20,height=20]{/web/philo-labo/images/icons/24x24/%s.png}}"),$Z,array('','',''));
	      break;
        }
    }

function item_markdown($id,$filtre='') // export en markdown
    {
    $nature=get_nature($id);
    switch ($nature)
        {
        case 'sujet-texte':
        case 'texte':
          $texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("<div style='text-align: justify'>%s</div>SDL**%s**SDL%sSDLSDL"),array('%s','%s','%s'));
          $texte=str_replace("SDL","\n",$texte); // vraiment utile comme ruse ?
          $texte=str_replace("Z","*",$texte); //???????
          $notes=array();
          $texte=preg_replace('/\\\note.\{([^\}]*)\}/','',$texte); // provisoire, on vire les notes marginales dans le markdown
          $questions=simple_query("select questions from ressources where id=$id");
          //if ($questions=='')
            return Slimdown::render($texte);
          $questions=str_replace('QUESTIONS','QUESTIONS',$questions);  // ???
          $questions=preg_replace('/(.°)/','<p/>\1',$questions);
          $questions=preg_replace('/ (.\))/','<p/>\1',$questions);
          return Slimdown::render($texte.$questions);
          break;
        case 'sujet-question':
        case 'Quizz':
        case 'image':
          //$url=simple_query("select url from ressources where ressources.id=$id"); // chargement de l'url de l'image
          $texte=select2html("select ressource,url,ressource,id_auteur from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("![%s](..%s \"%s %s\")"),array('%s','%s','%s'));
          return $texte."{#id .class width=540 }"; // pour passer les dimensions d'une image
          break;
        case 'Jcross':
        case 'JQuizz':
        case 'JCloze':
        case 'JMix':
        case 'JMatch':
        case 'LearningApps':
        case 'pdf':
        case 'document':
        case 'epub':
        case 'vimeo':
        case 'mp4':
        case 'mp3':
        case 'dailymotion':
          return "type $nature pas encore géré dans les exports markdown\n";
          break;
        case 'youtube':
          $youtube_ID=get_youtube_id($id);
          $ressource=simple_query("select ressource from ressources where id=$id");
          exec("wget https://img.youtube.com/vi/$youtube_ID/2.jpg -O ../fichiers/$youtube_ID.jpg");
          return "$ressource\n![](/web/philo-labo/fichiers/$youtube_ID.jpg)\n".select2html("select url from ressources where id=$id",h,array("[lien vers la vidéo](%s)"),array("%s","%s","%s"));           
          break;
        }
    }
    
$youtube='<object class="youtube-player" type="text/html" width="400" height="240" data="//www.youtube.com/embed/%s" frameborder="0"></object>';
function get_youtube_id($id)
    {
    $url=simple_query("select url from ressources where id=$id");
    $url=str_replace("www.youtube.com/watch?v=","a/",$url);
    $tablo=explode('/',$url);
    return array_pop($tablo);
    }

function guillemets_html($texte)
    {
    $texte=str_replace(chr(194).chr(160),' ',$texte); // no break space en utf8
    $stexte=str_replace('’’','"');
    $texte=preg_replace('/«( *)/','&laquo;&nbsp;',$texte);
    $texte=preg_replace('/( *)»/','&nbsp;&raquo;',$texte);
    $texte=preg_replace("/( *)([\?:;,!\.])/","$2",$texte); // \x est un caractères insécable ?
    $texte=preg_replace('/"([A-Za-zàâầäéèêếềëîïôöùûüçÀÉÊẾỀÎÏÔÖÙÛÜÇ])/',"&laquo;&nbsp;$1",$texte);
    //$texte=preg_replace("/'([A-Za-z]*)'/","&laquo;&nbsp;$1&nbsp;&raquo;",$texte);
    $texte=preg_replace('/\'"/','\'&raquo;',$texte); // guillemets français propres
    $texte=preg_replace('/"/','&nbsp;&raquo;',$texte); // guillemets français propres
    return $texte;
    }
    
function item($mode,$id,$filtre)
    {
    $function="item_".$mode;
    return $function($id,$filtre);
    }
    
function item_html($id,$filtre) // en html standard on voit les textes avec toute leurs saveurs
    {
    return item_html5($id,'cgimp');
    }
    
function filtrage($texte,$filtre)  // m=marges i=interpolation c=colorations g=gras p=pauses 
    {
    if (strpos($filtre,'m') === false)
        {
        $texte=preg_replace('/<span class=\'sidenoteleft\'>(.*)<\/span>/U','',$texte); // ► note marginale gauche
        $texte=preg_replace('/<span class=\'sidenoteright\'>(.*)<\/span>/U','',$texte); // note marginale droite
        }
        // -------------------------- filtrage des interpolations
    if (strpos($filtre,'i') === false)
        $texte=preg_replace("/<span class='interpolation'(.*)<\/span>/U",'',$texte);
        // -------------------------- filtrage des colorations
    $texte=preg_replace('/"background-color: /',"'background-color:",$texte);
    $texte=str_replace(';">',";'>",$texte);
    if (strpos($filtre,'c') === false)
        $texte=preg_replace("/<span style='background-color:(.*)'>(.*)<\/span>/U",'\2',$texte);
        // -------------------------- filtrage du gras
    if (strpos($filtre,'g') === false) 
        $texte=str_replace('**','',$texte);
        // -------------------------- filtrage des pauses
    if (strpos($filtre,'p') === false) 
        $texte=str_replace('⏹','',$texte);
    return $texte;
    }
    
function item_html5($id,$filtre='',$paper=0) // export en html ou popup
    {
    global $youtube;
    $nature=get_nature($id);
    //echo "filtre='$filtre'<br/>";
    switch ($nature)
        {
        case 'texte':
        case 'sujet-texte':
          $texte=select2html("select texte,id_auteur,reference from ressources".jn(ressource,auteur)." where ressources.id=$id",'h',array("<div class='jolitexte'><p>\n%s</p><div class='auteur'><p>%s</p></div><div class='reference'><p>%s</p></div></div>   "),array('%s','%s','%s'));
          $texte=traiter_latex($texte);
          // traitement des titres - si on met de vrais titres (h1 h2) ça fout la merdre avec les paragrahes
          $texte=str_replace("\n",'²²²',"\n$texte");
          $texte=preg_replace('/²²²# (.*?)²²²/','²²²<b><big><big>\1</big></big></b>²²²',$texte);
          $texte=preg_replace('/²²²## (.*?)²²²/','²²²<b><big>\1</big></b>²²²',$texte);
          $texte=preg_replace('/²²²### (.*?)²²²/','²²²<b>\1</b>²²²',$texte);
          $texte=str_replace('²²²',"\n",$texte);
          $texte=str_replace('--',"&mdash;",$texte); // pour les dialogues
          $texte=str_replace('\\\\','<br/>',$texte);
          $texte=str_replace("\n",'</p><p>',$texte);
          $texte=str_replace('¬','',$texte);
          $texte=str_replace('$\neq$','&ne;',$texte);
          $texte=str_replace('\asterism','<center>⁂</center>',$texte);
          //$texte=str_replace('\\\\','<br/>',$texte);
          $texte=preg_replace('/\\\jolititre\{([^\}]*)\}/','<center><big><b>\1</b>  </big></center>',$texte);
          //$texte=preg_replace('/<font color="#ff0000">([^<]*)<\/font>/','<div class=\'sidenote-left\'><div>\1</div></div>',$texte); // ► note marginale gauche
          // -------------------------- filtrage des marges
          $texte=filtrage($texte,$filtre);
          //$texte=str_replace("<p></p>",'<br/>',$texte);
          $questions=select2html("select questions from ressources where id=$id",'h',array('%s'),array('%s','%s','%s'));
          if ($questions=='')
            return guillemets_html(Slimdown::render($texte));//$texte; // str_replace("<p>","</p><p style='display:inline-block;'>",$texte)
          $questions=preg_replace('/(.°)/','<p/>\1',$questions);
          $questions=preg_replace('/ (.\))/','<p/>\1',$questions);
          $texte=str_replace("\n",'</p><p>',$texte);
          $texte=str_replace("<p></p>",'',$texte);
          //$texte=str_replace("<p>","<p/><p style='display:inline;'>",$texte); // ruse pour accepter les notes marginales
          return  Slimdown::render($texte.$questions);
	  break;
        case 'Quizz':
        case 'sujet-question':
          return select2html('select ressource,serie,lieu,annee from ressources where id='.$id,'h',array('<b>%s</b><br/><br/>série: %s<br/>lieu: %s<br/>année: %s'),array('%s','%s','%s'));
            break;
        case 'image':
          return affimage(simple_query('select url from ressources where id='.$id),simple_query('select ressource from ressources where id='.$id),'html');
          break;
        case 'Jcross':
        case 'JQuizz':
        case 'JCloze':
        case 'JMix':
        case 'JMatch':
        case 'LearningApps':
          return '<iframe width=800 style="min-height:600px;max-height:700px;scrolling:yes;" scrolling="yes"  src="'.simple_query('select url from ressources where id='.$id).'"></iframe>';
          break;
        case 'site':
            return select2html('select nature,url,ressource,texte from ressources where id='.$id,'h',array('<img src="/images/icons/24x24/%s.png"/> <a href="%s">%s</a><br/>%s'),array('%s','%s','%s'));
          break;  
        case 'pdf':
        case 'epub':
            $suffix=4;
            $ajout='';
            if ($nature=='epub')
                { 
                $suffix=5; 
                $ajout=".EPUB";
                }
            $url=simple_query('select url from ressources where id='.$id);
            $base=substr($url,0,-$suffix);
            //echo "$base<img \"$base$ajout.png\">";
            if (file_exists("/web/philo-labo/$base$ajout.png"))
                return "<a href='$url'><img height=500 src=\"$base$ajout.png\"></a>";
            else
                return '<a href="'.simple_query('select url from ressources where id='.$id)."\"><img src='/images/icons/$nature.png'></a>";
          break;
        case 'document':
          return '<a href="'.simple_query('select url from ressources where id='.$id)."\"><img src='/images/icons/$nature.png'></a>";
          break;
        case 'vimeo':
          $url=simple_query('select url from ressources where id='.$id);
          $VIDEO_ID=str_replace('https://vimeo.com/','',$url);
          return '<iframe src="//player.vimeo.com/video/'.$VIDEO_ID.'?portrait=0&color=333" width="480" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
          break;
        case 'mp4':
          $filename=simple_query('select url from ressources where id='.$id);
          $filename=substr($filename,0,strlen($filename)-4);
          return "<video width='480' height='360' controls='controls' poster='$filename.jpg'><source src='$filename.mp4' type='video/mp4' /></video>";
          break;
        case 'mp3':
          $filename=simple_query('select url from ressources where id='.$id);
          return "<audio controls='controls'source src='$filename'  type='audio/mp3'/><br/></audio>";
          break;
        case 'dailymotion':
          $url=simple_query('select url from ressources where id='.$id);
          $code=str_replace('http://dai.ly/','',$url);
          return '<iframe frameborder="0" width="480" height="360"  src="//www.dailymotion.com/embed/video/'.$code.'">allowfullscreen</iframe>';
          break;
        case 'youtube':
          $ressource=simple_query("select ressource from ressources where id=$id");
          $youtube_ID=get_youtube_id($id);
          $debut=simple_query("select debut from ressources where id=$id");
          $fin=simple_query("select fin from ressources where id=$id");
          if ($paper==0) 
            return sprintf($youtube,"$youtube_ID?rel=0&start=$debut&end=$fin");
          else 
            return "Vidéo: <a href='https://youtu.be/$youtube_ID?rel=0&start=$debut&end=$fin'>$ressource</a>";
          break;
        case 'h5p':
          $numero=simple_query("select reference from ressources where id=$id");
          return '<iframe width=800 style="min-height:600px;max-height:700px;scrolling:yes;"s scrolling="yes" src="https://h5p.org/h5p/embed/'.$numero.'"></iframe>';
          break;
        case 'graphe':
          $nom=simple_query("select ressource from ressources where id=$id");
          $dot=simple_query("select texte from ressources where id=$id");
          $dot=str_replace(array('\\"',"\\'"),array('"',"'"),$dot);
          //echo $dot;
	      $dot_filename = md5( $dot);
          $DotFiles="/web/philo-labo/data/graphs";
	     if ( ! file_exists( $DotFiles ."/". $dot_filename)) 
            {
            $dot_file = fopen( $DotFiles ."/". $dot_filename.".dot","wb");
            fputs( $dot_file, "digraph toto { $dot }");
            }  
            exec("cd $DotFiles;dot -Tpng $dot_filename.dot -o $dot_filename.png");
            return "<figure><img src='https://philo-labo.fr/data/graphs/$dot_filename.png'/><figcaption>$nom<figcaption></figure>";
        case 'graphique':
            return graphique($id,'html');
        case 'dialogue':
            return dialogue($id,'html');
        case 'lilypond':
            return lilypond($id,'html');
        default:
          return "[Ressource n°$id de nature <b>$nature</b> non encore affichable]";
        }
    }
    
function guillemets_latex($texte)
    {
    $texte=str_replace(chr(194).chr(160),' ',$texte); // no break space en utf8
    $texte=preg_replace('/«( *)/','\og ',$texte);
    $texte=preg_replace('/( *)»/','\fg{}',$texte);
    $texte=preg_replace("/( *)([\?:,;!\.])/","$2",$texte);
    $texte=preg_replace('/"([A-Za-zàâầäéèêếềëîïôöùûüçÀÉÊẾỀÎÏÔÖÙÛÜÇ])/',"\og $1",$texte);
    $texte=preg_replace("/'([A-Za-zàâầäéèêếềëîïôöùûüçÀÉÊẾỀÎÏÔÖÙÛÜÇ]*)'/",'\og $1 \\fg{}',$texte);
    $texte=preg_replace('/( *)"/','\fg{}',$texte); // guillemets français propres
    return $texte;
    }
    
?>
