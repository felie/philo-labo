<?php
//require('timer.php');
//echo temps('dans version_echo');
require('../philosophemes/secure.php');
require('version.php');

$doc=urldecode($_GET['doc']);
$format=$_GET['format'];
$partage=$_GET['partage']; // semble inutilisé

//echo "doc=$doc format=$format partage=$partage";

if ($partage=='oui')
    $repuser="/web/philo-labo/partage";
else
    $repuser="/web/philo-labo/users/$Author/compositeur";

//aiguillage si solo
if (isset($_GET['solo']))
    {
    $source=explode(" ",$doc);
    $id=array_shift($source);
    $source="@$id\n";
    //echo temps("avant le lancement d'export solo id=$id doc=$doc");
    $result=export_solo($doc,$format);
    //$result=version($source,$format);
    }
else
    {
    //echo temps("dans version_echo doc=$doc");
    $docbefore=$doc;
    $source=file_get_contents("$repuser/$docbefore.tree");
    $result=version($source,$format);
    if ($format=='html')
        $result=sprintf($header,$result);
    }
    
echo "Document <i>$doc</i> au format <b>$format</b><hr/>$result";
?>
