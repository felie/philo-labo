<?php if (!defined('PmWiki')) exit();
/**
 * set_menu_file.php
 * @author V.Krishn (vkrishn@insteps.net), Copyright 2005-2014
 * @note This file is part of Cookbook recipe "Phplm" for PmWiki;
 * You can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the  Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version. @see pmwiki.php for full details.
 * @package phplm
 */

## Set Default / Group and Page menus
  // **********************************************************
  SDV($Phplm['default']['MenuFilePath'], "uploads/Phplm/"); #not implemented
  SDV($Phplm['default']['MenuFileExt'], ".lm.txt"); #not implemented
  SDV($Phplm['default']['MenuFile'], $Phplm['Current']['dir'].'/defaultmenu.txt');

  $Phplm['menu']['PageNameFmt']['Site'] =
      $Phplm['default']['MenuPageGroup'] . '.' .
      $Phplm['default']['MenuPageName'];
  $Phplm['menu']['PageNameFmt']['Group'] =
      $Phplm['page']['CurrentGroup'] . '.' .
      $Phplm['default']['MenuPageName'];
  $Phplm['menu']['PageNameFmt']['Page'] =
      $Phplm['page']['CurrentGroup'] . '.' .
      $Phplm['page']['CurrentName'] .
      $Phplm['default']['MenuPageName'];
  $Phplm['menu']['PageNameFmt']['Default'] =
      $Phplm['default']['MenuFile']; // '.' string check
  // **********************************************************

## set menu page
  // *******************************************************
  ## site
  $Phplm['menu']['PageFmt']['Site'] =
      'phplm_get_page_content($Phplm["menu"]["PageNameFmt"]["Site"], $lmpagesection)';
  ## group
  $Phplm['menu']['PageFmt']['Group'] =
      'phplm_get_page_content($Phplm["menu"]["PageNameFmt"]["Group"], $lmpagesection)';
  ## page
  $Phplm['menu']['PageFmt']['Page'] =
      'phplm_get_page_content($Phplm["menu"]["PageNameFmt"]["Page"], $lmpagesection)';
  ## default
  $Phplm['menu']['PageFmt']['Default'] =
      'phplm_get_file_content($Phplm["menu"]["PageNameFmt"]["Default"])';
  ## text
  $Phplm['menu']['PageFmt']['Text'] =
      'phplm_set_text_content($Phplm["menu"]["PageNameFmt"]["Text"])';
  ## page list
  $Phplm['menu']['PageFmt']['Page-list'] =
      'phplm_set_pagelist_content($Phplm["menu"]["PageNameFmt"]["Page"], $lmpagesection)';
  ## group list
  $Phplm['menu']['PageFmt']['Group-list'] =
      'phplm_set_pagelist_content($Phplm["menu"]["PageNameFmt"]["Group"], $lmpagesection)';
  ## site list
  $Phplm['menu']['PageFmt']['Site-list'] =
      'phplm_set_pagelist_content($Phplm["menu"]["PageNameFmt"]["Site"], $lmpagesection)';

  // **********************************************************

  function phplm_get_menu_text($pagetype, $lmpagesection) {  //page,group,site,default,text,list
    global $Phplm, $lmPageTypes;

    $temp = ucfirst($pagetype);


    if(
       in_array($pagetype, $lmPageTypes)
         //&& PageExists($Phplm['menu']['PageNameFmt'][$temp])
      )
    {
      $Phplm['menu']['TextFmt'][$temp] = trim(eval('return '.$Phplm['menu']['PageFmt'][$temp].';'));
    }
    if( $Phplm['menu']['TextFmt'][$temp] <> '') {
      return $Phplm['menu']['TextFmt'][$temp];
    } else {
      return trim(eval('return '.$Phplm['menu']['PageFmt']['Default'].';'));
    }
    return;
  }


