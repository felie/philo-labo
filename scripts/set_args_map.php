<?php if (!defined('PmWiki')) exit();
/**
 * set_args_map.php
 * @author V.Krishn (vkrishn@insteps.net), Copyright 2005-2014
 * @note This file is part of Cookbook recipe "Phplm" for PmWiki;
 * You can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the  Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version. @see pmwiki.php for full details.
 * @package phplm
 */

// *******************************************************
SDV($DefaultlmID, 'lmMainID');
SDV($DefaultlmOrient, 'horizontal');
SDV($DefaultlmTheme, 'default');
SDV($DefaultlmPageType, 'default');
SDV($DefaultlmPageNameFmt, 'PAGE-NAME-FMT-Base'); //todo
// *******************************************************
$lmID = $DefaultlmID;
$lmOrient = $DefaultlmOrient;
$lmTheme = $DefaultlmTheme;
$lmPageType = $DefaultlmPageType;
$lmPageNameFmt = $DefaultlmPageNameFmt;

// *******************************************************
global $lmDrawParam,
       $lmDrawDefault,
       $lmOrients, $lmMenuThemes, $lmTreeThemes, $lmPageTypes;
//(:phplmDraw id orient theme pageType pageNameFormat:)
$lmDrawParam = array('lmID', 'lmOrient', 'lmTheme', 'lmPageType', 'lmPageSection', 'lmPageNameFmt');
$lmDrawDefault = compact($lmDrawParam);
SDVA($lmOrients, array('vertical', 'horizontal', 'tree'));
SDVA($lmMenuThemes, array( #Themes for Dropdown Menus
 'default' => 'default',
 'galaxy' => 'galaxy',
 'gtk2' => 'gtk2',
 'keramik' => 'keramik',
 'old' => 'old',
 ));
SDVA($lmTreeThemes, array( #Themes for Tree
 'default' => 'default',
 'galeon' => 'galeon',
 'gmc' => 'gmc',
 'kde' => 'kde',
 'mozilla' => 'mozilla',
 'nautilus' => 'nautilus',
 ));
SDVA($lmToDoThemes, array( #Themes to do
 'gnome' => 'gnome', #?? for menus # todo
 'plain' => 'plain', #?
 ));
//$lmThemes = array_merge($lmMenuThemes, $lmTreeThemes);
SDVA($lmPageTypes, array('page', 'group', 'site','default','text','page-list','group-list','site-list'));
// *******************************************************

function getlmDrawMap(&$lmDrawParams) {
  global $Phplm, $lmDrawParam,
         $lmDrawDefault,
         $lmOrients, $lmMenuThemes, $lmTreeThemes, $lmPageTypes,
         $EnablePhplmDebug;

  $args = ParseArgs($lmDrawParams);
  $count = count($args[""]);
  if($EnablePhplmDebug > 0) {
      //$Phplm['debug']['msg']['parseArgs'][] = $args;
  }

    $lmDrawValue = array();
    if ($count == '5') {
      foreach ($args[""] as $k => $v) { $lmDrawValue[$lmDrawParam[$k]] = $v; }
      foreach ($lmDrawValue as $k => $v) { $$k = $v; }
    }
    if ($count == '4') {
      foreach ($args[""] as $k => $v) { $lmDrawValue[$lmDrawParam[$k]] = $v; }
      foreach ($lmDrawValue as $k => $v) { $$k = $v; }
    }
    if($EnablePhplmDebug > 0) {
      $Phplm['debug']['msg']['lmDrawValue'][] = $lmDrawValue;
    }

    if (!preg_match('/^[a-zA-Z0-9]+$/', $lmID)) {
      $lmID = $lmDrawDefault['lmID'];
    }
    $lmOrient = (in_array($lmDrawValue['lmOrient'], $lmOrients)) ?
        $lmDrawValue['lmOrient'] : $lmDrawDefault['lmOrient'];

    $t = $lmDrawValue['lmTheme'];
    switch ($lmDrawValue['lmOrient']) {
      case 'horizontal':
        $lmTheme = array_key_exists($t, $lmMenuThemes) ? $t : $lmDrawDefault['lmTheme'];
        break;
      case 'vertical':
        $lmTheme = array_key_exists($t, $lmMenuThemes) ? $t : $lmDrawDefault['lmTheme'];
        break;
      case 'tree':
        $lmTheme = array_key_exists($t, $lmTreeThemes) ? $t : $lmDrawDefault['lmTheme'];
        break;
      default:
        $lmTheme = $lmDrawDefault['lmTheme'];
    }

    if($pos = strpos($lmDrawValue['lmPageType'], '#')) {
      $t = substr($lmDrawValue['lmPageType'], 0, $pos);
      } else { $t = $lmDrawValue['lmPageType']; }
    $lmPageType = (in_array($t, $lmPageTypes)) ? $t : $lmDrawDefault['lmPageType'];
    $lmPageSection = stristr($lmDrawValue['lmPageType'], '#');

    //todo pageNameFormat

    $lmDrawMap = compact($lmDrawParam);
    if(isset($args['type'])) { $lmDrawMap['type'] = $args['type']; }
    $Phplm['lm']['clickopen'] = isset($args['clickopen']) ? (int)$args['clickopen'] : 0;

    return $lmDrawMap;

}
//(:phplmDraw id orient theme pageType pageNameFormat:)
//echo '<pre>'; print_r($lmDrawParams); echo '</pre>'; exit;
