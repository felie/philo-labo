<?php if (!defined('PmWiki')) exit();
/**
 * set_farm_field.php
 * @author V.Krishn (vkrishn@insteps.net), Copyright 2005-2014
 * @note This file is part of Cookbook recipe "Phplm" for PmWiki;
 * You can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the  Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version. @see pmwiki.php for full details.
 * @package phplm
 */

  /* FARMs/FIELDs Options */
  // *******************************************************
  if ($thisInstall == '0'):
    /* Use this if installing in individual fields */
    $thisScriptDUrl = preg_replace('#/[^/]*$#',"/$thisInstallDirFmt",$ScriptUrl,1);
    $thisIconsDirFmt = "pub/$thisIconsDir";
    $thisIconsDirUrl = "$PubDirUrl/$thisIconsDir";

  elseif ($thisInstall == '1'):
    /* Use this if installing at farms */
    $thisScriptDUrl = preg_replace('#/[^/]*$#',"/$thisInstallDirFmt",$FarmPubDirUrl,1);
    $thisIconsDirFmt = "$FarmD/pub/$thisIconsDir";
    $thisIconsDirUrl = "$FarmPubDirUrl/$thisIconsDir";

  elseif ($thisInstall == '2'):
    /* Use this if installing elsewhere */
    $thisScriptDUrl = 'http://'. $_SERVER['HTTP_HOST'] . "/$thisInstallDirFmt";
    $thisIconsDirFmt = $_SERVER['DOCUMENT_ROOT'] . "/$thisIconsDir";
    $thisIconsDirUrl = 'http://'. $_SERVER['HTTP_HOST'] . "/$thisIconsDir";

  else: return;
  endif;

  $thisScriptDUrlPath = str_ireplace('http://'. $_SERVER['HTTP_HOST'], "", $thisScriptDUrl);
  $thisIconsDirUrlPath = str_ireplace('http://'. $_SERVER['HTTP_HOST'], "", $thisIconsDirUrl);

// *******************************************************

