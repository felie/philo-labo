<?php if (!defined('PmWiki')) exit();
/**
 * set_path_file.php
 * @author V.Krishn (vkrishn@insteps.net), Copyright 2005-2014
 * @note This file is part of Cookbook recipe "Phplm" for PmWiki;
 * You can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the  Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version. @see pmwiki.php for full details.
 * @package phplm
 */

## Set modules/cookbook install dir/path variables
## based on install location.
  // *******************************************************
  $Phplm['Install']['dirfmt'] = $Phplm['Install']['path'] . DS . $Phplm['Install']['dir'];

  $thisInstall = $Phplm['Install']['location'];
  $thisInstallDir = $Phplm['Install']['path'];
  $thisDir = $Phplm['Install']['dir'];
  $thisIconsDir = $Phplm['Install']['icon']['dir'];
  /* FARMs/FIELDs Options */
  $thisInstallDirFmt = $Phplm['Install']['dirfmt'];
  $thisScriptD = $Phplm['Current']['dir'];

  global $ScriptUrl, $PubDirUrl, $FarmD, $FarmPubDirUrl; //v2.2 not needed
  include_once('set_farm_field.php');

  // farm/field options set in module array
  $Phplm['Install']['dirUrl'] = $thisScriptDUrl;
  $Phplm['Install']['dirUrlPath'] = $thisScriptDUrlPath;
  $Phplm['Install']['icon']['dirFmt'] = $thisIconsDirFmt;
  $Phplm['Install']['icon']['dirUrl'] = $thisIconsDirUrl;
  $Phplm['Install']['icon']['dirUrlPath'] = $thisIconsDirUrlPath;
  // **********************************************************








