<?php
/*  Copyright 2005 Patrick R. Michaud (pmichaud@pobox.com)
    This file is chessboard.php; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  

    This script generates a GIF image of a chessboard from a board
    position given in Forsyth-Edwards Notation (FEN) [1].  The parameters
    understood by this script include:

        fen=[position]         A game position in FEN notation
        w=[width]              Width of GIF image (default 240 pixels)
        h=[height]             Height of GIF image (default same as width)
        light=[rrggbb]         Color of light squares (default "ffffff")
        dark=[rrggbb]          Color of dark squares (default "cccccc")
        border=[rrggbb]        Border color (default "999999")

    The chess piece images are held in an image file specified by 
    $default['pieces'].  This file is a single image containing
    all of the images to be used for pieces laid out end-to-end
    horizontally, starting with White's king, queen, rook, bishop, 
    knight, and pawn, and then the same sequence for Black's pieces.  

    The pieces-60.gif file distributed with chessboard.php was created
    from chess tile images provided by David Benbennick and available
    through the Wikimedia Commons [2] under either a public domain
    or GPL-compatible license.

    1. http://en.wikipedia.org/wiki/FEN
    2. http://commons.wikimedia.org/wiki/Image:Chess_tile_pd.png
*/

  $defaults = array(
    'w' => 240,                                              # chessboard width
    'fen' => 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR',  # default position
    'dark' => 'cccccc',                                      # dark squares
    'light' => 'ffffff',                                     # light squares
    'border' => '666666',                                    # border color
    'pieces' => 'pieces-60.gif',                             # image file
  );

  # merge the request parameters with the defaults
  $opt = array_merge($defaults, (array)$_REQUEST);

  # set the values we'll use
  $w = $opt['w'];
  $h = (@$opt['h']) ? $opt['h'] : $w;
  $fen = $opt['fen'];
  $dark = hexdec($opt['dark']);
  $light = hexdec($opt['light']);

  # if no FEN position specified, use the default
  if (!$fen) { $fen = $defaults['fen']; }

  # load the piece images
  $piece = imagecreatefromgif($defaults['pieces']);

  # determine the width and height of a single piece
  $pw = imagesx($piece)/12;
  $ph = imagesy($piece);

  # build an 8x8 board to draw pieces on
  $bw = $pw * 8; $bh = $ph * 8;
  $board = imagecreatetruecolor($bw, $bh);

  # draw the squares
  imagefilledrectangle($board, 0, 0, $bw, $bh, $dark);
  for($i = 0; $i < 8; $i += 2) {
    for($j = 0; $j < 8; $j += 2) {
      $x = $i * $pw; $y = $j * $ph;
      imagefilledrectangle($board, $x, $y, $x+$pw, $y+$ph, $light);
      $x += $pw; $y += $ph;
      imagefilledrectangle($board, $x, $y, $x+$pw, $y+$ph, $light);
    }
  }

  # locate the starting horizontal offset of each piece by name
  $str = "KQRBNPkqrbnp";
  for($i=0; $i < 12; $i++) { $k = $str{$i}; $pk[$k] = $i * $pw; }

  # Now, walk through the FEN record and draw the pieces in the
  # appropriate position.
  $file = 0; $rank = 0;
  $len = strlen($fen);
  for($i=0; $i<$len; $i++) {
    $k = $fen{$i};
    if ($k > 0) { $file+=$k; continue; }
    if ($k == '/') { $rank++; $file=0; continue; }
    if (isset($pk[$k])) {
      imagecopy($board, $piece, $file*$pw, $rank*$ph, $pk[$k], 0, $pw, $ph);
      $file++;
    }
  }
  imagedestroy($piece);

  # resize the board if needed
  if ($w != $bw || $h != $bh) {
    $old = $board;
    $board = imagecreatetruecolor($w, $h);
    imagecopyresampled($board, $old, 0, 0, 0, 0, $w, $h, $bw, $bh);
    imagedestroy($old);
  }

  # draw a border if requested
  if ($opt['border']) 
    imagerectangle($board, 0, 0, $w-1, $h-1, hexdec($opt['border']));

  # return the completed chessboard image
  header("Content-type: image/gif");
  imagegif($board);
  imagedestroy($board);

