/** This JavaScript file was taken from a tutorial about Tree menus. 
	It was just modified to work with PmWiki*/

/** Some variables */

//file type
	strIcoType		= '.gif';

//icon subdirectory, don't forget the "/" at the end
	strIcoDir		= '18x18/';

//script directory (set by parameter of init_treemenu)
	strScriptDir	= '';

//name of graphic representing file
	strFile		= 'file';

//name of graphics representing folder
	arrFolders		= new Array('closed', 'open');

//branch
	strBranch		= 'branch';

//vertical line
	strLine		= 'line';

//Space
	strSpace		= 'space';

//Start
	strStart		= 'top';

//Icons for expansion
	arrFolderEntries	= new Array('plus', 'minus');

//Target window for recognizing active tree branch
	objTargetWindow	= 'self';

//Ignore GET-Parameter when searching the actual link
	blnIgnoreQuery	= true;

//Ignore anchor when searching the actual link
	blnIgnoreAnchor	= false;

//Display properties for menu items
	arrDisplay		= new Array('none','inline');

	arrTree=new Array();

/** Is objImg  a button for expansion */

function is_entry(objImg)
{
	return(objImg.tagName == 'IMG' && objImg.name == 'entry')
}

/** Write img-Code for strSrc */

function img_html(strSrc, blnFunction)
{
	strFunction = (blnFunction)
		? 'onclick="expand(this.parentNode)"name="entry"'
		: '';

	return('<img src="' + strScriptDir 
				+ strIcoDir
				+ strSrc
				+ strIcoType
				+ '"border="0"align="top"' + strFunction + '/>');
}

function expand(obj)
{
	intEvent = (obj.lastChild.style.display == 'none' || expand.arguments.length > 1)
		? 1
		: 0;

	arrRegExp=new Array();
	arrRegExp.push(new RegExp(arrFolderEntries[1]));
	arrRegExp.push(new RegExp(arrFolderEntries[0]));

	obj.lastChild.style.display=arrDisplay[intEvent];

	for(i=0;i<obj.childNodes.length;++i)
	{
		if(is_entry(obj.childNodes[i]))
		{
			obj.childNodes[i].src =
			obj.childNodes[i].src.replace(arrRegExp[intEvent],arrFolderEntries[intEvent]);

	obj.childNodes[i].nextSibling.src =
		strScriptDir + strIcoDir + arrFolders[intEvent] + strIcoType;
		}
	}
}

/** Parsing of the list and generation of tree */

function build_tree()
{
	if( arrTree.length <= intDimension)
	{
		arrTree.push(strBranch);
	}

	strIco = (!is_file(objItem)) ? arrFolders[0] : strFile;
	strTree = '<br>';
	strEntry = (!is_file(objItem)) ? arrFolderEntries[0] : strBranch;
	strEntry += (is_end()) ? '_end' : '';

	for (v = 0; v < intDimension; ++v)
	{
		strTree+=img_html(arrTree[v]);
	}

	objItem.innerHTML = strTree
				+ img_html(strEntry,!is_file(objItem))
				+ img_html(strIco)+objItem.innerHTML;

	if (!is_file(objItem))
	{
		arrTree[intDimension] = 'line';
		if (is_end(objItem))
		{
			arrTree[intDimension] = 'space';
		}
	}
}

/** Hide dimensions > 1 and remove lsi properties */

function collapse_menu()
{
	objItem.style.listStyleType = 'none';
	objItem.style.display = 'inline';
	objItem.style.padding = 0;
	objItem.parentNode.style.display = (get_dimension(objItem) == 0)
									? 'inline'
									: 'none';
}

/** Is the object within the menu */

function in_menu(obj)
{
	objParentNode = obj.parentNode;
	while(objParentNode != document.getElementById('treemenu') && objParentNode.tagName != 'BODY')
	{
		objParentNode = objParentNode.parentNode;
	}
	return(objParentNode == document.getElementById('treemenu'));
}

    /** does the object contain an UL-list */

function is_file()
{
	return(!objItem.hasChildNodes()||objItem.lastChild.tagName!='UL')
}

function get_dimension()
{
	intDimension=-1;
	objParentNode=objItem.parentNode;
	while(objParentNode!=document.getElementById('treemenu'))
	{
		if(objParentNode.tagName=='UL'){intDimension++;}
		objParentNode=objParentNode.parentNode;
	}
	return intDimension;
}

function is_end()
{
	(objItem.parentNode.lastChild.tagName);
	return(objItem==objItem.parentNode.lastChild);
}

function strip_spaces(str)
{
	strOut=str.replace(/>\s+</gm,'><');
	strOut=str.replace(/>\s+/gm,'>');
	strOut=str.replace(/\s+</gm,'<');
	return strOut;
}

/** Going through all LI elements */

function init_treemenu(ScriptUrl)
{
	beginning=1;
	strTree = '<br>';
	if (!document.getElementsByTagName || typeof document.getElementsByTagName('html')[0].innerHTML != 'string')
	{
		return;
	}
	document.getElementById('treemenu').innerHTML = strip_spaces(document.getElementById('treemenu').innerHTML);
	if (ScriptUrl) {strScriptDir = ScriptUrl + '/treemenu/';};
	for (l = 0; l < document.getElementsByTagName('li').length; ++l)
	{
		objItem = document.getElementsByTagName('li')[l];
		if (in_menu(objItem))
		{
			intDimension = get_dimension(objItem);
			collapse_menu(objItem);
			if (beginning == 1) {
				beginning=0;
				objItem.innerHTML = strTree+img_html(strStart)+objItem.innerHTML;
			}
			else {
				build_tree(objItem);
			};
		}
	}
	document.getElementById('treemenu').innerHTML = strip_spaces(document.getElementById('treemenu').innerHTML);
	opening();
}

/** Get actual link and expand menu */

function opening()
{
	for(a = 0; a < document.links.length; ++a)
	{
		if (in_menu(document.links[a]))
		{
			document.links[a].style.textDecoration = 'none';
			objFolder = document.links[a].parentNode;
			if (is_active_link(document.links[a].href))
			{
				while(objFolder.parentNode != document.getElementById('treemenu'))
				{
					if(objFolder.tagName == 'UL')
					{
						expand(objFolder.parentNode,1);
					}
					document.links[a].style.fontWeight = 'bold';
					objFolder=objFolder.parentNode;
				}
			}
		}
	}
}

/** check whether two links are identical */

function is_active_link(strUrl)
{
	arrUrls = new Array(strUrl,String(eval(objTargetWindow + '.location')));
	for(u = 0; u < arrUrls.length; ++u)
	{
		if (blnIgnoreAnchor)
		{
			arrUrls[u] = arrUrls[u].replace(/#.*?$/, '');
		}
		if (blnIgnoreQuery)
		{
			arrUrls[u]=arrUrls[u].replace(/\?[^#]*/g, '');
		}
	}
	return(arrUrls[0]==arrUrls[1]);
}

/**
* Start everything...Can optionally be done also with "BODY onload",
* always after loading the menu elements
*/

/*    init_treemenu();*/