Name: academian
Version: 0.5 (2013-11-08)
Description: Skin for PmWiki
Author: Ian MacGregor
Contact: http://www.ianmacgregor.net/Profiles/Ardchoille
Copyright: Copyright 2013 Ian MacGregor
xhtml validation: Passed (http://validator.w3.org/)
css validation: Passed (http://jigsaw.w3.org/css-validator/)

Files:
academian.tmpl - template file
style.css - cascading style sheet
ReadMe.txt - this file

This skin is released under the GNU General Public License
as published by the Free Software Foundation; either version
2 of the License, or (at your option) any later version.


Installation:
1) Un-zip academian.zip into your skins directory
2) Enable the rounded skin in your config.php with:

$Skin = 'academian';

Changelog

v. 0.5 2013-11-08
 * original release

