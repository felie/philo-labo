/**
  Flip Checkbox Recipe for PmWiki
  Written by (c) Petko Yotov 2008-2015

  This text is written for PmWiki; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published
  by the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version. See pmwiki.php for full details
  and lack of warranty.

  Copyright 2008-2015 Petko Yotov pmwiki.org/petko

  Version 20150102
*/
FlipboxStatus = new Array();

function _id(id)
{
  if(document.getElementById(id))
    return document.getElementById(id);
  return false;
}
function flipboxsortClassId(a, b) {
  var A = a.className;
  var B = b.className;
  if(A<B) return -1;
  if(A>B) return 1;
  var A = parseInt(a.id.replace(/^[^0-9]+/, ''));
  var B = parseInt(b.id.replace(/^[^0-9]+/, ''));
  return A-B;
}
function flipboxsort(el) {
  var items0 = el.getElementsByTagName('li');
  if(! items0.length) return;

  var items = [ ];
  for(var i=0; i<items0.length; i++) items[i] = items0[i];
  items.sort(flipboxsortClassId);
  for(var i=0; i<items.length; i++) {
    el.appendChild(items[i]);
  }
}
function flipbox(id, st, update)
{
  if(typeof(FlipboxStatus[id]) == 'undefined' )FlipboxStatus[id] = st;

  var idx = FlipboxChoices.indexOf(FlipboxStatus[id]);
  if(idx==-1) return;

  FlipboxStatus[id] = FlipboxChoices.charAt(idx+1);
  var newst = FlipboxStatus[id];
  if (! update) newst += FlipboxStatus[id];
  var line = _id('_fbl'+id);
  if(line){
    line.className = "fb"+FlipboxStatus[id];
  }

  var icon = _id('_fbi'+id);
  if(icon) {
    if( typeof(icon.src) != 'undefined' ) {
      icon.src = FlipboxPubDirUrl+"/"+FlipboxIcon[0]+FlipboxStatus[id]+FlipboxIcon[1];
      icon.alt = newst;
    }
    else
      icon.innerHTML = "["+newst+"]";  
  }
  
  if(line) {
    var lst = line.parentNode;
    if(lst.className.indexOf('fbxsort')>=0) flipboxsort(lst);
  }
  if(update)  {
    var ajaxdot = new Image();
    ajaxdot.src= FlipboxPageUrl + id + '&state='+FlipboxStatus[id]+'&r='+Math.random();
  }
}

function getElementsByTagAndClassName(tag,cname) {
 var tags=document.getElementsByTagName(tag);
 var cEls=new Array();
 for (i=0; i<tags.length; i++) {
  var rE = new RegExp("(^|\\s)" + cname + "(\\s|$)");
   if (cname=='' || rE.test(tags[i].className)) {
   cEls.push(tags[i]);
   }
  }
 return cEls;
}
window.addEventListener("DOMContentLoaded", function(){
  var uls = getElementsByTagAndClassName('ul','fbxsort');
  for(var i=0; i<uls.length; i++) flipboxsort(uls[i]);
  }, false);


